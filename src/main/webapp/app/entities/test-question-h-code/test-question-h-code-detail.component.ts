import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TestQuestionHCode } from './test-question-h-code.model';
import { TestQuestionHCodeService } from './test-question-h-code.service';

@Component({
    selector: 'jhi-test-question-h-code-detail',
    templateUrl: './test-question-h-code-detail.component.html'
})
export class TestQuestionHCodeDetailComponent implements OnInit, OnDestroy {

    testQuestion: TestQuestionHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private testQuestionService: TestQuestionHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTestQuestions();
    }

    load(id) {
        this.testQuestionService.find(id)
            .subscribe((testQuestionResponse: HttpResponse<TestQuestionHCode>) => {
                this.testQuestion = testQuestionResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTestQuestions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'testQuestionListModification',
            (response) => this.load(this.testQuestion.id)
        );
    }
}
