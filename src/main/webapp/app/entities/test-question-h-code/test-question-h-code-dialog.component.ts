import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TestQuestionHCode } from './test-question-h-code.model';
import { TestQuestionHCodePopupService } from './test-question-h-code-popup.service';
import { TestQuestionHCodeService } from './test-question-h-code.service';
import { CourseIssueHCode, CourseIssueHCodeService } from '../course-issue-h-code';

@Component({
    selector: 'jhi-test-question-h-code-dialog',
    templateUrl: './test-question-h-code-dialog.component.html'
})
export class TestQuestionHCodeDialogComponent implements OnInit {

    testQuestion: TestQuestionHCode;
    isSaving: boolean;

    courseissues: CourseIssueHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private testQuestionService: TestQuestionHCodeService,
        private courseIssueService: CourseIssueHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.courseIssueService.query()
            .subscribe((res: HttpResponse<CourseIssueHCode[]>) => { this.courseissues = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.testQuestion.id !== undefined) {
            this.subscribeToSaveResponse(
                this.testQuestionService.update(this.testQuestion));
        } else {
            this.subscribeToSaveResponse(
                this.testQuestionService.create(this.testQuestion));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TestQuestionHCode>>) {
        result.subscribe((res: HttpResponse<TestQuestionHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TestQuestionHCode) {
        this.eventManager.broadcast({ name: 'testQuestionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCourseIssueById(index: number, item: CourseIssueHCode) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-test-question-h-code-popup',
    template: ''
})
export class TestQuestionHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private testQuestionPopupService: TestQuestionHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.testQuestionPopupService
                    .open(TestQuestionHCodeDialogComponent as Component, params['id']);
            } else {
                this.testQuestionPopupService
                    .open(TestQuestionHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
