import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TestQuestionHCode } from './test-question-h-code.model';
import { TestQuestionHCodePopupService } from './test-question-h-code-popup.service';
import { TestQuestionHCodeService } from './test-question-h-code.service';

@Component({
    selector: 'jhi-test-question-h-code-delete-dialog',
    templateUrl: './test-question-h-code-delete-dialog.component.html'
})
export class TestQuestionHCodeDeleteDialogComponent {

    testQuestion: TestQuestionHCode;

    constructor(
        private testQuestionService: TestQuestionHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.testQuestionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'testQuestionListModification',
                content: 'Deleted an testQuestion'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-test-question-h-code-delete-popup',
    template: ''
})
export class TestQuestionHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private testQuestionPopupService: TestQuestionHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.testQuestionPopupService
                .open(TestQuestionHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
