import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    TestQuestionHCodeService,
    TestQuestionHCodePopupService,
    TestQuestionHCodeComponent,
    TestQuestionHCodeDetailComponent,
    TestQuestionHCodeDialogComponent,
    TestQuestionHCodePopupComponent,
    TestQuestionHCodeDeletePopupComponent,
    TestQuestionHCodeDeleteDialogComponent,
    testQuestionRoute,
    testQuestionPopupRoute,
    TestQuestionHCodeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...testQuestionRoute,
    ...testQuestionPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TestQuestionHCodeComponent,
        TestQuestionHCodeDetailComponent,
        TestQuestionHCodeDialogComponent,
        TestQuestionHCodeDeleteDialogComponent,
        TestQuestionHCodePopupComponent,
        TestQuestionHCodeDeletePopupComponent,
    ],
    entryComponents: [
        TestQuestionHCodeComponent,
        TestQuestionHCodeDialogComponent,
        TestQuestionHCodePopupComponent,
        TestQuestionHCodeDeleteDialogComponent,
        TestQuestionHCodeDeletePopupComponent,
    ],
    providers: [
        TestQuestionHCodeService,
        TestQuestionHCodePopupService,
        TestQuestionHCodeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeTestQuestionHCodeModule {}
