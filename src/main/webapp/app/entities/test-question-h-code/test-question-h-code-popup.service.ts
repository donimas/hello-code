import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { TestQuestionHCode } from './test-question-h-code.model';
import { TestQuestionHCodeService } from './test-question-h-code.service';

@Injectable()
export class TestQuestionHCodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private testQuestionService: TestQuestionHCodeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.testQuestionService.find(id)
                    .subscribe((testQuestionResponse: HttpResponse<TestQuestionHCode>) => {
                        const testQuestion: TestQuestionHCode = testQuestionResponse.body;
                        this.ngbModalRef = this.testQuestionModalRef(component, testQuestion);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.testQuestionModalRef(component, new TestQuestionHCode());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    testQuestionModalRef(component: Component, testQuestion: TestQuestionHCode): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.testQuestion = testQuestion;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
