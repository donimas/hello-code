import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { TestQuestionHCodeComponent } from './test-question-h-code.component';
import { TestQuestionHCodeDetailComponent } from './test-question-h-code-detail.component';
import { TestQuestionHCodePopupComponent } from './test-question-h-code-dialog.component';
import { TestQuestionHCodeDeletePopupComponent } from './test-question-h-code-delete-dialog.component';

@Injectable()
export class TestQuestionHCodeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const testQuestionRoute: Routes = [
    {
        path: 'test-question-h-code',
        component: TestQuestionHCodeComponent,
        resolve: {
            'pagingParams': TestQuestionHCodeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testQuestion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'test-question-h-code/:id',
        component: TestQuestionHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testQuestion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const testQuestionPopupRoute: Routes = [
    {
        path: 'test-question-h-code-new',
        component: TestQuestionHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testQuestion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'test-question-h-code/:id/edit',
        component: TestQuestionHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testQuestion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'test-question-h-code/:id/delete',
        component: TestQuestionHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testQuestion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
