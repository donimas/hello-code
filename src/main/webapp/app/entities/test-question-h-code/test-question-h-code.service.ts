import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { TestQuestionHCode } from './test-question-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TestQuestionHCode>;

@Injectable()
export class TestQuestionHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/test-questions';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/test-questions';

    constructor(private http: HttpClient) { }

    create(testQuestion: TestQuestionHCode): Observable<EntityResponseType> {
        const copy = this.convert(testQuestion);
        return this.http.post<TestQuestionHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(testQuestion: TestQuestionHCode): Observable<EntityResponseType> {
        const copy = this.convert(testQuestion);
        return this.http.put<TestQuestionHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<TestQuestionHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TestQuestionHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<TestQuestionHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TestQuestionHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<TestQuestionHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<TestQuestionHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TestQuestionHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TestQuestionHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TestQuestionHCode[]>): HttpResponse<TestQuestionHCode[]> {
        const jsonResponse: TestQuestionHCode[] = res.body;
        const body: TestQuestionHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TestQuestionHCode.
     */
    private convertItemFromServer(testQuestion: TestQuestionHCode): TestQuestionHCode {
        const copy: TestQuestionHCode = Object.assign({}, testQuestion);
        return copy;
    }

    /**
     * Convert a TestQuestionHCode to a JSON which can be sent to the server.
     */
    private convert(testQuestion: TestQuestionHCode): TestQuestionHCode {
        const copy: TestQuestionHCode = Object.assign({}, testQuestion);
        return copy;
    }
}
