import { BaseEntity } from './../../shared';

export class TestQuestionHCode implements BaseEntity {
    constructor(
        public id?: number,
        public bodyRu?: string,
        public bodyKk?: string,
        public bodyEn?: string,
        public flagDeleted?: boolean,
        public issue?: BaseEntity,
        public variants?: BaseEntity[],
    ) {
        this.flagDeleted = false;
    }
}
