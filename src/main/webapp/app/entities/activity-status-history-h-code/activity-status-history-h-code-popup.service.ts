import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { ActivityStatusHistoryHCode } from './activity-status-history-h-code.model';
import { ActivityStatusHistoryHCodeService } from './activity-status-history-h-code.service';

@Injectable()
export class ActivityStatusHistoryHCodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private activityStatusHistoryService: ActivityStatusHistoryHCodeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.activityStatusHistoryService.find(id)
                    .subscribe((activityStatusHistoryResponse: HttpResponse<ActivityStatusHistoryHCode>) => {
                        const activityStatusHistory: ActivityStatusHistoryHCode = activityStatusHistoryResponse.body;
                        activityStatusHistory.startedDate = this.datePipe
                            .transform(activityStatusHistory.startedDate, 'yyyy-MM-ddTHH:mm:ss');
                        activityStatusHistory.finishedDate = this.datePipe
                            .transform(activityStatusHistory.finishedDate, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.activityStatusHistoryModalRef(component, activityStatusHistory);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.activityStatusHistoryModalRef(component, new ActivityStatusHistoryHCode());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    activityStatusHistoryModalRef(component: Component, activityStatusHistory: ActivityStatusHistoryHCode): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.activityStatusHistory = activityStatusHistory;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
