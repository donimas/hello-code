import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ActivityStatusHistoryHCodeComponent } from './activity-status-history-h-code.component';
import { ActivityStatusHistoryHCodeDetailComponent } from './activity-status-history-h-code-detail.component';
import { ActivityStatusHistoryHCodePopupComponent } from './activity-status-history-h-code-dialog.component';
import { ActivityStatusHistoryHCodeDeletePopupComponent } from './activity-status-history-h-code-delete-dialog.component';

@Injectable()
export class ActivityStatusHistoryHCodeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const activityStatusHistoryRoute: Routes = [
    {
        path: 'activity-status-history-h-code',
        component: ActivityStatusHistoryHCodeComponent,
        resolve: {
            'pagingParams': ActivityStatusHistoryHCodeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.activityStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'activity-status-history-h-code/:id',
        component: ActivityStatusHistoryHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.activityStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const activityStatusHistoryPopupRoute: Routes = [
    {
        path: 'activity-status-history-h-code-new',
        component: ActivityStatusHistoryHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.activityStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'activity-status-history-h-code/:id/edit',
        component: ActivityStatusHistoryHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.activityStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'activity-status-history-h-code/:id/delete',
        component: ActivityStatusHistoryHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.activityStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
