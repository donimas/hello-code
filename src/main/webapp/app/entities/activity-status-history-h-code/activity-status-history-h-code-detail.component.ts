import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ActivityStatusHistoryHCode } from './activity-status-history-h-code.model';
import { ActivityStatusHistoryHCodeService } from './activity-status-history-h-code.service';

@Component({
    selector: 'jhi-activity-status-history-h-code-detail',
    templateUrl: './activity-status-history-h-code-detail.component.html'
})
export class ActivityStatusHistoryHCodeDetailComponent implements OnInit, OnDestroy {

    activityStatusHistory: ActivityStatusHistoryHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private activityStatusHistoryService: ActivityStatusHistoryHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInActivityStatusHistories();
    }

    load(id) {
        this.activityStatusHistoryService.find(id)
            .subscribe((activityStatusHistoryResponse: HttpResponse<ActivityStatusHistoryHCode>) => {
                this.activityStatusHistory = activityStatusHistoryResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInActivityStatusHistories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'activityStatusHistoryListModification',
            (response) => this.load(this.activityStatusHistory.id)
        );
    }
}
