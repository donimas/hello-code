import { BaseEntity } from './../../shared';

export const enum ActivityStatus {
    'IN_PROGRESS',
    'SKIPPED',
    'PASSED',
    'FAILED'
}

export class ActivityStatusHistoryHCode implements BaseEntity {
    constructor(
        public id?: number,
        public status?: ActivityStatus,
        public startedDate?: any,
        public finishedDate?: any,
        public activity?: BaseEntity,
        public activityResult?: BaseEntity,
    ) {
    }
}
