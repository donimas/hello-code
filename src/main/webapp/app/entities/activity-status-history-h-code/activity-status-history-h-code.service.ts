import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ActivityStatusHistoryHCode } from './activity-status-history-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ActivityStatusHistoryHCode>;

@Injectable()
export class ActivityStatusHistoryHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/activity-status-histories';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/activity-status-histories';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(activityStatusHistory: ActivityStatusHistoryHCode): Observable<EntityResponseType> {
        const copy = this.convert(activityStatusHistory);
        return this.http.post<ActivityStatusHistoryHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(activityStatusHistory: ActivityStatusHistoryHCode): Observable<EntityResponseType> {
        const copy = this.convert(activityStatusHistory);
        return this.http.put<ActivityStatusHistoryHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ActivityStatusHistoryHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ActivityStatusHistoryHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<ActivityStatusHistoryHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ActivityStatusHistoryHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ActivityStatusHistoryHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<ActivityStatusHistoryHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ActivityStatusHistoryHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ActivityStatusHistoryHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ActivityStatusHistoryHCode[]>): HttpResponse<ActivityStatusHistoryHCode[]> {
        const jsonResponse: ActivityStatusHistoryHCode[] = res.body;
        const body: ActivityStatusHistoryHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ActivityStatusHistoryHCode.
     */
    private convertItemFromServer(activityStatusHistory: ActivityStatusHistoryHCode): ActivityStatusHistoryHCode {
        const copy: ActivityStatusHistoryHCode = Object.assign({}, activityStatusHistory);
        copy.startedDate = this.dateUtils
            .convertDateTimeFromServer(activityStatusHistory.startedDate);
        copy.finishedDate = this.dateUtils
            .convertDateTimeFromServer(activityStatusHistory.finishedDate);
        return copy;
    }

    /**
     * Convert a ActivityStatusHistoryHCode to a JSON which can be sent to the server.
     */
    private convert(activityStatusHistory: ActivityStatusHistoryHCode): ActivityStatusHistoryHCode {
        const copy: ActivityStatusHistoryHCode = Object.assign({}, activityStatusHistory);

        copy.startedDate = this.dateUtils.toDate(activityStatusHistory.startedDate);

        copy.finishedDate = this.dateUtils.toDate(activityStatusHistory.finishedDate);
        return copy;
    }
}
