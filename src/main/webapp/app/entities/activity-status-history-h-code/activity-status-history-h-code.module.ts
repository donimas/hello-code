import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    ActivityStatusHistoryHCodeService,
    ActivityStatusHistoryHCodePopupService,
    ActivityStatusHistoryHCodeComponent,
    ActivityStatusHistoryHCodeDetailComponent,
    ActivityStatusHistoryHCodeDialogComponent,
    ActivityStatusHistoryHCodePopupComponent,
    ActivityStatusHistoryHCodeDeletePopupComponent,
    ActivityStatusHistoryHCodeDeleteDialogComponent,
    activityStatusHistoryRoute,
    activityStatusHistoryPopupRoute,
    ActivityStatusHistoryHCodeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...activityStatusHistoryRoute,
    ...activityStatusHistoryPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ActivityStatusHistoryHCodeComponent,
        ActivityStatusHistoryHCodeDetailComponent,
        ActivityStatusHistoryHCodeDialogComponent,
        ActivityStatusHistoryHCodeDeleteDialogComponent,
        ActivityStatusHistoryHCodePopupComponent,
        ActivityStatusHistoryHCodeDeletePopupComponent,
    ],
    entryComponents: [
        ActivityStatusHistoryHCodeComponent,
        ActivityStatusHistoryHCodeDialogComponent,
        ActivityStatusHistoryHCodePopupComponent,
        ActivityStatusHistoryHCodeDeleteDialogComponent,
        ActivityStatusHistoryHCodeDeletePopupComponent,
    ],
    providers: [
        ActivityStatusHistoryHCodeService,
        ActivityStatusHistoryHCodePopupService,
        ActivityStatusHistoryHCodeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeActivityStatusHistoryHCodeModule {}
