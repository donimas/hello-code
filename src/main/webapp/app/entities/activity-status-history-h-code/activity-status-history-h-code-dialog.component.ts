import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ActivityStatusHistoryHCode } from './activity-status-history-h-code.model';
import { ActivityStatusHistoryHCodePopupService } from './activity-status-history-h-code-popup.service';
import { ActivityStatusHistoryHCodeService } from './activity-status-history-h-code.service';
import { LearnerActivityHCode, LearnerActivityHCodeService } from '../learner-activity-h-code';
import { ActivityResultHCode, ActivityResultHCodeService } from '../activity-result-h-code';

@Component({
    selector: 'jhi-activity-status-history-h-code-dialog',
    templateUrl: './activity-status-history-h-code-dialog.component.html'
})
export class ActivityStatusHistoryHCodeDialogComponent implements OnInit {

    activityStatusHistory: ActivityStatusHistoryHCode;
    isSaving: boolean;

    learneractivities: LearnerActivityHCode[];

    activityresults: ActivityResultHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private activityStatusHistoryService: ActivityStatusHistoryHCodeService,
        private learnerActivityService: LearnerActivityHCodeService,
        private activityResultService: ActivityResultHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.learnerActivityService.query()
            .subscribe((res: HttpResponse<LearnerActivityHCode[]>) => { this.learneractivities = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.activityResultService.query()
            .subscribe((res: HttpResponse<ActivityResultHCode[]>) => { this.activityresults = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.activityStatusHistory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.activityStatusHistoryService.update(this.activityStatusHistory));
        } else {
            this.subscribeToSaveResponse(
                this.activityStatusHistoryService.create(this.activityStatusHistory));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ActivityStatusHistoryHCode>>) {
        result.subscribe((res: HttpResponse<ActivityStatusHistoryHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ActivityStatusHistoryHCode) {
        this.eventManager.broadcast({ name: 'activityStatusHistoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackLearnerActivityById(index: number, item: LearnerActivityHCode) {
        return item.id;
    }

    trackActivityResultById(index: number, item: ActivityResultHCode) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-activity-status-history-h-code-popup',
    template: ''
})
export class ActivityStatusHistoryHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private activityStatusHistoryPopupService: ActivityStatusHistoryHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.activityStatusHistoryPopupService
                    .open(ActivityStatusHistoryHCodeDialogComponent as Component, params['id']);
            } else {
                this.activityStatusHistoryPopupService
                    .open(ActivityStatusHistoryHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
