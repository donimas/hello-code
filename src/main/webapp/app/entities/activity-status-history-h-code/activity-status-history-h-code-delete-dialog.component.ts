import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ActivityStatusHistoryHCode } from './activity-status-history-h-code.model';
import { ActivityStatusHistoryHCodePopupService } from './activity-status-history-h-code-popup.service';
import { ActivityStatusHistoryHCodeService } from './activity-status-history-h-code.service';

@Component({
    selector: 'jhi-activity-status-history-h-code-delete-dialog',
    templateUrl: './activity-status-history-h-code-delete-dialog.component.html'
})
export class ActivityStatusHistoryHCodeDeleteDialogComponent {

    activityStatusHistory: ActivityStatusHistoryHCode;

    constructor(
        private activityStatusHistoryService: ActivityStatusHistoryHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.activityStatusHistoryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'activityStatusHistoryListModification',
                content: 'Deleted an activityStatusHistory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-activity-status-history-h-code-delete-popup',
    template: ''
})
export class ActivityStatusHistoryHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private activityStatusHistoryPopupService: ActivityStatusHistoryHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.activityStatusHistoryPopupService
                .open(ActivityStatusHistoryHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
