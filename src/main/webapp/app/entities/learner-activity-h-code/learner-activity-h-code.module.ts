import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import { HelloCodeAdminModule } from '../../admin/admin.module';
import {
    LearnerActivityHCodeService,
    LearnerActivityHCodePopupService,
    LearnerActivityHCodeComponent,
    LearnerActivityHCodeDetailComponent,
    LearnerActivityHCodeDialogComponent,
    LearnerActivityHCodePopupComponent,
    LearnerActivityHCodeDeletePopupComponent,
    LearnerActivityHCodeDeleteDialogComponent,
    learnerActivityRoute,
    learnerActivityPopupRoute,
    LearnerActivityHCodeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...learnerActivityRoute,
    ...learnerActivityPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        HelloCodeAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        LearnerActivityHCodeComponent,
        LearnerActivityHCodeDetailComponent,
        LearnerActivityHCodeDialogComponent,
        LearnerActivityHCodeDeleteDialogComponent,
        LearnerActivityHCodePopupComponent,
        LearnerActivityHCodeDeletePopupComponent,
    ],
    entryComponents: [
        LearnerActivityHCodeComponent,
        LearnerActivityHCodeDialogComponent,
        LearnerActivityHCodePopupComponent,
        LearnerActivityHCodeDeleteDialogComponent,
        LearnerActivityHCodeDeletePopupComponent,
    ],
    providers: [
        LearnerActivityHCodeService,
        LearnerActivityHCodePopupService,
        LearnerActivityHCodeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeLearnerActivityHCodeModule {}
