import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LearnerActivityHCode } from './learner-activity-h-code.model';
import { LearnerActivityHCodePopupService } from './learner-activity-h-code-popup.service';
import { LearnerActivityHCodeService } from './learner-activity-h-code.service';

@Component({
    selector: 'jhi-learner-activity-h-code-delete-dialog',
    templateUrl: './learner-activity-h-code-delete-dialog.component.html'
})
export class LearnerActivityHCodeDeleteDialogComponent {

    learnerActivity: LearnerActivityHCode;

    constructor(
        private learnerActivityService: LearnerActivityHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.learnerActivityService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'learnerActivityListModification',
                content: 'Deleted an learnerActivity'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-learner-activity-h-code-delete-popup',
    template: ''
})
export class LearnerActivityHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private learnerActivityPopupService: LearnerActivityHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.learnerActivityPopupService
                .open(LearnerActivityHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
