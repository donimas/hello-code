import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LearnerActivityHCode } from './learner-activity-h-code.model';
import { LearnerActivityHCodePopupService } from './learner-activity-h-code-popup.service';
import { LearnerActivityHCodeService } from './learner-activity-h-code.service';
import { CourseHCode, CourseHCodeService } from '../course-h-code';
import { CourseIssueHCode, CourseIssueHCodeService } from '../course-issue-h-code';
import { User, UserService } from '../../shared';
import { ActivityStatusHistoryHCode, ActivityStatusHistoryHCodeService } from '../activity-status-history-h-code';

@Component({
    selector: 'jhi-learner-activity-h-code-dialog',
    templateUrl: './learner-activity-h-code-dialog.component.html'
})
export class LearnerActivityHCodeDialogComponent implements OnInit {

    learnerActivity: LearnerActivityHCode;
    isSaving: boolean;

    courses: CourseHCode[];

    courseissues: CourseIssueHCode[];

    users: User[];

    activitystatushistories: ActivityStatusHistoryHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private learnerActivityService: LearnerActivityHCodeService,
        private courseService: CourseHCodeService,
        private courseIssueService: CourseIssueHCodeService,
        private userService: UserService,
        private activityStatusHistoryService: ActivityStatusHistoryHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.courseService.query()
            .subscribe((res: HttpResponse<CourseHCode[]>) => { this.courses = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.courseIssueService.query()
            .subscribe((res: HttpResponse<CourseIssueHCode[]>) => { this.courseissues = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.activityStatusHistoryService.query()
            .subscribe((res: HttpResponse<ActivityStatusHistoryHCode[]>) => { this.activitystatushistories = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.learnerActivity.id !== undefined) {
            this.subscribeToSaveResponse(
                this.learnerActivityService.update(this.learnerActivity));
        } else {
            this.subscribeToSaveResponse(
                this.learnerActivityService.create(this.learnerActivity));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<LearnerActivityHCode>>) {
        result.subscribe((res: HttpResponse<LearnerActivityHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: LearnerActivityHCode) {
        this.eventManager.broadcast({ name: 'learnerActivityListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCourseById(index: number, item: CourseHCode) {
        return item.id;
    }

    trackCourseIssueById(index: number, item: CourseIssueHCode) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackActivityStatusHistoryById(index: number, item: ActivityStatusHistoryHCode) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-learner-activity-h-code-popup',
    template: ''
})
export class LearnerActivityHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private learnerActivityPopupService: LearnerActivityHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.learnerActivityPopupService
                    .open(LearnerActivityHCodeDialogComponent as Component, params['id']);
            } else {
                this.learnerActivityPopupService
                    .open(LearnerActivityHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
