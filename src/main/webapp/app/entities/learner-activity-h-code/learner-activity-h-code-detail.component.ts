import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { LearnerActivityHCode } from './learner-activity-h-code.model';
import { LearnerActivityHCodeService } from './learner-activity-h-code.service';

@Component({
    selector: 'jhi-learner-activity-h-code-detail',
    templateUrl: './learner-activity-h-code-detail.component.html'
})
export class LearnerActivityHCodeDetailComponent implements OnInit, OnDestroy {

    learnerActivity: LearnerActivityHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private learnerActivityService: LearnerActivityHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLearnerActivities();
    }

    load(id) {
        this.learnerActivityService.find(id)
            .subscribe((learnerActivityResponse: HttpResponse<LearnerActivityHCode>) => {
                this.learnerActivity = learnerActivityResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLearnerActivities() {
        this.eventSubscriber = this.eventManager.subscribe(
            'learnerActivityListModification',
            (response) => this.load(this.learnerActivity.id)
        );
    }
}
