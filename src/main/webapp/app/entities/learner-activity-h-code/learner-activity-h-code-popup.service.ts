import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { LearnerActivityHCode } from './learner-activity-h-code.model';
import { LearnerActivityHCodeService } from './learner-activity-h-code.service';

@Injectable()
export class LearnerActivityHCodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private learnerActivityService: LearnerActivityHCodeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.learnerActivityService.find(id)
                    .subscribe((learnerActivityResponse: HttpResponse<LearnerActivityHCode>) => {
                        const learnerActivity: LearnerActivityHCode = learnerActivityResponse.body;
                        this.ngbModalRef = this.learnerActivityModalRef(component, learnerActivity);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.learnerActivityModalRef(component, new LearnerActivityHCode());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    learnerActivityModalRef(component: Component, learnerActivity: LearnerActivityHCode): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.learnerActivity = learnerActivity;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
