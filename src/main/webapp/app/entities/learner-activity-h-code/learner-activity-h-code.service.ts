import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { LearnerActivityHCode } from './learner-activity-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<LearnerActivityHCode>;

@Injectable()
export class LearnerActivityHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/learner-activities';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/learner-activities';

    constructor(private http: HttpClient) { }

    create(learnerActivity: LearnerActivityHCode): Observable<EntityResponseType> {
        const copy = this.convert(learnerActivity);
        return this.http.post<LearnerActivityHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(learnerActivity: LearnerActivityHCode): Observable<EntityResponseType> {
        const copy = this.convert(learnerActivity);
        return this.http.put<LearnerActivityHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<LearnerActivityHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<LearnerActivityHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<LearnerActivityHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<LearnerActivityHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<LearnerActivityHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<LearnerActivityHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<LearnerActivityHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: LearnerActivityHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<LearnerActivityHCode[]>): HttpResponse<LearnerActivityHCode[]> {
        const jsonResponse: LearnerActivityHCode[] = res.body;
        const body: LearnerActivityHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to LearnerActivityHCode.
     */
    private convertItemFromServer(learnerActivity: LearnerActivityHCode): LearnerActivityHCode {
        const copy: LearnerActivityHCode = Object.assign({}, learnerActivity);
        return copy;
    }

    /**
     * Convert a LearnerActivityHCode to a JSON which can be sent to the server.
     */
    private convert(learnerActivity: LearnerActivityHCode): LearnerActivityHCode {
        const copy: LearnerActivityHCode = Object.assign({}, learnerActivity);
        return copy;
    }
}
