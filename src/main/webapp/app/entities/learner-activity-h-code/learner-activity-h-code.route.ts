import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { LearnerActivityHCodeComponent } from './learner-activity-h-code.component';
import { LearnerActivityHCodeDetailComponent } from './learner-activity-h-code-detail.component';
import { LearnerActivityHCodePopupComponent } from './learner-activity-h-code-dialog.component';
import { LearnerActivityHCodeDeletePopupComponent } from './learner-activity-h-code-delete-dialog.component';

@Injectable()
export class LearnerActivityHCodeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const learnerActivityRoute: Routes = [
    {
        path: 'learner-activity-h-code',
        component: LearnerActivityHCodeComponent,
        resolve: {
            'pagingParams': LearnerActivityHCodeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.learnerActivity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'learner-activity-h-code/:id',
        component: LearnerActivityHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.learnerActivity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const learnerActivityPopupRoute: Routes = [
    {
        path: 'learner-activity-h-code-new',
        component: LearnerActivityHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.learnerActivity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'learner-activity-h-code/:id/edit',
        component: LearnerActivityHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.learnerActivity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'learner-activity-h-code/:id/delete',
        component: LearnerActivityHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.learnerActivity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
