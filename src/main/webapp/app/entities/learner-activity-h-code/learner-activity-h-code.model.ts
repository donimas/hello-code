import { BaseEntity, User } from './../../shared';

export class LearnerActivityHCode implements BaseEntity {
    constructor(
        public id?: number,
        public flagPassed?: boolean,
        public course?: BaseEntity,
        public issue?: BaseEntity,
        public user?: User,
        public statusHistory?: BaseEntity,
    ) {
        this.flagPassed = false;
    }
}
