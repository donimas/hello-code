import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { CourseChapterHCodeComponent } from './course-chapter-h-code.component';
import { CourseChapterHCodeDetailComponent } from './course-chapter-h-code-detail.component';
import { CourseChapterHCodePopupComponent } from './course-chapter-h-code-dialog.component';
import { CourseChapterHCodeDeletePopupComponent } from './course-chapter-h-code-delete-dialog.component';

export const courseChapterRoute: Routes = [
    {
        path: 'course-chapter-h-code',
        component: CourseChapterHCodeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseChapter.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'course-chapter-h-code/:id',
        component: CourseChapterHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseChapter.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const courseChapterPopupRoute: Routes = [
    {
        path: 'course-chapter-h-code-new',
        component: CourseChapterHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseChapter.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'course-chapter-h-code/:id/edit',
        component: CourseChapterHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseChapter.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'course-chapter-h-code/:id/delete',
        component: CourseChapterHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseChapter.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
