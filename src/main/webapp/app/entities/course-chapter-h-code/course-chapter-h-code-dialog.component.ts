import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CourseChapterHCode } from './course-chapter-h-code.model';
import { CourseChapterHCodePopupService } from './course-chapter-h-code-popup.service';
import { CourseChapterHCodeService } from './course-chapter-h-code.service';
import { CourseHCode, CourseHCodeService } from '../course-h-code';

@Component({
    selector: 'jhi-course-chapter-h-code-dialog',
    templateUrl: './course-chapter-h-code-dialog.component.html'
})
export class CourseChapterHCodeDialogComponent implements OnInit {

    courseChapter: CourseChapterHCode;
    isSaving: boolean;

    courses: CourseHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private courseChapterService: CourseChapterHCodeService,
        private courseService: CourseHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.courseService.query()
            .subscribe((res: HttpResponse<CourseHCode[]>) => { this.courses = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.courseChapter.id !== undefined) {
            this.subscribeToSaveResponse(
                this.courseChapterService.update(this.courseChapter));
        } else {
            this.subscribeToSaveResponse(
                this.courseChapterService.create(this.courseChapter));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CourseChapterHCode>>) {
        result.subscribe((res: HttpResponse<CourseChapterHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CourseChapterHCode) {
        this.eventManager.broadcast({ name: 'courseChapterListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCourseById(index: number, item: CourseHCode) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-course-chapter-h-code-popup',
    template: ''
})
export class CourseChapterHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private courseChapterPopupService: CourseChapterHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.courseChapterPopupService
                    .open(CourseChapterHCodeDialogComponent as Component, params['id']);
            } else {
                this.courseChapterPopupService
                    .open(CourseChapterHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
