import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { CourseChapterHCode } from './course-chapter-h-code.model';
import { CourseChapterHCodeService } from './course-chapter-h-code.service';

@Injectable()
export class CourseChapterHCodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private courseChapterService: CourseChapterHCodeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.courseChapterService.find(id)
                    .subscribe((courseChapterResponse: HttpResponse<CourseChapterHCode>) => {
                        const courseChapter: CourseChapterHCode = courseChapterResponse.body;
                        this.ngbModalRef = this.courseChapterModalRef(component, courseChapter);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.courseChapterModalRef(component, new CourseChapterHCode());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    courseChapterModalRef(component: Component, courseChapter: CourseChapterHCode): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.courseChapter = courseChapter;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
