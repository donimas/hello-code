import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { CourseChapterHCode } from './course-chapter-h-code.model';
import { CourseChapterHCodeService } from './course-chapter-h-code.service';

@Component({
    selector: 'jhi-course-chapter-h-code-detail',
    templateUrl: './course-chapter-h-code-detail.component.html'
})
export class CourseChapterHCodeDetailComponent implements OnInit, OnDestroy {

    courseChapter: CourseChapterHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private courseChapterService: CourseChapterHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCourseChapters();
    }

    load(id) {
        this.courseChapterService.find(id)
            .subscribe((courseChapterResponse: HttpResponse<CourseChapterHCode>) => {
                this.courseChapter = courseChapterResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCourseChapters() {
        this.eventSubscriber = this.eventManager.subscribe(
            'courseChapterListModification',
            (response) => this.load(this.courseChapter.id)
        );
    }
}
