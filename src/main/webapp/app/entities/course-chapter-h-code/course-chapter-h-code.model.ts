import { BaseEntity } from './../../shared';

export class CourseChapterHCode implements BaseEntity {
    constructor(
        public id?: number,
        public noteRu?: string,
        public noteKk?: string,
        public noteEn?: string,
        public order?: number,
        public flagDeleted?: boolean,
        public course?: BaseEntity,
        public issues?: BaseEntity[],
    ) {
        this.flagDeleted = false;
    }
}
