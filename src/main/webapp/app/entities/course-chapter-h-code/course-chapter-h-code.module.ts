import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    CourseChapterHCodeService,
    CourseChapterHCodePopupService,
    CourseChapterHCodeComponent,
    CourseChapterHCodeDetailComponent,
    CourseChapterHCodeDialogComponent,
    CourseChapterHCodePopupComponent,
    CourseChapterHCodeDeletePopupComponent,
    CourseChapterHCodeDeleteDialogComponent,
    courseChapterRoute,
    courseChapterPopupRoute,
} from './';

const ENTITY_STATES = [
    ...courseChapterRoute,
    ...courseChapterPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CourseChapterHCodeComponent,
        CourseChapterHCodeDetailComponent,
        CourseChapterHCodeDialogComponent,
        CourseChapterHCodeDeleteDialogComponent,
        CourseChapterHCodePopupComponent,
        CourseChapterHCodeDeletePopupComponent,
    ],
    entryComponents: [
        CourseChapterHCodeComponent,
        CourseChapterHCodeDialogComponent,
        CourseChapterHCodePopupComponent,
        CourseChapterHCodeDeleteDialogComponent,
        CourseChapterHCodeDeletePopupComponent,
    ],
    providers: [
        CourseChapterHCodeService,
        CourseChapterHCodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeCourseChapterHCodeModule {}
