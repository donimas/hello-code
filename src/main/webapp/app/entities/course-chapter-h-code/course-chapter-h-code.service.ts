import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { CourseChapterHCode } from './course-chapter-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<CourseChapterHCode>;

@Injectable()
export class CourseChapterHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/course-chapters';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/course-chapters';

    constructor(private http: HttpClient) { }

    create(courseChapter: CourseChapterHCode): Observable<EntityResponseType> {
        const copy = this.convert(courseChapter);
        return this.http.post<CourseChapterHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(courseChapter: CourseChapterHCode): Observable<EntityResponseType> {
        const copy = this.convert(courseChapter);
        return this.http.put<CourseChapterHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CourseChapterHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CourseChapterHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<CourseChapterHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CourseChapterHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<CourseChapterHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<CourseChapterHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CourseChapterHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CourseChapterHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CourseChapterHCode[]>): HttpResponse<CourseChapterHCode[]> {
        const jsonResponse: CourseChapterHCode[] = res.body;
        const body: CourseChapterHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CourseChapterHCode.
     */
    private convertItemFromServer(courseChapter: CourseChapterHCode): CourseChapterHCode {
        const copy: CourseChapterHCode = Object.assign({}, courseChapter);
        return copy;
    }

    /**
     * Convert a CourseChapterHCode to a JSON which can be sent to the server.
     */
    private convert(courseChapter: CourseChapterHCode): CourseChapterHCode {
        const copy: CourseChapterHCode = Object.assign({}, courseChapter);
        return copy;
    }
}
