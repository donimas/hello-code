import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CourseChapterHCode } from './course-chapter-h-code.model';
import { CourseChapterHCodePopupService } from './course-chapter-h-code-popup.service';
import { CourseChapterHCodeService } from './course-chapter-h-code.service';

@Component({
    selector: 'jhi-course-chapter-h-code-delete-dialog',
    templateUrl: './course-chapter-h-code-delete-dialog.component.html'
})
export class CourseChapterHCodeDeleteDialogComponent {

    courseChapter: CourseChapterHCode;

    constructor(
        private courseChapterService: CourseChapterHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.courseChapterService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'courseChapterListModification',
                content: 'Deleted an courseChapter'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-course-chapter-h-code-delete-popup',
    template: ''
})
export class CourseChapterHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private courseChapterPopupService: CourseChapterHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.courseChapterPopupService
                .open(CourseChapterHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
