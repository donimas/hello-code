import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    SkratchIssueHCodeService,
    SkratchIssueHCodePopupService,
    SkratchIssueHCodeComponent,
    SkratchIssueHCodeDetailComponent,
    SkratchIssueHCodeDialogComponent,
    SkratchIssueHCodePopupComponent,
    SkratchIssueHCodeDeletePopupComponent,
    SkratchIssueHCodeDeleteDialogComponent,
    skratchIssueRoute,
    skratchIssuePopupRoute,
    SkratchIssueHCodeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...skratchIssueRoute,
    ...skratchIssuePopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SkratchIssueHCodeComponent,
        SkratchIssueHCodeDetailComponent,
        SkratchIssueHCodeDialogComponent,
        SkratchIssueHCodeDeleteDialogComponent,
        SkratchIssueHCodePopupComponent,
        SkratchIssueHCodeDeletePopupComponent,
    ],
    entryComponents: [
        SkratchIssueHCodeComponent,
        SkratchIssueHCodeDialogComponent,
        SkratchIssueHCodePopupComponent,
        SkratchIssueHCodeDeleteDialogComponent,
        SkratchIssueHCodeDeletePopupComponent,
    ],
    providers: [
        SkratchIssueHCodeService,
        SkratchIssueHCodePopupService,
        SkratchIssueHCodeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeSkratchIssueHCodeModule {}
