import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SkratchIssueHCode } from './skratch-issue-h-code.model';
import { SkratchIssueHCodePopupService } from './skratch-issue-h-code-popup.service';
import { SkratchIssueHCodeService } from './skratch-issue-h-code.service';
import { CourseIssueHCode, CourseIssueHCodeService } from '../course-issue-h-code';

@Component({
    selector: 'jhi-skratch-issue-h-code-dialog',
    templateUrl: './skratch-issue-h-code-dialog.component.html'
})
export class SkratchIssueHCodeDialogComponent implements OnInit {

    skratchIssue: SkratchIssueHCode;
    isSaving: boolean;

    courseissues: CourseIssueHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private skratchIssueService: SkratchIssueHCodeService,
        private courseIssueService: CourseIssueHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.courseIssueService.query()
            .subscribe((res: HttpResponse<CourseIssueHCode[]>) => { this.courseissues = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.skratchIssue.id !== undefined) {
            this.subscribeToSaveResponse(
                this.skratchIssueService.update(this.skratchIssue));
        } else {
            this.subscribeToSaveResponse(
                this.skratchIssueService.create(this.skratchIssue));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SkratchIssueHCode>>) {
        result.subscribe((res: HttpResponse<SkratchIssueHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SkratchIssueHCode) {
        this.eventManager.broadcast({ name: 'skratchIssueListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCourseIssueById(index: number, item: CourseIssueHCode) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-skratch-issue-h-code-popup',
    template: ''
})
export class SkratchIssueHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private skratchIssuePopupService: SkratchIssueHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.skratchIssuePopupService
                    .open(SkratchIssueHCodeDialogComponent as Component, params['id']);
            } else {
                this.skratchIssuePopupService
                    .open(SkratchIssueHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
