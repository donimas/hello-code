import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SkratchIssueHCodeComponent } from './skratch-issue-h-code.component';
import { SkratchIssueHCodeDetailComponent } from './skratch-issue-h-code-detail.component';
import { SkratchIssueHCodePopupComponent } from './skratch-issue-h-code-dialog.component';
import { SkratchIssueHCodeDeletePopupComponent } from './skratch-issue-h-code-delete-dialog.component';

@Injectable()
export class SkratchIssueHCodeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const skratchIssueRoute: Routes = [
    {
        path: 'skratch-issue-h-code',
        component: SkratchIssueHCodeComponent,
        resolve: {
            'pagingParams': SkratchIssueHCodeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.skratchIssue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'skratch-issue-h-code/:id',
        component: SkratchIssueHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.skratchIssue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const skratchIssuePopupRoute: Routes = [
    {
        path: 'skratch-issue-h-code-new',
        component: SkratchIssueHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.skratchIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'skratch-issue-h-code/:id/edit',
        component: SkratchIssueHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.skratchIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'skratch-issue-h-code/:id/delete',
        component: SkratchIssueHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.skratchIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
