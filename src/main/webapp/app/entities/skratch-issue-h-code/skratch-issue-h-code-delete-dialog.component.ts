import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SkratchIssueHCode } from './skratch-issue-h-code.model';
import { SkratchIssueHCodePopupService } from './skratch-issue-h-code-popup.service';
import { SkratchIssueHCodeService } from './skratch-issue-h-code.service';

@Component({
    selector: 'jhi-skratch-issue-h-code-delete-dialog',
    templateUrl: './skratch-issue-h-code-delete-dialog.component.html'
})
export class SkratchIssueHCodeDeleteDialogComponent {

    skratchIssue: SkratchIssueHCode;

    constructor(
        private skratchIssueService: SkratchIssueHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.skratchIssueService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'skratchIssueListModification',
                content: 'Deleted an skratchIssue'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-skratch-issue-h-code-delete-popup',
    template: ''
})
export class SkratchIssueHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private skratchIssuePopupService: SkratchIssueHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.skratchIssuePopupService
                .open(SkratchIssueHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
