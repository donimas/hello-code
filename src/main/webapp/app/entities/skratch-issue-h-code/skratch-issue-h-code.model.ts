import { BaseEntity } from './../../shared';

export class SkratchIssueHCode implements BaseEntity {
    constructor(
        public id?: number,
        public url?: string,
        public note?: string,
        public flagDeleted?: boolean,
        public issue?: BaseEntity,
    ) {
        this.flagDeleted = false;
    }
}
