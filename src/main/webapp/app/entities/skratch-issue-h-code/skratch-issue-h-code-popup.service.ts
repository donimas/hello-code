import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { SkratchIssueHCode } from './skratch-issue-h-code.model';
import { SkratchIssueHCodeService } from './skratch-issue-h-code.service';

@Injectable()
export class SkratchIssueHCodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private skratchIssueService: SkratchIssueHCodeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.skratchIssueService.find(id)
                    .subscribe((skratchIssueResponse: HttpResponse<SkratchIssueHCode>) => {
                        const skratchIssue: SkratchIssueHCode = skratchIssueResponse.body;
                        this.ngbModalRef = this.skratchIssueModalRef(component, skratchIssue);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.skratchIssueModalRef(component, new SkratchIssueHCode());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    skratchIssueModalRef(component: Component, skratchIssue: SkratchIssueHCode): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.skratchIssue = skratchIssue;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
