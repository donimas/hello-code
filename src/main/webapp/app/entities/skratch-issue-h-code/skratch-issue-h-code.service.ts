import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { SkratchIssueHCode } from './skratch-issue-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<SkratchIssueHCode>;

@Injectable()
export class SkratchIssueHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/skratch-issues';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/skratch-issues';

    constructor(private http: HttpClient) { }

    create(skratchIssue: SkratchIssueHCode): Observable<EntityResponseType> {
        const copy = this.convert(skratchIssue);
        return this.http.post<SkratchIssueHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(skratchIssue: SkratchIssueHCode): Observable<EntityResponseType> {
        const copy = this.convert(skratchIssue);
        return this.http.put<SkratchIssueHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SkratchIssueHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<SkratchIssueHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<SkratchIssueHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SkratchIssueHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<SkratchIssueHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<SkratchIssueHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SkratchIssueHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SkratchIssueHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<SkratchIssueHCode[]>): HttpResponse<SkratchIssueHCode[]> {
        const jsonResponse: SkratchIssueHCode[] = res.body;
        const body: SkratchIssueHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to SkratchIssueHCode.
     */
    private convertItemFromServer(skratchIssue: SkratchIssueHCode): SkratchIssueHCode {
        const copy: SkratchIssueHCode = Object.assign({}, skratchIssue);
        return copy;
    }

    /**
     * Convert a SkratchIssueHCode to a JSON which can be sent to the server.
     */
    private convert(skratchIssue: SkratchIssueHCode): SkratchIssueHCode {
        const copy: SkratchIssueHCode = Object.assign({}, skratchIssue);
        return copy;
    }
}
