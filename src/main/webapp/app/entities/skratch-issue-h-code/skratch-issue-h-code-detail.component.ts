import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { SkratchIssueHCode } from './skratch-issue-h-code.model';
import { SkratchIssueHCodeService } from './skratch-issue-h-code.service';

@Component({
    selector: 'jhi-skratch-issue-h-code-detail',
    templateUrl: './skratch-issue-h-code-detail.component.html'
})
export class SkratchIssueHCodeDetailComponent implements OnInit, OnDestroy {

    skratchIssue: SkratchIssueHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private skratchIssueService: SkratchIssueHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSkratchIssues();
    }

    load(id) {
        this.skratchIssueService.find(id)
            .subscribe((skratchIssueResponse: HttpResponse<SkratchIssueHCode>) => {
                this.skratchIssue = skratchIssueResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSkratchIssues() {
        this.eventSubscriber = this.eventManager.subscribe(
            'skratchIssueListModification',
            (response) => this.load(this.skratchIssue.id)
        );
    }
}
