import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CourseIssueHCode } from './course-issue-h-code.model';
import { CourseIssueHCodePopupService } from './course-issue-h-code-popup.service';
import { CourseIssueHCodeService } from './course-issue-h-code.service';

@Component({
    selector: 'jhi-course-issue-h-code-delete-dialog',
    templateUrl: './course-issue-h-code-delete-dialog.component.html'
})
export class CourseIssueHCodeDeleteDialogComponent {

    courseIssue: CourseIssueHCode;

    constructor(
        private courseIssueService: CourseIssueHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.courseIssueService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'courseIssueListModification',
                content: 'Deleted an courseIssue'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-course-issue-h-code-delete-popup',
    template: ''
})
export class CourseIssueHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private courseIssuePopupService: CourseIssueHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.courseIssuePopupService
                .open(CourseIssueHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
