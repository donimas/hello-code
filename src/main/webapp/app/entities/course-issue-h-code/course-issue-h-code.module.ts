import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    CourseIssueHCodeService,
    CourseIssueHCodePopupService,
    CourseIssueHCodeComponent,
    CourseIssueHCodeDetailComponent,
    CourseIssueHCodeDialogComponent,
    CourseIssueHCodePopupComponent,
    CourseIssueHCodeDeletePopupComponent,
    CourseIssueHCodeDeleteDialogComponent,
    courseIssueRoute,
    courseIssuePopupRoute,
} from './';

const ENTITY_STATES = [
    ...courseIssueRoute,
    ...courseIssuePopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CourseIssueHCodeComponent,
        CourseIssueHCodeDetailComponent,
        CourseIssueHCodeDialogComponent,
        CourseIssueHCodeDeleteDialogComponent,
        CourseIssueHCodePopupComponent,
        CourseIssueHCodeDeletePopupComponent,
    ],
    entryComponents: [
        CourseIssueHCodeComponent,
        CourseIssueHCodeDialogComponent,
        CourseIssueHCodePopupComponent,
        CourseIssueHCodeDeleteDialogComponent,
        CourseIssueHCodeDeletePopupComponent,
    ],
    providers: [
        CourseIssueHCodeService,
        CourseIssueHCodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeCourseIssueHCodeModule {}
