import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { CourseIssueHCode } from './course-issue-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<CourseIssueHCode>;

@Injectable()
export class CourseIssueHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/course-issues';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/course-issues';

    constructor(private http: HttpClient) { }

    create(courseIssue: CourseIssueHCode): Observable<EntityResponseType> {
        const copy = this.convert(courseIssue);
        return this.http.post<CourseIssueHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(courseIssue: CourseIssueHCode): Observable<EntityResponseType> {
        const copy = this.convert(courseIssue);
        return this.http.put<CourseIssueHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CourseIssueHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CourseIssueHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<CourseIssueHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CourseIssueHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<CourseIssueHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<CourseIssueHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CourseIssueHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CourseIssueHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CourseIssueHCode[]>): HttpResponse<CourseIssueHCode[]> {
        const jsonResponse: CourseIssueHCode[] = res.body;
        const body: CourseIssueHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CourseIssueHCode.
     */
    private convertItemFromServer(courseIssue: CourseIssueHCode): CourseIssueHCode {
        const copy: CourseIssueHCode = Object.assign({}, courseIssue);
        return copy;
    }

    /**
     * Convert a CourseIssueHCode to a JSON which can be sent to the server.
     */
    private convert(courseIssue: CourseIssueHCode): CourseIssueHCode {
        const copy: CourseIssueHCode = Object.assign({}, courseIssue);
        return copy;
    }
}
