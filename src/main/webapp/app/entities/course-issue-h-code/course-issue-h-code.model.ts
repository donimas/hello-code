import { BaseEntity } from './../../shared';

export const enum IssueType {
    'VIDEO',
    'TEST',
    'TEXT',
    'WEBINAR',
    'SKRATCH'
}

export class CourseIssueHCode implements BaseEntity {
    constructor(
        public id?: number,
        public issueType?: IssueType,
        public nameRu?: string,
        public nameKk?: string,
        public nameEn?: string,
        public noteRu?: string,
        public noteKk?: string,
        public noteEn?: string,
        public order?: number,
        public flagDeleted?: boolean,
        public chapter?: BaseEntity,
        public course?: BaseEntity,
    ) {
        this.flagDeleted = false;
    }
}
