import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { CourseIssueHCode } from './course-issue-h-code.model';
import { CourseIssueHCodeService } from './course-issue-h-code.service';

@Injectable()
export class CourseIssueHCodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private courseIssueService: CourseIssueHCodeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.courseIssueService.find(id)
                    .subscribe((courseIssueResponse: HttpResponse<CourseIssueHCode>) => {
                        const courseIssue: CourseIssueHCode = courseIssueResponse.body;
                        this.ngbModalRef = this.courseIssueModalRef(component, courseIssue);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.courseIssueModalRef(component, new CourseIssueHCode());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    courseIssueModalRef(component: Component, courseIssue: CourseIssueHCode): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.courseIssue = courseIssue;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
