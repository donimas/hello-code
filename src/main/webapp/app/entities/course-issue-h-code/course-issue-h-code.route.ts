import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { CourseIssueHCodeComponent } from './course-issue-h-code.component';
import { CourseIssueHCodeDetailComponent } from './course-issue-h-code-detail.component';
import { CourseIssueHCodePopupComponent } from './course-issue-h-code-dialog.component';
import { CourseIssueHCodeDeletePopupComponent } from './course-issue-h-code-delete-dialog.component';

export const courseIssueRoute: Routes = [
    {
        path: 'course-issue-h-code',
        component: CourseIssueHCodeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseIssue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'course-issue-h-code/:id',
        component: CourseIssueHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseIssue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const courseIssuePopupRoute: Routes = [
    {
        path: 'course-issue-h-code-new',
        component: CourseIssueHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'course-issue-h-code/:id/edit',
        component: CourseIssueHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'course-issue-h-code/:id/delete',
        component: CourseIssueHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
