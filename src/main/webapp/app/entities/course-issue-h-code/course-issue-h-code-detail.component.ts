import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { CourseIssueHCode } from './course-issue-h-code.model';
import { CourseIssueHCodeService } from './course-issue-h-code.service';

@Component({
    selector: 'jhi-course-issue-h-code-detail',
    templateUrl: './course-issue-h-code-detail.component.html'
})
export class CourseIssueHCodeDetailComponent implements OnInit, OnDestroy {

    courseIssue: CourseIssueHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private courseIssueService: CourseIssueHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCourseIssues();
    }

    load(id) {
        this.courseIssueService.find(id)
            .subscribe((courseIssueResponse: HttpResponse<CourseIssueHCode>) => {
                this.courseIssue = courseIssueResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCourseIssues() {
        this.eventSubscriber = this.eventManager.subscribe(
            'courseIssueListModification',
            (response) => this.load(this.courseIssue.id)
        );
    }
}
