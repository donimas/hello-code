import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CourseIssueHCode } from './course-issue-h-code.model';
import { CourseIssueHCodePopupService } from './course-issue-h-code-popup.service';
import { CourseIssueHCodeService } from './course-issue-h-code.service';
import { CourseChapterHCode, CourseChapterHCodeService } from '../course-chapter-h-code';
import { CourseHCode, CourseHCodeService } from '../course-h-code';

@Component({
    selector: 'jhi-course-issue-h-code-dialog',
    templateUrl: './course-issue-h-code-dialog.component.html'
})
export class CourseIssueHCodeDialogComponent implements OnInit {

    courseIssue: CourseIssueHCode;
    isSaving: boolean;

    coursechapters: CourseChapterHCode[];

    courses: CourseHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private courseIssueService: CourseIssueHCodeService,
        private courseChapterService: CourseChapterHCodeService,
        private courseService: CourseHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.courseChapterService.query()
            .subscribe((res: HttpResponse<CourseChapterHCode[]>) => { this.coursechapters = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.courseService.query()
            .subscribe((res: HttpResponse<CourseHCode[]>) => { this.courses = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.courseIssue.id !== undefined) {
            this.subscribeToSaveResponse(
                this.courseIssueService.update(this.courseIssue));
        } else {
            this.subscribeToSaveResponse(
                this.courseIssueService.create(this.courseIssue));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CourseIssueHCode>>) {
        result.subscribe((res: HttpResponse<CourseIssueHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CourseIssueHCode) {
        this.eventManager.broadcast({ name: 'courseIssueListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCourseChapterById(index: number, item: CourseChapterHCode) {
        return item.id;
    }

    trackCourseById(index: number, item: CourseHCode) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-course-issue-h-code-popup',
    template: ''
})
export class CourseIssueHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private courseIssuePopupService: CourseIssueHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.courseIssuePopupService
                    .open(CourseIssueHCodeDialogComponent as Component, params['id']);
            } else {
                this.courseIssuePopupService
                    .open(CourseIssueHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
