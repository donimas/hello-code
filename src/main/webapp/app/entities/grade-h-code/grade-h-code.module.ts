import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    GradeHCodeService,
    GradeHCodePopupService,
    GradeHCodeComponent,
    GradeHCodeDetailComponent,
    GradeHCodeDialogComponent,
    GradeHCodePopupComponent,
    GradeHCodeDeletePopupComponent,
    GradeHCodeDeleteDialogComponent,
    gradeRoute,
    gradePopupRoute,
} from './';

const ENTITY_STATES = [
    ...gradeRoute,
    ...gradePopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        GradeHCodeComponent,
        GradeHCodeDetailComponent,
        GradeHCodeDialogComponent,
        GradeHCodeDeleteDialogComponent,
        GradeHCodePopupComponent,
        GradeHCodeDeletePopupComponent,
    ],
    entryComponents: [
        GradeHCodeComponent,
        GradeHCodeDialogComponent,
        GradeHCodePopupComponent,
        GradeHCodeDeleteDialogComponent,
        GradeHCodeDeletePopupComponent,
    ],
    providers: [
        GradeHCodeService,
        GradeHCodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeGradeHCodeModule {}
