import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { GradeHCodeComponent } from './grade-h-code.component';
import { GradeHCodeDetailComponent } from './grade-h-code-detail.component';
import { GradeHCodePopupComponent } from './grade-h-code-dialog.component';
import { GradeHCodeDeletePopupComponent } from './grade-h-code-delete-dialog.component';

export const gradeRoute: Routes = [
    {
        path: 'grade-h-code',
        component: GradeHCodeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.grade.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'grade-h-code/:id',
        component: GradeHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.grade.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const gradePopupRoute: Routes = [
    {
        path: 'grade-h-code-new',
        component: GradeHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.grade.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'grade-h-code/:id/edit',
        component: GradeHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.grade.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'grade-h-code/:id/delete',
        component: GradeHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.grade.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
