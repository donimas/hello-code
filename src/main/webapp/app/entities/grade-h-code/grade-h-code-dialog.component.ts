import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { GradeHCode } from './grade-h-code.model';
import { GradeHCodePopupService } from './grade-h-code-popup.service';
import { GradeHCodeService } from './grade-h-code.service';
import { ProgramHCode, ProgramHCodeService } from '../program-h-code';
import { CourseHCode, CourseHCodeService } from '../course-h-code';

@Component({
    selector: 'jhi-grade-h-code-dialog',
    templateUrl: './grade-h-code-dialog.component.html'
})
export class GradeHCodeDialogComponent implements OnInit {

    grade: GradeHCode;
    isSaving: boolean;

    programs: ProgramHCode[];

    courses: CourseHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private gradeService: GradeHCodeService,
        private programService: ProgramHCodeService,
        private courseService: CourseHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.programService.query()
            .subscribe((res: HttpResponse<ProgramHCode[]>) => { this.programs = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.courseService.query()
            .subscribe((res: HttpResponse<CourseHCode[]>) => { this.courses = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.grade.id !== undefined) {
            this.subscribeToSaveResponse(
                this.gradeService.update(this.grade));
        } else {
            this.subscribeToSaveResponse(
                this.gradeService.create(this.grade));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<GradeHCode>>) {
        result.subscribe((res: HttpResponse<GradeHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: GradeHCode) {
        this.eventManager.broadcast({ name: 'gradeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackProgramById(index: number, item: ProgramHCode) {
        return item.id;
    }

    trackCourseById(index: number, item: CourseHCode) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-grade-h-code-popup',
    template: ''
})
export class GradeHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private gradePopupService: GradeHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.gradePopupService
                    .open(GradeHCodeDialogComponent as Component, params['id']);
            } else {
                this.gradePopupService
                    .open(GradeHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
