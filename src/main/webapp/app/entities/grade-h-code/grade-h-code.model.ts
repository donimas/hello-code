import { BaseEntity } from './../../shared';

export class GradeHCode implements BaseEntity {
    constructor(
        public id?: number,
        public nameRu?: string,
        public nameKk?: string,
        public nameEn?: string,
        public flagDeleted?: boolean,
        public programs?: BaseEntity[],
        public courses?: BaseEntity[],
    ) {
        this.flagDeleted = false;
    }
}
