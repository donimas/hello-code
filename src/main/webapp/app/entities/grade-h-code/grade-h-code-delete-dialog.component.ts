import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { GradeHCode } from './grade-h-code.model';
import { GradeHCodePopupService } from './grade-h-code-popup.service';
import { GradeHCodeService } from './grade-h-code.service';

@Component({
    selector: 'jhi-grade-h-code-delete-dialog',
    templateUrl: './grade-h-code-delete-dialog.component.html'
})
export class GradeHCodeDeleteDialogComponent {

    grade: GradeHCode;

    constructor(
        private gradeService: GradeHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.gradeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'gradeListModification',
                content: 'Deleted an grade'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-grade-h-code-delete-popup',
    template: ''
})
export class GradeHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private gradePopupService: GradeHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.gradePopupService
                .open(GradeHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
