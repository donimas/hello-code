import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { GradeHCode } from './grade-h-code.model';
import { GradeHCodeService } from './grade-h-code.service';

@Component({
    selector: 'jhi-grade-h-code-detail',
    templateUrl: './grade-h-code-detail.component.html'
})
export class GradeHCodeDetailComponent implements OnInit, OnDestroy {

    grade: GradeHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private gradeService: GradeHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInGrades();
    }

    load(id) {
        this.gradeService.find(id)
            .subscribe((gradeResponse: HttpResponse<GradeHCode>) => {
                this.grade = gradeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInGrades() {
        this.eventSubscriber = this.eventManager.subscribe(
            'gradeListModification',
            (response) => this.load(this.grade.id)
        );
    }
}
