import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { GradeHCode } from './grade-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<GradeHCode>;

@Injectable()
export class GradeHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/grades';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/grades';

    constructor(private http: HttpClient) { }

    create(grade: GradeHCode): Observable<EntityResponseType> {
        const copy = this.convert(grade);
        return this.http.post<GradeHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(grade: GradeHCode): Observable<EntityResponseType> {
        const copy = this.convert(grade);
        return this.http.put<GradeHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<GradeHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<GradeHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<GradeHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<GradeHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<GradeHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<GradeHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<GradeHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: GradeHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<GradeHCode[]>): HttpResponse<GradeHCode[]> {
        const jsonResponse: GradeHCode[] = res.body;
        const body: GradeHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to GradeHCode.
     */
    private convertItemFromServer(grade: GradeHCode): GradeHCode {
        const copy: GradeHCode = Object.assign({}, grade);
        return copy;
    }

    /**
     * Convert a GradeHCode to a JSON which can be sent to the server.
     */
    private convert(grade: GradeHCode): GradeHCode {
        const copy: GradeHCode = Object.assign({}, grade);
        return copy;
    }
}
