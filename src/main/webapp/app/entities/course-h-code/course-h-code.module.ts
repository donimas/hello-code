import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import { HelloCodeAdminModule } from '../../admin/admin.module';
import {
    CourseHCodeService,
    CourseHCodePopupService,
    CourseHCodeComponent,
    CourseHCodeDetailComponent,
    CourseHCodeDialogComponent,
    CourseHCodePopupComponent,
    CourseHCodeDeletePopupComponent,
    CourseHCodeDeleteDialogComponent,
    courseRoute,
    coursePopupRoute,
    CourseHCodeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...courseRoute,
    ...coursePopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        HelloCodeAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CourseHCodeComponent,
        CourseHCodeDetailComponent,
        CourseHCodeDialogComponent,
        CourseHCodeDeleteDialogComponent,
        CourseHCodePopupComponent,
        CourseHCodeDeletePopupComponent,
    ],
    entryComponents: [
        CourseHCodeComponent,
        CourseHCodeDialogComponent,
        CourseHCodePopupComponent,
        CourseHCodeDeleteDialogComponent,
        CourseHCodeDeletePopupComponent,
    ],
    providers: [
        CourseHCodeService,
        CourseHCodePopupService,
        CourseHCodeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeCourseHCodeModule {}
