import { BaseEntity, User } from './../../shared';

export class CourseHCode implements BaseEntity {
    constructor(
        public id?: number,
        public totalIssues?: number,
        public lecturesAmount?: number,
        public videoDuration?: number,
        public nameRu?: string,
        public nameKk?: string,
        public nameEn?: string,
        public descriptionRu?: string,
        public descriptionKk?: string,
        public descriptionEn?: string,
        public briefRu?: string,
        public briefKk?: string,
        public briefEn?: string,
        public flagDeleted?: boolean,
        public imageContentType?: string,
        public image?: any,
        public lecturer?: User,
        public grades?: BaseEntity[],
        public categories?: BaseEntity[],
        public courseChapters?: BaseEntity[],
    ) {
        this.flagDeleted = false;
    }
}
