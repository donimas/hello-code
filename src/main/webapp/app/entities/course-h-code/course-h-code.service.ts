import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { CourseHCode } from './course-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<CourseHCode>;

@Injectable()
export class CourseHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/courses';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/courses';

    constructor(private http: HttpClient) { }

    create(course: CourseHCode): Observable<EntityResponseType> {
        const copy = this.convert(course);
        return this.http.post<CourseHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(course: CourseHCode): Observable<EntityResponseType> {
        const copy = this.convert(course);
        return this.http.put<CourseHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CourseHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CourseHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<CourseHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CourseHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<CourseHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<CourseHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CourseHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CourseHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CourseHCode[]>): HttpResponse<CourseHCode[]> {
        const jsonResponse: CourseHCode[] = res.body;
        const body: CourseHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CourseHCode.
     */
    private convertItemFromServer(course: CourseHCode): CourseHCode {
        const copy: CourseHCode = Object.assign({}, course);
        return copy;
    }

    /**
     * Convert a CourseHCode to a JSON which can be sent to the server.
     */
    private convert(course: CourseHCode): CourseHCode {
        const copy: CourseHCode = Object.assign({}, course);
        return copy;
    }
}
