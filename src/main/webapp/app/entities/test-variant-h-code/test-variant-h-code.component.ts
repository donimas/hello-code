import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TestVariantHCode } from './test-variant-h-code.model';
import { TestVariantHCodeService } from './test-variant-h-code.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-test-variant-h-code',
    templateUrl: './test-variant-h-code.component.html'
})
export class TestVariantHCodeComponent implements OnInit, OnDestroy {
testVariants: TestVariantHCode[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private testVariantService: TestVariantHCodeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.testVariantService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<TestVariantHCode[]>) => this.testVariants = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.testVariantService.query().subscribe(
            (res: HttpResponse<TestVariantHCode[]>) => {
                this.testVariants = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTestVariants();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TestVariantHCode) {
        return item.id;
    }
    registerChangeInTestVariants() {
        this.eventSubscriber = this.eventManager.subscribe('testVariantListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
