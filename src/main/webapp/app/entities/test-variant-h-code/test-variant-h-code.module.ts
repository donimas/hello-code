import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    TestVariantHCodeService,
    TestVariantHCodePopupService,
    TestVariantHCodeComponent,
    TestVariantHCodeDetailComponent,
    TestVariantHCodeDialogComponent,
    TestVariantHCodePopupComponent,
    TestVariantHCodeDeletePopupComponent,
    TestVariantHCodeDeleteDialogComponent,
    testVariantRoute,
    testVariantPopupRoute,
} from './';

const ENTITY_STATES = [
    ...testVariantRoute,
    ...testVariantPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TestVariantHCodeComponent,
        TestVariantHCodeDetailComponent,
        TestVariantHCodeDialogComponent,
        TestVariantHCodeDeleteDialogComponent,
        TestVariantHCodePopupComponent,
        TestVariantHCodeDeletePopupComponent,
    ],
    entryComponents: [
        TestVariantHCodeComponent,
        TestVariantHCodeDialogComponent,
        TestVariantHCodePopupComponent,
        TestVariantHCodeDeleteDialogComponent,
        TestVariantHCodeDeletePopupComponent,
    ],
    providers: [
        TestVariantHCodeService,
        TestVariantHCodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeTestVariantHCodeModule {}
