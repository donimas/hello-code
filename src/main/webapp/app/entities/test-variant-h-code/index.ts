export * from './test-variant-h-code.model';
export * from './test-variant-h-code-popup.service';
export * from './test-variant-h-code.service';
export * from './test-variant-h-code-dialog.component';
export * from './test-variant-h-code-delete-dialog.component';
export * from './test-variant-h-code-detail.component';
export * from './test-variant-h-code.component';
export * from './test-variant-h-code.route';
