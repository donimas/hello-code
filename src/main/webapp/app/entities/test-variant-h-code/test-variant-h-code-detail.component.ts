import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TestVariantHCode } from './test-variant-h-code.model';
import { TestVariantHCodeService } from './test-variant-h-code.service';

@Component({
    selector: 'jhi-test-variant-h-code-detail',
    templateUrl: './test-variant-h-code-detail.component.html'
})
export class TestVariantHCodeDetailComponent implements OnInit, OnDestroy {

    testVariant: TestVariantHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private testVariantService: TestVariantHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTestVariants();
    }

    load(id) {
        this.testVariantService.find(id)
            .subscribe((testVariantResponse: HttpResponse<TestVariantHCode>) => {
                this.testVariant = testVariantResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTestVariants() {
        this.eventSubscriber = this.eventManager.subscribe(
            'testVariantListModification',
            (response) => this.load(this.testVariant.id)
        );
    }
}
