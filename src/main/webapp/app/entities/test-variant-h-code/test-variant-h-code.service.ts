import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { TestVariantHCode } from './test-variant-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TestVariantHCode>;

@Injectable()
export class TestVariantHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/test-variants';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/test-variants';

    constructor(private http: HttpClient) { }

    create(testVariant: TestVariantHCode): Observable<EntityResponseType> {
        const copy = this.convert(testVariant);
        return this.http.post<TestVariantHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(testVariant: TestVariantHCode): Observable<EntityResponseType> {
        const copy = this.convert(testVariant);
        return this.http.put<TestVariantHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<TestVariantHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TestVariantHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<TestVariantHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TestVariantHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<TestVariantHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<TestVariantHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TestVariantHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TestVariantHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TestVariantHCode[]>): HttpResponse<TestVariantHCode[]> {
        const jsonResponse: TestVariantHCode[] = res.body;
        const body: TestVariantHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TestVariantHCode.
     */
    private convertItemFromServer(testVariant: TestVariantHCode): TestVariantHCode {
        const copy: TestVariantHCode = Object.assign({}, testVariant);
        return copy;
    }

    /**
     * Convert a TestVariantHCode to a JSON which can be sent to the server.
     */
    private convert(testVariant: TestVariantHCode): TestVariantHCode {
        const copy: TestVariantHCode = Object.assign({}, testVariant);
        return copy;
    }
}
