import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TestVariantHCode } from './test-variant-h-code.model';
import { TestVariantHCodePopupService } from './test-variant-h-code-popup.service';
import { TestVariantHCodeService } from './test-variant-h-code.service';
import { TestQuestionHCode, TestQuestionHCodeService } from '../test-question-h-code';

@Component({
    selector: 'jhi-test-variant-h-code-dialog',
    templateUrl: './test-variant-h-code-dialog.component.html'
})
export class TestVariantHCodeDialogComponent implements OnInit {

    testVariant: TestVariantHCode;
    isSaving: boolean;

    testquestions: TestQuestionHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private testVariantService: TestVariantHCodeService,
        private testQuestionService: TestQuestionHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.testQuestionService.query()
            .subscribe((res: HttpResponse<TestQuestionHCode[]>) => { this.testquestions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.testVariant.id !== undefined) {
            this.subscribeToSaveResponse(
                this.testVariantService.update(this.testVariant));
        } else {
            this.subscribeToSaveResponse(
                this.testVariantService.create(this.testVariant));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TestVariantHCode>>) {
        result.subscribe((res: HttpResponse<TestVariantHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TestVariantHCode) {
        this.eventManager.broadcast({ name: 'testVariantListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackTestQuestionById(index: number, item: TestQuestionHCode) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-test-variant-h-code-popup',
    template: ''
})
export class TestVariantHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private testVariantPopupService: TestVariantHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.testVariantPopupService
                    .open(TestVariantHCodeDialogComponent as Component, params['id']);
            } else {
                this.testVariantPopupService
                    .open(TestVariantHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
