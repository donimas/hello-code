import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TestVariantHCode } from './test-variant-h-code.model';
import { TestVariantHCodePopupService } from './test-variant-h-code-popup.service';
import { TestVariantHCodeService } from './test-variant-h-code.service';

@Component({
    selector: 'jhi-test-variant-h-code-delete-dialog',
    templateUrl: './test-variant-h-code-delete-dialog.component.html'
})
export class TestVariantHCodeDeleteDialogComponent {

    testVariant: TestVariantHCode;

    constructor(
        private testVariantService: TestVariantHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.testVariantService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'testVariantListModification',
                content: 'Deleted an testVariant'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-test-variant-h-code-delete-popup',
    template: ''
})
export class TestVariantHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private testVariantPopupService: TestVariantHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.testVariantPopupService
                .open(TestVariantHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
