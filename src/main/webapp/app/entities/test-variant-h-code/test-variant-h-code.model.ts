import { BaseEntity } from './../../shared';

export class TestVariantHCode implements BaseEntity {
    constructor(
        public id?: number,
        public bodyRu?: string,
        public bodyKk?: string,
        public bodyEn?: string,
        public flagCorrect?: boolean,
        public flagDeleted?: boolean,
        public question?: BaseEntity,
    ) {
        this.flagCorrect = false;
        this.flagDeleted = false;
    }
}
