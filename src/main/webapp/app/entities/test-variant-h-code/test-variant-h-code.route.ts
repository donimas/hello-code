import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TestVariantHCodeComponent } from './test-variant-h-code.component';
import { TestVariantHCodeDetailComponent } from './test-variant-h-code-detail.component';
import { TestVariantHCodePopupComponent } from './test-variant-h-code-dialog.component';
import { TestVariantHCodeDeletePopupComponent } from './test-variant-h-code-delete-dialog.component';

export const testVariantRoute: Routes = [
    {
        path: 'test-variant-h-code',
        component: TestVariantHCodeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testVariant.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'test-variant-h-code/:id',
        component: TestVariantHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testVariant.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const testVariantPopupRoute: Routes = [
    {
        path: 'test-variant-h-code-new',
        component: TestVariantHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testVariant.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'test-variant-h-code/:id/edit',
        component: TestVariantHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testVariant.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'test-variant-h-code/:id/delete',
        component: TestVariantHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testVariant.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
