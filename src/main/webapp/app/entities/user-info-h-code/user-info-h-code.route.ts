import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { UserInfoHCodeComponent } from './user-info-h-code.component';
import { UserInfoHCodeDetailComponent } from './user-info-h-code-detail.component';
import { UserInfoHCodePopupComponent } from './user-info-h-code-dialog.component';
import { UserInfoHCodeDeletePopupComponent } from './user-info-h-code-delete-dialog.component';

export const userInfoRoute: Routes = [
    {
        path: 'user-info-h-code',
        component: UserInfoHCodeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.userInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'user-info-h-code/:id',
        component: UserInfoHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.userInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const userInfoPopupRoute: Routes = [
    {
        path: 'user-info-h-code-new',
        component: UserInfoHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.userInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'user-info-h-code/:id/edit',
        component: UserInfoHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.userInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'user-info-h-code/:id/delete',
        component: UserInfoHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.userInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
