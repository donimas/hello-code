import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { UserInfoHCode } from './user-info-h-code.model';
import { UserInfoHCodeService } from './user-info-h-code.service';

@Component({
    selector: 'jhi-user-info-h-code-detail',
    templateUrl: './user-info-h-code-detail.component.html'
})
export class UserInfoHCodeDetailComponent implements OnInit, OnDestroy {

    userInfo: UserInfoHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private userInfoService: UserInfoHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUserInfos();
    }

    load(id) {
        this.userInfoService.find(id)
            .subscribe((userInfoResponse: HttpResponse<UserInfoHCode>) => {
                this.userInfo = userInfoResponse.body;
            });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUserInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'userInfoListModification',
            (response) => this.load(this.userInfo.id)
        );
    }
}
