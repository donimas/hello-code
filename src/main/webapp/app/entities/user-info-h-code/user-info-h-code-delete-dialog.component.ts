import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UserInfoHCode } from './user-info-h-code.model';
import { UserInfoHCodePopupService } from './user-info-h-code-popup.service';
import { UserInfoHCodeService } from './user-info-h-code.service';

@Component({
    selector: 'jhi-user-info-h-code-delete-dialog',
    templateUrl: './user-info-h-code-delete-dialog.component.html'
})
export class UserInfoHCodeDeleteDialogComponent {

    userInfo: UserInfoHCode;

    constructor(
        private userInfoService: UserInfoHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.userInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'userInfoListModification',
                content: 'Deleted an userInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-user-info-h-code-delete-popup',
    template: ''
})
export class UserInfoHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private userInfoPopupService: UserInfoHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.userInfoPopupService
                .open(UserInfoHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
