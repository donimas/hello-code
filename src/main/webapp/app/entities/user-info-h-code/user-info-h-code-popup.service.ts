import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { UserInfoHCode } from './user-info-h-code.model';
import { UserInfoHCodeService } from './user-info-h-code.service';

@Injectable()
export class UserInfoHCodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private userInfoService: UserInfoHCodeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.userInfoService.find(id)
                    .subscribe((userInfoResponse: HttpResponse<UserInfoHCode>) => {
                        const userInfo: UserInfoHCode = userInfoResponse.body;
                        if (userInfo.dob) {
                            userInfo.dob = {
                                year: userInfo.dob.getFullYear(),
                                month: userInfo.dob.getMonth() + 1,
                                day: userInfo.dob.getDate()
                            };
                        }
                        this.ngbModalRef = this.userInfoModalRef(component, userInfo);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.userInfoModalRef(component, new UserInfoHCode());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    userInfoModalRef(component: Component, userInfo: UserInfoHCode): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.userInfo = userInfo;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
