import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import { HelloCodeAdminModule } from '../../admin/admin.module';
import {
    UserInfoHCodeService,
    UserInfoHCodePopupService,
    UserInfoHCodeComponent,
    UserInfoHCodeDetailComponent,
    UserInfoHCodeDialogComponent,
    UserInfoHCodePopupComponent,
    UserInfoHCodeDeletePopupComponent,
    UserInfoHCodeDeleteDialogComponent,
    userInfoRoute,
    userInfoPopupRoute,
} from './';

const ENTITY_STATES = [
    ...userInfoRoute,
    ...userInfoPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        HelloCodeAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        UserInfoHCodeComponent,
        UserInfoHCodeDetailComponent,
        UserInfoHCodeDialogComponent,
        UserInfoHCodeDeleteDialogComponent,
        UserInfoHCodePopupComponent,
        UserInfoHCodeDeletePopupComponent,
    ],
    entryComponents: [
        UserInfoHCodeComponent,
        UserInfoHCodeDialogComponent,
        UserInfoHCodePopupComponent,
        UserInfoHCodeDeleteDialogComponent,
        UserInfoHCodeDeletePopupComponent,
    ],
    providers: [
        UserInfoHCodeService,
        UserInfoHCodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeUserInfoHCodeModule {}
