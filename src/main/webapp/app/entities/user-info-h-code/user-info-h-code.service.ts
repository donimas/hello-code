import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { UserInfoHCode } from './user-info-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<UserInfoHCode>;

@Injectable()
export class UserInfoHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/user-infos';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/user-infos';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(userInfo: UserInfoHCode): Observable<EntityResponseType> {
        const copy = this.convert(userInfo);
        return this.http.post<UserInfoHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(userInfo: UserInfoHCode): Observable<EntityResponseType> {
        const copy = this.convert(userInfo);
        return this.http.put<UserInfoHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<UserInfoHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<UserInfoHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<UserInfoHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<UserInfoHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<UserInfoHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<UserInfoHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<UserInfoHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: UserInfoHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<UserInfoHCode[]>): HttpResponse<UserInfoHCode[]> {
        const jsonResponse: UserInfoHCode[] = res.body;
        const body: UserInfoHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to UserInfoHCode.
     */
    private convertItemFromServer(userInfo: UserInfoHCode): UserInfoHCode {
        const copy: UserInfoHCode = Object.assign({}, userInfo);
        copy.dob = this.dateUtils
            .convertLocalDateFromServer(userInfo.dob);
        return copy;
    }

    /**
     * Convert a UserInfoHCode to a JSON which can be sent to the server.
     */
    private convert(userInfo: UserInfoHCode): UserInfoHCode {
        const copy: UserInfoHCode = Object.assign({}, userInfo);
        copy.dob = this.dateUtils
            .convertLocalDateToServer(userInfo.dob);
        return copy;
    }
}
