import { BaseEntity, User } from './../../shared';

export const enum UserType {
    'LEARNER',
    'TEACHER',
    'ADMIN',
    'PARENT'
}

export class UserInfoHCode implements BaseEntity {
    constructor(
        public id?: number,
        public imageContentType?: string,
        public image?: any,
        public dob?: any,
        public rate?: number,
        public fullAddress?: string,
        public lat?: string,
        public lon?: string,
        public userType?: UserType,
        public iin?: string,
        public user?: User,
    ) {
    }
}
