import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ProgramHCode } from './program-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ProgramHCode>;

@Injectable()
export class ProgramHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/programs';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/programs';

    constructor(private http: HttpClient) { }

    create(program: ProgramHCode): Observable<EntityResponseType> {
        const copy = this.convert(program);
        return this.http.post<ProgramHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(program: ProgramHCode): Observable<EntityResponseType> {
        const copy = this.convert(program);
        return this.http.put<ProgramHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ProgramHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ProgramHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<ProgramHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ProgramHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ProgramHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<ProgramHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ProgramHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ProgramHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ProgramHCode[]>): HttpResponse<ProgramHCode[]> {
        const jsonResponse: ProgramHCode[] = res.body;
        const body: ProgramHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ProgramHCode.
     */
    private convertItemFromServer(program: ProgramHCode): ProgramHCode {
        const copy: ProgramHCode = Object.assign({}, program);
        return copy;
    }

    /**
     * Convert a ProgramHCode to a JSON which can be sent to the server.
     */
    private convert(program: ProgramHCode): ProgramHCode {
        const copy: ProgramHCode = Object.assign({}, program);
        return copy;
    }
}
