import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ProgramHCodeComponent } from './program-h-code.component';
import { ProgramHCodeDetailComponent } from './program-h-code-detail.component';
import { ProgramHCodePopupComponent } from './program-h-code-dialog.component';
import { ProgramHCodeDeletePopupComponent } from './program-h-code-delete-dialog.component';

@Injectable()
export class ProgramHCodeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const programRoute: Routes = [
    {
        path: 'program-h-code',
        component: ProgramHCodeComponent,
        resolve: {
            'pagingParams': ProgramHCodeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.program.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'program-h-code/:id',
        component: ProgramHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.program.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const programPopupRoute: Routes = [
    {
        path: 'program-h-code-new',
        component: ProgramHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.program.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'program-h-code/:id/edit',
        component: ProgramHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.program.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'program-h-code/:id/delete',
        component: ProgramHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.program.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
