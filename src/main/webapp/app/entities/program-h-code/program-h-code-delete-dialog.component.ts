import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ProgramHCode } from './program-h-code.model';
import { ProgramHCodePopupService } from './program-h-code-popup.service';
import { ProgramHCodeService } from './program-h-code.service';

@Component({
    selector: 'jhi-program-h-code-delete-dialog',
    templateUrl: './program-h-code-delete-dialog.component.html'
})
export class ProgramHCodeDeleteDialogComponent {

    program: ProgramHCode;

    constructor(
        private programService: ProgramHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.programService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'programListModification',
                content: 'Deleted an program'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-program-h-code-delete-popup',
    template: ''
})
export class ProgramHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private programPopupService: ProgramHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.programPopupService
                .open(ProgramHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
