import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProgramHCode } from './program-h-code.model';
import { ProgramHCodePopupService } from './program-h-code-popup.service';
import { ProgramHCodeService } from './program-h-code.service';
import { SchoolHCode, SchoolHCodeService } from '../school-h-code';
import { CourseHCode, CourseHCodeService } from '../course-h-code';
import { GradeHCode, GradeHCodeService } from '../grade-h-code';

@Component({
    selector: 'jhi-program-h-code-dialog',
    templateUrl: './program-h-code-dialog.component.html'
})
export class ProgramHCodeDialogComponent implements OnInit {

    program: ProgramHCode;
    isSaving: boolean;

    schools: SchoolHCode[];

    courses: CourseHCode[];

    grades: GradeHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private programService: ProgramHCodeService,
        private schoolService: SchoolHCodeService,
        private courseService: CourseHCodeService,
        private gradeService: GradeHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.schoolService.query()
            .subscribe((res: HttpResponse<SchoolHCode[]>) => { this.schools = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.courseService.query()
            .subscribe((res: HttpResponse<CourseHCode[]>) => { this.courses = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.gradeService.query()
            .subscribe((res: HttpResponse<GradeHCode[]>) => { this.grades = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.program.id !== undefined) {
            this.subscribeToSaveResponse(
                this.programService.update(this.program));
        } else {
            this.subscribeToSaveResponse(
                this.programService.create(this.program));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ProgramHCode>>) {
        result.subscribe((res: HttpResponse<ProgramHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ProgramHCode) {
        this.eventManager.broadcast({ name: 'programListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSchoolById(index: number, item: SchoolHCode) {
        return item.id;
    }

    trackCourseById(index: number, item: CourseHCode) {
        return item.id;
    }

    trackGradeById(index: number, item: GradeHCode) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-program-h-code-popup',
    template: ''
})
export class ProgramHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private programPopupService: ProgramHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.programPopupService
                    .open(ProgramHCodeDialogComponent as Component, params['id']);
            } else {
                this.programPopupService
                    .open(ProgramHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
