import { BaseEntity } from './../../shared';

export class ProgramHCode implements BaseEntity {
    constructor(
        public id?: number,
        public nameRu?: string,
        public nameKk?: string,
        public nameEn?: string,
        public flagDeleted?: boolean,
        public school?: BaseEntity,
        public courses?: BaseEntity[],
        public grades?: BaseEntity[],
    ) {
        this.flagDeleted = false;
    }
}
