import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    ProgramHCodeService,
    ProgramHCodePopupService,
    ProgramHCodeComponent,
    ProgramHCodeDetailComponent,
    ProgramHCodeDialogComponent,
    ProgramHCodePopupComponent,
    ProgramHCodeDeletePopupComponent,
    ProgramHCodeDeleteDialogComponent,
    programRoute,
    programPopupRoute,
    ProgramHCodeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...programRoute,
    ...programPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ProgramHCodeComponent,
        ProgramHCodeDetailComponent,
        ProgramHCodeDialogComponent,
        ProgramHCodeDeleteDialogComponent,
        ProgramHCodePopupComponent,
        ProgramHCodeDeletePopupComponent,
    ],
    entryComponents: [
        ProgramHCodeComponent,
        ProgramHCodeDialogComponent,
        ProgramHCodePopupComponent,
        ProgramHCodeDeleteDialogComponent,
        ProgramHCodeDeletePopupComponent,
    ],
    providers: [
        ProgramHCodeService,
        ProgramHCodePopupService,
        ProgramHCodeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeProgramHCodeModule {}
