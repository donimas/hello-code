import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ProgramHCode } from './program-h-code.model';
import { ProgramHCodeService } from './program-h-code.service';

@Component({
    selector: 'jhi-program-h-code-detail',
    templateUrl: './program-h-code-detail.component.html'
})
export class ProgramHCodeDetailComponent implements OnInit, OnDestroy {

    program: ProgramHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private programService: ProgramHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPrograms();
    }

    load(id) {
        this.programService.find(id)
            .subscribe((programResponse: HttpResponse<ProgramHCode>) => {
                this.program = programResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPrograms() {
        this.eventSubscriber = this.eventManager.subscribe(
            'programListModification',
            (response) => this.load(this.program.id)
        );
    }
}
