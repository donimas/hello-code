import { BaseEntity } from './../../shared';

export class WebinarIssueHCode implements BaseEntity {
    constructor(
        public id?: number,
        public url?: string,
        public key?: string,
        public cabinte?: string,
        public note?: string,
        public flagDeleted?: boolean,
        public issue?: BaseEntity,
    ) {
        this.flagDeleted = false;
    }
}
