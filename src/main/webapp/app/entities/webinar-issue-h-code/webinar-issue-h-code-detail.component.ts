import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { WebinarIssueHCode } from './webinar-issue-h-code.model';
import { WebinarIssueHCodeService } from './webinar-issue-h-code.service';

@Component({
    selector: 'jhi-webinar-issue-h-code-detail',
    templateUrl: './webinar-issue-h-code-detail.component.html'
})
export class WebinarIssueHCodeDetailComponent implements OnInit, OnDestroy {

    webinarIssue: WebinarIssueHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private webinarIssueService: WebinarIssueHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInWebinarIssues();
    }

    load(id) {
        this.webinarIssueService.find(id)
            .subscribe((webinarIssueResponse: HttpResponse<WebinarIssueHCode>) => {
                this.webinarIssue = webinarIssueResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInWebinarIssues() {
        this.eventSubscriber = this.eventManager.subscribe(
            'webinarIssueListModification',
            (response) => this.load(this.webinarIssue.id)
        );
    }
}
