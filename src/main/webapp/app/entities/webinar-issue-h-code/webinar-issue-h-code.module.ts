import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    WebinarIssueHCodeService,
    WebinarIssueHCodePopupService,
    WebinarIssueHCodeComponent,
    WebinarIssueHCodeDetailComponent,
    WebinarIssueHCodeDialogComponent,
    WebinarIssueHCodePopupComponent,
    WebinarIssueHCodeDeletePopupComponent,
    WebinarIssueHCodeDeleteDialogComponent,
    webinarIssueRoute,
    webinarIssuePopupRoute,
    WebinarIssueHCodeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...webinarIssueRoute,
    ...webinarIssuePopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        WebinarIssueHCodeComponent,
        WebinarIssueHCodeDetailComponent,
        WebinarIssueHCodeDialogComponent,
        WebinarIssueHCodeDeleteDialogComponent,
        WebinarIssueHCodePopupComponent,
        WebinarIssueHCodeDeletePopupComponent,
    ],
    entryComponents: [
        WebinarIssueHCodeComponent,
        WebinarIssueHCodeDialogComponent,
        WebinarIssueHCodePopupComponent,
        WebinarIssueHCodeDeleteDialogComponent,
        WebinarIssueHCodeDeletePopupComponent,
    ],
    providers: [
        WebinarIssueHCodeService,
        WebinarIssueHCodePopupService,
        WebinarIssueHCodeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeWebinarIssueHCodeModule {}
