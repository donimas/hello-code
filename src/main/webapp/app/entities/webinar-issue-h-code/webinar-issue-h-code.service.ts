import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { WebinarIssueHCode } from './webinar-issue-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<WebinarIssueHCode>;

@Injectable()
export class WebinarIssueHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/webinar-issues';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/webinar-issues';

    constructor(private http: HttpClient) { }

    create(webinarIssue: WebinarIssueHCode): Observable<EntityResponseType> {
        const copy = this.convert(webinarIssue);
        return this.http.post<WebinarIssueHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(webinarIssue: WebinarIssueHCode): Observable<EntityResponseType> {
        const copy = this.convert(webinarIssue);
        return this.http.put<WebinarIssueHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<WebinarIssueHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<WebinarIssueHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<WebinarIssueHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<WebinarIssueHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<WebinarIssueHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<WebinarIssueHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<WebinarIssueHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: WebinarIssueHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<WebinarIssueHCode[]>): HttpResponse<WebinarIssueHCode[]> {
        const jsonResponse: WebinarIssueHCode[] = res.body;
        const body: WebinarIssueHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to WebinarIssueHCode.
     */
    private convertItemFromServer(webinarIssue: WebinarIssueHCode): WebinarIssueHCode {
        const copy: WebinarIssueHCode = Object.assign({}, webinarIssue);
        return copy;
    }

    /**
     * Convert a WebinarIssueHCode to a JSON which can be sent to the server.
     */
    private convert(webinarIssue: WebinarIssueHCode): WebinarIssueHCode {
        const copy: WebinarIssueHCode = Object.assign({}, webinarIssue);
        return copy;
    }
}
