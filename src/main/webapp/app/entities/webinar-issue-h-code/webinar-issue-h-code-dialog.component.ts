import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { WebinarIssueHCode } from './webinar-issue-h-code.model';
import { WebinarIssueHCodePopupService } from './webinar-issue-h-code-popup.service';
import { WebinarIssueHCodeService } from './webinar-issue-h-code.service';
import { CourseIssueHCode, CourseIssueHCodeService } from '../course-issue-h-code';

@Component({
    selector: 'jhi-webinar-issue-h-code-dialog',
    templateUrl: './webinar-issue-h-code-dialog.component.html'
})
export class WebinarIssueHCodeDialogComponent implements OnInit {

    webinarIssue: WebinarIssueHCode;
    isSaving: boolean;

    courseissues: CourseIssueHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private webinarIssueService: WebinarIssueHCodeService,
        private courseIssueService: CourseIssueHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.courseIssueService.query()
            .subscribe((res: HttpResponse<CourseIssueHCode[]>) => { this.courseissues = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.webinarIssue.id !== undefined) {
            this.subscribeToSaveResponse(
                this.webinarIssueService.update(this.webinarIssue));
        } else {
            this.subscribeToSaveResponse(
                this.webinarIssueService.create(this.webinarIssue));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<WebinarIssueHCode>>) {
        result.subscribe((res: HttpResponse<WebinarIssueHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: WebinarIssueHCode) {
        this.eventManager.broadcast({ name: 'webinarIssueListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCourseIssueById(index: number, item: CourseIssueHCode) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-webinar-issue-h-code-popup',
    template: ''
})
export class WebinarIssueHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private webinarIssuePopupService: WebinarIssueHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.webinarIssuePopupService
                    .open(WebinarIssueHCodeDialogComponent as Component, params['id']);
            } else {
                this.webinarIssuePopupService
                    .open(WebinarIssueHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
