import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { WebinarIssueHCodeComponent } from './webinar-issue-h-code.component';
import { WebinarIssueHCodeDetailComponent } from './webinar-issue-h-code-detail.component';
import { WebinarIssueHCodePopupComponent } from './webinar-issue-h-code-dialog.component';
import { WebinarIssueHCodeDeletePopupComponent } from './webinar-issue-h-code-delete-dialog.component';

@Injectable()
export class WebinarIssueHCodeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const webinarIssueRoute: Routes = [
    {
        path: 'webinar-issue-h-code',
        component: WebinarIssueHCodeComponent,
        resolve: {
            'pagingParams': WebinarIssueHCodeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.webinarIssue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'webinar-issue-h-code/:id',
        component: WebinarIssueHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.webinarIssue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const webinarIssuePopupRoute: Routes = [
    {
        path: 'webinar-issue-h-code-new',
        component: WebinarIssueHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.webinarIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'webinar-issue-h-code/:id/edit',
        component: WebinarIssueHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.webinarIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'webinar-issue-h-code/:id/delete',
        component: WebinarIssueHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.webinarIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
