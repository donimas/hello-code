import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { WebinarIssueHCode } from './webinar-issue-h-code.model';
import { WebinarIssueHCodePopupService } from './webinar-issue-h-code-popup.service';
import { WebinarIssueHCodeService } from './webinar-issue-h-code.service';

@Component({
    selector: 'jhi-webinar-issue-h-code-delete-dialog',
    templateUrl: './webinar-issue-h-code-delete-dialog.component.html'
})
export class WebinarIssueHCodeDeleteDialogComponent {

    webinarIssue: WebinarIssueHCode;

    constructor(
        private webinarIssueService: WebinarIssueHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.webinarIssueService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'webinarIssueListModification',
                content: 'Deleted an webinarIssue'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-webinar-issue-h-code-delete-popup',
    template: ''
})
export class WebinarIssueHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private webinarIssuePopupService: WebinarIssueHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.webinarIssuePopupService
                .open(WebinarIssueHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
