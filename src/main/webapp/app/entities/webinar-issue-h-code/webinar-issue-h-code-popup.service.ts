import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { WebinarIssueHCode } from './webinar-issue-h-code.model';
import { WebinarIssueHCodeService } from './webinar-issue-h-code.service';

@Injectable()
export class WebinarIssueHCodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private webinarIssueService: WebinarIssueHCodeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.webinarIssueService.find(id)
                    .subscribe((webinarIssueResponse: HttpResponse<WebinarIssueHCode>) => {
                        const webinarIssue: WebinarIssueHCode = webinarIssueResponse.body;
                        this.ngbModalRef = this.webinarIssueModalRef(component, webinarIssue);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.webinarIssueModalRef(component, new WebinarIssueHCode());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    webinarIssueModalRef(component: Component, webinarIssue: WebinarIssueHCode): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.webinarIssue = webinarIssue;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
