import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { TextIssueHCodeComponent } from './text-issue-h-code.component';
import { TextIssueHCodeDetailComponent } from './text-issue-h-code-detail.component';
import { TextIssueHCodePopupComponent } from './text-issue-h-code-dialog.component';
import { TextIssueHCodeDeletePopupComponent } from './text-issue-h-code-delete-dialog.component';

@Injectable()
export class TextIssueHCodeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const textIssueRoute: Routes = [
    {
        path: 'text-issue-h-code',
        component: TextIssueHCodeComponent,
        resolve: {
            'pagingParams': TextIssueHCodeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.textIssue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'text-issue-h-code/:id',
        component: TextIssueHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.textIssue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const textIssuePopupRoute: Routes = [
    {
        path: 'text-issue-h-code-new',
        component: TextIssueHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.textIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'text-issue-h-code/:id/edit',
        component: TextIssueHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.textIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'text-issue-h-code/:id/delete',
        component: TextIssueHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.textIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
