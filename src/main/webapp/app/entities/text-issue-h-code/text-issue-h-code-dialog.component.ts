import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TextIssueHCode } from './text-issue-h-code.model';
import { TextIssueHCodePopupService } from './text-issue-h-code-popup.service';
import { TextIssueHCodeService } from './text-issue-h-code.service';
import { CourseIssueHCode, CourseIssueHCodeService } from '../course-issue-h-code';

@Component({
    selector: 'jhi-text-issue-h-code-dialog',
    templateUrl: './text-issue-h-code-dialog.component.html'
})
export class TextIssueHCodeDialogComponent implements OnInit {

    textIssue: TextIssueHCode;
    isSaving: boolean;

    courseissues: CourseIssueHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private textIssueService: TextIssueHCodeService,
        private courseIssueService: CourseIssueHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.courseIssueService.query()
            .subscribe((res: HttpResponse<CourseIssueHCode[]>) => { this.courseissues = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.textIssue.id !== undefined) {
            this.subscribeToSaveResponse(
                this.textIssueService.update(this.textIssue));
        } else {
            this.subscribeToSaveResponse(
                this.textIssueService.create(this.textIssue));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TextIssueHCode>>) {
        result.subscribe((res: HttpResponse<TextIssueHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TextIssueHCode) {
        this.eventManager.broadcast({ name: 'textIssueListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCourseIssueById(index: number, item: CourseIssueHCode) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-text-issue-h-code-popup',
    template: ''
})
export class TextIssueHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private textIssuePopupService: TextIssueHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.textIssuePopupService
                    .open(TextIssueHCodeDialogComponent as Component, params['id']);
            } else {
                this.textIssuePopupService
                    .open(TextIssueHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
