import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TextIssueHCode } from './text-issue-h-code.model';
import { TextIssueHCodeService } from './text-issue-h-code.service';

@Component({
    selector: 'jhi-text-issue-h-code-detail',
    templateUrl: './text-issue-h-code-detail.component.html'
})
export class TextIssueHCodeDetailComponent implements OnInit, OnDestroy {

    textIssue: TextIssueHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private textIssueService: TextIssueHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTextIssues();
    }

    load(id) {
        this.textIssueService.find(id)
            .subscribe((textIssueResponse: HttpResponse<TextIssueHCode>) => {
                this.textIssue = textIssueResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTextIssues() {
        this.eventSubscriber = this.eventManager.subscribe(
            'textIssueListModification',
            (response) => this.load(this.textIssue.id)
        );
    }
}
