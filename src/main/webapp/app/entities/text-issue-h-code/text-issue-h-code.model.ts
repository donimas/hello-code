import { BaseEntity } from './../../shared';

export class TextIssueHCode implements BaseEntity {
    constructor(
        public id?: number,
        public bodyRu?: string,
        public bodyKk?: string,
        public bodyEn?: string,
        public noteRu?: string,
        public noteKk?: string,
        public noteEn?: string,
        public flagDeleted?: boolean,
        public issue?: BaseEntity,
    ) {
        this.flagDeleted = false;
    }
}
