import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TextIssueHCode } from './text-issue-h-code.model';
import { TextIssueHCodePopupService } from './text-issue-h-code-popup.service';
import { TextIssueHCodeService } from './text-issue-h-code.service';

@Component({
    selector: 'jhi-text-issue-h-code-delete-dialog',
    templateUrl: './text-issue-h-code-delete-dialog.component.html'
})
export class TextIssueHCodeDeleteDialogComponent {

    textIssue: TextIssueHCode;

    constructor(
        private textIssueService: TextIssueHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.textIssueService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'textIssueListModification',
                content: 'Deleted an textIssue'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-text-issue-h-code-delete-popup',
    template: ''
})
export class TextIssueHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private textIssuePopupService: TextIssueHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.textIssuePopupService
                .open(TextIssueHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
