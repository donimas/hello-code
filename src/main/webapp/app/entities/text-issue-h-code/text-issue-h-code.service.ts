import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { TextIssueHCode } from './text-issue-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TextIssueHCode>;

@Injectable()
export class TextIssueHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/text-issues';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/text-issues';

    constructor(private http: HttpClient) { }

    create(textIssue: TextIssueHCode): Observable<EntityResponseType> {
        const copy = this.convert(textIssue);
        return this.http.post<TextIssueHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(textIssue: TextIssueHCode): Observable<EntityResponseType> {
        const copy = this.convert(textIssue);
        return this.http.put<TextIssueHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<TextIssueHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TextIssueHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<TextIssueHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TextIssueHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<TextIssueHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<TextIssueHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TextIssueHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TextIssueHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TextIssueHCode[]>): HttpResponse<TextIssueHCode[]> {
        const jsonResponse: TextIssueHCode[] = res.body;
        const body: TextIssueHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TextIssueHCode.
     */
    private convertItemFromServer(textIssue: TextIssueHCode): TextIssueHCode {
        const copy: TextIssueHCode = Object.assign({}, textIssue);
        return copy;
    }

    /**
     * Convert a TextIssueHCode to a JSON which can be sent to the server.
     */
    private convert(textIssue: TextIssueHCode): TextIssueHCode {
        const copy: TextIssueHCode = Object.assign({}, textIssue);
        return copy;
    }
}
