import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    TextIssueHCodeService,
    TextIssueHCodePopupService,
    TextIssueHCodeComponent,
    TextIssueHCodeDetailComponent,
    TextIssueHCodeDialogComponent,
    TextIssueHCodePopupComponent,
    TextIssueHCodeDeletePopupComponent,
    TextIssueHCodeDeleteDialogComponent,
    textIssueRoute,
    textIssuePopupRoute,
    TextIssueHCodeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...textIssueRoute,
    ...textIssuePopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TextIssueHCodeComponent,
        TextIssueHCodeDetailComponent,
        TextIssueHCodeDialogComponent,
        TextIssueHCodeDeleteDialogComponent,
        TextIssueHCodePopupComponent,
        TextIssueHCodeDeletePopupComponent,
    ],
    entryComponents: [
        TextIssueHCodeComponent,
        TextIssueHCodeDialogComponent,
        TextIssueHCodePopupComponent,
        TextIssueHCodeDeleteDialogComponent,
        TextIssueHCodeDeletePopupComponent,
    ],
    providers: [
        TextIssueHCodeService,
        TextIssueHCodePopupService,
        TextIssueHCodeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeTextIssueHCodeModule {}
