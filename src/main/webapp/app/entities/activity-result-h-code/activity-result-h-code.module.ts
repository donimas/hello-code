import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    ActivityResultHCodeService,
    ActivityResultHCodePopupService,
    ActivityResultHCodeComponent,
    ActivityResultHCodeDetailComponent,
    ActivityResultHCodeDialogComponent,
    ActivityResultHCodePopupComponent,
    ActivityResultHCodeDeletePopupComponent,
    ActivityResultHCodeDeleteDialogComponent,
    activityResultRoute,
    activityResultPopupRoute,
} from './';

const ENTITY_STATES = [
    ...activityResultRoute,
    ...activityResultPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ActivityResultHCodeComponent,
        ActivityResultHCodeDetailComponent,
        ActivityResultHCodeDialogComponent,
        ActivityResultHCodeDeleteDialogComponent,
        ActivityResultHCodePopupComponent,
        ActivityResultHCodeDeletePopupComponent,
    ],
    entryComponents: [
        ActivityResultHCodeComponent,
        ActivityResultHCodeDialogComponent,
        ActivityResultHCodePopupComponent,
        ActivityResultHCodeDeleteDialogComponent,
        ActivityResultHCodeDeletePopupComponent,
    ],
    providers: [
        ActivityResultHCodeService,
        ActivityResultHCodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeActivityResultHCodeModule {}
