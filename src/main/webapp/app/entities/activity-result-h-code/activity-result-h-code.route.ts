import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ActivityResultHCodeComponent } from './activity-result-h-code.component';
import { ActivityResultHCodeDetailComponent } from './activity-result-h-code-detail.component';
import { ActivityResultHCodePopupComponent } from './activity-result-h-code-dialog.component';
import { ActivityResultHCodeDeletePopupComponent } from './activity-result-h-code-delete-dialog.component';

export const activityResultRoute: Routes = [
    {
        path: 'activity-result-h-code',
        component: ActivityResultHCodeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.activityResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'activity-result-h-code/:id',
        component: ActivityResultHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.activityResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const activityResultPopupRoute: Routes = [
    {
        path: 'activity-result-h-code-new',
        component: ActivityResultHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.activityResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'activity-result-h-code/:id/edit',
        component: ActivityResultHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.activityResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'activity-result-h-code/:id/delete',
        component: ActivityResultHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.activityResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
