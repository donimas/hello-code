import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ActivityResultHCode } from './activity-result-h-code.model';
import { ActivityResultHCodeService } from './activity-result-h-code.service';

@Component({
    selector: 'jhi-activity-result-h-code-detail',
    templateUrl: './activity-result-h-code-detail.component.html'
})
export class ActivityResultHCodeDetailComponent implements OnInit, OnDestroy {

    activityResult: ActivityResultHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private activityResultService: ActivityResultHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInActivityResults();
    }

    load(id) {
        this.activityResultService.find(id)
            .subscribe((activityResultResponse: HttpResponse<ActivityResultHCode>) => {
                this.activityResult = activityResultResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInActivityResults() {
        this.eventSubscriber = this.eventManager.subscribe(
            'activityResultListModification',
            (response) => this.load(this.activityResult.id)
        );
    }
}
