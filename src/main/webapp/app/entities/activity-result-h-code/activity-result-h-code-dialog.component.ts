import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ActivityResultHCode } from './activity-result-h-code.model';
import { ActivityResultHCodePopupService } from './activity-result-h-code-popup.service';
import { ActivityResultHCodeService } from './activity-result-h-code.service';

@Component({
    selector: 'jhi-activity-result-h-code-dialog',
    templateUrl: './activity-result-h-code-dialog.component.html'
})
export class ActivityResultHCodeDialogComponent implements OnInit {

    activityResult: ActivityResultHCode;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private activityResultService: ActivityResultHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.activityResult.id !== undefined) {
            this.subscribeToSaveResponse(
                this.activityResultService.update(this.activityResult));
        } else {
            this.subscribeToSaveResponse(
                this.activityResultService.create(this.activityResult));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ActivityResultHCode>>) {
        result.subscribe((res: HttpResponse<ActivityResultHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ActivityResultHCode) {
        this.eventManager.broadcast({ name: 'activityResultListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-activity-result-h-code-popup',
    template: ''
})
export class ActivityResultHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private activityResultPopupService: ActivityResultHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.activityResultPopupService
                    .open(ActivityResultHCodeDialogComponent as Component, params['id']);
            } else {
                this.activityResultPopupService
                    .open(ActivityResultHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
