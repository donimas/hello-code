import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ActivityResultHCode } from './activity-result-h-code.model';
import { ActivityResultHCodePopupService } from './activity-result-h-code-popup.service';
import { ActivityResultHCodeService } from './activity-result-h-code.service';

@Component({
    selector: 'jhi-activity-result-h-code-delete-dialog',
    templateUrl: './activity-result-h-code-delete-dialog.component.html'
})
export class ActivityResultHCodeDeleteDialogComponent {

    activityResult: ActivityResultHCode;

    constructor(
        private activityResultService: ActivityResultHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.activityResultService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'activityResultListModification',
                content: 'Deleted an activityResult'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-activity-result-h-code-delete-popup',
    template: ''
})
export class ActivityResultHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private activityResultPopupService: ActivityResultHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.activityResultPopupService
                .open(ActivityResultHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
