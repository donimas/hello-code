import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ActivityResultHCode } from './activity-result-h-code.model';
import { ActivityResultHCodeService } from './activity-result-h-code.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-activity-result-h-code',
    templateUrl: './activity-result-h-code.component.html'
})
export class ActivityResultHCodeComponent implements OnInit, OnDestroy {
activityResults: ActivityResultHCode[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private activityResultService: ActivityResultHCodeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.activityResultService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<ActivityResultHCode[]>) => this.activityResults = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.activityResultService.query().subscribe(
            (res: HttpResponse<ActivityResultHCode[]>) => {
                this.activityResults = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInActivityResults();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ActivityResultHCode) {
        return item.id;
    }
    registerChangeInActivityResults() {
        this.eventSubscriber = this.eventManager.subscribe('activityResultListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
