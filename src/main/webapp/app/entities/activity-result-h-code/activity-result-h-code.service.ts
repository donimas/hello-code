import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ActivityResultHCode } from './activity-result-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ActivityResultHCode>;

@Injectable()
export class ActivityResultHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/activity-results';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/activity-results';

    constructor(private http: HttpClient) { }

    create(activityResult: ActivityResultHCode): Observable<EntityResponseType> {
        const copy = this.convert(activityResult);
        return this.http.post<ActivityResultHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(activityResult: ActivityResultHCode): Observable<EntityResponseType> {
        const copy = this.convert(activityResult);
        return this.http.put<ActivityResultHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ActivityResultHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ActivityResultHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<ActivityResultHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ActivityResultHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ActivityResultHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<ActivityResultHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ActivityResultHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ActivityResultHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ActivityResultHCode[]>): HttpResponse<ActivityResultHCode[]> {
        const jsonResponse: ActivityResultHCode[] = res.body;
        const body: ActivityResultHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ActivityResultHCode.
     */
    private convertItemFromServer(activityResult: ActivityResultHCode): ActivityResultHCode {
        const copy: ActivityResultHCode = Object.assign({}, activityResult);
        return copy;
    }

    /**
     * Convert a ActivityResultHCode to a JSON which can be sent to the server.
     */
    private convert(activityResult: ActivityResultHCode): ActivityResultHCode {
        const copy: ActivityResultHCode = Object.assign({}, activityResult);
        return copy;
    }
}
