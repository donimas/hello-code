import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TestResultHCode } from './test-result-h-code.model';
import { TestResultHCodePopupService } from './test-result-h-code-popup.service';
import { TestResultHCodeService } from './test-result-h-code.service';

@Component({
    selector: 'jhi-test-result-h-code-delete-dialog',
    templateUrl: './test-result-h-code-delete-dialog.component.html'
})
export class TestResultHCodeDeleteDialogComponent {

    testResult: TestResultHCode;

    constructor(
        private testResultService: TestResultHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.testResultService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'testResultListModification',
                content: 'Deleted an testResult'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-test-result-h-code-delete-popup',
    template: ''
})
export class TestResultHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private testResultPopupService: TestResultHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.testResultPopupService
                .open(TestResultHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
