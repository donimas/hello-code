import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { TestResultHCode } from './test-result-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TestResultHCode>;

@Injectable()
export class TestResultHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/test-results';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/test-results';

    constructor(private http: HttpClient) { }

    create(testResult: TestResultHCode): Observable<EntityResponseType> {
        const copy = this.convert(testResult);
        return this.http.post<TestResultHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(testResult: TestResultHCode): Observable<EntityResponseType> {
        const copy = this.convert(testResult);
        return this.http.put<TestResultHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<TestResultHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TestResultHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<TestResultHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TestResultHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<TestResultHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<TestResultHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TestResultHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TestResultHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TestResultHCode[]>): HttpResponse<TestResultHCode[]> {
        const jsonResponse: TestResultHCode[] = res.body;
        const body: TestResultHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TestResultHCode.
     */
    private convertItemFromServer(testResult: TestResultHCode): TestResultHCode {
        const copy: TestResultHCode = Object.assign({}, testResult);
        return copy;
    }

    /**
     * Convert a TestResultHCode to a JSON which can be sent to the server.
     */
    private convert(testResult: TestResultHCode): TestResultHCode {
        const copy: TestResultHCode = Object.assign({}, testResult);
        return copy;
    }
}
