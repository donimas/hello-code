import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TestResultHCodeComponent } from './test-result-h-code.component';
import { TestResultHCodeDetailComponent } from './test-result-h-code-detail.component';
import { TestResultHCodePopupComponent } from './test-result-h-code-dialog.component';
import { TestResultHCodeDeletePopupComponent } from './test-result-h-code-delete-dialog.component';

export const testResultRoute: Routes = [
    {
        path: 'test-result-h-code',
        component: TestResultHCodeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'test-result-h-code/:id',
        component: TestResultHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const testResultPopupRoute: Routes = [
    {
        path: 'test-result-h-code-new',
        component: TestResultHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'test-result-h-code/:id/edit',
        component: TestResultHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'test-result-h-code/:id/delete',
        component: TestResultHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.testResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
