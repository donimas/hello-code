import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import { HelloCodeAdminModule } from '../../admin/admin.module';
import {
    TestResultHCodeService,
    TestResultHCodePopupService,
    TestResultHCodeComponent,
    TestResultHCodeDetailComponent,
    TestResultHCodeDialogComponent,
    TestResultHCodePopupComponent,
    TestResultHCodeDeletePopupComponent,
    TestResultHCodeDeleteDialogComponent,
    testResultRoute,
    testResultPopupRoute,
} from './';

const ENTITY_STATES = [
    ...testResultRoute,
    ...testResultPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        HelloCodeAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TestResultHCodeComponent,
        TestResultHCodeDetailComponent,
        TestResultHCodeDialogComponent,
        TestResultHCodeDeleteDialogComponent,
        TestResultHCodePopupComponent,
        TestResultHCodeDeletePopupComponent,
    ],
    entryComponents: [
        TestResultHCodeComponent,
        TestResultHCodeDialogComponent,
        TestResultHCodePopupComponent,
        TestResultHCodeDeleteDialogComponent,
        TestResultHCodeDeletePopupComponent,
    ],
    providers: [
        TestResultHCodeService,
        TestResultHCodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeTestResultHCodeModule {}
