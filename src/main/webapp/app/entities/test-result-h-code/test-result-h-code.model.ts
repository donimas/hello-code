import { BaseEntity, User } from './../../shared';

export class TestResultHCode implements BaseEntity {
    constructor(
        public id?: number,
        public flagCorrect?: boolean,
        public question?: BaseEntity,
        public response?: BaseEntity,
        public user?: User,
    ) {
        this.flagCorrect = false;
    }
}
