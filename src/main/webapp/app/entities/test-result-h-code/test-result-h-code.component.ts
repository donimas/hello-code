import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TestResultHCode } from './test-result-h-code.model';
import { TestResultHCodeService } from './test-result-h-code.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-test-result-h-code',
    templateUrl: './test-result-h-code.component.html'
})
export class TestResultHCodeComponent implements OnInit, OnDestroy {
testResults: TestResultHCode[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private testResultService: TestResultHCodeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.testResultService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<TestResultHCode[]>) => this.testResults = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.testResultService.query().subscribe(
            (res: HttpResponse<TestResultHCode[]>) => {
                this.testResults = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTestResults();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TestResultHCode) {
        return item.id;
    }
    registerChangeInTestResults() {
        this.eventSubscriber = this.eventManager.subscribe('testResultListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
