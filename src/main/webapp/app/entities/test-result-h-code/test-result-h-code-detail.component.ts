import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TestResultHCode } from './test-result-h-code.model';
import { TestResultHCodeService } from './test-result-h-code.service';

@Component({
    selector: 'jhi-test-result-h-code-detail',
    templateUrl: './test-result-h-code-detail.component.html'
})
export class TestResultHCodeDetailComponent implements OnInit, OnDestroy {

    testResult: TestResultHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private testResultService: TestResultHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTestResults();
    }

    load(id) {
        this.testResultService.find(id)
            .subscribe((testResultResponse: HttpResponse<TestResultHCode>) => {
                this.testResult = testResultResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTestResults() {
        this.eventSubscriber = this.eventManager.subscribe(
            'testResultListModification',
            (response) => this.load(this.testResult.id)
        );
    }
}
