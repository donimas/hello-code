import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TestResultHCode } from './test-result-h-code.model';
import { TestResultHCodePopupService } from './test-result-h-code-popup.service';
import { TestResultHCodeService } from './test-result-h-code.service';
import { TestQuestionHCode, TestQuestionHCodeService } from '../test-question-h-code';
import { TestVariantHCode, TestVariantHCodeService } from '../test-variant-h-code';
import { User, UserService } from '../../shared';

@Component({
    selector: 'jhi-test-result-h-code-dialog',
    templateUrl: './test-result-h-code-dialog.component.html'
})
export class TestResultHCodeDialogComponent implements OnInit {

    testResult: TestResultHCode;
    isSaving: boolean;

    testquestions: TestQuestionHCode[];

    testvariants: TestVariantHCode[];

    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private testResultService: TestResultHCodeService,
        private testQuestionService: TestQuestionHCodeService,
        private testVariantService: TestVariantHCodeService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.testQuestionService.query()
            .subscribe((res: HttpResponse<TestQuestionHCode[]>) => { this.testquestions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.testVariantService.query()
            .subscribe((res: HttpResponse<TestVariantHCode[]>) => { this.testvariants = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.testResult.id !== undefined) {
            this.subscribeToSaveResponse(
                this.testResultService.update(this.testResult));
        } else {
            this.subscribeToSaveResponse(
                this.testResultService.create(this.testResult));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TestResultHCode>>) {
        result.subscribe((res: HttpResponse<TestResultHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TestResultHCode) {
        this.eventManager.broadcast({ name: 'testResultListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackTestQuestionById(index: number, item: TestQuestionHCode) {
        return item.id;
    }

    trackTestVariantById(index: number, item: TestVariantHCode) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-test-result-h-code-popup',
    template: ''
})
export class TestResultHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private testResultPopupService: TestResultHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.testResultPopupService
                    .open(TestResultHCodeDialogComponent as Component, params['id']);
            } else {
                this.testResultPopupService
                    .open(TestResultHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
