import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { LearnerGroupHCode } from './learner-group-h-code.model';
import { LearnerGroupHCodeService } from './learner-group-h-code.service';

@Component({
    selector: 'jhi-learner-group-h-code-detail',
    templateUrl: './learner-group-h-code-detail.component.html'
})
export class LearnerGroupHCodeDetailComponent implements OnInit, OnDestroy {

    learnerGroup: LearnerGroupHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private learnerGroupService: LearnerGroupHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLearnerGroups();
    }

    load(id) {
        this.learnerGroupService.find(id)
            .subscribe((learnerGroupResponse: HttpResponse<LearnerGroupHCode>) => {
                this.learnerGroup = learnerGroupResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLearnerGroups() {
        this.eventSubscriber = this.eventManager.subscribe(
            'learnerGroupListModification',
            (response) => this.load(this.learnerGroup.id)
        );
    }
}
