import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import { HelloCodeAdminModule } from '../../admin/admin.module';
import {
    LearnerGroupHCodeService,
    LearnerGroupHCodePopupService,
    LearnerGroupHCodeComponent,
    LearnerGroupHCodeDetailComponent,
    LearnerGroupHCodeDialogComponent,
    LearnerGroupHCodePopupComponent,
    LearnerGroupHCodeDeletePopupComponent,
    LearnerGroupHCodeDeleteDialogComponent,
    learnerGroupRoute,
    learnerGroupPopupRoute,
    LearnerGroupHCodeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...learnerGroupRoute,
    ...learnerGroupPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        HelloCodeAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        LearnerGroupHCodeComponent,
        LearnerGroupHCodeDetailComponent,
        LearnerGroupHCodeDialogComponent,
        LearnerGroupHCodeDeleteDialogComponent,
        LearnerGroupHCodePopupComponent,
        LearnerGroupHCodeDeletePopupComponent,
    ],
    entryComponents: [
        LearnerGroupHCodeComponent,
        LearnerGroupHCodeDialogComponent,
        LearnerGroupHCodePopupComponent,
        LearnerGroupHCodeDeleteDialogComponent,
        LearnerGroupHCodeDeletePopupComponent,
    ],
    providers: [
        LearnerGroupHCodeService,
        LearnerGroupHCodePopupService,
        LearnerGroupHCodeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeLearnerGroupHCodeModule {}
