import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LearnerGroupHCode } from './learner-group-h-code.model';
import { LearnerGroupHCodePopupService } from './learner-group-h-code-popup.service';
import { LearnerGroupHCodeService } from './learner-group-h-code.service';
import { User, UserService } from '../../shared';
import { SchoolHCode, SchoolHCodeService } from '../school-h-code';
import { GradeHCode, GradeHCodeService } from '../grade-h-code';

@Component({
    selector: 'jhi-learner-group-h-code-dialog',
    templateUrl: './learner-group-h-code-dialog.component.html'
})
export class LearnerGroupHCodeDialogComponent implements OnInit {

    learnerGroup: LearnerGroupHCode;
    isSaving: boolean;

    users: User[];

    schools: SchoolHCode[];

    grades: GradeHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private learnerGroupService: LearnerGroupHCodeService,
        private userService: UserService,
        private schoolService: SchoolHCodeService,
        private gradeService: GradeHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.schoolService.query()
            .subscribe((res: HttpResponse<SchoolHCode[]>) => { this.schools = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.gradeService.query()
            .subscribe((res: HttpResponse<GradeHCode[]>) => { this.grades = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.learnerGroup.id !== undefined) {
            this.subscribeToSaveResponse(
                this.learnerGroupService.update(this.learnerGroup));
        } else {
            this.subscribeToSaveResponse(
                this.learnerGroupService.create(this.learnerGroup));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<LearnerGroupHCode>>) {
        result.subscribe((res: HttpResponse<LearnerGroupHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: LearnerGroupHCode) {
        this.eventManager.broadcast({ name: 'learnerGroupListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackSchoolById(index: number, item: SchoolHCode) {
        return item.id;
    }

    trackGradeById(index: number, item: GradeHCode) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-learner-group-h-code-popup',
    template: ''
})
export class LearnerGroupHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private learnerGroupPopupService: LearnerGroupHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.learnerGroupPopupService
                    .open(LearnerGroupHCodeDialogComponent as Component, params['id']);
            } else {
                this.learnerGroupPopupService
                    .open(LearnerGroupHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
