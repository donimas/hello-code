import { BaseEntity, User } from './../../shared';

export class LearnerGroupHCode implements BaseEntity {
    constructor(
        public id?: number,
        public nameRu?: string,
        public nameKk?: string,
        public nameEn?: string,
        public descriptionRu?: string,
        public descriptionKk?: string,
        public desciprtionEn?: string,
        public graduationYear?: number,
        public teacher?: User,
        public school?: BaseEntity,
        public grade?: BaseEntity,
        public learners?: User[],
    ) {
    }
}
