import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LearnerGroupHCode } from './learner-group-h-code.model';
import { LearnerGroupHCodePopupService } from './learner-group-h-code-popup.service';
import { LearnerGroupHCodeService } from './learner-group-h-code.service';

@Component({
    selector: 'jhi-learner-group-h-code-delete-dialog',
    templateUrl: './learner-group-h-code-delete-dialog.component.html'
})
export class LearnerGroupHCodeDeleteDialogComponent {

    learnerGroup: LearnerGroupHCode;

    constructor(
        private learnerGroupService: LearnerGroupHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.learnerGroupService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'learnerGroupListModification',
                content: 'Deleted an learnerGroup'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-learner-group-h-code-delete-popup',
    template: ''
})
export class LearnerGroupHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private learnerGroupPopupService: LearnerGroupHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.learnerGroupPopupService
                .open(LearnerGroupHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
