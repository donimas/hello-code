import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { LearnerGroupHCode } from './learner-group-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<LearnerGroupHCode>;

@Injectable()
export class LearnerGroupHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/learner-groups';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/learner-groups';

    constructor(private http: HttpClient) { }

    create(learnerGroup: LearnerGroupHCode): Observable<EntityResponseType> {
        const copy = this.convert(learnerGroup);
        return this.http.post<LearnerGroupHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(learnerGroup: LearnerGroupHCode): Observable<EntityResponseType> {
        const copy = this.convert(learnerGroup);
        return this.http.put<LearnerGroupHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<LearnerGroupHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<LearnerGroupHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<LearnerGroupHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<LearnerGroupHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<LearnerGroupHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<LearnerGroupHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<LearnerGroupHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: LearnerGroupHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<LearnerGroupHCode[]>): HttpResponse<LearnerGroupHCode[]> {
        const jsonResponse: LearnerGroupHCode[] = res.body;
        const body: LearnerGroupHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to LearnerGroupHCode.
     */
    private convertItemFromServer(learnerGroup: LearnerGroupHCode): LearnerGroupHCode {
        const copy: LearnerGroupHCode = Object.assign({}, learnerGroup);
        return copy;
    }

    /**
     * Convert a LearnerGroupHCode to a JSON which can be sent to the server.
     */
    private convert(learnerGroup: LearnerGroupHCode): LearnerGroupHCode {
        const copy: LearnerGroupHCode = Object.assign({}, learnerGroup);
        return copy;
    }
}
