import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { LearnerGroupHCodeComponent } from './learner-group-h-code.component';
import { LearnerGroupHCodeDetailComponent } from './learner-group-h-code-detail.component';
import { LearnerGroupHCodePopupComponent } from './learner-group-h-code-dialog.component';
import { LearnerGroupHCodeDeletePopupComponent } from './learner-group-h-code-delete-dialog.component';

@Injectable()
export class LearnerGroupHCodeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const learnerGroupRoute: Routes = [
    {
        path: 'learner-group-h-code',
        component: LearnerGroupHCodeComponent,
        resolve: {
            'pagingParams': LearnerGroupHCodeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.learnerGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'learner-group-h-code/:id',
        component: LearnerGroupHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.learnerGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const learnerGroupPopupRoute: Routes = [
    {
        path: 'learner-group-h-code-new',
        component: LearnerGroupHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.learnerGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'learner-group-h-code/:id/edit',
        component: LearnerGroupHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.learnerGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'learner-group-h-code/:id/delete',
        component: LearnerGroupHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.learnerGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
