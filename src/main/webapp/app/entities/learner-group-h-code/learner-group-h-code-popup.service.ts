import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { LearnerGroupHCode } from './learner-group-h-code.model';
import { LearnerGroupHCodeService } from './learner-group-h-code.service';

@Injectable()
export class LearnerGroupHCodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private learnerGroupService: LearnerGroupHCodeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.learnerGroupService.find(id)
                    .subscribe((learnerGroupResponse: HttpResponse<LearnerGroupHCode>) => {
                        const learnerGroup: LearnerGroupHCode = learnerGroupResponse.body;
                        this.ngbModalRef = this.learnerGroupModalRef(component, learnerGroup);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.learnerGroupModalRef(component, new LearnerGroupHCode());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    learnerGroupModalRef(component: Component, learnerGroup: LearnerGroupHCode): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.learnerGroup = learnerGroup;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
