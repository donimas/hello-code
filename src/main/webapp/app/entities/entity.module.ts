import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { HelloCodeUserInfoHCodeModule } from './user-info-h-code/user-info-h-code.module';
import { HelloCodeSchoolHCodeModule } from './school-h-code/school-h-code.module';
import { HelloCodeLearnerGroupHCodeModule } from './learner-group-h-code/learner-group-h-code.module';
import { HelloCodeGradeHCodeModule } from './grade-h-code/grade-h-code.module';
import { HelloCodeCategoryHCodeModule } from './category-h-code/category-h-code.module';
import { HelloCodeProgramHCodeModule } from './program-h-code/program-h-code.module';
import { HelloCodeCourseHCodeModule } from './course-h-code/course-h-code.module';
import { HelloCodeCourseChapterHCodeModule } from './course-chapter-h-code/course-chapter-h-code.module';
import { HelloCodeCourseIssueHCodeModule } from './course-issue-h-code/course-issue-h-code.module';
import { HelloCodeLearnerActivityHCodeModule } from './learner-activity-h-code/learner-activity-h-code.module';
import { HelloCodeActivityStatusHistoryHCodeModule } from './activity-status-history-h-code/activity-status-history-h-code.module';
import { HelloCodeActivityResultHCodeModule } from './activity-result-h-code/activity-result-h-code.module';
import { HelloCodeTestQuestionHCodeModule } from './test-question-h-code/test-question-h-code.module';
import { HelloCodeTestVariantHCodeModule } from './test-variant-h-code/test-variant-h-code.module';
import { HelloCodeTestResultHCodeModule } from './test-result-h-code/test-result-h-code.module';
import { HelloCodeTextIssueHCodeModule } from './text-issue-h-code/text-issue-h-code.module';
import { HelloCodeSkratchIssueHCodeModule } from './skratch-issue-h-code/skratch-issue-h-code.module';
import { HelloCodeWebinarIssueHCodeModule } from './webinar-issue-h-code/webinar-issue-h-code.module';
import { HelloCodeVideoIssueHCodeModule } from './video-issue-h-code/video-issue-h-code.module';
import { HelloCodeCourseProgressHCodeModule } from './course-progress-h-code/course-progress-h-code.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        HelloCodeUserInfoHCodeModule,
        HelloCodeSchoolHCodeModule,
        HelloCodeLearnerGroupHCodeModule,
        HelloCodeGradeHCodeModule,
        HelloCodeCategoryHCodeModule,
        HelloCodeProgramHCodeModule,
        HelloCodeCourseHCodeModule,
        HelloCodeCourseChapterHCodeModule,
        HelloCodeCourseIssueHCodeModule,
        HelloCodeLearnerActivityHCodeModule,
        HelloCodeActivityStatusHistoryHCodeModule,
        HelloCodeActivityResultHCodeModule,
        HelloCodeTestQuestionHCodeModule,
        HelloCodeTestVariantHCodeModule,
        HelloCodeTestResultHCodeModule,
        HelloCodeTextIssueHCodeModule,
        HelloCodeSkratchIssueHCodeModule,
        HelloCodeWebinarIssueHCodeModule,
        HelloCodeVideoIssueHCodeModule,
        HelloCodeCourseProgressHCodeModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeEntityModule {}
