import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { CourseProgressHCodeComponent } from './course-progress-h-code.component';
import { CourseProgressHCodeDetailComponent } from './course-progress-h-code-detail.component';
import { CourseProgressHCodePopupComponent } from './course-progress-h-code-dialog.component';
import { CourseProgressHCodeDeletePopupComponent } from './course-progress-h-code-delete-dialog.component';

export const courseProgressRoute: Routes = [
    {
        path: 'course-progress-h-code',
        component: CourseProgressHCodeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseProgress.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'course-progress-h-code/:id',
        component: CourseProgressHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseProgress.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const courseProgressPopupRoute: Routes = [
    {
        path: 'course-progress-h-code-new',
        component: CourseProgressHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseProgress.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'course-progress-h-code/:id/edit',
        component: CourseProgressHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseProgress.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'course-progress-h-code/:id/delete',
        component: CourseProgressHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.courseProgress.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
