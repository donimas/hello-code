import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CourseProgressHCode } from './course-progress-h-code.model';
import { CourseProgressHCodePopupService } from './course-progress-h-code-popup.service';
import { CourseProgressHCodeService } from './course-progress-h-code.service';
import { CourseHCode, CourseHCodeService } from '../course-h-code';
import { User, UserService } from '../../shared';
import { CourseIssueHCode, CourseIssueHCodeService } from '../course-issue-h-code';

@Component({
    selector: 'jhi-course-progress-h-code-dialog',
    templateUrl: './course-progress-h-code-dialog.component.html'
})
export class CourseProgressHCodeDialogComponent implements OnInit {

    courseProgress: CourseProgressHCode;
    isSaving: boolean;

    courses: CourseHCode[];

    users: User[];

    courseissues: CourseIssueHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private courseProgressService: CourseProgressHCodeService,
        private courseService: CourseHCodeService,
        private userService: UserService,
        private courseIssueService: CourseIssueHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.courseService.query()
            .subscribe((res: HttpResponse<CourseHCode[]>) => { this.courses = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.courseIssueService.query()
            .subscribe((res: HttpResponse<CourseIssueHCode[]>) => { this.courseissues = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.courseProgress.id !== undefined) {
            this.subscribeToSaveResponse(
                this.courseProgressService.update(this.courseProgress));
        } else {
            this.subscribeToSaveResponse(
                this.courseProgressService.create(this.courseProgress));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CourseProgressHCode>>) {
        result.subscribe((res: HttpResponse<CourseProgressHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CourseProgressHCode) {
        this.eventManager.broadcast({ name: 'courseProgressListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCourseById(index: number, item: CourseHCode) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackCourseIssueById(index: number, item: CourseIssueHCode) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-course-progress-h-code-popup',
    template: ''
})
export class CourseProgressHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private courseProgressPopupService: CourseProgressHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.courseProgressPopupService
                    .open(CourseProgressHCodeDialogComponent as Component, params['id']);
            } else {
                this.courseProgressPopupService
                    .open(CourseProgressHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
