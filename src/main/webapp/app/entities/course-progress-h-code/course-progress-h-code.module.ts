import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import { HelloCodeAdminModule } from '../../admin/admin.module';
import {
    CourseProgressHCodeService,
    CourseProgressHCodePopupService,
    CourseProgressHCodeComponent,
    CourseProgressHCodeDetailComponent,
    CourseProgressHCodeDialogComponent,
    CourseProgressHCodePopupComponent,
    CourseProgressHCodeDeletePopupComponent,
    CourseProgressHCodeDeleteDialogComponent,
    courseProgressRoute,
    courseProgressPopupRoute,
} from './';

const ENTITY_STATES = [
    ...courseProgressRoute,
    ...courseProgressPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        HelloCodeAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CourseProgressHCodeComponent,
        CourseProgressHCodeDetailComponent,
        CourseProgressHCodeDialogComponent,
        CourseProgressHCodeDeleteDialogComponent,
        CourseProgressHCodePopupComponent,
        CourseProgressHCodeDeletePopupComponent,
    ],
    entryComponents: [
        CourseProgressHCodeComponent,
        CourseProgressHCodeDialogComponent,
        CourseProgressHCodePopupComponent,
        CourseProgressHCodeDeleteDialogComponent,
        CourseProgressHCodeDeletePopupComponent,
    ],
    providers: [
        CourseProgressHCodeService,
        CourseProgressHCodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeCourseProgressHCodeModule {}
