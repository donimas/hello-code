import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { CourseProgressHCode } from './course-progress-h-code.model';
import { CourseProgressHCodeService } from './course-progress-h-code.service';

@Injectable()
export class CourseProgressHCodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private courseProgressService: CourseProgressHCodeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.courseProgressService.find(id)
                    .subscribe((courseProgressResponse: HttpResponse<CourseProgressHCode>) => {
                        const courseProgress: CourseProgressHCode = courseProgressResponse.body;
                        courseProgress.lastActivityDate = this.datePipe
                            .transform(courseProgress.lastActivityDate, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.courseProgressModalRef(component, courseProgress);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.courseProgressModalRef(component, new CourseProgressHCode());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    courseProgressModalRef(component: Component, courseProgress: CourseProgressHCode): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.courseProgress = courseProgress;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
