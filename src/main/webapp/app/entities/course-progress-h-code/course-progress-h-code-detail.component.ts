import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { CourseProgressHCode } from './course-progress-h-code.model';
import { CourseProgressHCodeService } from './course-progress-h-code.service';

@Component({
    selector: 'jhi-course-progress-h-code-detail',
    templateUrl: './course-progress-h-code-detail.component.html'
})
export class CourseProgressHCodeDetailComponent implements OnInit, OnDestroy {

    courseProgress: CourseProgressHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private courseProgressService: CourseProgressHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCourseProgresses();
    }

    load(id) {
        this.courseProgressService.find(id)
            .subscribe((courseProgressResponse: HttpResponse<CourseProgressHCode>) => {
                this.courseProgress = courseProgressResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCourseProgresses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'courseProgressListModification',
            (response) => this.load(this.courseProgress.id)
        );
    }
}
