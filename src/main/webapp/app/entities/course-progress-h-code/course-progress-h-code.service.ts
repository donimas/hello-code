import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { CourseProgressHCode } from './course-progress-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<CourseProgressHCode>;

@Injectable()
export class CourseProgressHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/course-progresses';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/course-progresses';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(courseProgress: CourseProgressHCode): Observable<EntityResponseType> {
        const copy = this.convert(courseProgress);
        return this.http.post<CourseProgressHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(courseProgress: CourseProgressHCode): Observable<EntityResponseType> {
        const copy = this.convert(courseProgress);
        return this.http.put<CourseProgressHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CourseProgressHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CourseProgressHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<CourseProgressHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CourseProgressHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<CourseProgressHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<CourseProgressHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CourseProgressHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CourseProgressHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CourseProgressHCode[]>): HttpResponse<CourseProgressHCode[]> {
        const jsonResponse: CourseProgressHCode[] = res.body;
        const body: CourseProgressHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CourseProgressHCode.
     */
    private convertItemFromServer(courseProgress: CourseProgressHCode): CourseProgressHCode {
        const copy: CourseProgressHCode = Object.assign({}, courseProgress);
        copy.lastActivityDate = this.dateUtils
            .convertDateTimeFromServer(courseProgress.lastActivityDate);
        return copy;
    }

    /**
     * Convert a CourseProgressHCode to a JSON which can be sent to the server.
     */
    private convert(courseProgress: CourseProgressHCode): CourseProgressHCode {
        const copy: CourseProgressHCode = Object.assign({}, courseProgress);

        copy.lastActivityDate = this.dateUtils.toDate(courseProgress.lastActivityDate);
        return copy;
    }
}
