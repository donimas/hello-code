import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CourseProgressHCode } from './course-progress-h-code.model';
import { CourseProgressHCodePopupService } from './course-progress-h-code-popup.service';
import { CourseProgressHCodeService } from './course-progress-h-code.service';

@Component({
    selector: 'jhi-course-progress-h-code-delete-dialog',
    templateUrl: './course-progress-h-code-delete-dialog.component.html'
})
export class CourseProgressHCodeDeleteDialogComponent {

    courseProgress: CourseProgressHCode;

    constructor(
        private courseProgressService: CourseProgressHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.courseProgressService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'courseProgressListModification',
                content: 'Deleted an courseProgress'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-course-progress-h-code-delete-popup',
    template: ''
})
export class CourseProgressHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private courseProgressPopupService: CourseProgressHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.courseProgressPopupService
                .open(CourseProgressHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
