import { BaseEntity, User } from './../../shared';

export class CourseProgressHCode implements BaseEntity {
    constructor(
        public id?: number,
        public passedIssues?: number,
        public totalIssues?: number,
        public lastActivityDate?: any,
        public course?: BaseEntity,
        public user?: User,
        public lastIssue?: BaseEntity,
    ) {
    }
}
