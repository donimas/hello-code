import { BaseEntity } from './../../shared';

export class SchoolHCode implements BaseEntity {
    constructor(
        public id?: number,
        public nameRu?: string,
        public nameKk?: string,
        public nameEn?: string,
        public descriptionRu?: string,
        public descriptionKk?: string,
        public descriptionEn?: string,
        public fullAddress?: string,
        public lat?: string,
        public lon?: string,
        public imageContentType?: string,
        public image?: any,
    ) {
    }
}
