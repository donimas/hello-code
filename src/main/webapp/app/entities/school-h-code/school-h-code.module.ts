import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    SchoolHCodeService,
    SchoolHCodePopupService,
    SchoolHCodeComponent,
    SchoolHCodeDetailComponent,
    SchoolHCodeDialogComponent,
    SchoolHCodePopupComponent,
    SchoolHCodeDeletePopupComponent,
    SchoolHCodeDeleteDialogComponent,
    schoolRoute,
    schoolPopupRoute,
} from './';

const ENTITY_STATES = [
    ...schoolRoute,
    ...schoolPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SchoolHCodeComponent,
        SchoolHCodeDetailComponent,
        SchoolHCodeDialogComponent,
        SchoolHCodeDeleteDialogComponent,
        SchoolHCodePopupComponent,
        SchoolHCodeDeletePopupComponent,
    ],
    entryComponents: [
        SchoolHCodeComponent,
        SchoolHCodeDialogComponent,
        SchoolHCodePopupComponent,
        SchoolHCodeDeleteDialogComponent,
        SchoolHCodeDeletePopupComponent,
    ],
    providers: [
        SchoolHCodeService,
        SchoolHCodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeSchoolHCodeModule {}
