import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { SchoolHCodeComponent } from './school-h-code.component';
import { SchoolHCodeDetailComponent } from './school-h-code-detail.component';
import { SchoolHCodePopupComponent } from './school-h-code-dialog.component';
import { SchoolHCodeDeletePopupComponent } from './school-h-code-delete-dialog.component';

export const schoolRoute: Routes = [
    {
        path: 'school-h-code',
        component: SchoolHCodeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.school.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'school-h-code/:id',
        component: SchoolHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.school.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const schoolPopupRoute: Routes = [
    {
        path: 'school-h-code-new',
        component: SchoolHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.school.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'school-h-code/:id/edit',
        component: SchoolHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.school.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'school-h-code/:id/delete',
        component: SchoolHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.school.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
