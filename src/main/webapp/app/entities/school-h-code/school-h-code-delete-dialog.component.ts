import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SchoolHCode } from './school-h-code.model';
import { SchoolHCodePopupService } from './school-h-code-popup.service';
import { SchoolHCodeService } from './school-h-code.service';

@Component({
    selector: 'jhi-school-h-code-delete-dialog',
    templateUrl: './school-h-code-delete-dialog.component.html'
})
export class SchoolHCodeDeleteDialogComponent {

    school: SchoolHCode;

    constructor(
        private schoolService: SchoolHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.schoolService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'schoolListModification',
                content: 'Deleted an school'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-school-h-code-delete-popup',
    template: ''
})
export class SchoolHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private schoolPopupService: SchoolHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.schoolPopupService
                .open(SchoolHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
