import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { SchoolHCode } from './school-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<SchoolHCode>;

@Injectable()
export class SchoolHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/schools';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/schools';

    constructor(private http: HttpClient) { }

    create(school: SchoolHCode): Observable<EntityResponseType> {
        const copy = this.convert(school);
        return this.http.post<SchoolHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(school: SchoolHCode): Observable<EntityResponseType> {
        const copy = this.convert(school);
        return this.http.put<SchoolHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SchoolHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<SchoolHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<SchoolHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SchoolHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<SchoolHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<SchoolHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SchoolHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SchoolHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<SchoolHCode[]>): HttpResponse<SchoolHCode[]> {
        const jsonResponse: SchoolHCode[] = res.body;
        const body: SchoolHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to SchoolHCode.
     */
    private convertItemFromServer(school: SchoolHCode): SchoolHCode {
        const copy: SchoolHCode = Object.assign({}, school);
        return copy;
    }

    /**
     * Convert a SchoolHCode to a JSON which can be sent to the server.
     */
    private convert(school: SchoolHCode): SchoolHCode {
        const copy: SchoolHCode = Object.assign({}, school);
        return copy;
    }
}
