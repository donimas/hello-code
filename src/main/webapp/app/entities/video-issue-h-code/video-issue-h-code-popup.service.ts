import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { VideoIssueHCode } from './video-issue-h-code.model';
import { VideoIssueHCodeService } from './video-issue-h-code.service';

@Injectable()
export class VideoIssueHCodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private videoIssueService: VideoIssueHCodeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.videoIssueService.find(id)
                    .subscribe((videoIssueResponse: HttpResponse<VideoIssueHCode>) => {
                        const videoIssue: VideoIssueHCode = videoIssueResponse.body;
                        this.ngbModalRef = this.videoIssueModalRef(component, videoIssue);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.videoIssueModalRef(component, new VideoIssueHCode());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    videoIssueModalRef(component: Component, videoIssue: VideoIssueHCode): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.videoIssue = videoIssue;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
