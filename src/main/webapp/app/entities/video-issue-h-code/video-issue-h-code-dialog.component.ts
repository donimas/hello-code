import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VideoIssueHCode } from './video-issue-h-code.model';
import { VideoIssueHCodePopupService } from './video-issue-h-code-popup.service';
import { VideoIssueHCodeService } from './video-issue-h-code.service';
import { CourseIssueHCode, CourseIssueHCodeService } from '../course-issue-h-code';

@Component({
    selector: 'jhi-video-issue-h-code-dialog',
    templateUrl: './video-issue-h-code-dialog.component.html'
})
export class VideoIssueHCodeDialogComponent implements OnInit {

    videoIssue: VideoIssueHCode;
    isSaving: boolean;

    courseissues: CourseIssueHCode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private videoIssueService: VideoIssueHCodeService,
        private courseIssueService: CourseIssueHCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.courseIssueService.query()
            .subscribe((res: HttpResponse<CourseIssueHCode[]>) => { this.courseissues = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.videoIssue.id !== undefined) {
            this.subscribeToSaveResponse(
                this.videoIssueService.update(this.videoIssue));
        } else {
            this.subscribeToSaveResponse(
                this.videoIssueService.create(this.videoIssue));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<VideoIssueHCode>>) {
        result.subscribe((res: HttpResponse<VideoIssueHCode>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: VideoIssueHCode) {
        this.eventManager.broadcast({ name: 'videoIssueListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCourseIssueById(index: number, item: CourseIssueHCode) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-video-issue-h-code-popup',
    template: ''
})
export class VideoIssueHCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private videoIssuePopupService: VideoIssueHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.videoIssuePopupService
                    .open(VideoIssueHCodeDialogComponent as Component, params['id']);
            } else {
                this.videoIssuePopupService
                    .open(VideoIssueHCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
