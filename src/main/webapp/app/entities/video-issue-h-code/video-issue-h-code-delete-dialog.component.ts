import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { VideoIssueHCode } from './video-issue-h-code.model';
import { VideoIssueHCodePopupService } from './video-issue-h-code-popup.service';
import { VideoIssueHCodeService } from './video-issue-h-code.service';

@Component({
    selector: 'jhi-video-issue-h-code-delete-dialog',
    templateUrl: './video-issue-h-code-delete-dialog.component.html'
})
export class VideoIssueHCodeDeleteDialogComponent {

    videoIssue: VideoIssueHCode;

    constructor(
        private videoIssueService: VideoIssueHCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.videoIssueService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'videoIssueListModification',
                content: 'Deleted an videoIssue'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-video-issue-h-code-delete-popup',
    template: ''
})
export class VideoIssueHCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private videoIssuePopupService: VideoIssueHCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.videoIssuePopupService
                .open(VideoIssueHCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
