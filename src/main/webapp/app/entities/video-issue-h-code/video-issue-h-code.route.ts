import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { VideoIssueHCodeComponent } from './video-issue-h-code.component';
import { VideoIssueHCodeDetailComponent } from './video-issue-h-code-detail.component';
import { VideoIssueHCodePopupComponent } from './video-issue-h-code-dialog.component';
import { VideoIssueHCodeDeletePopupComponent } from './video-issue-h-code-delete-dialog.component';

@Injectable()
export class VideoIssueHCodeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const videoIssueRoute: Routes = [
    {
        path: 'video-issue-h-code',
        component: VideoIssueHCodeComponent,
        resolve: {
            'pagingParams': VideoIssueHCodeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.videoIssue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'video-issue-h-code/:id',
        component: VideoIssueHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.videoIssue.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const videoIssuePopupRoute: Routes = [
    {
        path: 'video-issue-h-code-new',
        component: VideoIssueHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.videoIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'video-issue-h-code/:id/edit',
        component: VideoIssueHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.videoIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'video-issue-h-code/:id/delete',
        component: VideoIssueHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.videoIssue.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
