import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { VideoIssueHCode } from './video-issue-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<VideoIssueHCode>;

@Injectable()
export class VideoIssueHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/video-issues';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/video-issues';

    constructor(private http: HttpClient) { }

    create(videoIssue: VideoIssueHCode): Observable<EntityResponseType> {
        const copy = this.convert(videoIssue);
        return this.http.post<VideoIssueHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(videoIssue: VideoIssueHCode): Observable<EntityResponseType> {
        const copy = this.convert(videoIssue);
        return this.http.put<VideoIssueHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<VideoIssueHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<VideoIssueHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<VideoIssueHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<VideoIssueHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<VideoIssueHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<VideoIssueHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<VideoIssueHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: VideoIssueHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<VideoIssueHCode[]>): HttpResponse<VideoIssueHCode[]> {
        const jsonResponse: VideoIssueHCode[] = res.body;
        const body: VideoIssueHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to VideoIssueHCode.
     */
    private convertItemFromServer(videoIssue: VideoIssueHCode): VideoIssueHCode {
        const copy: VideoIssueHCode = Object.assign({}, videoIssue);
        return copy;
    }

    /**
     * Convert a VideoIssueHCode to a JSON which can be sent to the server.
     */
    private convert(videoIssue: VideoIssueHCode): VideoIssueHCode {
        const copy: VideoIssueHCode = Object.assign({}, videoIssue);
        return copy;
    }
}
