import { BaseEntity } from './../../shared';

export class VideoIssueHCode implements BaseEntity {
    constructor(
        public id?: number,
        public url?: string,
        public duration?: number,
        public name?: string,
        public login?: string,
        public key?: string,
        public note?: string,
        public flagDeleted?: boolean,
        public issue?: BaseEntity,
    ) {
        this.flagDeleted = false;
    }
}
