import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    VideoIssueHCodeService,
    VideoIssueHCodePopupService,
    VideoIssueHCodeComponent,
    VideoIssueHCodeDetailComponent,
    VideoIssueHCodeDialogComponent,
    VideoIssueHCodePopupComponent,
    VideoIssueHCodeDeletePopupComponent,
    VideoIssueHCodeDeleteDialogComponent,
    videoIssueRoute,
    videoIssuePopupRoute,
    VideoIssueHCodeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...videoIssueRoute,
    ...videoIssuePopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        VideoIssueHCodeComponent,
        VideoIssueHCodeDetailComponent,
        VideoIssueHCodeDialogComponent,
        VideoIssueHCodeDeleteDialogComponent,
        VideoIssueHCodePopupComponent,
        VideoIssueHCodeDeletePopupComponent,
    ],
    entryComponents: [
        VideoIssueHCodeComponent,
        VideoIssueHCodeDialogComponent,
        VideoIssueHCodePopupComponent,
        VideoIssueHCodeDeleteDialogComponent,
        VideoIssueHCodeDeletePopupComponent,
    ],
    providers: [
        VideoIssueHCodeService,
        VideoIssueHCodePopupService,
        VideoIssueHCodeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeVideoIssueHCodeModule {}
