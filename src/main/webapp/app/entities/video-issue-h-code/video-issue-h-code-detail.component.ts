import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { VideoIssueHCode } from './video-issue-h-code.model';
import { VideoIssueHCodeService } from './video-issue-h-code.service';

@Component({
    selector: 'jhi-video-issue-h-code-detail',
    templateUrl: './video-issue-h-code-detail.component.html'
})
export class VideoIssueHCodeDetailComponent implements OnInit, OnDestroy {

    videoIssue: VideoIssueHCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private videoIssueService: VideoIssueHCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInVideoIssues();
    }

    load(id) {
        this.videoIssueService.find(id)
            .subscribe((videoIssueResponse: HttpResponse<VideoIssueHCode>) => {
                this.videoIssue = videoIssueResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInVideoIssues() {
        this.eventSubscriber = this.eventManager.subscribe(
            'videoIssueListModification',
            (response) => this.load(this.videoIssue.id)
        );
    }
}
