import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { CategoryHCodeComponent } from './category-h-code.component';
import { CategoryHCodeDetailComponent } from './category-h-code-detail.component';
import { CategoryHCodePopupComponent } from './category-h-code-dialog.component';
import { CategoryHCodeDeletePopupComponent } from './category-h-code-delete-dialog.component';

export const categoryRoute: Routes = [
    {
        path: 'category-h-code',
        component: CategoryHCodeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.category.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'category-h-code/:id',
        component: CategoryHCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.category.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const categoryPopupRoute: Routes = [
    {
        path: 'category-h-code-new',
        component: CategoryHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.category.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'category-h-code/:id/edit',
        component: CategoryHCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.category.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'category-h-code/:id/delete',
        component: CategoryHCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'helloCodeApp.category.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
