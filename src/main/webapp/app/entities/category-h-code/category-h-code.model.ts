import { BaseEntity } from './../../shared';

export class CategoryHCode implements BaseEntity {
    constructor(
        public id?: number,
        public nameRu?: string,
        public nameKk?: string,
        public nameEn?: string,
        public flagDeleted?: boolean,
        public courses?: BaseEntity[],
    ) {
        this.flagDeleted = false;
    }
}
