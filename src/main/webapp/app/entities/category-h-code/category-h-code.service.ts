import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { CategoryHCode } from './category-h-code.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<CategoryHCode>;

@Injectable()
export class CategoryHCodeService {

    private resourceUrl =  SERVER_API_URL + 'api/categories';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/categories';

    constructor(private http: HttpClient) { }

    create(category: CategoryHCode): Observable<EntityResponseType> {
        const copy = this.convert(category);
        return this.http.post<CategoryHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(category: CategoryHCode): Observable<EntityResponseType> {
        const copy = this.convert(category);
        return this.http.put<CategoryHCode>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CategoryHCode>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CategoryHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<CategoryHCode[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CategoryHCode[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<CategoryHCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<CategoryHCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CategoryHCode[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CategoryHCode = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CategoryHCode[]>): HttpResponse<CategoryHCode[]> {
        const jsonResponse: CategoryHCode[] = res.body;
        const body: CategoryHCode[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CategoryHCode.
     */
    private convertItemFromServer(category: CategoryHCode): CategoryHCode {
        const copy: CategoryHCode = Object.assign({}, category);
        return copy;
    }

    /**
     * Convert a CategoryHCode to a JSON which can be sent to the server.
     */
    private convert(category: CategoryHCode): CategoryHCode {
        const copy: CategoryHCode = Object.assign({}, category);
        return copy;
    }
}
