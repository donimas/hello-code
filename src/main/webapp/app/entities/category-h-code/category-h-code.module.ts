import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelloCodeSharedModule } from '../../shared';
import {
    CategoryHCodeService,
    CategoryHCodePopupService,
    CategoryHCodeComponent,
    CategoryHCodeDetailComponent,
    CategoryHCodeDialogComponent,
    CategoryHCodePopupComponent,
    CategoryHCodeDeletePopupComponent,
    CategoryHCodeDeleteDialogComponent,
    categoryRoute,
    categoryPopupRoute,
} from './';

const ENTITY_STATES = [
    ...categoryRoute,
    ...categoryPopupRoute,
];

@NgModule({
    imports: [
        HelloCodeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CategoryHCodeComponent,
        CategoryHCodeDetailComponent,
        CategoryHCodeDialogComponent,
        CategoryHCodeDeleteDialogComponent,
        CategoryHCodePopupComponent,
        CategoryHCodeDeletePopupComponent,
    ],
    entryComponents: [
        CategoryHCodeComponent,
        CategoryHCodeDialogComponent,
        CategoryHCodePopupComponent,
        CategoryHCodeDeleteDialogComponent,
        CategoryHCodeDeletePopupComponent,
    ],
    providers: [
        CategoryHCodeService,
        CategoryHCodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HelloCodeCategoryHCodeModule {}
