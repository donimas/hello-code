package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.LearnerGroup;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the LearnerGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LearnerGroupRepository extends JpaRepository<LearnerGroup, Long> {

    @Query("select learner_group from LearnerGroup learner_group where learner_group.teacher.login = ?#{principal.username}")
    List<LearnerGroup> findByTeacherIsCurrentUser();
    @Query("select distinct learner_group from LearnerGroup learner_group left join fetch learner_group.learners")
    List<LearnerGroup> findAllWithEagerRelationships();

    @Query("select learner_group from LearnerGroup learner_group left join fetch learner_group.learners where learner_group.id =:id")
    LearnerGroup findOneWithEagerRelationships(@Param("id") Long id);

}
