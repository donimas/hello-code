package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.VideoIssue;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VideoIssue entity.
 */
public interface VideoIssueSearchRepository extends ElasticsearchRepository<VideoIssue, Long> {
}
