package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.Course;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Course entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

    @Query("select course from Course course where course.lecturer.login = ?#{principal.username}")
    List<Course> findByLecturerIsCurrentUser();
    @Query("select distinct course from Course course left join fetch course.grades left join fetch course.categories")
    List<Course> findAllWithEagerRelationships();

    @Query("select course from Course course left join fetch course.grades left join fetch course.categories where course.id =:id")
    Course findOneWithEagerRelationships(@Param("id") Long id);

}
