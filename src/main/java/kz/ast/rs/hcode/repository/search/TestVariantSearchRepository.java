package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.TestVariant;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TestVariant entity.
 */
public interface TestVariantSearchRepository extends ElasticsearchRepository<TestVariant, Long> {
}
