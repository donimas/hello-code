package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.WebinarIssue;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WebinarIssue entity.
 */
public interface WebinarIssueSearchRepository extends ElasticsearchRepository<WebinarIssue, Long> {
}
