package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.CourseProgress;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CourseProgress entity.
 */
public interface CourseProgressSearchRepository extends ElasticsearchRepository<CourseProgress, Long> {
}
