package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.CourseIssue;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CourseIssue entity.
 */
public interface CourseIssueSearchRepository extends ElasticsearchRepository<CourseIssue, Long> {
}
