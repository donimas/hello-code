package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.CourseIssue;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CourseIssue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CourseIssueRepository extends JpaRepository<CourseIssue, Long> {

}
