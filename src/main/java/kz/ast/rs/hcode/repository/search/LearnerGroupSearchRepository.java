package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.LearnerGroup;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the LearnerGroup entity.
 */
public interface LearnerGroupSearchRepository extends ElasticsearchRepository<LearnerGroup, Long> {
}
