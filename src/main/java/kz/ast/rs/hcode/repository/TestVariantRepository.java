package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.TestVariant;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TestVariant entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TestVariantRepository extends JpaRepository<TestVariant, Long> {

}
