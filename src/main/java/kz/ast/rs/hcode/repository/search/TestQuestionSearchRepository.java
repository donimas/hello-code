package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.TestQuestion;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TestQuestion entity.
 */
public interface TestQuestionSearchRepository extends ElasticsearchRepository<TestQuestion, Long> {
}
