package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.VideoIssue;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the VideoIssue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VideoIssueRepository extends JpaRepository<VideoIssue, Long> {

}
