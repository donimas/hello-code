package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.Grade;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Grade entity.
 */
public interface GradeSearchRepository extends ElasticsearchRepository<Grade, Long> {
}
