package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.SkratchIssue;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SkratchIssue entity.
 */
public interface SkratchIssueSearchRepository extends ElasticsearchRepository<SkratchIssue, Long> {
}
