package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.WebinarIssue;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the WebinarIssue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WebinarIssueRepository extends JpaRepository<WebinarIssue, Long> {

}
