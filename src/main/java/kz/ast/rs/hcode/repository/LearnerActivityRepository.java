package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.LearnerActivity;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the LearnerActivity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LearnerActivityRepository extends JpaRepository<LearnerActivity, Long> {

    @Query("select learner_activity from LearnerActivity learner_activity where learner_activity.user.login = ?#{principal.username}")
    List<LearnerActivity> findByUserIsCurrentUser();

}
