package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.CourseChapter;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CourseChapter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CourseChapterRepository extends JpaRepository<CourseChapter, Long> {

}
