package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.TextIssue;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TextIssue entity.
 */
public interface TextIssueSearchRepository extends ElasticsearchRepository<TextIssue, Long> {
}
