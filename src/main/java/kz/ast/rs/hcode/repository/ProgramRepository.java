package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.Program;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Program entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramRepository extends JpaRepository<Program, Long> {
    @Query("select distinct program from Program program left join fetch program.courses left join fetch program.grades")
    List<Program> findAllWithEagerRelationships();

    @Query("select program from Program program left join fetch program.courses left join fetch program.grades where program.id =:id")
    Program findOneWithEagerRelationships(@Param("id") Long id);

}
