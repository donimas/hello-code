package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.LearnerActivity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the LearnerActivity entity.
 */
public interface LearnerActivitySearchRepository extends ElasticsearchRepository<LearnerActivity, Long> {
}
