package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.ActivityResult;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ActivityResult entity.
 */
public interface ActivityResultSearchRepository extends ElasticsearchRepository<ActivityResult, Long> {
}
