package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.TestResult;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TestResult entity.
 */
public interface TestResultSearchRepository extends ElasticsearchRepository<TestResult, Long> {
}
