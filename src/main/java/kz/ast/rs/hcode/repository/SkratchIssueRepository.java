package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.SkratchIssue;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SkratchIssue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SkratchIssueRepository extends JpaRepository<SkratchIssue, Long> {

}
