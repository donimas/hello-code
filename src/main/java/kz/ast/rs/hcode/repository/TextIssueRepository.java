package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.TextIssue;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TextIssue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TextIssueRepository extends JpaRepository<TextIssue, Long> {

}
