package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.TestResult;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the TestResult entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TestResultRepository extends JpaRepository<TestResult, Long> {

    @Query("select test_result from TestResult test_result where test_result.user.login = ?#{principal.username}")
    List<TestResult> findByUserIsCurrentUser();

}
