package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.ActivityStatusHistory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ActivityStatusHistory entity.
 */
public interface ActivityStatusHistorySearchRepository extends ElasticsearchRepository<ActivityStatusHistory, Long> {
}
