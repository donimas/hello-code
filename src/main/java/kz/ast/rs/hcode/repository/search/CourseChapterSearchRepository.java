package kz.ast.rs.hcode.repository.search;

import kz.ast.rs.hcode.domain.CourseChapter;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CourseChapter entity.
 */
public interface CourseChapterSearchRepository extends ElasticsearchRepository<CourseChapter, Long> {
}
