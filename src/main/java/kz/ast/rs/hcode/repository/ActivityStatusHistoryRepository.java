package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.ActivityStatusHistory;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ActivityStatusHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ActivityStatusHistoryRepository extends JpaRepository<ActivityStatusHistory, Long> {

}
