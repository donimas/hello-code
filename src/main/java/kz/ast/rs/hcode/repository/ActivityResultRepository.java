package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.ActivityResult;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ActivityResult entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ActivityResultRepository extends JpaRepository<ActivityResult, Long> {

}
