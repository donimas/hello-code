package kz.ast.rs.hcode.repository;

import kz.ast.rs.hcode.domain.CourseProgress;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the CourseProgress entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CourseProgressRepository extends JpaRepository<CourseProgress, Long> {

    @Query("select course_progress from CourseProgress course_progress where course_progress.user.login = ?#{principal.username}")
    List<CourseProgress> findByUserIsCurrentUser();

}
