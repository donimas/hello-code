package kz.ast.rs.hcode.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A TestQuestion.
 */
@Entity
@Table(name = "test_question")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "testquestion")
public class TestQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "body_ru")
    private String bodyRu;

    @Column(name = "body_kk")
    private String bodyKk;

    @Column(name = "body_en")
    private String bodyEn;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @ManyToOne
    private CourseIssue issue;

    @OneToMany(mappedBy = "question")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TestVariant> variants = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBodyRu() {
        return bodyRu;
    }

    public TestQuestion bodyRu(String bodyRu) {
        this.bodyRu = bodyRu;
        return this;
    }

    public void setBodyRu(String bodyRu) {
        this.bodyRu = bodyRu;
    }

    public String getBodyKk() {
        return bodyKk;
    }

    public TestQuestion bodyKk(String bodyKk) {
        this.bodyKk = bodyKk;
        return this;
    }

    public void setBodyKk(String bodyKk) {
        this.bodyKk = bodyKk;
    }

    public String getBodyEn() {
        return bodyEn;
    }

    public TestQuestion bodyEn(String bodyEn) {
        this.bodyEn = bodyEn;
        return this;
    }

    public void setBodyEn(String bodyEn) {
        this.bodyEn = bodyEn;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public TestQuestion flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public CourseIssue getIssue() {
        return issue;
    }

    public TestQuestion issue(CourseIssue courseIssue) {
        this.issue = courseIssue;
        return this;
    }

    public void setIssue(CourseIssue courseIssue) {
        this.issue = courseIssue;
    }

    public Set<TestVariant> getVariants() {
        return variants;
    }

    public TestQuestion variants(Set<TestVariant> testVariants) {
        this.variants = testVariants;
        return this;
    }

    public TestQuestion addVariant(TestVariant testVariant) {
        this.variants.add(testVariant);
        testVariant.setQuestion(this);
        return this;
    }

    public TestQuestion removeVariant(TestVariant testVariant) {
        this.variants.remove(testVariant);
        testVariant.setQuestion(null);
        return this;
    }

    public void setVariants(Set<TestVariant> testVariants) {
        this.variants = testVariants;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TestQuestion testQuestion = (TestQuestion) o;
        if (testQuestion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), testQuestion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TestQuestion{" +
            "id=" + getId() +
            ", bodyRu='" + getBodyRu() + "'" +
            ", bodyKk='" + getBodyKk() + "'" +
            ", bodyEn='" + getBodyEn() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}
