package kz.ast.rs.hcode.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Course.
 */
@Entity
@Table(name = "course")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "course")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "total_issues", precision=10, scale=2)
    private BigDecimal totalIssues;

    @Column(name = "lectures_amount", precision=10, scale=2)
    private BigDecimal lecturesAmount;

    @Column(name = "video_duration", precision=10, scale=2)
    private BigDecimal videoDuration;

    @NotNull
    @Column(name = "name_ru", nullable = false)
    private String nameRu;

    @NotNull
    @Column(name = "name_kk", nullable = false)
    private String nameKk;

    @Column(name = "name_en")
    private String nameEn;

    @Column(name = "description_ru")
    private String descriptionRu;

    @Column(name = "description_kk")
    private String descriptionKk;

    @Column(name = "description_en")
    private String descriptionEn;

    @Column(name = "brief_ru")
    private String briefRu;

    @Column(name = "brief_kk")
    private String briefKk;

    @Column(name = "brief_en")
    private String briefEn;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;

    @ManyToOne
    private User lecturer;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "course_grade",
               joinColumns = @JoinColumn(name="courses_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="grades_id", referencedColumnName="id"))
    private Set<Grade> grades = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "course_category",
               joinColumns = @JoinColumn(name="courses_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="categories_id", referencedColumnName="id"))
    private Set<Category> categories = new HashSet<>();

    @OneToMany(mappedBy = "course")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CourseChapter> courseChapters = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getTotalIssues() {
        return totalIssues;
    }

    public Course totalIssues(BigDecimal totalIssues) {
        this.totalIssues = totalIssues;
        return this;
    }

    public void setTotalIssues(BigDecimal totalIssues) {
        this.totalIssues = totalIssues;
    }

    public BigDecimal getLecturesAmount() {
        return lecturesAmount;
    }

    public Course lecturesAmount(BigDecimal lecturesAmount) {
        this.lecturesAmount = lecturesAmount;
        return this;
    }

    public void setLecturesAmount(BigDecimal lecturesAmount) {
        this.lecturesAmount = lecturesAmount;
    }

    public BigDecimal getVideoDuration() {
        return videoDuration;
    }

    public Course videoDuration(BigDecimal videoDuration) {
        this.videoDuration = videoDuration;
        return this;
    }

    public void setVideoDuration(BigDecimal videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getNameRu() {
        return nameRu;
    }

    public Course nameRu(String nameRu) {
        this.nameRu = nameRu;
        return this;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameKk() {
        return nameKk;
    }

    public Course nameKk(String nameKk) {
        this.nameKk = nameKk;
        return this;
    }

    public void setNameKk(String nameKk) {
        this.nameKk = nameKk;
    }

    public String getNameEn() {
        return nameEn;
    }

    public Course nameEn(String nameEn) {
        this.nameEn = nameEn;
        return this;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getDescriptionRu() {
        return descriptionRu;
    }

    public Course descriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
        return this;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    public String getDescriptionKk() {
        return descriptionKk;
    }

    public Course descriptionKk(String descriptionKk) {
        this.descriptionKk = descriptionKk;
        return this;
    }

    public void setDescriptionKk(String descriptionKk) {
        this.descriptionKk = descriptionKk;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public Course descriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
        return this;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public String getBriefRu() {
        return briefRu;
    }

    public Course briefRu(String briefRu) {
        this.briefRu = briefRu;
        return this;
    }

    public void setBriefRu(String briefRu) {
        this.briefRu = briefRu;
    }

    public String getBriefKk() {
        return briefKk;
    }

    public Course briefKk(String briefKk) {
        this.briefKk = briefKk;
        return this;
    }

    public void setBriefKk(String briefKk) {
        this.briefKk = briefKk;
    }

    public String getBriefEn() {
        return briefEn;
    }

    public Course briefEn(String briefEn) {
        this.briefEn = briefEn;
        return this;
    }

    public void setBriefEn(String briefEn) {
        this.briefEn = briefEn;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public Course flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public byte[] getImage() {
        return image;
    }

    public Course image(byte[] image) {
        this.image = image;
        return this;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public Course imageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
        return this;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public User getLecturer() {
        return lecturer;
    }

    public Course lecturer(User user) {
        this.lecturer = user;
        return this;
    }

    public void setLecturer(User user) {
        this.lecturer = user;
    }

    public Set<Grade> getGrades() {
        return grades;
    }

    public Course grades(Set<Grade> grades) {
        this.grades = grades;
        return this;
    }

    public Course addGrade(Grade grade) {
        this.grades.add(grade);
        grade.getCourses().add(this);
        return this;
    }

    public Course removeGrade(Grade grade) {
        this.grades.remove(grade);
        grade.getCourses().remove(this);
        return this;
    }

    public void setGrades(Set<Grade> grades) {
        this.grades = grades;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public Course categories(Set<Category> categories) {
        this.categories = categories;
        return this;
    }

    public Course addCategory(Category category) {
        this.categories.add(category);
        category.getCourses().add(this);
        return this;
    }

    public Course removeCategory(Category category) {
        this.categories.remove(category);
        category.getCourses().remove(this);
        return this;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public Set<CourseChapter> getCourseChapters() {
        return courseChapters;
    }

    public Course courseChapters(Set<CourseChapter> courseChapters) {
        this.courseChapters = courseChapters;
        return this;
    }

    public Course addCourseChapter(CourseChapter courseChapter) {
        this.courseChapters.add(courseChapter);
        courseChapter.setCourse(this);
        return this;
    }

    public Course removeCourseChapter(CourseChapter courseChapter) {
        this.courseChapters.remove(courseChapter);
        courseChapter.setCourse(null);
        return this;
    }

    public void setCourseChapters(Set<CourseChapter> courseChapters) {
        this.courseChapters = courseChapters;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Course course = (Course) o;
        if (course.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), course.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Course{" +
            "id=" + getId() +
            ", totalIssues=" + getTotalIssues() +
            ", lecturesAmount=" + getLecturesAmount() +
            ", videoDuration=" + getVideoDuration() +
            ", nameRu='" + getNameRu() + "'" +
            ", nameKk='" + getNameKk() + "'" +
            ", nameEn='" + getNameEn() + "'" +
            ", descriptionRu='" + getDescriptionRu() + "'" +
            ", descriptionKk='" + getDescriptionKk() + "'" +
            ", descriptionEn='" + getDescriptionEn() + "'" +
            ", briefRu='" + getBriefRu() + "'" +
            ", briefKk='" + getBriefKk() + "'" +
            ", briefEn='" + getBriefEn() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            ", image='" + getImage() + "'" +
            ", imageContentType='" + getImageContentType() + "'" +
            "}";
    }
}
