package kz.ast.rs.hcode.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A VideoIssue.
 */
@Entity
@Table(name = "video_issue")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "videoissue")
public class VideoIssue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "url")
    private String url;

    @Column(name = "duration", precision=10, scale=2)
    private BigDecimal duration;

    @Column(name = "name")
    private String name;

    @Column(name = "login")
    private String login;

    @Column(name = "jhi_key")
    private String key;

    @Column(name = "note")
    private String note;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @ManyToOne
    private CourseIssue issue;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public VideoIssue url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public BigDecimal getDuration() {
        return duration;
    }

    public VideoIssue duration(BigDecimal duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public VideoIssue name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public VideoIssue login(String login) {
        this.login = login;
        return this;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getKey() {
        return key;
    }

    public VideoIssue key(String key) {
        this.key = key;
        return this;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNote() {
        return note;
    }

    public VideoIssue note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public VideoIssue flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public CourseIssue getIssue() {
        return issue;
    }

    public VideoIssue issue(CourseIssue courseIssue) {
        this.issue = courseIssue;
        return this;
    }

    public void setIssue(CourseIssue courseIssue) {
        this.issue = courseIssue;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VideoIssue videoIssue = (VideoIssue) o;
        if (videoIssue.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), videoIssue.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "VideoIssue{" +
            "id=" + getId() +
            ", url='" + getUrl() + "'" +
            ", duration=" + getDuration() +
            ", name='" + getName() + "'" +
            ", login='" + getLogin() + "'" +
            ", key='" + getKey() + "'" +
            ", note='" + getNote() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}
