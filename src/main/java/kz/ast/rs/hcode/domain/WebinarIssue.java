package kz.ast.rs.hcode.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A WebinarIssue.
 */
@Entity
@Table(name = "webinar_issue")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "webinarissue")
public class WebinarIssue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "url")
    private String url;

    @Column(name = "jhi_key")
    private String key;

    @Column(name = "cabinte")
    private String cabinte;

    @Column(name = "note")
    private String note;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @ManyToOne
    private CourseIssue issue;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public WebinarIssue url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getKey() {
        return key;
    }

    public WebinarIssue key(String key) {
        this.key = key;
        return this;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCabinte() {
        return cabinte;
    }

    public WebinarIssue cabinte(String cabinte) {
        this.cabinte = cabinte;
        return this;
    }

    public void setCabinte(String cabinte) {
        this.cabinte = cabinte;
    }

    public String getNote() {
        return note;
    }

    public WebinarIssue note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public WebinarIssue flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public CourseIssue getIssue() {
        return issue;
    }

    public WebinarIssue issue(CourseIssue courseIssue) {
        this.issue = courseIssue;
        return this;
    }

    public void setIssue(CourseIssue courseIssue) {
        this.issue = courseIssue;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WebinarIssue webinarIssue = (WebinarIssue) o;
        if (webinarIssue.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), webinarIssue.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WebinarIssue{" +
            "id=" + getId() +
            ", url='" + getUrl() + "'" +
            ", key='" + getKey() + "'" +
            ", cabinte='" + getCabinte() + "'" +
            ", note='" + getNote() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}
