package kz.ast.rs.hcode.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A LearnerActivity.
 */
@Entity
@Table(name = "learner_activity")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "learneractivity")
public class LearnerActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "flag_passed")
    private Boolean flagPassed;

    @ManyToOne
    private Course course;

    @ManyToOne
    private CourseIssue issue;

    @ManyToOne
    private User user;

    @ManyToOne
    private ActivityStatusHistory statusHistory;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isFlagPassed() {
        return flagPassed;
    }

    public LearnerActivity flagPassed(Boolean flagPassed) {
        this.flagPassed = flagPassed;
        return this;
    }

    public void setFlagPassed(Boolean flagPassed) {
        this.flagPassed = flagPassed;
    }

    public Course getCourse() {
        return course;
    }

    public LearnerActivity course(Course course) {
        this.course = course;
        return this;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public CourseIssue getIssue() {
        return issue;
    }

    public LearnerActivity issue(CourseIssue courseIssue) {
        this.issue = courseIssue;
        return this;
    }

    public void setIssue(CourseIssue courseIssue) {
        this.issue = courseIssue;
    }

    public User getUser() {
        return user;
    }

    public LearnerActivity user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ActivityStatusHistory getStatusHistory() {
        return statusHistory;
    }

    public LearnerActivity statusHistory(ActivityStatusHistory activityStatusHistory) {
        this.statusHistory = activityStatusHistory;
        return this;
    }

    public void setStatusHistory(ActivityStatusHistory activityStatusHistory) {
        this.statusHistory = activityStatusHistory;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LearnerActivity learnerActivity = (LearnerActivity) o;
        if (learnerActivity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), learnerActivity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LearnerActivity{" +
            "id=" + getId() +
            ", flagPassed='" + isFlagPassed() + "'" +
            "}";
    }
}
