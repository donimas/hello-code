package kz.ast.rs.hcode.domain.enumeration;

/**
 * The ActivityStatus enumeration.
 */
public enum ActivityStatus {
    IN_PROGRESS, SKIPPED, PASSED, FAILED
}
