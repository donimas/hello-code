package kz.ast.rs.hcode.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CourseChapter.
 */
@Entity
@Table(name = "course_chapter")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "coursechapter")
public class CourseChapter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "note_ru", nullable = false)
    private String noteRu;

    @NotNull
    @Column(name = "note_kk", nullable = false)
    private String noteKk;

    @Column(name = "note_en")
    private String noteEn;

    @Column(name = "jhi_order", precision=10, scale=2)
    private BigDecimal order;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @ManyToOne
    private Course course;

    @OneToMany(mappedBy = "chapter")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CourseIssue> issues = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNoteRu() {
        return noteRu;
    }

    public CourseChapter noteRu(String noteRu) {
        this.noteRu = noteRu;
        return this;
    }

    public void setNoteRu(String noteRu) {
        this.noteRu = noteRu;
    }

    public String getNoteKk() {
        return noteKk;
    }

    public CourseChapter noteKk(String noteKk) {
        this.noteKk = noteKk;
        return this;
    }

    public void setNoteKk(String noteKk) {
        this.noteKk = noteKk;
    }

    public String getNoteEn() {
        return noteEn;
    }

    public CourseChapter noteEn(String noteEn) {
        this.noteEn = noteEn;
        return this;
    }

    public void setNoteEn(String noteEn) {
        this.noteEn = noteEn;
    }

    public BigDecimal getOrder() {
        return order;
    }

    public CourseChapter order(BigDecimal order) {
        this.order = order;
        return this;
    }

    public void setOrder(BigDecimal order) {
        this.order = order;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public CourseChapter flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public Course getCourse() {
        return course;
    }

    public CourseChapter course(Course course) {
        this.course = course;
        return this;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Set<CourseIssue> getIssues() {
        return issues;
    }

    public CourseChapter issues(Set<CourseIssue> courseIssues) {
        this.issues = courseIssues;
        return this;
    }

    public CourseChapter addIssue(CourseIssue courseIssue) {
        this.issues.add(courseIssue);
        courseIssue.setChapter(this);
        return this;
    }

    public CourseChapter removeIssue(CourseIssue courseIssue) {
        this.issues.remove(courseIssue);
        courseIssue.setChapter(null);
        return this;
    }

    public void setIssues(Set<CourseIssue> courseIssues) {
        this.issues = courseIssues;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CourseChapter courseChapter = (CourseChapter) o;
        if (courseChapter.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), courseChapter.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CourseChapter{" +
            "id=" + getId() +
            ", noteRu='" + getNoteRu() + "'" +
            ", noteKk='" + getNoteKk() + "'" +
            ", noteEn='" + getNoteEn() + "'" +
            ", order=" + getOrder() +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}
