package kz.ast.rs.hcode.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import kz.ast.rs.hcode.domain.enumeration.ActivityStatus;

/**
 * A ActivityStatusHistory.
 */
@Entity
@Table(name = "activity_status_history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "activitystatushistory")
public class ActivityStatusHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ActivityStatus status;

    @NotNull
    @Column(name = "started_date", nullable = false)
    private ZonedDateTime startedDate;

    @Column(name = "finished_date")
    private ZonedDateTime finishedDate;

    @ManyToOne
    private LearnerActivity activity;

    @ManyToOne
    private ActivityResult activityResult;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ActivityStatus getStatus() {
        return status;
    }

    public ActivityStatusHistory status(ActivityStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(ActivityStatus status) {
        this.status = status;
    }

    public ZonedDateTime getStartedDate() {
        return startedDate;
    }

    public ActivityStatusHistory startedDate(ZonedDateTime startedDate) {
        this.startedDate = startedDate;
        return this;
    }

    public void setStartedDate(ZonedDateTime startedDate) {
        this.startedDate = startedDate;
    }

    public ZonedDateTime getFinishedDate() {
        return finishedDate;
    }

    public ActivityStatusHistory finishedDate(ZonedDateTime finishedDate) {
        this.finishedDate = finishedDate;
        return this;
    }

    public void setFinishedDate(ZonedDateTime finishedDate) {
        this.finishedDate = finishedDate;
    }

    public LearnerActivity getActivity() {
        return activity;
    }

    public ActivityStatusHistory activity(LearnerActivity learnerActivity) {
        this.activity = learnerActivity;
        return this;
    }

    public void setActivity(LearnerActivity learnerActivity) {
        this.activity = learnerActivity;
    }

    public ActivityResult getActivityResult() {
        return activityResult;
    }

    public ActivityStatusHistory activityResult(ActivityResult activityResult) {
        this.activityResult = activityResult;
        return this;
    }

    public void setActivityResult(ActivityResult activityResult) {
        this.activityResult = activityResult;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ActivityStatusHistory activityStatusHistory = (ActivityStatusHistory) o;
        if (activityStatusHistory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), activityStatusHistory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ActivityStatusHistory{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", startedDate='" + getStartedDate() + "'" +
            ", finishedDate='" + getFinishedDate() + "'" +
            "}";
    }
}
