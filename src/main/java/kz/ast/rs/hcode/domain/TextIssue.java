package kz.ast.rs.hcode.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TextIssue.
 */
@Entity
@Table(name = "text_issue")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "textissue")
public class TextIssue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "body_ru")
    private String bodyRu;

    @Column(name = "body_kk")
    private String bodyKk;

    @Column(name = "body_en")
    private String bodyEn;

    @Column(name = "note_ru")
    private String noteRu;

    @Column(name = "note_kk")
    private String noteKk;

    @Column(name = "note_en")
    private String noteEn;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @ManyToOne
    private CourseIssue issue;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBodyRu() {
        return bodyRu;
    }

    public TextIssue bodyRu(String bodyRu) {
        this.bodyRu = bodyRu;
        return this;
    }

    public void setBodyRu(String bodyRu) {
        this.bodyRu = bodyRu;
    }

    public String getBodyKk() {
        return bodyKk;
    }

    public TextIssue bodyKk(String bodyKk) {
        this.bodyKk = bodyKk;
        return this;
    }

    public void setBodyKk(String bodyKk) {
        this.bodyKk = bodyKk;
    }

    public String getBodyEn() {
        return bodyEn;
    }

    public TextIssue bodyEn(String bodyEn) {
        this.bodyEn = bodyEn;
        return this;
    }

    public void setBodyEn(String bodyEn) {
        this.bodyEn = bodyEn;
    }

    public String getNoteRu() {
        return noteRu;
    }

    public TextIssue noteRu(String noteRu) {
        this.noteRu = noteRu;
        return this;
    }

    public void setNoteRu(String noteRu) {
        this.noteRu = noteRu;
    }

    public String getNoteKk() {
        return noteKk;
    }

    public TextIssue noteKk(String noteKk) {
        this.noteKk = noteKk;
        return this;
    }

    public void setNoteKk(String noteKk) {
        this.noteKk = noteKk;
    }

    public String getNoteEn() {
        return noteEn;
    }

    public TextIssue noteEn(String noteEn) {
        this.noteEn = noteEn;
        return this;
    }

    public void setNoteEn(String noteEn) {
        this.noteEn = noteEn;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public TextIssue flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public CourseIssue getIssue() {
        return issue;
    }

    public TextIssue issue(CourseIssue courseIssue) {
        this.issue = courseIssue;
        return this;
    }

    public void setIssue(CourseIssue courseIssue) {
        this.issue = courseIssue;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TextIssue textIssue = (TextIssue) o;
        if (textIssue.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), textIssue.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TextIssue{" +
            "id=" + getId() +
            ", bodyRu='" + getBodyRu() + "'" +
            ", bodyKk='" + getBodyKk() + "'" +
            ", bodyEn='" + getBodyEn() + "'" +
            ", noteRu='" + getNoteRu() + "'" +
            ", noteKk='" + getNoteKk() + "'" +
            ", noteEn='" + getNoteEn() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}
