package kz.ast.rs.hcode.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TestVariant.
 */
@Entity
@Table(name = "test_variant")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "testvariant")
public class TestVariant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "body_ru")
    private String bodyRu;

    @Column(name = "body_kk")
    private String bodyKk;

    @Column(name = "body_en")
    private String bodyEn;

    @NotNull
    @Column(name = "flag_correct", nullable = false)
    private Boolean flagCorrect;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @ManyToOne
    private TestQuestion question;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBodyRu() {
        return bodyRu;
    }

    public TestVariant bodyRu(String bodyRu) {
        this.bodyRu = bodyRu;
        return this;
    }

    public void setBodyRu(String bodyRu) {
        this.bodyRu = bodyRu;
    }

    public String getBodyKk() {
        return bodyKk;
    }

    public TestVariant bodyKk(String bodyKk) {
        this.bodyKk = bodyKk;
        return this;
    }

    public void setBodyKk(String bodyKk) {
        this.bodyKk = bodyKk;
    }

    public String getBodyEn() {
        return bodyEn;
    }

    public TestVariant bodyEn(String bodyEn) {
        this.bodyEn = bodyEn;
        return this;
    }

    public void setBodyEn(String bodyEn) {
        this.bodyEn = bodyEn;
    }

    public Boolean isFlagCorrect() {
        return flagCorrect;
    }

    public TestVariant flagCorrect(Boolean flagCorrect) {
        this.flagCorrect = flagCorrect;
        return this;
    }

    public void setFlagCorrect(Boolean flagCorrect) {
        this.flagCorrect = flagCorrect;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public TestVariant flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public TestQuestion getQuestion() {
        return question;
    }

    public TestVariant question(TestQuestion testQuestion) {
        this.question = testQuestion;
        return this;
    }

    public void setQuestion(TestQuestion testQuestion) {
        this.question = testQuestion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TestVariant testVariant = (TestVariant) o;
        if (testVariant.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), testVariant.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TestVariant{" +
            "id=" + getId() +
            ", bodyRu='" + getBodyRu() + "'" +
            ", bodyKk='" + getBodyKk() + "'" +
            ", bodyEn='" + getBodyEn() + "'" +
            ", flagCorrect='" + isFlagCorrect() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}
