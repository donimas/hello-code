package kz.ast.rs.hcode.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A CourseProgress.
 */
@Entity
@Table(name = "course_progress")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "courseprogress")
public class CourseProgress implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "passed_issues", precision=10, scale=2)
    private BigDecimal passedIssues;

    @Column(name = "total_issues", precision=10, scale=2)
    private BigDecimal totalIssues;

    @Column(name = "last_activity_date")
    private ZonedDateTime lastActivityDate;

    @ManyToOne
    private Course course;

    @ManyToOne
    private User user;

    @ManyToOne
    private CourseIssue lastIssue;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPassedIssues() {
        return passedIssues;
    }

    public CourseProgress passedIssues(BigDecimal passedIssues) {
        this.passedIssues = passedIssues;
        return this;
    }

    public void setPassedIssues(BigDecimal passedIssues) {
        this.passedIssues = passedIssues;
    }

    public BigDecimal getTotalIssues() {
        return totalIssues;
    }

    public CourseProgress totalIssues(BigDecimal totalIssues) {
        this.totalIssues = totalIssues;
        return this;
    }

    public void setTotalIssues(BigDecimal totalIssues) {
        this.totalIssues = totalIssues;
    }

    public ZonedDateTime getLastActivityDate() {
        return lastActivityDate;
    }

    public CourseProgress lastActivityDate(ZonedDateTime lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
        return this;
    }

    public void setLastActivityDate(ZonedDateTime lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }

    public Course getCourse() {
        return course;
    }

    public CourseProgress course(Course course) {
        this.course = course;
        return this;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public User getUser() {
        return user;
    }

    public CourseProgress user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CourseIssue getLastIssue() {
        return lastIssue;
    }

    public CourseProgress lastIssue(CourseIssue courseIssue) {
        this.lastIssue = courseIssue;
        return this;
    }

    public void setLastIssue(CourseIssue courseIssue) {
        this.lastIssue = courseIssue;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CourseProgress courseProgress = (CourseProgress) o;
        if (courseProgress.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), courseProgress.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CourseProgress{" +
            "id=" + getId() +
            ", passedIssues=" + getPassedIssues() +
            ", totalIssues=" + getTotalIssues() +
            ", lastActivityDate='" + getLastActivityDate() + "'" +
            "}";
    }
}
