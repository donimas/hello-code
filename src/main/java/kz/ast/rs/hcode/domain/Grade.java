package kz.ast.rs.hcode.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Grade.
 */
@Entity
@Table(name = "grade")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "grade")
public class Grade implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name_ru", nullable = false)
    private String nameRu;

    @NotNull
    @Column(name = "name_kk", nullable = false)
    private String nameKk;

    @Column(name = "name_en")
    private String nameEn;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @ManyToMany(mappedBy = "grades")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Program> programs = new HashSet<>();

    @ManyToMany(mappedBy = "grades")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Course> courses = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameRu() {
        return nameRu;
    }

    public Grade nameRu(String nameRu) {
        this.nameRu = nameRu;
        return this;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameKk() {
        return nameKk;
    }

    public Grade nameKk(String nameKk) {
        this.nameKk = nameKk;
        return this;
    }

    public void setNameKk(String nameKk) {
        this.nameKk = nameKk;
    }

    public String getNameEn() {
        return nameEn;
    }

    public Grade nameEn(String nameEn) {
        this.nameEn = nameEn;
        return this;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public Grade flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public Set<Program> getPrograms() {
        return programs;
    }

    public Grade programs(Set<Program> programs) {
        this.programs = programs;
        return this;
    }

    public Grade addProgram(Program program) {
        this.programs.add(program);
        program.getGrades().add(this);
        return this;
    }

    public Grade removeProgram(Program program) {
        this.programs.remove(program);
        program.getGrades().remove(this);
        return this;
    }

    public void setPrograms(Set<Program> programs) {
        this.programs = programs;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public Grade courses(Set<Course> courses) {
        this.courses = courses;
        return this;
    }

    public Grade addCourse(Course course) {
        this.courses.add(course);
        course.getGrades().add(this);
        return this;
    }

    public Grade removeCourse(Course course) {
        this.courses.remove(course);
        course.getGrades().remove(this);
        return this;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Grade grade = (Grade) o;
        if (grade.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), grade.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Grade{" +
            "id=" + getId() +
            ", nameRu='" + getNameRu() + "'" +
            ", nameKk='" + getNameKk() + "'" +
            ", nameEn='" + getNameEn() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}
