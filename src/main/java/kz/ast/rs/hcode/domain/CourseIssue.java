package kz.ast.rs.hcode.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import kz.ast.rs.hcode.domain.enumeration.IssueType;

/**
 * A CourseIssue.
 */
@Entity
@Table(name = "course_issue")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "courseissue")
public class CourseIssue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "issue_type", nullable = false)
    private IssueType issueType;

    @NotNull
    @Column(name = "name_ru", nullable = false)
    private String nameRu;

    @NotNull
    @Column(name = "name_kk", nullable = false)
    private String nameKk;

    @Column(name = "name_en")
    private String nameEn;

    @Column(name = "note_ru")
    private String noteRu;

    @Column(name = "note_kk")
    private String noteKk;

    @Column(name = "note_en")
    private String noteEn;

    @Column(name = "jhi_order", precision=10, scale=2)
    private BigDecimal order;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @ManyToOne
    private CourseChapter chapter;

    @ManyToOne
    private Course course;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IssueType getIssueType() {
        return issueType;
    }

    public CourseIssue issueType(IssueType issueType) {
        this.issueType = issueType;
        return this;
    }

    public void setIssueType(IssueType issueType) {
        this.issueType = issueType;
    }

    public String getNameRu() {
        return nameRu;
    }

    public CourseIssue nameRu(String nameRu) {
        this.nameRu = nameRu;
        return this;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameKk() {
        return nameKk;
    }

    public CourseIssue nameKk(String nameKk) {
        this.nameKk = nameKk;
        return this;
    }

    public void setNameKk(String nameKk) {
        this.nameKk = nameKk;
    }

    public String getNameEn() {
        return nameEn;
    }

    public CourseIssue nameEn(String nameEn) {
        this.nameEn = nameEn;
        return this;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNoteRu() {
        return noteRu;
    }

    public CourseIssue noteRu(String noteRu) {
        this.noteRu = noteRu;
        return this;
    }

    public void setNoteRu(String noteRu) {
        this.noteRu = noteRu;
    }

    public String getNoteKk() {
        return noteKk;
    }

    public CourseIssue noteKk(String noteKk) {
        this.noteKk = noteKk;
        return this;
    }

    public void setNoteKk(String noteKk) {
        this.noteKk = noteKk;
    }

    public String getNoteEn() {
        return noteEn;
    }

    public CourseIssue noteEn(String noteEn) {
        this.noteEn = noteEn;
        return this;
    }

    public void setNoteEn(String noteEn) {
        this.noteEn = noteEn;
    }

    public BigDecimal getOrder() {
        return order;
    }

    public CourseIssue order(BigDecimal order) {
        this.order = order;
        return this;
    }

    public void setOrder(BigDecimal order) {
        this.order = order;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public CourseIssue flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public CourseChapter getChapter() {
        return chapter;
    }

    public CourseIssue chapter(CourseChapter courseChapter) {
        this.chapter = courseChapter;
        return this;
    }

    public void setChapter(CourseChapter courseChapter) {
        this.chapter = courseChapter;
    }

    public Course getCourse() {
        return course;
    }

    public CourseIssue course(Course course) {
        this.course = course;
        return this;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CourseIssue courseIssue = (CourseIssue) o;
        if (courseIssue.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), courseIssue.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CourseIssue{" +
            "id=" + getId() +
            ", issueType='" + getIssueType() + "'" +
            ", nameRu='" + getNameRu() + "'" +
            ", nameKk='" + getNameKk() + "'" +
            ", nameEn='" + getNameEn() + "'" +
            ", noteRu='" + getNoteRu() + "'" +
            ", noteKk='" + getNoteKk() + "'" +
            ", noteEn='" + getNoteEn() + "'" +
            ", order=" + getOrder() +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}
