package kz.ast.rs.hcode.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SkratchIssue.
 */
@Entity
@Table(name = "skratch_issue")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "skratchissue")
public class SkratchIssue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "url")
    private String url;

    @Column(name = "note")
    private String note;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @ManyToOne
    private CourseIssue issue;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public SkratchIssue url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNote() {
        return note;
    }

    public SkratchIssue note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public SkratchIssue flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public CourseIssue getIssue() {
        return issue;
    }

    public SkratchIssue issue(CourseIssue courseIssue) {
        this.issue = courseIssue;
        return this;
    }

    public void setIssue(CourseIssue courseIssue) {
        this.issue = courseIssue;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SkratchIssue skratchIssue = (SkratchIssue) o;
        if (skratchIssue.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), skratchIssue.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SkratchIssue{" +
            "id=" + getId() +
            ", url='" + getUrl() + "'" +
            ", note='" + getNote() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}
