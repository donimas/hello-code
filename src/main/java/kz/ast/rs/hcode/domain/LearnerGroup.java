package kz.ast.rs.hcode.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A LearnerGroup.
 */
@Entity
@Table(name = "learner_group")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "learnergroup")
public class LearnerGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name_ru", nullable = false)
    private String nameRu;

    @NotNull
    @Column(name = "name_kk", nullable = false)
    private String nameKk;

    @Column(name = "name_en")
    private String nameEn;

    @Column(name = "description_ru")
    private String descriptionRu;

    @Column(name = "description_kk")
    private String descriptionKk;

    @Column(name = "desciprtion_en")
    private String desciprtionEn;

    @Column(name = "graduation_year")
    private Integer graduationYear;

    @ManyToOne
    private User teacher;

    @ManyToOne
    private School school;

    @ManyToOne
    private Grade grade;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "learner_group_learner",
               joinColumns = @JoinColumn(name="learner_groups_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="learners_id", referencedColumnName="id"))
    private Set<User> learners = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameRu() {
        return nameRu;
    }

    public LearnerGroup nameRu(String nameRu) {
        this.nameRu = nameRu;
        return this;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameKk() {
        return nameKk;
    }

    public LearnerGroup nameKk(String nameKk) {
        this.nameKk = nameKk;
        return this;
    }

    public void setNameKk(String nameKk) {
        this.nameKk = nameKk;
    }

    public String getNameEn() {
        return nameEn;
    }

    public LearnerGroup nameEn(String nameEn) {
        this.nameEn = nameEn;
        return this;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getDescriptionRu() {
        return descriptionRu;
    }

    public LearnerGroup descriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
        return this;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    public String getDescriptionKk() {
        return descriptionKk;
    }

    public LearnerGroup descriptionKk(String descriptionKk) {
        this.descriptionKk = descriptionKk;
        return this;
    }

    public void setDescriptionKk(String descriptionKk) {
        this.descriptionKk = descriptionKk;
    }

    public String getDesciprtionEn() {
        return desciprtionEn;
    }

    public LearnerGroup desciprtionEn(String desciprtionEn) {
        this.desciprtionEn = desciprtionEn;
        return this;
    }

    public void setDesciprtionEn(String desciprtionEn) {
        this.desciprtionEn = desciprtionEn;
    }

    public Integer getGraduationYear() {
        return graduationYear;
    }

    public LearnerGroup graduationYear(Integer graduationYear) {
        this.graduationYear = graduationYear;
        return this;
    }

    public void setGraduationYear(Integer graduationYear) {
        this.graduationYear = graduationYear;
    }

    public User getTeacher() {
        return teacher;
    }

    public LearnerGroup teacher(User user) {
        this.teacher = user;
        return this;
    }

    public void setTeacher(User user) {
        this.teacher = user;
    }

    public School getSchool() {
        return school;
    }

    public LearnerGroup school(School school) {
        this.school = school;
        return this;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Grade getGrade() {
        return grade;
    }

    public LearnerGroup grade(Grade grade) {
        this.grade = grade;
        return this;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    public Set<User> getLearners() {
        return learners;
    }

    public LearnerGroup learners(Set<User> users) {
        this.learners = users;
        return this;
    }

    public LearnerGroup addLearner(User user) {
        this.learners.add(user);
        return this;
    }

    public LearnerGroup removeLearner(User user) {
        this.learners.remove(user);
        return this;
    }

    public void setLearners(Set<User> users) {
        this.learners = users;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LearnerGroup learnerGroup = (LearnerGroup) o;
        if (learnerGroup.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), learnerGroup.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LearnerGroup{" +
            "id=" + getId() +
            ", nameRu='" + getNameRu() + "'" +
            ", nameKk='" + getNameKk() + "'" +
            ", nameEn='" + getNameEn() + "'" +
            ", descriptionRu='" + getDescriptionRu() + "'" +
            ", descriptionKk='" + getDescriptionKk() + "'" +
            ", desciprtionEn='" + getDesciprtionEn() + "'" +
            ", graduationYear=" + getGraduationYear() +
            "}";
    }
}
