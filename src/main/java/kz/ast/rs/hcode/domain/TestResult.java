package kz.ast.rs.hcode.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TestResult.
 */
@Entity
@Table(name = "test_result")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "testresult")
public class TestResult implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "flag_correct", nullable = false)
    private Boolean flagCorrect;

    @ManyToOne
    private TestQuestion question;

    @ManyToOne
    private TestVariant response;

    @ManyToOne
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isFlagCorrect() {
        return flagCorrect;
    }

    public TestResult flagCorrect(Boolean flagCorrect) {
        this.flagCorrect = flagCorrect;
        return this;
    }

    public void setFlagCorrect(Boolean flagCorrect) {
        this.flagCorrect = flagCorrect;
    }

    public TestQuestion getQuestion() {
        return question;
    }

    public TestResult question(TestQuestion testQuestion) {
        this.question = testQuestion;
        return this;
    }

    public void setQuestion(TestQuestion testQuestion) {
        this.question = testQuestion;
    }

    public TestVariant getResponse() {
        return response;
    }

    public TestResult response(TestVariant testVariant) {
        this.response = testVariant;
        return this;
    }

    public void setResponse(TestVariant testVariant) {
        this.response = testVariant;
    }

    public User getUser() {
        return user;
    }

    public TestResult user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TestResult testResult = (TestResult) o;
        if (testResult.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), testResult.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TestResult{" +
            "id=" + getId() +
            ", flagCorrect='" + isFlagCorrect() + "'" +
            "}";
    }
}
