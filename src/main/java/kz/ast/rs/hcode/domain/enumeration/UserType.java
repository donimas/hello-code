package kz.ast.rs.hcode.domain.enumeration;

/**
 * The UserType enumeration.
 */
public enum UserType {
    LEARNER, TEACHER, ADMIN, PARENT
}
