package kz.ast.rs.hcode.domain.enumeration;

/**
 * The IssueType enumeration.
 */
public enum IssueType {
    VIDEO, TEST, TEXT, WEBINAR, SKRATCH
}
