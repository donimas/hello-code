package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.WebinarIssue;
import kz.ast.rs.hcode.repository.WebinarIssueRepository;
import kz.ast.rs.hcode.repository.search.WebinarIssueSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WebinarIssue.
 */
@Service
@Transactional
public class WebinarIssueService {

    private final Logger log = LoggerFactory.getLogger(WebinarIssueService.class);

    private final WebinarIssueRepository webinarIssueRepository;

    private final WebinarIssueSearchRepository webinarIssueSearchRepository;

    public WebinarIssueService(WebinarIssueRepository webinarIssueRepository, WebinarIssueSearchRepository webinarIssueSearchRepository) {
        this.webinarIssueRepository = webinarIssueRepository;
        this.webinarIssueSearchRepository = webinarIssueSearchRepository;
    }

    /**
     * Save a webinarIssue.
     *
     * @param webinarIssue the entity to save
     * @return the persisted entity
     */
    public WebinarIssue save(WebinarIssue webinarIssue) {
        log.debug("Request to save WebinarIssue : {}", webinarIssue);
        WebinarIssue result = webinarIssueRepository.save(webinarIssue);
        webinarIssueSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the webinarIssues.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WebinarIssue> findAll(Pageable pageable) {
        log.debug("Request to get all WebinarIssues");
        return webinarIssueRepository.findAll(pageable);
    }

    /**
     * Get one webinarIssue by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public WebinarIssue findOne(Long id) {
        log.debug("Request to get WebinarIssue : {}", id);
        return webinarIssueRepository.findOne(id);
    }

    /**
     * Delete the webinarIssue by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete WebinarIssue : {}", id);
        webinarIssueRepository.delete(id);
        webinarIssueSearchRepository.delete(id);
    }

    /**
     * Search for the webinarIssue corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<WebinarIssue> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WebinarIssues for query {}", query);
        Page<WebinarIssue> result = webinarIssueSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
