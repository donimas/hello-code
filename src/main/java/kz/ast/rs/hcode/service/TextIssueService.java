package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.TextIssue;
import kz.ast.rs.hcode.repository.TextIssueRepository;
import kz.ast.rs.hcode.repository.search.TextIssueSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TextIssue.
 */
@Service
@Transactional
public class TextIssueService {

    private final Logger log = LoggerFactory.getLogger(TextIssueService.class);

    private final TextIssueRepository textIssueRepository;

    private final TextIssueSearchRepository textIssueSearchRepository;

    public TextIssueService(TextIssueRepository textIssueRepository, TextIssueSearchRepository textIssueSearchRepository) {
        this.textIssueRepository = textIssueRepository;
        this.textIssueSearchRepository = textIssueSearchRepository;
    }

    /**
     * Save a textIssue.
     *
     * @param textIssue the entity to save
     * @return the persisted entity
     */
    public TextIssue save(TextIssue textIssue) {
        log.debug("Request to save TextIssue : {}", textIssue);
        TextIssue result = textIssueRepository.save(textIssue);
        textIssueSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the textIssues.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TextIssue> findAll(Pageable pageable) {
        log.debug("Request to get all TextIssues");
        return textIssueRepository.findAll(pageable);
    }

    /**
     * Get one textIssue by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public TextIssue findOne(Long id) {
        log.debug("Request to get TextIssue : {}", id);
        return textIssueRepository.findOne(id);
    }

    /**
     * Delete the textIssue by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TextIssue : {}", id);
        textIssueRepository.delete(id);
        textIssueSearchRepository.delete(id);
    }

    /**
     * Search for the textIssue corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TextIssue> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TextIssues for query {}", query);
        Page<TextIssue> result = textIssueSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
