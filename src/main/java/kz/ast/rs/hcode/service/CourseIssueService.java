package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.CourseIssue;
import kz.ast.rs.hcode.repository.CourseIssueRepository;
import kz.ast.rs.hcode.repository.search.CourseIssueSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing CourseIssue.
 */
@Service
@Transactional
public class CourseIssueService {

    private final Logger log = LoggerFactory.getLogger(CourseIssueService.class);

    private final CourseIssueRepository courseIssueRepository;

    private final CourseIssueSearchRepository courseIssueSearchRepository;

    public CourseIssueService(CourseIssueRepository courseIssueRepository, CourseIssueSearchRepository courseIssueSearchRepository) {
        this.courseIssueRepository = courseIssueRepository;
        this.courseIssueSearchRepository = courseIssueSearchRepository;
    }

    /**
     * Save a courseIssue.
     *
     * @param courseIssue the entity to save
     * @return the persisted entity
     */
    public CourseIssue save(CourseIssue courseIssue) {
        log.debug("Request to save CourseIssue : {}", courseIssue);
        CourseIssue result = courseIssueRepository.save(courseIssue);
        courseIssueSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the courseIssues.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CourseIssue> findAll(Pageable pageable) {
        log.debug("Request to get all CourseIssues");
        return courseIssueRepository.findAll(pageable);
    }

    /**
     * Get one courseIssue by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CourseIssue findOne(Long id) {
        log.debug("Request to get CourseIssue : {}", id);
        return courseIssueRepository.findOne(id);
    }

    /**
     * Delete the courseIssue by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CourseIssue : {}", id);
        courseIssueRepository.delete(id);
        courseIssueSearchRepository.delete(id);
    }

    /**
     * Search for the courseIssue corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CourseIssue> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CourseIssues for query {}", query);
        Page<CourseIssue> result = courseIssueSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
