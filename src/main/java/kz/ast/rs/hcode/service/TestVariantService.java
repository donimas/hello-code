package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.TestVariant;
import kz.ast.rs.hcode.repository.TestVariantRepository;
import kz.ast.rs.hcode.repository.search.TestVariantSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TestVariant.
 */
@Service
@Transactional
public class TestVariantService {

    private final Logger log = LoggerFactory.getLogger(TestVariantService.class);

    private final TestVariantRepository testVariantRepository;

    private final TestVariantSearchRepository testVariantSearchRepository;

    public TestVariantService(TestVariantRepository testVariantRepository, TestVariantSearchRepository testVariantSearchRepository) {
        this.testVariantRepository = testVariantRepository;
        this.testVariantSearchRepository = testVariantSearchRepository;
    }

    /**
     * Save a testVariant.
     *
     * @param testVariant the entity to save
     * @return the persisted entity
     */
    public TestVariant save(TestVariant testVariant) {
        log.debug("Request to save TestVariant : {}", testVariant);
        TestVariant result = testVariantRepository.save(testVariant);
        testVariantSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the testVariants.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<TestVariant> findAll() {
        log.debug("Request to get all TestVariants");
        return testVariantRepository.findAll();
    }

    /**
     * Get one testVariant by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public TestVariant findOne(Long id) {
        log.debug("Request to get TestVariant : {}", id);
        return testVariantRepository.findOne(id);
    }

    /**
     * Delete the testVariant by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TestVariant : {}", id);
        testVariantRepository.delete(id);
        testVariantSearchRepository.delete(id);
    }

    /**
     * Search for the testVariant corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<TestVariant> search(String query) {
        log.debug("Request to search TestVariants for query {}", query);
        return StreamSupport
            .stream(testVariantSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
