package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.LearnerGroup;
import kz.ast.rs.hcode.repository.LearnerGroupRepository;
import kz.ast.rs.hcode.repository.search.LearnerGroupSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing LearnerGroup.
 */
@Service
@Transactional
public class LearnerGroupService {

    private final Logger log = LoggerFactory.getLogger(LearnerGroupService.class);

    private final LearnerGroupRepository learnerGroupRepository;

    private final LearnerGroupSearchRepository learnerGroupSearchRepository;

    public LearnerGroupService(LearnerGroupRepository learnerGroupRepository, LearnerGroupSearchRepository learnerGroupSearchRepository) {
        this.learnerGroupRepository = learnerGroupRepository;
        this.learnerGroupSearchRepository = learnerGroupSearchRepository;
    }

    /**
     * Save a learnerGroup.
     *
     * @param learnerGroup the entity to save
     * @return the persisted entity
     */
    public LearnerGroup save(LearnerGroup learnerGroup) {
        log.debug("Request to save LearnerGroup : {}", learnerGroup);
        LearnerGroup result = learnerGroupRepository.save(learnerGroup);
        learnerGroupSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the learnerGroups.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LearnerGroup> findAll(Pageable pageable) {
        log.debug("Request to get all LearnerGroups");
        return learnerGroupRepository.findAll(pageable);
    }

    /**
     * Get one learnerGroup by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public LearnerGroup findOne(Long id) {
        log.debug("Request to get LearnerGroup : {}", id);
        return learnerGroupRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the learnerGroup by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete LearnerGroup : {}", id);
        learnerGroupRepository.delete(id);
        learnerGroupSearchRepository.delete(id);
    }

    /**
     * Search for the learnerGroup corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LearnerGroup> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of LearnerGroups for query {}", query);
        Page<LearnerGroup> result = learnerGroupSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
