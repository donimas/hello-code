package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.TestResult;
import kz.ast.rs.hcode.repository.TestResultRepository;
import kz.ast.rs.hcode.repository.search.TestResultSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TestResult.
 */
@Service
@Transactional
public class TestResultService {

    private final Logger log = LoggerFactory.getLogger(TestResultService.class);

    private final TestResultRepository testResultRepository;

    private final TestResultSearchRepository testResultSearchRepository;

    public TestResultService(TestResultRepository testResultRepository, TestResultSearchRepository testResultSearchRepository) {
        this.testResultRepository = testResultRepository;
        this.testResultSearchRepository = testResultSearchRepository;
    }

    /**
     * Save a testResult.
     *
     * @param testResult the entity to save
     * @return the persisted entity
     */
    public TestResult save(TestResult testResult) {
        log.debug("Request to save TestResult : {}", testResult);
        TestResult result = testResultRepository.save(testResult);
        testResultSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the testResults.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<TestResult> findAll() {
        log.debug("Request to get all TestResults");
        return testResultRepository.findAll();
    }

    /**
     * Get one testResult by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public TestResult findOne(Long id) {
        log.debug("Request to get TestResult : {}", id);
        return testResultRepository.findOne(id);
    }

    /**
     * Delete the testResult by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TestResult : {}", id);
        testResultRepository.delete(id);
        testResultSearchRepository.delete(id);
    }

    /**
     * Search for the testResult corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<TestResult> search(String query) {
        log.debug("Request to search TestResults for query {}", query);
        return StreamSupport
            .stream(testResultSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
