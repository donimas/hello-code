package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.ActivityResult;
import kz.ast.rs.hcode.repository.ActivityResultRepository;
import kz.ast.rs.hcode.repository.search.ActivityResultSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ActivityResult.
 */
@Service
@Transactional
public class ActivityResultService {

    private final Logger log = LoggerFactory.getLogger(ActivityResultService.class);

    private final ActivityResultRepository activityResultRepository;

    private final ActivityResultSearchRepository activityResultSearchRepository;

    public ActivityResultService(ActivityResultRepository activityResultRepository, ActivityResultSearchRepository activityResultSearchRepository) {
        this.activityResultRepository = activityResultRepository;
        this.activityResultSearchRepository = activityResultSearchRepository;
    }

    /**
     * Save a activityResult.
     *
     * @param activityResult the entity to save
     * @return the persisted entity
     */
    public ActivityResult save(ActivityResult activityResult) {
        log.debug("Request to save ActivityResult : {}", activityResult);
        ActivityResult result = activityResultRepository.save(activityResult);
        activityResultSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the activityResults.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ActivityResult> findAll() {
        log.debug("Request to get all ActivityResults");
        return activityResultRepository.findAll();
    }

    /**
     * Get one activityResult by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ActivityResult findOne(Long id) {
        log.debug("Request to get ActivityResult : {}", id);
        return activityResultRepository.findOne(id);
    }

    /**
     * Delete the activityResult by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ActivityResult : {}", id);
        activityResultRepository.delete(id);
        activityResultSearchRepository.delete(id);
    }

    /**
     * Search for the activityResult corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ActivityResult> search(String query) {
        log.debug("Request to search ActivityResults for query {}", query);
        return StreamSupport
            .stream(activityResultSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
