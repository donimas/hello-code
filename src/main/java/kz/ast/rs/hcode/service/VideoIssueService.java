package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.VideoIssue;
import kz.ast.rs.hcode.repository.VideoIssueRepository;
import kz.ast.rs.hcode.repository.search.VideoIssueSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing VideoIssue.
 */
@Service
@Transactional
public class VideoIssueService {

    private final Logger log = LoggerFactory.getLogger(VideoIssueService.class);

    private final VideoIssueRepository videoIssueRepository;

    private final VideoIssueSearchRepository videoIssueSearchRepository;

    public VideoIssueService(VideoIssueRepository videoIssueRepository, VideoIssueSearchRepository videoIssueSearchRepository) {
        this.videoIssueRepository = videoIssueRepository;
        this.videoIssueSearchRepository = videoIssueSearchRepository;
    }

    /**
     * Save a videoIssue.
     *
     * @param videoIssue the entity to save
     * @return the persisted entity
     */
    public VideoIssue save(VideoIssue videoIssue) {
        log.debug("Request to save VideoIssue : {}", videoIssue);
        VideoIssue result = videoIssueRepository.save(videoIssue);
        videoIssueSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the videoIssues.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VideoIssue> findAll(Pageable pageable) {
        log.debug("Request to get all VideoIssues");
        return videoIssueRepository.findAll(pageable);
    }

    /**
     * Get one videoIssue by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public VideoIssue findOne(Long id) {
        log.debug("Request to get VideoIssue : {}", id);
        return videoIssueRepository.findOne(id);
    }

    /**
     * Delete the videoIssue by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete VideoIssue : {}", id);
        videoIssueRepository.delete(id);
        videoIssueSearchRepository.delete(id);
    }

    /**
     * Search for the videoIssue corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<VideoIssue> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of VideoIssues for query {}", query);
        Page<VideoIssue> result = videoIssueSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
