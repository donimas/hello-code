package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.ActivityStatusHistory;
import kz.ast.rs.hcode.repository.ActivityStatusHistoryRepository;
import kz.ast.rs.hcode.repository.search.ActivityStatusHistorySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ActivityStatusHistory.
 */
@Service
@Transactional
public class ActivityStatusHistoryService {

    private final Logger log = LoggerFactory.getLogger(ActivityStatusHistoryService.class);

    private final ActivityStatusHistoryRepository activityStatusHistoryRepository;

    private final ActivityStatusHistorySearchRepository activityStatusHistorySearchRepository;

    public ActivityStatusHistoryService(ActivityStatusHistoryRepository activityStatusHistoryRepository, ActivityStatusHistorySearchRepository activityStatusHistorySearchRepository) {
        this.activityStatusHistoryRepository = activityStatusHistoryRepository;
        this.activityStatusHistorySearchRepository = activityStatusHistorySearchRepository;
    }

    /**
     * Save a activityStatusHistory.
     *
     * @param activityStatusHistory the entity to save
     * @return the persisted entity
     */
    public ActivityStatusHistory save(ActivityStatusHistory activityStatusHistory) {
        log.debug("Request to save ActivityStatusHistory : {}", activityStatusHistory);
        ActivityStatusHistory result = activityStatusHistoryRepository.save(activityStatusHistory);
        activityStatusHistorySearchRepository.save(result);
        return result;
    }

    /**
     * Get all the activityStatusHistories.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ActivityStatusHistory> findAll(Pageable pageable) {
        log.debug("Request to get all ActivityStatusHistories");
        return activityStatusHistoryRepository.findAll(pageable);
    }

    /**
     * Get one activityStatusHistory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ActivityStatusHistory findOne(Long id) {
        log.debug("Request to get ActivityStatusHistory : {}", id);
        return activityStatusHistoryRepository.findOne(id);
    }

    /**
     * Delete the activityStatusHistory by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ActivityStatusHistory : {}", id);
        activityStatusHistoryRepository.delete(id);
        activityStatusHistorySearchRepository.delete(id);
    }

    /**
     * Search for the activityStatusHistory corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ActivityStatusHistory> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ActivityStatusHistories for query {}", query);
        Page<ActivityStatusHistory> result = activityStatusHistorySearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
