package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.CourseProgress;
import kz.ast.rs.hcode.repository.CourseProgressRepository;
import kz.ast.rs.hcode.repository.search.CourseProgressSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing CourseProgress.
 */
@Service
@Transactional
public class CourseProgressService {

    private final Logger log = LoggerFactory.getLogger(CourseProgressService.class);

    private final CourseProgressRepository courseProgressRepository;

    private final CourseProgressSearchRepository courseProgressSearchRepository;

    public CourseProgressService(CourseProgressRepository courseProgressRepository, CourseProgressSearchRepository courseProgressSearchRepository) {
        this.courseProgressRepository = courseProgressRepository;
        this.courseProgressSearchRepository = courseProgressSearchRepository;
    }

    /**
     * Save a courseProgress.
     *
     * @param courseProgress the entity to save
     * @return the persisted entity
     */
    public CourseProgress save(CourseProgress courseProgress) {
        log.debug("Request to save CourseProgress : {}", courseProgress);
        CourseProgress result = courseProgressRepository.save(courseProgress);
        courseProgressSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the courseProgresses.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CourseProgress> findAll(Pageable pageable) {
        log.debug("Request to get all CourseProgresses");
        return courseProgressRepository.findAll(pageable);
    }

    /**
     * Get one courseProgress by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CourseProgress findOne(Long id) {
        log.debug("Request to get CourseProgress : {}", id);
        return courseProgressRepository.findOne(id);
    }

    /**
     * Delete the courseProgress by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CourseProgress : {}", id);
        courseProgressRepository.delete(id);
        courseProgressSearchRepository.delete(id);
    }

    /**
     * Search for the courseProgress corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CourseProgress> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CourseProgresses for query {}", query);
        Page<CourseProgress> result = courseProgressSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
