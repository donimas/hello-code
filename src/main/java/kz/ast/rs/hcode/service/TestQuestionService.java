package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.TestQuestion;
import kz.ast.rs.hcode.repository.TestQuestionRepository;
import kz.ast.rs.hcode.repository.search.TestQuestionSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TestQuestion.
 */
@Service
@Transactional
public class TestQuestionService {

    private final Logger log = LoggerFactory.getLogger(TestQuestionService.class);

    private final TestQuestionRepository testQuestionRepository;

    private final TestQuestionSearchRepository testQuestionSearchRepository;

    public TestQuestionService(TestQuestionRepository testQuestionRepository, TestQuestionSearchRepository testQuestionSearchRepository) {
        this.testQuestionRepository = testQuestionRepository;
        this.testQuestionSearchRepository = testQuestionSearchRepository;
    }

    /**
     * Save a testQuestion.
     *
     * @param testQuestion the entity to save
     * @return the persisted entity
     */
    public TestQuestion save(TestQuestion testQuestion) {
        log.debug("Request to save TestQuestion : {}", testQuestion);
        TestQuestion result = testQuestionRepository.save(testQuestion);
        testQuestionSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the testQuestions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TestQuestion> findAll(Pageable pageable) {
        log.debug("Request to get all TestQuestions");
        return testQuestionRepository.findAll(pageable);
    }

    /**
     * Get one testQuestion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public TestQuestion findOne(Long id) {
        log.debug("Request to get TestQuestion : {}", id);
        return testQuestionRepository.findOne(id);
    }

    /**
     * Delete the testQuestion by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TestQuestion : {}", id);
        testQuestionRepository.delete(id);
        testQuestionSearchRepository.delete(id);
    }

    /**
     * Search for the testQuestion corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TestQuestion> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TestQuestions for query {}", query);
        Page<TestQuestion> result = testQuestionSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
