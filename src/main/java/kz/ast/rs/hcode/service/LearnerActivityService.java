package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.LearnerActivity;
import kz.ast.rs.hcode.repository.LearnerActivityRepository;
import kz.ast.rs.hcode.repository.search.LearnerActivitySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing LearnerActivity.
 */
@Service
@Transactional
public class LearnerActivityService {

    private final Logger log = LoggerFactory.getLogger(LearnerActivityService.class);

    private final LearnerActivityRepository learnerActivityRepository;

    private final LearnerActivitySearchRepository learnerActivitySearchRepository;

    public LearnerActivityService(LearnerActivityRepository learnerActivityRepository, LearnerActivitySearchRepository learnerActivitySearchRepository) {
        this.learnerActivityRepository = learnerActivityRepository;
        this.learnerActivitySearchRepository = learnerActivitySearchRepository;
    }

    /**
     * Save a learnerActivity.
     *
     * @param learnerActivity the entity to save
     * @return the persisted entity
     */
    public LearnerActivity save(LearnerActivity learnerActivity) {
        log.debug("Request to save LearnerActivity : {}", learnerActivity);
        LearnerActivity result = learnerActivityRepository.save(learnerActivity);
        learnerActivitySearchRepository.save(result);
        return result;
    }

    /**
     * Get all the learnerActivities.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LearnerActivity> findAll(Pageable pageable) {
        log.debug("Request to get all LearnerActivities");
        return learnerActivityRepository.findAll(pageable);
    }

    /**
     * Get one learnerActivity by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public LearnerActivity findOne(Long id) {
        log.debug("Request to get LearnerActivity : {}", id);
        return learnerActivityRepository.findOne(id);
    }

    /**
     * Delete the learnerActivity by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete LearnerActivity : {}", id);
        learnerActivityRepository.delete(id);
        learnerActivitySearchRepository.delete(id);
    }

    /**
     * Search for the learnerActivity corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LearnerActivity> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of LearnerActivities for query {}", query);
        Page<LearnerActivity> result = learnerActivitySearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
