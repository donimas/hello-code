package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.CourseChapter;
import kz.ast.rs.hcode.repository.CourseChapterRepository;
import kz.ast.rs.hcode.repository.search.CourseChapterSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing CourseChapter.
 */
@Service
@Transactional
public class CourseChapterService {

    private final Logger log = LoggerFactory.getLogger(CourseChapterService.class);

    private final CourseChapterRepository courseChapterRepository;

    private final CourseChapterSearchRepository courseChapterSearchRepository;

    public CourseChapterService(CourseChapterRepository courseChapterRepository, CourseChapterSearchRepository courseChapterSearchRepository) {
        this.courseChapterRepository = courseChapterRepository;
        this.courseChapterSearchRepository = courseChapterSearchRepository;
    }

    /**
     * Save a courseChapter.
     *
     * @param courseChapter the entity to save
     * @return the persisted entity
     */
    public CourseChapter save(CourseChapter courseChapter) {
        log.debug("Request to save CourseChapter : {}", courseChapter);
        CourseChapter result = courseChapterRepository.save(courseChapter);
        courseChapterSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the courseChapters.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CourseChapter> findAll(Pageable pageable) {
        log.debug("Request to get all CourseChapters");
        return courseChapterRepository.findAll(pageable);
    }

    /**
     * Get one courseChapter by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CourseChapter findOne(Long id) {
        log.debug("Request to get CourseChapter : {}", id);
        return courseChapterRepository.findOne(id);
    }

    /**
     * Delete the courseChapter by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CourseChapter : {}", id);
        courseChapterRepository.delete(id);
        courseChapterSearchRepository.delete(id);
    }

    /**
     * Search for the courseChapter corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CourseChapter> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CourseChapters for query {}", query);
        Page<CourseChapter> result = courseChapterSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
