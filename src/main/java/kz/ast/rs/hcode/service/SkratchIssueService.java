package kz.ast.rs.hcode.service;

import kz.ast.rs.hcode.domain.SkratchIssue;
import kz.ast.rs.hcode.repository.SkratchIssueRepository;
import kz.ast.rs.hcode.repository.search.SkratchIssueSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SkratchIssue.
 */
@Service
@Transactional
public class SkratchIssueService {

    private final Logger log = LoggerFactory.getLogger(SkratchIssueService.class);

    private final SkratchIssueRepository skratchIssueRepository;

    private final SkratchIssueSearchRepository skratchIssueSearchRepository;

    public SkratchIssueService(SkratchIssueRepository skratchIssueRepository, SkratchIssueSearchRepository skratchIssueSearchRepository) {
        this.skratchIssueRepository = skratchIssueRepository;
        this.skratchIssueSearchRepository = skratchIssueSearchRepository;
    }

    /**
     * Save a skratchIssue.
     *
     * @param skratchIssue the entity to save
     * @return the persisted entity
     */
    public SkratchIssue save(SkratchIssue skratchIssue) {
        log.debug("Request to save SkratchIssue : {}", skratchIssue);
        SkratchIssue result = skratchIssueRepository.save(skratchIssue);
        skratchIssueSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the skratchIssues.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SkratchIssue> findAll(Pageable pageable) {
        log.debug("Request to get all SkratchIssues");
        return skratchIssueRepository.findAll(pageable);
    }

    /**
     * Get one skratchIssue by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SkratchIssue findOne(Long id) {
        log.debug("Request to get SkratchIssue : {}", id);
        return skratchIssueRepository.findOne(id);
    }

    /**
     * Delete the skratchIssue by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SkratchIssue : {}", id);
        skratchIssueRepository.delete(id);
        skratchIssueSearchRepository.delete(id);
    }

    /**
     * Search for the skratchIssue corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SkratchIssue> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SkratchIssues for query {}", query);
        Page<SkratchIssue> result = skratchIssueSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
