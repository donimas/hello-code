package kz.ast.rs.hcode.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(kz.ast.rs.hcode.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.SocialUserConnection.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.UserInfo.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.School.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.LearnerGroup.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.LearnerGroup.class.getName() + ".learners", jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Grade.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Grade.class.getName() + ".programs", jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Grade.class.getName() + ".courses", jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Category.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Category.class.getName() + ".courses", jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Program.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Program.class.getName() + ".courses", jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Program.class.getName() + ".grades", jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Course.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Course.class.getName() + ".grades", jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Course.class.getName() + ".categories", jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.Course.class.getName() + ".courseChapters", jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.CourseChapter.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.CourseChapter.class.getName() + ".issues", jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.CourseIssue.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.LearnerActivity.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.ActivityStatusHistory.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.ActivityResult.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.TestQuestion.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.TestQuestion.class.getName() + ".variants", jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.TestVariant.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.TestResult.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.TextIssue.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.SkratchIssue.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.WebinarIssue.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.VideoIssue.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.rs.hcode.domain.CourseProgress.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
