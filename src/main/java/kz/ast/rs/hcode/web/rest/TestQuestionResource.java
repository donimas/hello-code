package kz.ast.rs.hcode.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.rs.hcode.domain.TestQuestion;
import kz.ast.rs.hcode.service.TestQuestionService;
import kz.ast.rs.hcode.web.rest.errors.BadRequestAlertException;
import kz.ast.rs.hcode.web.rest.util.HeaderUtil;
import kz.ast.rs.hcode.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TestQuestion.
 */
@RestController
@RequestMapping("/api")
public class TestQuestionResource {

    private final Logger log = LoggerFactory.getLogger(TestQuestionResource.class);

    private static final String ENTITY_NAME = "testQuestion";

    private final TestQuestionService testQuestionService;

    public TestQuestionResource(TestQuestionService testQuestionService) {
        this.testQuestionService = testQuestionService;
    }

    /**
     * POST  /test-questions : Create a new testQuestion.
     *
     * @param testQuestion the testQuestion to create
     * @return the ResponseEntity with status 201 (Created) and with body the new testQuestion, or with status 400 (Bad Request) if the testQuestion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/test-questions")
    @Timed
    public ResponseEntity<TestQuestion> createTestQuestion(@RequestBody TestQuestion testQuestion) throws URISyntaxException {
        log.debug("REST request to save TestQuestion : {}", testQuestion);
        if (testQuestion.getId() != null) {
            throw new BadRequestAlertException("A new testQuestion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TestQuestion result = testQuestionService.save(testQuestion);
        return ResponseEntity.created(new URI("/api/test-questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /test-questions : Updates an existing testQuestion.
     *
     * @param testQuestion the testQuestion to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated testQuestion,
     * or with status 400 (Bad Request) if the testQuestion is not valid,
     * or with status 500 (Internal Server Error) if the testQuestion couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/test-questions")
    @Timed
    public ResponseEntity<TestQuestion> updateTestQuestion(@RequestBody TestQuestion testQuestion) throws URISyntaxException {
        log.debug("REST request to update TestQuestion : {}", testQuestion);
        if (testQuestion.getId() == null) {
            return createTestQuestion(testQuestion);
        }
        TestQuestion result = testQuestionService.save(testQuestion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, testQuestion.getId().toString()))
            .body(result);
    }

    /**
     * GET  /test-questions : get all the testQuestions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of testQuestions in body
     */
    @GetMapping("/test-questions")
    @Timed
    public ResponseEntity<List<TestQuestion>> getAllTestQuestions(Pageable pageable) {
        log.debug("REST request to get a page of TestQuestions");
        Page<TestQuestion> page = testQuestionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/test-questions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /test-questions/:id : get the "id" testQuestion.
     *
     * @param id the id of the testQuestion to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the testQuestion, or with status 404 (Not Found)
     */
    @GetMapping("/test-questions/{id}")
    @Timed
    public ResponseEntity<TestQuestion> getTestQuestion(@PathVariable Long id) {
        log.debug("REST request to get TestQuestion : {}", id);
        TestQuestion testQuestion = testQuestionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(testQuestion));
    }

    /**
     * DELETE  /test-questions/:id : delete the "id" testQuestion.
     *
     * @param id the id of the testQuestion to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/test-questions/{id}")
    @Timed
    public ResponseEntity<Void> deleteTestQuestion(@PathVariable Long id) {
        log.debug("REST request to delete TestQuestion : {}", id);
        testQuestionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/test-questions?query=:query : search for the testQuestion corresponding
     * to the query.
     *
     * @param query the query of the testQuestion search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/test-questions")
    @Timed
    public ResponseEntity<List<TestQuestion>> searchTestQuestions(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of TestQuestions for query {}", query);
        Page<TestQuestion> page = testQuestionService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/test-questions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
