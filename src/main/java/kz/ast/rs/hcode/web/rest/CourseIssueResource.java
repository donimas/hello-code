package kz.ast.rs.hcode.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.rs.hcode.domain.CourseIssue;
import kz.ast.rs.hcode.service.CourseIssueService;
import kz.ast.rs.hcode.web.rest.errors.BadRequestAlertException;
import kz.ast.rs.hcode.web.rest.util.HeaderUtil;
import kz.ast.rs.hcode.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CourseIssue.
 */
@RestController
@RequestMapping("/api")
public class CourseIssueResource {

    private final Logger log = LoggerFactory.getLogger(CourseIssueResource.class);

    private static final String ENTITY_NAME = "courseIssue";

    private final CourseIssueService courseIssueService;

    public CourseIssueResource(CourseIssueService courseIssueService) {
        this.courseIssueService = courseIssueService;
    }

    /**
     * POST  /course-issues : Create a new courseIssue.
     *
     * @param courseIssue the courseIssue to create
     * @return the ResponseEntity with status 201 (Created) and with body the new courseIssue, or with status 400 (Bad Request) if the courseIssue has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/course-issues")
    @Timed
    public ResponseEntity<CourseIssue> createCourseIssue(@Valid @RequestBody CourseIssue courseIssue) throws URISyntaxException {
        log.debug("REST request to save CourseIssue : {}", courseIssue);
        if (courseIssue.getId() != null) {
            throw new BadRequestAlertException("A new courseIssue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CourseIssue result = courseIssueService.save(courseIssue);
        return ResponseEntity.created(new URI("/api/course-issues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /course-issues : Updates an existing courseIssue.
     *
     * @param courseIssue the courseIssue to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated courseIssue,
     * or with status 400 (Bad Request) if the courseIssue is not valid,
     * or with status 500 (Internal Server Error) if the courseIssue couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/course-issues")
    @Timed
    public ResponseEntity<CourseIssue> updateCourseIssue(@Valid @RequestBody CourseIssue courseIssue) throws URISyntaxException {
        log.debug("REST request to update CourseIssue : {}", courseIssue);
        if (courseIssue.getId() == null) {
            return createCourseIssue(courseIssue);
        }
        CourseIssue result = courseIssueService.save(courseIssue);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, courseIssue.getId().toString()))
            .body(result);
    }

    /**
     * GET  /course-issues : get all the courseIssues.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of courseIssues in body
     */
    @GetMapping("/course-issues")
    @Timed
    public ResponseEntity<List<CourseIssue>> getAllCourseIssues(Pageable pageable) {
        log.debug("REST request to get a page of CourseIssues");
        Page<CourseIssue> page = courseIssueService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/course-issues");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /course-issues/:id : get the "id" courseIssue.
     *
     * @param id the id of the courseIssue to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the courseIssue, or with status 404 (Not Found)
     */
    @GetMapping("/course-issues/{id}")
    @Timed
    public ResponseEntity<CourseIssue> getCourseIssue(@PathVariable Long id) {
        log.debug("REST request to get CourseIssue : {}", id);
        CourseIssue courseIssue = courseIssueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(courseIssue));
    }

    /**
     * DELETE  /course-issues/:id : delete the "id" courseIssue.
     *
     * @param id the id of the courseIssue to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/course-issues/{id}")
    @Timed
    public ResponseEntity<Void> deleteCourseIssue(@PathVariable Long id) {
        log.debug("REST request to delete CourseIssue : {}", id);
        courseIssueService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/course-issues?query=:query : search for the courseIssue corresponding
     * to the query.
     *
     * @param query the query of the courseIssue search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/course-issues")
    @Timed
    public ResponseEntity<List<CourseIssue>> searchCourseIssues(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of CourseIssues for query {}", query);
        Page<CourseIssue> page = courseIssueService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/course-issues");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
