package kz.ast.rs.hcode.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.rs.hcode.domain.ActivityStatusHistory;
import kz.ast.rs.hcode.service.ActivityStatusHistoryService;
import kz.ast.rs.hcode.web.rest.errors.BadRequestAlertException;
import kz.ast.rs.hcode.web.rest.util.HeaderUtil;
import kz.ast.rs.hcode.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ActivityStatusHistory.
 */
@RestController
@RequestMapping("/api")
public class ActivityStatusHistoryResource {

    private final Logger log = LoggerFactory.getLogger(ActivityStatusHistoryResource.class);

    private static final String ENTITY_NAME = "activityStatusHistory";

    private final ActivityStatusHistoryService activityStatusHistoryService;

    public ActivityStatusHistoryResource(ActivityStatusHistoryService activityStatusHistoryService) {
        this.activityStatusHistoryService = activityStatusHistoryService;
    }

    /**
     * POST  /activity-status-histories : Create a new activityStatusHistory.
     *
     * @param activityStatusHistory the activityStatusHistory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new activityStatusHistory, or with status 400 (Bad Request) if the activityStatusHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/activity-status-histories")
    @Timed
    public ResponseEntity<ActivityStatusHistory> createActivityStatusHistory(@Valid @RequestBody ActivityStatusHistory activityStatusHistory) throws URISyntaxException {
        log.debug("REST request to save ActivityStatusHistory : {}", activityStatusHistory);
        if (activityStatusHistory.getId() != null) {
            throw new BadRequestAlertException("A new activityStatusHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ActivityStatusHistory result = activityStatusHistoryService.save(activityStatusHistory);
        return ResponseEntity.created(new URI("/api/activity-status-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /activity-status-histories : Updates an existing activityStatusHistory.
     *
     * @param activityStatusHistory the activityStatusHistory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated activityStatusHistory,
     * or with status 400 (Bad Request) if the activityStatusHistory is not valid,
     * or with status 500 (Internal Server Error) if the activityStatusHistory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/activity-status-histories")
    @Timed
    public ResponseEntity<ActivityStatusHistory> updateActivityStatusHistory(@Valid @RequestBody ActivityStatusHistory activityStatusHistory) throws URISyntaxException {
        log.debug("REST request to update ActivityStatusHistory : {}", activityStatusHistory);
        if (activityStatusHistory.getId() == null) {
            return createActivityStatusHistory(activityStatusHistory);
        }
        ActivityStatusHistory result = activityStatusHistoryService.save(activityStatusHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, activityStatusHistory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /activity-status-histories : get all the activityStatusHistories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of activityStatusHistories in body
     */
    @GetMapping("/activity-status-histories")
    @Timed
    public ResponseEntity<List<ActivityStatusHistory>> getAllActivityStatusHistories(Pageable pageable) {
        log.debug("REST request to get a page of ActivityStatusHistories");
        Page<ActivityStatusHistory> page = activityStatusHistoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/activity-status-histories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /activity-status-histories/:id : get the "id" activityStatusHistory.
     *
     * @param id the id of the activityStatusHistory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the activityStatusHistory, or with status 404 (Not Found)
     */
    @GetMapping("/activity-status-histories/{id}")
    @Timed
    public ResponseEntity<ActivityStatusHistory> getActivityStatusHistory(@PathVariable Long id) {
        log.debug("REST request to get ActivityStatusHistory : {}", id);
        ActivityStatusHistory activityStatusHistory = activityStatusHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(activityStatusHistory));
    }

    /**
     * DELETE  /activity-status-histories/:id : delete the "id" activityStatusHistory.
     *
     * @param id the id of the activityStatusHistory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/activity-status-histories/{id}")
    @Timed
    public ResponseEntity<Void> deleteActivityStatusHistory(@PathVariable Long id) {
        log.debug("REST request to delete ActivityStatusHistory : {}", id);
        activityStatusHistoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/activity-status-histories?query=:query : search for the activityStatusHistory corresponding
     * to the query.
     *
     * @param query the query of the activityStatusHistory search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/activity-status-histories")
    @Timed
    public ResponseEntity<List<ActivityStatusHistory>> searchActivityStatusHistories(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ActivityStatusHistories for query {}", query);
        Page<ActivityStatusHistory> page = activityStatusHistoryService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/activity-status-histories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
