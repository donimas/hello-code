package kz.ast.rs.hcode.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.rs.hcode.domain.LearnerActivity;
import kz.ast.rs.hcode.service.LearnerActivityService;
import kz.ast.rs.hcode.web.rest.errors.BadRequestAlertException;
import kz.ast.rs.hcode.web.rest.util.HeaderUtil;
import kz.ast.rs.hcode.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing LearnerActivity.
 */
@RestController
@RequestMapping("/api")
public class LearnerActivityResource {

    private final Logger log = LoggerFactory.getLogger(LearnerActivityResource.class);

    private static final String ENTITY_NAME = "learnerActivity";

    private final LearnerActivityService learnerActivityService;

    public LearnerActivityResource(LearnerActivityService learnerActivityService) {
        this.learnerActivityService = learnerActivityService;
    }

    /**
     * POST  /learner-activities : Create a new learnerActivity.
     *
     * @param learnerActivity the learnerActivity to create
     * @return the ResponseEntity with status 201 (Created) and with body the new learnerActivity, or with status 400 (Bad Request) if the learnerActivity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/learner-activities")
    @Timed
    public ResponseEntity<LearnerActivity> createLearnerActivity(@RequestBody LearnerActivity learnerActivity) throws URISyntaxException {
        log.debug("REST request to save LearnerActivity : {}", learnerActivity);
        if (learnerActivity.getId() != null) {
            throw new BadRequestAlertException("A new learnerActivity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LearnerActivity result = learnerActivityService.save(learnerActivity);
        return ResponseEntity.created(new URI("/api/learner-activities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /learner-activities : Updates an existing learnerActivity.
     *
     * @param learnerActivity the learnerActivity to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated learnerActivity,
     * or with status 400 (Bad Request) if the learnerActivity is not valid,
     * or with status 500 (Internal Server Error) if the learnerActivity couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/learner-activities")
    @Timed
    public ResponseEntity<LearnerActivity> updateLearnerActivity(@RequestBody LearnerActivity learnerActivity) throws URISyntaxException {
        log.debug("REST request to update LearnerActivity : {}", learnerActivity);
        if (learnerActivity.getId() == null) {
            return createLearnerActivity(learnerActivity);
        }
        LearnerActivity result = learnerActivityService.save(learnerActivity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, learnerActivity.getId().toString()))
            .body(result);
    }

    /**
     * GET  /learner-activities : get all the learnerActivities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of learnerActivities in body
     */
    @GetMapping("/learner-activities")
    @Timed
    public ResponseEntity<List<LearnerActivity>> getAllLearnerActivities(Pageable pageable) {
        log.debug("REST request to get a page of LearnerActivities");
        Page<LearnerActivity> page = learnerActivityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/learner-activities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /learner-activities/:id : get the "id" learnerActivity.
     *
     * @param id the id of the learnerActivity to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the learnerActivity, or with status 404 (Not Found)
     */
    @GetMapping("/learner-activities/{id}")
    @Timed
    public ResponseEntity<LearnerActivity> getLearnerActivity(@PathVariable Long id) {
        log.debug("REST request to get LearnerActivity : {}", id);
        LearnerActivity learnerActivity = learnerActivityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(learnerActivity));
    }

    /**
     * DELETE  /learner-activities/:id : delete the "id" learnerActivity.
     *
     * @param id the id of the learnerActivity to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/learner-activities/{id}")
    @Timed
    public ResponseEntity<Void> deleteLearnerActivity(@PathVariable Long id) {
        log.debug("REST request to delete LearnerActivity : {}", id);
        learnerActivityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/learner-activities?query=:query : search for the learnerActivity corresponding
     * to the query.
     *
     * @param query the query of the learnerActivity search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/learner-activities")
    @Timed
    public ResponseEntity<List<LearnerActivity>> searchLearnerActivities(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of LearnerActivities for query {}", query);
        Page<LearnerActivity> page = learnerActivityService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/learner-activities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
