package kz.ast.rs.hcode.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.rs.hcode.domain.SkratchIssue;
import kz.ast.rs.hcode.service.SkratchIssueService;
import kz.ast.rs.hcode.web.rest.errors.BadRequestAlertException;
import kz.ast.rs.hcode.web.rest.util.HeaderUtil;
import kz.ast.rs.hcode.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SkratchIssue.
 */
@RestController
@RequestMapping("/api")
public class SkratchIssueResource {

    private final Logger log = LoggerFactory.getLogger(SkratchIssueResource.class);

    private static final String ENTITY_NAME = "skratchIssue";

    private final SkratchIssueService skratchIssueService;

    public SkratchIssueResource(SkratchIssueService skratchIssueService) {
        this.skratchIssueService = skratchIssueService;
    }

    /**
     * POST  /skratch-issues : Create a new skratchIssue.
     *
     * @param skratchIssue the skratchIssue to create
     * @return the ResponseEntity with status 201 (Created) and with body the new skratchIssue, or with status 400 (Bad Request) if the skratchIssue has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/skratch-issues")
    @Timed
    public ResponseEntity<SkratchIssue> createSkratchIssue(@RequestBody SkratchIssue skratchIssue) throws URISyntaxException {
        log.debug("REST request to save SkratchIssue : {}", skratchIssue);
        if (skratchIssue.getId() != null) {
            throw new BadRequestAlertException("A new skratchIssue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SkratchIssue result = skratchIssueService.save(skratchIssue);
        return ResponseEntity.created(new URI("/api/skratch-issues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /skratch-issues : Updates an existing skratchIssue.
     *
     * @param skratchIssue the skratchIssue to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated skratchIssue,
     * or with status 400 (Bad Request) if the skratchIssue is not valid,
     * or with status 500 (Internal Server Error) if the skratchIssue couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/skratch-issues")
    @Timed
    public ResponseEntity<SkratchIssue> updateSkratchIssue(@RequestBody SkratchIssue skratchIssue) throws URISyntaxException {
        log.debug("REST request to update SkratchIssue : {}", skratchIssue);
        if (skratchIssue.getId() == null) {
            return createSkratchIssue(skratchIssue);
        }
        SkratchIssue result = skratchIssueService.save(skratchIssue);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, skratchIssue.getId().toString()))
            .body(result);
    }

    /**
     * GET  /skratch-issues : get all the skratchIssues.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of skratchIssues in body
     */
    @GetMapping("/skratch-issues")
    @Timed
    public ResponseEntity<List<SkratchIssue>> getAllSkratchIssues(Pageable pageable) {
        log.debug("REST request to get a page of SkratchIssues");
        Page<SkratchIssue> page = skratchIssueService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/skratch-issues");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /skratch-issues/:id : get the "id" skratchIssue.
     *
     * @param id the id of the skratchIssue to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the skratchIssue, or with status 404 (Not Found)
     */
    @GetMapping("/skratch-issues/{id}")
    @Timed
    public ResponseEntity<SkratchIssue> getSkratchIssue(@PathVariable Long id) {
        log.debug("REST request to get SkratchIssue : {}", id);
        SkratchIssue skratchIssue = skratchIssueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(skratchIssue));
    }

    /**
     * DELETE  /skratch-issues/:id : delete the "id" skratchIssue.
     *
     * @param id the id of the skratchIssue to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/skratch-issues/{id}")
    @Timed
    public ResponseEntity<Void> deleteSkratchIssue(@PathVariable Long id) {
        log.debug("REST request to delete SkratchIssue : {}", id);
        skratchIssueService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/skratch-issues?query=:query : search for the skratchIssue corresponding
     * to the query.
     *
     * @param query the query of the skratchIssue search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/skratch-issues")
    @Timed
    public ResponseEntity<List<SkratchIssue>> searchSkratchIssues(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of SkratchIssues for query {}", query);
        Page<SkratchIssue> page = skratchIssueService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/skratch-issues");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
