package kz.ast.rs.hcode.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.rs.hcode.domain.TestVariant;
import kz.ast.rs.hcode.service.TestVariantService;
import kz.ast.rs.hcode.web.rest.errors.BadRequestAlertException;
import kz.ast.rs.hcode.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TestVariant.
 */
@RestController
@RequestMapping("/api")
public class TestVariantResource {

    private final Logger log = LoggerFactory.getLogger(TestVariantResource.class);

    private static final String ENTITY_NAME = "testVariant";

    private final TestVariantService testVariantService;

    public TestVariantResource(TestVariantService testVariantService) {
        this.testVariantService = testVariantService;
    }

    /**
     * POST  /test-variants : Create a new testVariant.
     *
     * @param testVariant the testVariant to create
     * @return the ResponseEntity with status 201 (Created) and with body the new testVariant, or with status 400 (Bad Request) if the testVariant has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/test-variants")
    @Timed
    public ResponseEntity<TestVariant> createTestVariant(@Valid @RequestBody TestVariant testVariant) throws URISyntaxException {
        log.debug("REST request to save TestVariant : {}", testVariant);
        if (testVariant.getId() != null) {
            throw new BadRequestAlertException("A new testVariant cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TestVariant result = testVariantService.save(testVariant);
        return ResponseEntity.created(new URI("/api/test-variants/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /test-variants : Updates an existing testVariant.
     *
     * @param testVariant the testVariant to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated testVariant,
     * or with status 400 (Bad Request) if the testVariant is not valid,
     * or with status 500 (Internal Server Error) if the testVariant couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/test-variants")
    @Timed
    public ResponseEntity<TestVariant> updateTestVariant(@Valid @RequestBody TestVariant testVariant) throws URISyntaxException {
        log.debug("REST request to update TestVariant : {}", testVariant);
        if (testVariant.getId() == null) {
            return createTestVariant(testVariant);
        }
        TestVariant result = testVariantService.save(testVariant);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, testVariant.getId().toString()))
            .body(result);
    }

    /**
     * GET  /test-variants : get all the testVariants.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of testVariants in body
     */
    @GetMapping("/test-variants")
    @Timed
    public List<TestVariant> getAllTestVariants() {
        log.debug("REST request to get all TestVariants");
        return testVariantService.findAll();
        }

    /**
     * GET  /test-variants/:id : get the "id" testVariant.
     *
     * @param id the id of the testVariant to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the testVariant, or with status 404 (Not Found)
     */
    @GetMapping("/test-variants/{id}")
    @Timed
    public ResponseEntity<TestVariant> getTestVariant(@PathVariable Long id) {
        log.debug("REST request to get TestVariant : {}", id);
        TestVariant testVariant = testVariantService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(testVariant));
    }

    /**
     * DELETE  /test-variants/:id : delete the "id" testVariant.
     *
     * @param id the id of the testVariant to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/test-variants/{id}")
    @Timed
    public ResponseEntity<Void> deleteTestVariant(@PathVariable Long id) {
        log.debug("REST request to delete TestVariant : {}", id);
        testVariantService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/test-variants?query=:query : search for the testVariant corresponding
     * to the query.
     *
     * @param query the query of the testVariant search
     * @return the result of the search
     */
    @GetMapping("/_search/test-variants")
    @Timed
    public List<TestVariant> searchTestVariants(@RequestParam String query) {
        log.debug("REST request to search TestVariants for query {}", query);
        return testVariantService.search(query);
    }

}
