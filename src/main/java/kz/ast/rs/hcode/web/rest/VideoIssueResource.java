package kz.ast.rs.hcode.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.rs.hcode.domain.VideoIssue;
import kz.ast.rs.hcode.service.VideoIssueService;
import kz.ast.rs.hcode.web.rest.errors.BadRequestAlertException;
import kz.ast.rs.hcode.web.rest.util.HeaderUtil;
import kz.ast.rs.hcode.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VideoIssue.
 */
@RestController
@RequestMapping("/api")
public class VideoIssueResource {

    private final Logger log = LoggerFactory.getLogger(VideoIssueResource.class);

    private static final String ENTITY_NAME = "videoIssue";

    private final VideoIssueService videoIssueService;

    public VideoIssueResource(VideoIssueService videoIssueService) {
        this.videoIssueService = videoIssueService;
    }

    /**
     * POST  /video-issues : Create a new videoIssue.
     *
     * @param videoIssue the videoIssue to create
     * @return the ResponseEntity with status 201 (Created) and with body the new videoIssue, or with status 400 (Bad Request) if the videoIssue has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/video-issues")
    @Timed
    public ResponseEntity<VideoIssue> createVideoIssue(@RequestBody VideoIssue videoIssue) throws URISyntaxException {
        log.debug("REST request to save VideoIssue : {}", videoIssue);
        if (videoIssue.getId() != null) {
            throw new BadRequestAlertException("A new videoIssue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VideoIssue result = videoIssueService.save(videoIssue);
        return ResponseEntity.created(new URI("/api/video-issues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /video-issues : Updates an existing videoIssue.
     *
     * @param videoIssue the videoIssue to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated videoIssue,
     * or with status 400 (Bad Request) if the videoIssue is not valid,
     * or with status 500 (Internal Server Error) if the videoIssue couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/video-issues")
    @Timed
    public ResponseEntity<VideoIssue> updateVideoIssue(@RequestBody VideoIssue videoIssue) throws URISyntaxException {
        log.debug("REST request to update VideoIssue : {}", videoIssue);
        if (videoIssue.getId() == null) {
            return createVideoIssue(videoIssue);
        }
        VideoIssue result = videoIssueService.save(videoIssue);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, videoIssue.getId().toString()))
            .body(result);
    }

    /**
     * GET  /video-issues : get all the videoIssues.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of videoIssues in body
     */
    @GetMapping("/video-issues")
    @Timed
    public ResponseEntity<List<VideoIssue>> getAllVideoIssues(Pageable pageable) {
        log.debug("REST request to get a page of VideoIssues");
        Page<VideoIssue> page = videoIssueService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/video-issues");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /video-issues/:id : get the "id" videoIssue.
     *
     * @param id the id of the videoIssue to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the videoIssue, or with status 404 (Not Found)
     */
    @GetMapping("/video-issues/{id}")
    @Timed
    public ResponseEntity<VideoIssue> getVideoIssue(@PathVariable Long id) {
        log.debug("REST request to get VideoIssue : {}", id);
        VideoIssue videoIssue = videoIssueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(videoIssue));
    }

    /**
     * DELETE  /video-issues/:id : delete the "id" videoIssue.
     *
     * @param id the id of the videoIssue to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/video-issues/{id}")
    @Timed
    public ResponseEntity<Void> deleteVideoIssue(@PathVariable Long id) {
        log.debug("REST request to delete VideoIssue : {}", id);
        videoIssueService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/video-issues?query=:query : search for the videoIssue corresponding
     * to the query.
     *
     * @param query the query of the videoIssue search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/video-issues")
    @Timed
    public ResponseEntity<List<VideoIssue>> searchVideoIssues(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of VideoIssues for query {}", query);
        Page<VideoIssue> page = videoIssueService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/video-issues");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
