package kz.ast.rs.hcode.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.rs.hcode.domain.TextIssue;
import kz.ast.rs.hcode.service.TextIssueService;
import kz.ast.rs.hcode.web.rest.errors.BadRequestAlertException;
import kz.ast.rs.hcode.web.rest.util.HeaderUtil;
import kz.ast.rs.hcode.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TextIssue.
 */
@RestController
@RequestMapping("/api")
public class TextIssueResource {

    private final Logger log = LoggerFactory.getLogger(TextIssueResource.class);

    private static final String ENTITY_NAME = "textIssue";

    private final TextIssueService textIssueService;

    public TextIssueResource(TextIssueService textIssueService) {
        this.textIssueService = textIssueService;
    }

    /**
     * POST  /text-issues : Create a new textIssue.
     *
     * @param textIssue the textIssue to create
     * @return the ResponseEntity with status 201 (Created) and with body the new textIssue, or with status 400 (Bad Request) if the textIssue has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/text-issues")
    @Timed
    public ResponseEntity<TextIssue> createTextIssue(@RequestBody TextIssue textIssue) throws URISyntaxException {
        log.debug("REST request to save TextIssue : {}", textIssue);
        if (textIssue.getId() != null) {
            throw new BadRequestAlertException("A new textIssue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TextIssue result = textIssueService.save(textIssue);
        return ResponseEntity.created(new URI("/api/text-issues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /text-issues : Updates an existing textIssue.
     *
     * @param textIssue the textIssue to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated textIssue,
     * or with status 400 (Bad Request) if the textIssue is not valid,
     * or with status 500 (Internal Server Error) if the textIssue couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/text-issues")
    @Timed
    public ResponseEntity<TextIssue> updateTextIssue(@RequestBody TextIssue textIssue) throws URISyntaxException {
        log.debug("REST request to update TextIssue : {}", textIssue);
        if (textIssue.getId() == null) {
            return createTextIssue(textIssue);
        }
        TextIssue result = textIssueService.save(textIssue);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, textIssue.getId().toString()))
            .body(result);
    }

    /**
     * GET  /text-issues : get all the textIssues.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of textIssues in body
     */
    @GetMapping("/text-issues")
    @Timed
    public ResponseEntity<List<TextIssue>> getAllTextIssues(Pageable pageable) {
        log.debug("REST request to get a page of TextIssues");
        Page<TextIssue> page = textIssueService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/text-issues");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /text-issues/:id : get the "id" textIssue.
     *
     * @param id the id of the textIssue to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the textIssue, or with status 404 (Not Found)
     */
    @GetMapping("/text-issues/{id}")
    @Timed
    public ResponseEntity<TextIssue> getTextIssue(@PathVariable Long id) {
        log.debug("REST request to get TextIssue : {}", id);
        TextIssue textIssue = textIssueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(textIssue));
    }

    /**
     * DELETE  /text-issues/:id : delete the "id" textIssue.
     *
     * @param id the id of the textIssue to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/text-issues/{id}")
    @Timed
    public ResponseEntity<Void> deleteTextIssue(@PathVariable Long id) {
        log.debug("REST request to delete TextIssue : {}", id);
        textIssueService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/text-issues?query=:query : search for the textIssue corresponding
     * to the query.
     *
     * @param query the query of the textIssue search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/text-issues")
    @Timed
    public ResponseEntity<List<TextIssue>> searchTextIssues(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of TextIssues for query {}", query);
        Page<TextIssue> page = textIssueService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/text-issues");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
