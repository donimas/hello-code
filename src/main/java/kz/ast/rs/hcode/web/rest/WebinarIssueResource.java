package kz.ast.rs.hcode.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.rs.hcode.domain.WebinarIssue;
import kz.ast.rs.hcode.service.WebinarIssueService;
import kz.ast.rs.hcode.web.rest.errors.BadRequestAlertException;
import kz.ast.rs.hcode.web.rest.util.HeaderUtil;
import kz.ast.rs.hcode.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WebinarIssue.
 */
@RestController
@RequestMapping("/api")
public class WebinarIssueResource {

    private final Logger log = LoggerFactory.getLogger(WebinarIssueResource.class);

    private static final String ENTITY_NAME = "webinarIssue";

    private final WebinarIssueService webinarIssueService;

    public WebinarIssueResource(WebinarIssueService webinarIssueService) {
        this.webinarIssueService = webinarIssueService;
    }

    /**
     * POST  /webinar-issues : Create a new webinarIssue.
     *
     * @param webinarIssue the webinarIssue to create
     * @return the ResponseEntity with status 201 (Created) and with body the new webinarIssue, or with status 400 (Bad Request) if the webinarIssue has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/webinar-issues")
    @Timed
    public ResponseEntity<WebinarIssue> createWebinarIssue(@RequestBody WebinarIssue webinarIssue) throws URISyntaxException {
        log.debug("REST request to save WebinarIssue : {}", webinarIssue);
        if (webinarIssue.getId() != null) {
            throw new BadRequestAlertException("A new webinarIssue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WebinarIssue result = webinarIssueService.save(webinarIssue);
        return ResponseEntity.created(new URI("/api/webinar-issues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /webinar-issues : Updates an existing webinarIssue.
     *
     * @param webinarIssue the webinarIssue to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated webinarIssue,
     * or with status 400 (Bad Request) if the webinarIssue is not valid,
     * or with status 500 (Internal Server Error) if the webinarIssue couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/webinar-issues")
    @Timed
    public ResponseEntity<WebinarIssue> updateWebinarIssue(@RequestBody WebinarIssue webinarIssue) throws URISyntaxException {
        log.debug("REST request to update WebinarIssue : {}", webinarIssue);
        if (webinarIssue.getId() == null) {
            return createWebinarIssue(webinarIssue);
        }
        WebinarIssue result = webinarIssueService.save(webinarIssue);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, webinarIssue.getId().toString()))
            .body(result);
    }

    /**
     * GET  /webinar-issues : get all the webinarIssues.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of webinarIssues in body
     */
    @GetMapping("/webinar-issues")
    @Timed
    public ResponseEntity<List<WebinarIssue>> getAllWebinarIssues(Pageable pageable) {
        log.debug("REST request to get a page of WebinarIssues");
        Page<WebinarIssue> page = webinarIssueService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/webinar-issues");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /webinar-issues/:id : get the "id" webinarIssue.
     *
     * @param id the id of the webinarIssue to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the webinarIssue, or with status 404 (Not Found)
     */
    @GetMapping("/webinar-issues/{id}")
    @Timed
    public ResponseEntity<WebinarIssue> getWebinarIssue(@PathVariable Long id) {
        log.debug("REST request to get WebinarIssue : {}", id);
        WebinarIssue webinarIssue = webinarIssueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(webinarIssue));
    }

    /**
     * DELETE  /webinar-issues/:id : delete the "id" webinarIssue.
     *
     * @param id the id of the webinarIssue to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/webinar-issues/{id}")
    @Timed
    public ResponseEntity<Void> deleteWebinarIssue(@PathVariable Long id) {
        log.debug("REST request to delete WebinarIssue : {}", id);
        webinarIssueService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/webinar-issues?query=:query : search for the webinarIssue corresponding
     * to the query.
     *
     * @param query the query of the webinarIssue search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/webinar-issues")
    @Timed
    public ResponseEntity<List<WebinarIssue>> searchWebinarIssues(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WebinarIssues for query {}", query);
        Page<WebinarIssue> page = webinarIssueService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/webinar-issues");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
