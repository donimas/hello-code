package kz.ast.rs.hcode.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.rs.hcode.domain.CourseProgress;
import kz.ast.rs.hcode.service.CourseProgressService;
import kz.ast.rs.hcode.web.rest.errors.BadRequestAlertException;
import kz.ast.rs.hcode.web.rest.util.HeaderUtil;
import kz.ast.rs.hcode.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CourseProgress.
 */
@RestController
@RequestMapping("/api")
public class CourseProgressResource {

    private final Logger log = LoggerFactory.getLogger(CourseProgressResource.class);

    private static final String ENTITY_NAME = "courseProgress";

    private final CourseProgressService courseProgressService;

    public CourseProgressResource(CourseProgressService courseProgressService) {
        this.courseProgressService = courseProgressService;
    }

    /**
     * POST  /course-progresses : Create a new courseProgress.
     *
     * @param courseProgress the courseProgress to create
     * @return the ResponseEntity with status 201 (Created) and with body the new courseProgress, or with status 400 (Bad Request) if the courseProgress has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/course-progresses")
    @Timed
    public ResponseEntity<CourseProgress> createCourseProgress(@RequestBody CourseProgress courseProgress) throws URISyntaxException {
        log.debug("REST request to save CourseProgress : {}", courseProgress);
        if (courseProgress.getId() != null) {
            throw new BadRequestAlertException("A new courseProgress cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CourseProgress result = courseProgressService.save(courseProgress);
        return ResponseEntity.created(new URI("/api/course-progresses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /course-progresses : Updates an existing courseProgress.
     *
     * @param courseProgress the courseProgress to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated courseProgress,
     * or with status 400 (Bad Request) if the courseProgress is not valid,
     * or with status 500 (Internal Server Error) if the courseProgress couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/course-progresses")
    @Timed
    public ResponseEntity<CourseProgress> updateCourseProgress(@RequestBody CourseProgress courseProgress) throws URISyntaxException {
        log.debug("REST request to update CourseProgress : {}", courseProgress);
        if (courseProgress.getId() == null) {
            return createCourseProgress(courseProgress);
        }
        CourseProgress result = courseProgressService.save(courseProgress);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, courseProgress.getId().toString()))
            .body(result);
    }

    /**
     * GET  /course-progresses : get all the courseProgresses.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of courseProgresses in body
     */
    @GetMapping("/course-progresses")
    @Timed
    public ResponseEntity<List<CourseProgress>> getAllCourseProgresses(Pageable pageable) {
        log.debug("REST request to get a page of CourseProgresses");
        Page<CourseProgress> page = courseProgressService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/course-progresses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /course-progresses/:id : get the "id" courseProgress.
     *
     * @param id the id of the courseProgress to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the courseProgress, or with status 404 (Not Found)
     */
    @GetMapping("/course-progresses/{id}")
    @Timed
    public ResponseEntity<CourseProgress> getCourseProgress(@PathVariable Long id) {
        log.debug("REST request to get CourseProgress : {}", id);
        CourseProgress courseProgress = courseProgressService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(courseProgress));
    }

    /**
     * DELETE  /course-progresses/:id : delete the "id" courseProgress.
     *
     * @param id the id of the courseProgress to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/course-progresses/{id}")
    @Timed
    public ResponseEntity<Void> deleteCourseProgress(@PathVariable Long id) {
        log.debug("REST request to delete CourseProgress : {}", id);
        courseProgressService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/course-progresses?query=:query : search for the courseProgress corresponding
     * to the query.
     *
     * @param query the query of the courseProgress search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/course-progresses")
    @Timed
    public ResponseEntity<List<CourseProgress>> searchCourseProgresses(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of CourseProgresses for query {}", query);
        Page<CourseProgress> page = courseProgressService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/course-progresses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
