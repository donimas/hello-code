package kz.ast.rs.hcode.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.rs.hcode.domain.LearnerGroup;
import kz.ast.rs.hcode.service.LearnerGroupService;
import kz.ast.rs.hcode.web.rest.errors.BadRequestAlertException;
import kz.ast.rs.hcode.web.rest.util.HeaderUtil;
import kz.ast.rs.hcode.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing LearnerGroup.
 */
@RestController
@RequestMapping("/api")
public class LearnerGroupResource {

    private final Logger log = LoggerFactory.getLogger(LearnerGroupResource.class);

    private static final String ENTITY_NAME = "learnerGroup";

    private final LearnerGroupService learnerGroupService;

    public LearnerGroupResource(LearnerGroupService learnerGroupService) {
        this.learnerGroupService = learnerGroupService;
    }

    /**
     * POST  /learner-groups : Create a new learnerGroup.
     *
     * @param learnerGroup the learnerGroup to create
     * @return the ResponseEntity with status 201 (Created) and with body the new learnerGroup, or with status 400 (Bad Request) if the learnerGroup has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/learner-groups")
    @Timed
    public ResponseEntity<LearnerGroup> createLearnerGroup(@Valid @RequestBody LearnerGroup learnerGroup) throws URISyntaxException {
        log.debug("REST request to save LearnerGroup : {}", learnerGroup);
        if (learnerGroup.getId() != null) {
            throw new BadRequestAlertException("A new learnerGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LearnerGroup result = learnerGroupService.save(learnerGroup);
        return ResponseEntity.created(new URI("/api/learner-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /learner-groups : Updates an existing learnerGroup.
     *
     * @param learnerGroup the learnerGroup to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated learnerGroup,
     * or with status 400 (Bad Request) if the learnerGroup is not valid,
     * or with status 500 (Internal Server Error) if the learnerGroup couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/learner-groups")
    @Timed
    public ResponseEntity<LearnerGroup> updateLearnerGroup(@Valid @RequestBody LearnerGroup learnerGroup) throws URISyntaxException {
        log.debug("REST request to update LearnerGroup : {}", learnerGroup);
        if (learnerGroup.getId() == null) {
            return createLearnerGroup(learnerGroup);
        }
        LearnerGroup result = learnerGroupService.save(learnerGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, learnerGroup.getId().toString()))
            .body(result);
    }

    /**
     * GET  /learner-groups : get all the learnerGroups.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of learnerGroups in body
     */
    @GetMapping("/learner-groups")
    @Timed
    public ResponseEntity<List<LearnerGroup>> getAllLearnerGroups(Pageable pageable) {
        log.debug("REST request to get a page of LearnerGroups");
        Page<LearnerGroup> page = learnerGroupService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/learner-groups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /learner-groups/:id : get the "id" learnerGroup.
     *
     * @param id the id of the learnerGroup to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the learnerGroup, or with status 404 (Not Found)
     */
    @GetMapping("/learner-groups/{id}")
    @Timed
    public ResponseEntity<LearnerGroup> getLearnerGroup(@PathVariable Long id) {
        log.debug("REST request to get LearnerGroup : {}", id);
        LearnerGroup learnerGroup = learnerGroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(learnerGroup));
    }

    /**
     * DELETE  /learner-groups/:id : delete the "id" learnerGroup.
     *
     * @param id the id of the learnerGroup to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/learner-groups/{id}")
    @Timed
    public ResponseEntity<Void> deleteLearnerGroup(@PathVariable Long id) {
        log.debug("REST request to delete LearnerGroup : {}", id);
        learnerGroupService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/learner-groups?query=:query : search for the learnerGroup corresponding
     * to the query.
     *
     * @param query the query of the learnerGroup search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/learner-groups")
    @Timed
    public ResponseEntity<List<LearnerGroup>> searchLearnerGroups(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of LearnerGroups for query {}", query);
        Page<LearnerGroup> page = learnerGroupService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/learner-groups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
