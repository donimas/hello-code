/**
 * View Models used by Spring MVC REST controllers.
 */
package kz.ast.rs.hcode.web.rest.vm;
