/**
 * Data Access Objects used by WebSocket services.
 */
package kz.ast.rs.hcode.web.websocket.dto;
