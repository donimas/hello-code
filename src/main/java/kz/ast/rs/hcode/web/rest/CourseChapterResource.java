package kz.ast.rs.hcode.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.rs.hcode.domain.CourseChapter;
import kz.ast.rs.hcode.service.CourseChapterService;
import kz.ast.rs.hcode.web.rest.errors.BadRequestAlertException;
import kz.ast.rs.hcode.web.rest.util.HeaderUtil;
import kz.ast.rs.hcode.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CourseChapter.
 */
@RestController
@RequestMapping("/api")
public class CourseChapterResource {

    private final Logger log = LoggerFactory.getLogger(CourseChapterResource.class);

    private static final String ENTITY_NAME = "courseChapter";

    private final CourseChapterService courseChapterService;

    public CourseChapterResource(CourseChapterService courseChapterService) {
        this.courseChapterService = courseChapterService;
    }

    /**
     * POST  /course-chapters : Create a new courseChapter.
     *
     * @param courseChapter the courseChapter to create
     * @return the ResponseEntity with status 201 (Created) and with body the new courseChapter, or with status 400 (Bad Request) if the courseChapter has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/course-chapters")
    @Timed
    public ResponseEntity<CourseChapter> createCourseChapter(@Valid @RequestBody CourseChapter courseChapter) throws URISyntaxException {
        log.debug("REST request to save CourseChapter : {}", courseChapter);
        if (courseChapter.getId() != null) {
            throw new BadRequestAlertException("A new courseChapter cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CourseChapter result = courseChapterService.save(courseChapter);
        return ResponseEntity.created(new URI("/api/course-chapters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /course-chapters : Updates an existing courseChapter.
     *
     * @param courseChapter the courseChapter to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated courseChapter,
     * or with status 400 (Bad Request) if the courseChapter is not valid,
     * or with status 500 (Internal Server Error) if the courseChapter couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/course-chapters")
    @Timed
    public ResponseEntity<CourseChapter> updateCourseChapter(@Valid @RequestBody CourseChapter courseChapter) throws URISyntaxException {
        log.debug("REST request to update CourseChapter : {}", courseChapter);
        if (courseChapter.getId() == null) {
            return createCourseChapter(courseChapter);
        }
        CourseChapter result = courseChapterService.save(courseChapter);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, courseChapter.getId().toString()))
            .body(result);
    }

    /**
     * GET  /course-chapters : get all the courseChapters.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of courseChapters in body
     */
    @GetMapping("/course-chapters")
    @Timed
    public ResponseEntity<List<CourseChapter>> getAllCourseChapters(Pageable pageable) {
        log.debug("REST request to get a page of CourseChapters");
        Page<CourseChapter> page = courseChapterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/course-chapters");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /course-chapters/:id : get the "id" courseChapter.
     *
     * @param id the id of the courseChapter to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the courseChapter, or with status 404 (Not Found)
     */
    @GetMapping("/course-chapters/{id}")
    @Timed
    public ResponseEntity<CourseChapter> getCourseChapter(@PathVariable Long id) {
        log.debug("REST request to get CourseChapter : {}", id);
        CourseChapter courseChapter = courseChapterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(courseChapter));
    }

    /**
     * DELETE  /course-chapters/:id : delete the "id" courseChapter.
     *
     * @param id the id of the courseChapter to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/course-chapters/{id}")
    @Timed
    public ResponseEntity<Void> deleteCourseChapter(@PathVariable Long id) {
        log.debug("REST request to delete CourseChapter : {}", id);
        courseChapterService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/course-chapters?query=:query : search for the courseChapter corresponding
     * to the query.
     *
     * @param query the query of the courseChapter search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/course-chapters")
    @Timed
    public ResponseEntity<List<CourseChapter>> searchCourseChapters(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of CourseChapters for query {}", query);
        Page<CourseChapter> page = courseChapterService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/course-chapters");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
