package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.TestVariant;
import kz.ast.rs.hcode.repository.TestVariantRepository;
import kz.ast.rs.hcode.service.TestVariantService;
import kz.ast.rs.hcode.repository.search.TestVariantSearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TestVariantResource REST controller.
 *
 * @see TestVariantResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class TestVariantResourceIntTest {

    private static final String DEFAULT_BODY_RU = "AAAAAAAAAA";
    private static final String UPDATED_BODY_RU = "BBBBBBBBBB";

    private static final String DEFAULT_BODY_KK = "AAAAAAAAAA";
    private static final String UPDATED_BODY_KK = "BBBBBBBBBB";

    private static final String DEFAULT_BODY_EN = "AAAAAAAAAA";
    private static final String UPDATED_BODY_EN = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FLAG_CORRECT = false;
    private static final Boolean UPDATED_FLAG_CORRECT = true;

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    @Autowired
    private TestVariantRepository testVariantRepository;

    @Autowired
    private TestVariantService testVariantService;

    @Autowired
    private TestVariantSearchRepository testVariantSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTestVariantMockMvc;

    private TestVariant testVariant;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TestVariantResource testVariantResource = new TestVariantResource(testVariantService);
        this.restTestVariantMockMvc = MockMvcBuilders.standaloneSetup(testVariantResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TestVariant createEntity(EntityManager em) {
        TestVariant testVariant = new TestVariant()
            .bodyRu(DEFAULT_BODY_RU)
            .bodyKk(DEFAULT_BODY_KK)
            .bodyEn(DEFAULT_BODY_EN)
            .flagCorrect(DEFAULT_FLAG_CORRECT)
            .flagDeleted(DEFAULT_FLAG_DELETED);
        return testVariant;
    }

    @Before
    public void initTest() {
        testVariantSearchRepository.deleteAll();
        testVariant = createEntity(em);
    }

    @Test
    @Transactional
    public void createTestVariant() throws Exception {
        int databaseSizeBeforeCreate = testVariantRepository.findAll().size();

        // Create the TestVariant
        restTestVariantMockMvc.perform(post("/api/test-variants")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testVariant)))
            .andExpect(status().isCreated());

        // Validate the TestVariant in the database
        List<TestVariant> testVariantList = testVariantRepository.findAll();
        assertThat(testVariantList).hasSize(databaseSizeBeforeCreate + 1);
        TestVariant testTestVariant = testVariantList.get(testVariantList.size() - 1);
        assertThat(testTestVariant.getBodyRu()).isEqualTo(DEFAULT_BODY_RU);
        assertThat(testTestVariant.getBodyKk()).isEqualTo(DEFAULT_BODY_KK);
        assertThat(testTestVariant.getBodyEn()).isEqualTo(DEFAULT_BODY_EN);
        assertThat(testTestVariant.isFlagCorrect()).isEqualTo(DEFAULT_FLAG_CORRECT);
        assertThat(testTestVariant.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);

        // Validate the TestVariant in Elasticsearch
        TestVariant testVariantEs = testVariantSearchRepository.findOne(testTestVariant.getId());
        assertThat(testVariantEs).isEqualToIgnoringGivenFields(testTestVariant);
    }

    @Test
    @Transactional
    public void createTestVariantWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = testVariantRepository.findAll().size();

        // Create the TestVariant with an existing ID
        testVariant.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTestVariantMockMvc.perform(post("/api/test-variants")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testVariant)))
            .andExpect(status().isBadRequest());

        // Validate the TestVariant in the database
        List<TestVariant> testVariantList = testVariantRepository.findAll();
        assertThat(testVariantList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFlagCorrectIsRequired() throws Exception {
        int databaseSizeBeforeTest = testVariantRepository.findAll().size();
        // set the field null
        testVariant.setFlagCorrect(null);

        // Create the TestVariant, which fails.

        restTestVariantMockMvc.perform(post("/api/test-variants")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testVariant)))
            .andExpect(status().isBadRequest());

        List<TestVariant> testVariantList = testVariantRepository.findAll();
        assertThat(testVariantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTestVariants() throws Exception {
        // Initialize the database
        testVariantRepository.saveAndFlush(testVariant);

        // Get all the testVariantList
        restTestVariantMockMvc.perform(get("/api/test-variants?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(testVariant.getId().intValue())))
            .andExpect(jsonPath("$.[*].bodyRu").value(hasItem(DEFAULT_BODY_RU.toString())))
            .andExpect(jsonPath("$.[*].bodyKk").value(hasItem(DEFAULT_BODY_KK.toString())))
            .andExpect(jsonPath("$.[*].bodyEn").value(hasItem(DEFAULT_BODY_EN.toString())))
            .andExpect(jsonPath("$.[*].flagCorrect").value(hasItem(DEFAULT_FLAG_CORRECT.booleanValue())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getTestVariant() throws Exception {
        // Initialize the database
        testVariantRepository.saveAndFlush(testVariant);

        // Get the testVariant
        restTestVariantMockMvc.perform(get("/api/test-variants/{id}", testVariant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(testVariant.getId().intValue()))
            .andExpect(jsonPath("$.bodyRu").value(DEFAULT_BODY_RU.toString()))
            .andExpect(jsonPath("$.bodyKk").value(DEFAULT_BODY_KK.toString()))
            .andExpect(jsonPath("$.bodyEn").value(DEFAULT_BODY_EN.toString()))
            .andExpect(jsonPath("$.flagCorrect").value(DEFAULT_FLAG_CORRECT.booleanValue()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTestVariant() throws Exception {
        // Get the testVariant
        restTestVariantMockMvc.perform(get("/api/test-variants/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTestVariant() throws Exception {
        // Initialize the database
        testVariantService.save(testVariant);

        int databaseSizeBeforeUpdate = testVariantRepository.findAll().size();

        // Update the testVariant
        TestVariant updatedTestVariant = testVariantRepository.findOne(testVariant.getId());
        // Disconnect from session so that the updates on updatedTestVariant are not directly saved in db
        em.detach(updatedTestVariant);
        updatedTestVariant
            .bodyRu(UPDATED_BODY_RU)
            .bodyKk(UPDATED_BODY_KK)
            .bodyEn(UPDATED_BODY_EN)
            .flagCorrect(UPDATED_FLAG_CORRECT)
            .flagDeleted(UPDATED_FLAG_DELETED);

        restTestVariantMockMvc.perform(put("/api/test-variants")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTestVariant)))
            .andExpect(status().isOk());

        // Validate the TestVariant in the database
        List<TestVariant> testVariantList = testVariantRepository.findAll();
        assertThat(testVariantList).hasSize(databaseSizeBeforeUpdate);
        TestVariant testTestVariant = testVariantList.get(testVariantList.size() - 1);
        assertThat(testTestVariant.getBodyRu()).isEqualTo(UPDATED_BODY_RU);
        assertThat(testTestVariant.getBodyKk()).isEqualTo(UPDATED_BODY_KK);
        assertThat(testTestVariant.getBodyEn()).isEqualTo(UPDATED_BODY_EN);
        assertThat(testTestVariant.isFlagCorrect()).isEqualTo(UPDATED_FLAG_CORRECT);
        assertThat(testTestVariant.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);

        // Validate the TestVariant in Elasticsearch
        TestVariant testVariantEs = testVariantSearchRepository.findOne(testTestVariant.getId());
        assertThat(testVariantEs).isEqualToIgnoringGivenFields(testTestVariant);
    }

    @Test
    @Transactional
    public void updateNonExistingTestVariant() throws Exception {
        int databaseSizeBeforeUpdate = testVariantRepository.findAll().size();

        // Create the TestVariant

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTestVariantMockMvc.perform(put("/api/test-variants")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testVariant)))
            .andExpect(status().isCreated());

        // Validate the TestVariant in the database
        List<TestVariant> testVariantList = testVariantRepository.findAll();
        assertThat(testVariantList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTestVariant() throws Exception {
        // Initialize the database
        testVariantService.save(testVariant);

        int databaseSizeBeforeDelete = testVariantRepository.findAll().size();

        // Get the testVariant
        restTestVariantMockMvc.perform(delete("/api/test-variants/{id}", testVariant.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean testVariantExistsInEs = testVariantSearchRepository.exists(testVariant.getId());
        assertThat(testVariantExistsInEs).isFalse();

        // Validate the database is empty
        List<TestVariant> testVariantList = testVariantRepository.findAll();
        assertThat(testVariantList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTestVariant() throws Exception {
        // Initialize the database
        testVariantService.save(testVariant);

        // Search the testVariant
        restTestVariantMockMvc.perform(get("/api/_search/test-variants?query=id:" + testVariant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(testVariant.getId().intValue())))
            .andExpect(jsonPath("$.[*].bodyRu").value(hasItem(DEFAULT_BODY_RU.toString())))
            .andExpect(jsonPath("$.[*].bodyKk").value(hasItem(DEFAULT_BODY_KK.toString())))
            .andExpect(jsonPath("$.[*].bodyEn").value(hasItem(DEFAULT_BODY_EN.toString())))
            .andExpect(jsonPath("$.[*].flagCorrect").value(hasItem(DEFAULT_FLAG_CORRECT.booleanValue())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TestVariant.class);
        TestVariant testVariant1 = new TestVariant();
        testVariant1.setId(1L);
        TestVariant testVariant2 = new TestVariant();
        testVariant2.setId(testVariant1.getId());
        assertThat(testVariant1).isEqualTo(testVariant2);
        testVariant2.setId(2L);
        assertThat(testVariant1).isNotEqualTo(testVariant2);
        testVariant1.setId(null);
        assertThat(testVariant1).isNotEqualTo(testVariant2);
    }
}
