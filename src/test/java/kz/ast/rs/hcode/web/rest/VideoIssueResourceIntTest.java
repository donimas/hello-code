package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.VideoIssue;
import kz.ast.rs.hcode.repository.VideoIssueRepository;
import kz.ast.rs.hcode.service.VideoIssueService;
import kz.ast.rs.hcode.repository.search.VideoIssueSearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VideoIssueResource REST controller.
 *
 * @see VideoIssueResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class VideoIssueResourceIntTest {

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_DURATION = new BigDecimal(1);
    private static final BigDecimal UPDATED_DURATION = new BigDecimal(2);

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LOGIN = "AAAAAAAAAA";
    private static final String UPDATED_LOGIN = "BBBBBBBBBB";

    private static final String DEFAULT_KEY = "AAAAAAAAAA";
    private static final String UPDATED_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    @Autowired
    private VideoIssueRepository videoIssueRepository;

    @Autowired
    private VideoIssueService videoIssueService;

    @Autowired
    private VideoIssueSearchRepository videoIssueSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restVideoIssueMockMvc;

    private VideoIssue videoIssue;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VideoIssueResource videoIssueResource = new VideoIssueResource(videoIssueService);
        this.restVideoIssueMockMvc = MockMvcBuilders.standaloneSetup(videoIssueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VideoIssue createEntity(EntityManager em) {
        VideoIssue videoIssue = new VideoIssue()
            .url(DEFAULT_URL)
            .duration(DEFAULT_DURATION)
            .name(DEFAULT_NAME)
            .login(DEFAULT_LOGIN)
            .key(DEFAULT_KEY)
            .note(DEFAULT_NOTE)
            .flagDeleted(DEFAULT_FLAG_DELETED);
        return videoIssue;
    }

    @Before
    public void initTest() {
        videoIssueSearchRepository.deleteAll();
        videoIssue = createEntity(em);
    }

    @Test
    @Transactional
    public void createVideoIssue() throws Exception {
        int databaseSizeBeforeCreate = videoIssueRepository.findAll().size();

        // Create the VideoIssue
        restVideoIssueMockMvc.perform(post("/api/video-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(videoIssue)))
            .andExpect(status().isCreated());

        // Validate the VideoIssue in the database
        List<VideoIssue> videoIssueList = videoIssueRepository.findAll();
        assertThat(videoIssueList).hasSize(databaseSizeBeforeCreate + 1);
        VideoIssue testVideoIssue = videoIssueList.get(videoIssueList.size() - 1);
        assertThat(testVideoIssue.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testVideoIssue.getDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testVideoIssue.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVideoIssue.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(testVideoIssue.getKey()).isEqualTo(DEFAULT_KEY);
        assertThat(testVideoIssue.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testVideoIssue.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);

        // Validate the VideoIssue in Elasticsearch
        VideoIssue videoIssueEs = videoIssueSearchRepository.findOne(testVideoIssue.getId());
        assertThat(videoIssueEs).isEqualToIgnoringGivenFields(testVideoIssue);
    }

    @Test
    @Transactional
    public void createVideoIssueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = videoIssueRepository.findAll().size();

        // Create the VideoIssue with an existing ID
        videoIssue.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVideoIssueMockMvc.perform(post("/api/video-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(videoIssue)))
            .andExpect(status().isBadRequest());

        // Validate the VideoIssue in the database
        List<VideoIssue> videoIssueList = videoIssueRepository.findAll();
        assertThat(videoIssueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllVideoIssues() throws Exception {
        // Initialize the database
        videoIssueRepository.saveAndFlush(videoIssue);

        // Get all the videoIssueList
        restVideoIssueMockMvc.perform(get("/api/video-issues?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(videoIssue.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].login").value(hasItem(DEFAULT_LOGIN.toString())))
            .andExpect(jsonPath("$.[*].key").value(hasItem(DEFAULT_KEY.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getVideoIssue() throws Exception {
        // Initialize the database
        videoIssueRepository.saveAndFlush(videoIssue);

        // Get the videoIssue
        restVideoIssueMockMvc.perform(get("/api/video-issues/{id}", videoIssue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(videoIssue.getId().intValue()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION.intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.login").value(DEFAULT_LOGIN.toString()))
            .andExpect(jsonPath("$.key").value(DEFAULT_KEY.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingVideoIssue() throws Exception {
        // Get the videoIssue
        restVideoIssueMockMvc.perform(get("/api/video-issues/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVideoIssue() throws Exception {
        // Initialize the database
        videoIssueService.save(videoIssue);

        int databaseSizeBeforeUpdate = videoIssueRepository.findAll().size();

        // Update the videoIssue
        VideoIssue updatedVideoIssue = videoIssueRepository.findOne(videoIssue.getId());
        // Disconnect from session so that the updates on updatedVideoIssue are not directly saved in db
        em.detach(updatedVideoIssue);
        updatedVideoIssue
            .url(UPDATED_URL)
            .duration(UPDATED_DURATION)
            .name(UPDATED_NAME)
            .login(UPDATED_LOGIN)
            .key(UPDATED_KEY)
            .note(UPDATED_NOTE)
            .flagDeleted(UPDATED_FLAG_DELETED);

        restVideoIssueMockMvc.perform(put("/api/video-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedVideoIssue)))
            .andExpect(status().isOk());

        // Validate the VideoIssue in the database
        List<VideoIssue> videoIssueList = videoIssueRepository.findAll();
        assertThat(videoIssueList).hasSize(databaseSizeBeforeUpdate);
        VideoIssue testVideoIssue = videoIssueList.get(videoIssueList.size() - 1);
        assertThat(testVideoIssue.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testVideoIssue.getDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testVideoIssue.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVideoIssue.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testVideoIssue.getKey()).isEqualTo(UPDATED_KEY);
        assertThat(testVideoIssue.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testVideoIssue.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);

        // Validate the VideoIssue in Elasticsearch
        VideoIssue videoIssueEs = videoIssueSearchRepository.findOne(testVideoIssue.getId());
        assertThat(videoIssueEs).isEqualToIgnoringGivenFields(testVideoIssue);
    }

    @Test
    @Transactional
    public void updateNonExistingVideoIssue() throws Exception {
        int databaseSizeBeforeUpdate = videoIssueRepository.findAll().size();

        // Create the VideoIssue

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVideoIssueMockMvc.perform(put("/api/video-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(videoIssue)))
            .andExpect(status().isCreated());

        // Validate the VideoIssue in the database
        List<VideoIssue> videoIssueList = videoIssueRepository.findAll();
        assertThat(videoIssueList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteVideoIssue() throws Exception {
        // Initialize the database
        videoIssueService.save(videoIssue);

        int databaseSizeBeforeDelete = videoIssueRepository.findAll().size();

        // Get the videoIssue
        restVideoIssueMockMvc.perform(delete("/api/video-issues/{id}", videoIssue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean videoIssueExistsInEs = videoIssueSearchRepository.exists(videoIssue.getId());
        assertThat(videoIssueExistsInEs).isFalse();

        // Validate the database is empty
        List<VideoIssue> videoIssueList = videoIssueRepository.findAll();
        assertThat(videoIssueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchVideoIssue() throws Exception {
        // Initialize the database
        videoIssueService.save(videoIssue);

        // Search the videoIssue
        restVideoIssueMockMvc.perform(get("/api/_search/video-issues?query=id:" + videoIssue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(videoIssue.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].login").value(hasItem(DEFAULT_LOGIN.toString())))
            .andExpect(jsonPath("$.[*].key").value(hasItem(DEFAULT_KEY.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VideoIssue.class);
        VideoIssue videoIssue1 = new VideoIssue();
        videoIssue1.setId(1L);
        VideoIssue videoIssue2 = new VideoIssue();
        videoIssue2.setId(videoIssue1.getId());
        assertThat(videoIssue1).isEqualTo(videoIssue2);
        videoIssue2.setId(2L);
        assertThat(videoIssue1).isNotEqualTo(videoIssue2);
        videoIssue1.setId(null);
        assertThat(videoIssue1).isNotEqualTo(videoIssue2);
    }
}
