package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.CourseProgress;
import kz.ast.rs.hcode.repository.CourseProgressRepository;
import kz.ast.rs.hcode.service.CourseProgressService;
import kz.ast.rs.hcode.repository.search.CourseProgressSearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.sameInstant;
import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CourseProgressResource REST controller.
 *
 * @see CourseProgressResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class CourseProgressResourceIntTest {

    private static final BigDecimal DEFAULT_PASSED_ISSUES = new BigDecimal(1);
    private static final BigDecimal UPDATED_PASSED_ISSUES = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TOTAL_ISSUES = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_ISSUES = new BigDecimal(2);

    private static final ZonedDateTime DEFAULT_LAST_ACTIVITY_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_ACTIVITY_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private CourseProgressRepository courseProgressRepository;

    @Autowired
    private CourseProgressService courseProgressService;

    @Autowired
    private CourseProgressSearchRepository courseProgressSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCourseProgressMockMvc;

    private CourseProgress courseProgress;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CourseProgressResource courseProgressResource = new CourseProgressResource(courseProgressService);
        this.restCourseProgressMockMvc = MockMvcBuilders.standaloneSetup(courseProgressResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CourseProgress createEntity(EntityManager em) {
        CourseProgress courseProgress = new CourseProgress()
            .passedIssues(DEFAULT_PASSED_ISSUES)
            .totalIssues(DEFAULT_TOTAL_ISSUES)
            .lastActivityDate(DEFAULT_LAST_ACTIVITY_DATE);
        return courseProgress;
    }

    @Before
    public void initTest() {
        courseProgressSearchRepository.deleteAll();
        courseProgress = createEntity(em);
    }

    @Test
    @Transactional
    public void createCourseProgress() throws Exception {
        int databaseSizeBeforeCreate = courseProgressRepository.findAll().size();

        // Create the CourseProgress
        restCourseProgressMockMvc.perform(post("/api/course-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseProgress)))
            .andExpect(status().isCreated());

        // Validate the CourseProgress in the database
        List<CourseProgress> courseProgressList = courseProgressRepository.findAll();
        assertThat(courseProgressList).hasSize(databaseSizeBeforeCreate + 1);
        CourseProgress testCourseProgress = courseProgressList.get(courseProgressList.size() - 1);
        assertThat(testCourseProgress.getPassedIssues()).isEqualTo(DEFAULT_PASSED_ISSUES);
        assertThat(testCourseProgress.getTotalIssues()).isEqualTo(DEFAULT_TOTAL_ISSUES);
        assertThat(testCourseProgress.getLastActivityDate()).isEqualTo(DEFAULT_LAST_ACTIVITY_DATE);

        // Validate the CourseProgress in Elasticsearch
        CourseProgress courseProgressEs = courseProgressSearchRepository.findOne(testCourseProgress.getId());
        assertThat(testCourseProgress.getLastActivityDate()).isEqualTo(testCourseProgress.getLastActivityDate());
        assertThat(courseProgressEs).isEqualToIgnoringGivenFields(testCourseProgress, "lastActivityDate");
    }

    @Test
    @Transactional
    public void createCourseProgressWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = courseProgressRepository.findAll().size();

        // Create the CourseProgress with an existing ID
        courseProgress.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCourseProgressMockMvc.perform(post("/api/course-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseProgress)))
            .andExpect(status().isBadRequest());

        // Validate the CourseProgress in the database
        List<CourseProgress> courseProgressList = courseProgressRepository.findAll();
        assertThat(courseProgressList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCourseProgresses() throws Exception {
        // Initialize the database
        courseProgressRepository.saveAndFlush(courseProgress);

        // Get all the courseProgressList
        restCourseProgressMockMvc.perform(get("/api/course-progresses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(courseProgress.getId().intValue())))
            .andExpect(jsonPath("$.[*].passedIssues").value(hasItem(DEFAULT_PASSED_ISSUES.intValue())))
            .andExpect(jsonPath("$.[*].totalIssues").value(hasItem(DEFAULT_TOTAL_ISSUES.intValue())))
            .andExpect(jsonPath("$.[*].lastActivityDate").value(hasItem(sameInstant(DEFAULT_LAST_ACTIVITY_DATE))));
    }

    @Test
    @Transactional
    public void getCourseProgress() throws Exception {
        // Initialize the database
        courseProgressRepository.saveAndFlush(courseProgress);

        // Get the courseProgress
        restCourseProgressMockMvc.perform(get("/api/course-progresses/{id}", courseProgress.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(courseProgress.getId().intValue()))
            .andExpect(jsonPath("$.passedIssues").value(DEFAULT_PASSED_ISSUES.intValue()))
            .andExpect(jsonPath("$.totalIssues").value(DEFAULT_TOTAL_ISSUES.intValue()))
            .andExpect(jsonPath("$.lastActivityDate").value(sameInstant(DEFAULT_LAST_ACTIVITY_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingCourseProgress() throws Exception {
        // Get the courseProgress
        restCourseProgressMockMvc.perform(get("/api/course-progresses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCourseProgress() throws Exception {
        // Initialize the database
        courseProgressService.save(courseProgress);

        int databaseSizeBeforeUpdate = courseProgressRepository.findAll().size();

        // Update the courseProgress
        CourseProgress updatedCourseProgress = courseProgressRepository.findOne(courseProgress.getId());
        // Disconnect from session so that the updates on updatedCourseProgress are not directly saved in db
        em.detach(updatedCourseProgress);
        updatedCourseProgress
            .passedIssues(UPDATED_PASSED_ISSUES)
            .totalIssues(UPDATED_TOTAL_ISSUES)
            .lastActivityDate(UPDATED_LAST_ACTIVITY_DATE);

        restCourseProgressMockMvc.perform(put("/api/course-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCourseProgress)))
            .andExpect(status().isOk());

        // Validate the CourseProgress in the database
        List<CourseProgress> courseProgressList = courseProgressRepository.findAll();
        assertThat(courseProgressList).hasSize(databaseSizeBeforeUpdate);
        CourseProgress testCourseProgress = courseProgressList.get(courseProgressList.size() - 1);
        assertThat(testCourseProgress.getPassedIssues()).isEqualTo(UPDATED_PASSED_ISSUES);
        assertThat(testCourseProgress.getTotalIssues()).isEqualTo(UPDATED_TOTAL_ISSUES);
        assertThat(testCourseProgress.getLastActivityDate()).isEqualTo(UPDATED_LAST_ACTIVITY_DATE);

        // Validate the CourseProgress in Elasticsearch
        CourseProgress courseProgressEs = courseProgressSearchRepository.findOne(testCourseProgress.getId());
        assertThat(testCourseProgress.getLastActivityDate()).isEqualTo(testCourseProgress.getLastActivityDate());
        assertThat(courseProgressEs).isEqualToIgnoringGivenFields(testCourseProgress, "lastActivityDate");
    }

    @Test
    @Transactional
    public void updateNonExistingCourseProgress() throws Exception {
        int databaseSizeBeforeUpdate = courseProgressRepository.findAll().size();

        // Create the CourseProgress

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCourseProgressMockMvc.perform(put("/api/course-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseProgress)))
            .andExpect(status().isCreated());

        // Validate the CourseProgress in the database
        List<CourseProgress> courseProgressList = courseProgressRepository.findAll();
        assertThat(courseProgressList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCourseProgress() throws Exception {
        // Initialize the database
        courseProgressService.save(courseProgress);

        int databaseSizeBeforeDelete = courseProgressRepository.findAll().size();

        // Get the courseProgress
        restCourseProgressMockMvc.perform(delete("/api/course-progresses/{id}", courseProgress.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean courseProgressExistsInEs = courseProgressSearchRepository.exists(courseProgress.getId());
        assertThat(courseProgressExistsInEs).isFalse();

        // Validate the database is empty
        List<CourseProgress> courseProgressList = courseProgressRepository.findAll();
        assertThat(courseProgressList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCourseProgress() throws Exception {
        // Initialize the database
        courseProgressService.save(courseProgress);

        // Search the courseProgress
        restCourseProgressMockMvc.perform(get("/api/_search/course-progresses?query=id:" + courseProgress.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(courseProgress.getId().intValue())))
            .andExpect(jsonPath("$.[*].passedIssues").value(hasItem(DEFAULT_PASSED_ISSUES.intValue())))
            .andExpect(jsonPath("$.[*].totalIssues").value(hasItem(DEFAULT_TOTAL_ISSUES.intValue())))
            .andExpect(jsonPath("$.[*].lastActivityDate").value(hasItem(sameInstant(DEFAULT_LAST_ACTIVITY_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CourseProgress.class);
        CourseProgress courseProgress1 = new CourseProgress();
        courseProgress1.setId(1L);
        CourseProgress courseProgress2 = new CourseProgress();
        courseProgress2.setId(courseProgress1.getId());
        assertThat(courseProgress1).isEqualTo(courseProgress2);
        courseProgress2.setId(2L);
        assertThat(courseProgress1).isNotEqualTo(courseProgress2);
        courseProgress1.setId(null);
        assertThat(courseProgress1).isNotEqualTo(courseProgress2);
    }
}
