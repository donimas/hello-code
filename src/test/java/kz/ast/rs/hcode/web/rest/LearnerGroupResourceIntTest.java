package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.LearnerGroup;
import kz.ast.rs.hcode.repository.LearnerGroupRepository;
import kz.ast.rs.hcode.service.LearnerGroupService;
import kz.ast.rs.hcode.repository.search.LearnerGroupSearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LearnerGroupResource REST controller.
 *
 * @see LearnerGroupResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class LearnerGroupResourceIntTest {

    private static final String DEFAULT_NAME_RU = "AAAAAAAAAA";
    private static final String UPDATED_NAME_RU = "BBBBBBBBBB";

    private static final String DEFAULT_NAME_KK = "AAAAAAAAAA";
    private static final String UPDATED_NAME_KK = "BBBBBBBBBB";

    private static final String DEFAULT_NAME_EN = "AAAAAAAAAA";
    private static final String UPDATED_NAME_EN = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_RU = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_RU = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_KK = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_KK = "BBBBBBBBBB";

    private static final String DEFAULT_DESCIPRTION_EN = "AAAAAAAAAA";
    private static final String UPDATED_DESCIPRTION_EN = "BBBBBBBBBB";

    private static final Integer DEFAULT_GRADUATION_YEAR = 1;
    private static final Integer UPDATED_GRADUATION_YEAR = 2;

    @Autowired
    private LearnerGroupRepository learnerGroupRepository;

    @Autowired
    private LearnerGroupService learnerGroupService;

    @Autowired
    private LearnerGroupSearchRepository learnerGroupSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLearnerGroupMockMvc;

    private LearnerGroup learnerGroup;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LearnerGroupResource learnerGroupResource = new LearnerGroupResource(learnerGroupService);
        this.restLearnerGroupMockMvc = MockMvcBuilders.standaloneSetup(learnerGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LearnerGroup createEntity(EntityManager em) {
        LearnerGroup learnerGroup = new LearnerGroup()
            .nameRu(DEFAULT_NAME_RU)
            .nameKk(DEFAULT_NAME_KK)
            .nameEn(DEFAULT_NAME_EN)
            .descriptionRu(DEFAULT_DESCRIPTION_RU)
            .descriptionKk(DEFAULT_DESCRIPTION_KK)
            .desciprtionEn(DEFAULT_DESCIPRTION_EN)
            .graduationYear(DEFAULT_GRADUATION_YEAR);
        return learnerGroup;
    }

    @Before
    public void initTest() {
        learnerGroupSearchRepository.deleteAll();
        learnerGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createLearnerGroup() throws Exception {
        int databaseSizeBeforeCreate = learnerGroupRepository.findAll().size();

        // Create the LearnerGroup
        restLearnerGroupMockMvc.perform(post("/api/learner-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(learnerGroup)))
            .andExpect(status().isCreated());

        // Validate the LearnerGroup in the database
        List<LearnerGroup> learnerGroupList = learnerGroupRepository.findAll();
        assertThat(learnerGroupList).hasSize(databaseSizeBeforeCreate + 1);
        LearnerGroup testLearnerGroup = learnerGroupList.get(learnerGroupList.size() - 1);
        assertThat(testLearnerGroup.getNameRu()).isEqualTo(DEFAULT_NAME_RU);
        assertThat(testLearnerGroup.getNameKk()).isEqualTo(DEFAULT_NAME_KK);
        assertThat(testLearnerGroup.getNameEn()).isEqualTo(DEFAULT_NAME_EN);
        assertThat(testLearnerGroup.getDescriptionRu()).isEqualTo(DEFAULT_DESCRIPTION_RU);
        assertThat(testLearnerGroup.getDescriptionKk()).isEqualTo(DEFAULT_DESCRIPTION_KK);
        assertThat(testLearnerGroup.getDesciprtionEn()).isEqualTo(DEFAULT_DESCIPRTION_EN);
        assertThat(testLearnerGroup.getGraduationYear()).isEqualTo(DEFAULT_GRADUATION_YEAR);

        // Validate the LearnerGroup in Elasticsearch
        LearnerGroup learnerGroupEs = learnerGroupSearchRepository.findOne(testLearnerGroup.getId());
        assertThat(learnerGroupEs).isEqualToIgnoringGivenFields(testLearnerGroup);
    }

    @Test
    @Transactional
    public void createLearnerGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = learnerGroupRepository.findAll().size();

        // Create the LearnerGroup with an existing ID
        learnerGroup.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLearnerGroupMockMvc.perform(post("/api/learner-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(learnerGroup)))
            .andExpect(status().isBadRequest());

        // Validate the LearnerGroup in the database
        List<LearnerGroup> learnerGroupList = learnerGroupRepository.findAll();
        assertThat(learnerGroupList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameRuIsRequired() throws Exception {
        int databaseSizeBeforeTest = learnerGroupRepository.findAll().size();
        // set the field null
        learnerGroup.setNameRu(null);

        // Create the LearnerGroup, which fails.

        restLearnerGroupMockMvc.perform(post("/api/learner-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(learnerGroup)))
            .andExpect(status().isBadRequest());

        List<LearnerGroup> learnerGroupList = learnerGroupRepository.findAll();
        assertThat(learnerGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameKkIsRequired() throws Exception {
        int databaseSizeBeforeTest = learnerGroupRepository.findAll().size();
        // set the field null
        learnerGroup.setNameKk(null);

        // Create the LearnerGroup, which fails.

        restLearnerGroupMockMvc.perform(post("/api/learner-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(learnerGroup)))
            .andExpect(status().isBadRequest());

        List<LearnerGroup> learnerGroupList = learnerGroupRepository.findAll();
        assertThat(learnerGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLearnerGroups() throws Exception {
        // Initialize the database
        learnerGroupRepository.saveAndFlush(learnerGroup);

        // Get all the learnerGroupList
        restLearnerGroupMockMvc.perform(get("/api/learner-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(learnerGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameRu").value(hasItem(DEFAULT_NAME_RU.toString())))
            .andExpect(jsonPath("$.[*].nameKk").value(hasItem(DEFAULT_NAME_KK.toString())))
            .andExpect(jsonPath("$.[*].nameEn").value(hasItem(DEFAULT_NAME_EN.toString())))
            .andExpect(jsonPath("$.[*].descriptionRu").value(hasItem(DEFAULT_DESCRIPTION_RU.toString())))
            .andExpect(jsonPath("$.[*].descriptionKk").value(hasItem(DEFAULT_DESCRIPTION_KK.toString())))
            .andExpect(jsonPath("$.[*].desciprtionEn").value(hasItem(DEFAULT_DESCIPRTION_EN.toString())))
            .andExpect(jsonPath("$.[*].graduationYear").value(hasItem(DEFAULT_GRADUATION_YEAR)));
    }

    @Test
    @Transactional
    public void getLearnerGroup() throws Exception {
        // Initialize the database
        learnerGroupRepository.saveAndFlush(learnerGroup);

        // Get the learnerGroup
        restLearnerGroupMockMvc.perform(get("/api/learner-groups/{id}", learnerGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(learnerGroup.getId().intValue()))
            .andExpect(jsonPath("$.nameRu").value(DEFAULT_NAME_RU.toString()))
            .andExpect(jsonPath("$.nameKk").value(DEFAULT_NAME_KK.toString()))
            .andExpect(jsonPath("$.nameEn").value(DEFAULT_NAME_EN.toString()))
            .andExpect(jsonPath("$.descriptionRu").value(DEFAULT_DESCRIPTION_RU.toString()))
            .andExpect(jsonPath("$.descriptionKk").value(DEFAULT_DESCRIPTION_KK.toString()))
            .andExpect(jsonPath("$.desciprtionEn").value(DEFAULT_DESCIPRTION_EN.toString()))
            .andExpect(jsonPath("$.graduationYear").value(DEFAULT_GRADUATION_YEAR));
    }

    @Test
    @Transactional
    public void getNonExistingLearnerGroup() throws Exception {
        // Get the learnerGroup
        restLearnerGroupMockMvc.perform(get("/api/learner-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLearnerGroup() throws Exception {
        // Initialize the database
        learnerGroupService.save(learnerGroup);

        int databaseSizeBeforeUpdate = learnerGroupRepository.findAll().size();

        // Update the learnerGroup
        LearnerGroup updatedLearnerGroup = learnerGroupRepository.findOne(learnerGroup.getId());
        // Disconnect from session so that the updates on updatedLearnerGroup are not directly saved in db
        em.detach(updatedLearnerGroup);
        updatedLearnerGroup
            .nameRu(UPDATED_NAME_RU)
            .nameKk(UPDATED_NAME_KK)
            .nameEn(UPDATED_NAME_EN)
            .descriptionRu(UPDATED_DESCRIPTION_RU)
            .descriptionKk(UPDATED_DESCRIPTION_KK)
            .desciprtionEn(UPDATED_DESCIPRTION_EN)
            .graduationYear(UPDATED_GRADUATION_YEAR);

        restLearnerGroupMockMvc.perform(put("/api/learner-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLearnerGroup)))
            .andExpect(status().isOk());

        // Validate the LearnerGroup in the database
        List<LearnerGroup> learnerGroupList = learnerGroupRepository.findAll();
        assertThat(learnerGroupList).hasSize(databaseSizeBeforeUpdate);
        LearnerGroup testLearnerGroup = learnerGroupList.get(learnerGroupList.size() - 1);
        assertThat(testLearnerGroup.getNameRu()).isEqualTo(UPDATED_NAME_RU);
        assertThat(testLearnerGroup.getNameKk()).isEqualTo(UPDATED_NAME_KK);
        assertThat(testLearnerGroup.getNameEn()).isEqualTo(UPDATED_NAME_EN);
        assertThat(testLearnerGroup.getDescriptionRu()).isEqualTo(UPDATED_DESCRIPTION_RU);
        assertThat(testLearnerGroup.getDescriptionKk()).isEqualTo(UPDATED_DESCRIPTION_KK);
        assertThat(testLearnerGroup.getDesciprtionEn()).isEqualTo(UPDATED_DESCIPRTION_EN);
        assertThat(testLearnerGroup.getGraduationYear()).isEqualTo(UPDATED_GRADUATION_YEAR);

        // Validate the LearnerGroup in Elasticsearch
        LearnerGroup learnerGroupEs = learnerGroupSearchRepository.findOne(testLearnerGroup.getId());
        assertThat(learnerGroupEs).isEqualToIgnoringGivenFields(testLearnerGroup);
    }

    @Test
    @Transactional
    public void updateNonExistingLearnerGroup() throws Exception {
        int databaseSizeBeforeUpdate = learnerGroupRepository.findAll().size();

        // Create the LearnerGroup

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLearnerGroupMockMvc.perform(put("/api/learner-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(learnerGroup)))
            .andExpect(status().isCreated());

        // Validate the LearnerGroup in the database
        List<LearnerGroup> learnerGroupList = learnerGroupRepository.findAll();
        assertThat(learnerGroupList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLearnerGroup() throws Exception {
        // Initialize the database
        learnerGroupService.save(learnerGroup);

        int databaseSizeBeforeDelete = learnerGroupRepository.findAll().size();

        // Get the learnerGroup
        restLearnerGroupMockMvc.perform(delete("/api/learner-groups/{id}", learnerGroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean learnerGroupExistsInEs = learnerGroupSearchRepository.exists(learnerGroup.getId());
        assertThat(learnerGroupExistsInEs).isFalse();

        // Validate the database is empty
        List<LearnerGroup> learnerGroupList = learnerGroupRepository.findAll();
        assertThat(learnerGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchLearnerGroup() throws Exception {
        // Initialize the database
        learnerGroupService.save(learnerGroup);

        // Search the learnerGroup
        restLearnerGroupMockMvc.perform(get("/api/_search/learner-groups?query=id:" + learnerGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(learnerGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameRu").value(hasItem(DEFAULT_NAME_RU.toString())))
            .andExpect(jsonPath("$.[*].nameKk").value(hasItem(DEFAULT_NAME_KK.toString())))
            .andExpect(jsonPath("$.[*].nameEn").value(hasItem(DEFAULT_NAME_EN.toString())))
            .andExpect(jsonPath("$.[*].descriptionRu").value(hasItem(DEFAULT_DESCRIPTION_RU.toString())))
            .andExpect(jsonPath("$.[*].descriptionKk").value(hasItem(DEFAULT_DESCRIPTION_KK.toString())))
            .andExpect(jsonPath("$.[*].desciprtionEn").value(hasItem(DEFAULT_DESCIPRTION_EN.toString())))
            .andExpect(jsonPath("$.[*].graduationYear").value(hasItem(DEFAULT_GRADUATION_YEAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LearnerGroup.class);
        LearnerGroup learnerGroup1 = new LearnerGroup();
        learnerGroup1.setId(1L);
        LearnerGroup learnerGroup2 = new LearnerGroup();
        learnerGroup2.setId(learnerGroup1.getId());
        assertThat(learnerGroup1).isEqualTo(learnerGroup2);
        learnerGroup2.setId(2L);
        assertThat(learnerGroup1).isNotEqualTo(learnerGroup2);
        learnerGroup1.setId(null);
        assertThat(learnerGroup1).isNotEqualTo(learnerGroup2);
    }
}
