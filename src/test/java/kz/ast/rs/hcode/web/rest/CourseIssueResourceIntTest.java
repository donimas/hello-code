package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.CourseIssue;
import kz.ast.rs.hcode.repository.CourseIssueRepository;
import kz.ast.rs.hcode.service.CourseIssueService;
import kz.ast.rs.hcode.repository.search.CourseIssueSearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.ast.rs.hcode.domain.enumeration.IssueType;
/**
 * Test class for the CourseIssueResource REST controller.
 *
 * @see CourseIssueResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class CourseIssueResourceIntTest {

    private static final IssueType DEFAULT_ISSUE_TYPE = IssueType.VIDEO;
    private static final IssueType UPDATED_ISSUE_TYPE = IssueType.TEST;

    private static final String DEFAULT_NAME_RU = "AAAAAAAAAA";
    private static final String UPDATED_NAME_RU = "BBBBBBBBBB";

    private static final String DEFAULT_NAME_KK = "AAAAAAAAAA";
    private static final String UPDATED_NAME_KK = "BBBBBBBBBB";

    private static final String DEFAULT_NAME_EN = "AAAAAAAAAA";
    private static final String UPDATED_NAME_EN = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE_RU = "AAAAAAAAAA";
    private static final String UPDATED_NOTE_RU = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE_KK = "AAAAAAAAAA";
    private static final String UPDATED_NOTE_KK = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE_EN = "AAAAAAAAAA";
    private static final String UPDATED_NOTE_EN = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_ORDER = new BigDecimal(1);
    private static final BigDecimal UPDATED_ORDER = new BigDecimal(2);

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    @Autowired
    private CourseIssueRepository courseIssueRepository;

    @Autowired
    private CourseIssueService courseIssueService;

    @Autowired
    private CourseIssueSearchRepository courseIssueSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCourseIssueMockMvc;

    private CourseIssue courseIssue;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CourseIssueResource courseIssueResource = new CourseIssueResource(courseIssueService);
        this.restCourseIssueMockMvc = MockMvcBuilders.standaloneSetup(courseIssueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CourseIssue createEntity(EntityManager em) {
        CourseIssue courseIssue = new CourseIssue()
            .issueType(DEFAULT_ISSUE_TYPE)
            .nameRu(DEFAULT_NAME_RU)
            .nameKk(DEFAULT_NAME_KK)
            .nameEn(DEFAULT_NAME_EN)
            .noteRu(DEFAULT_NOTE_RU)
            .noteKk(DEFAULT_NOTE_KK)
            .noteEn(DEFAULT_NOTE_EN)
            .order(DEFAULT_ORDER)
            .flagDeleted(DEFAULT_FLAG_DELETED);
        return courseIssue;
    }

    @Before
    public void initTest() {
        courseIssueSearchRepository.deleteAll();
        courseIssue = createEntity(em);
    }

    @Test
    @Transactional
    public void createCourseIssue() throws Exception {
        int databaseSizeBeforeCreate = courseIssueRepository.findAll().size();

        // Create the CourseIssue
        restCourseIssueMockMvc.perform(post("/api/course-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseIssue)))
            .andExpect(status().isCreated());

        // Validate the CourseIssue in the database
        List<CourseIssue> courseIssueList = courseIssueRepository.findAll();
        assertThat(courseIssueList).hasSize(databaseSizeBeforeCreate + 1);
        CourseIssue testCourseIssue = courseIssueList.get(courseIssueList.size() - 1);
        assertThat(testCourseIssue.getIssueType()).isEqualTo(DEFAULT_ISSUE_TYPE);
        assertThat(testCourseIssue.getNameRu()).isEqualTo(DEFAULT_NAME_RU);
        assertThat(testCourseIssue.getNameKk()).isEqualTo(DEFAULT_NAME_KK);
        assertThat(testCourseIssue.getNameEn()).isEqualTo(DEFAULT_NAME_EN);
        assertThat(testCourseIssue.getNoteRu()).isEqualTo(DEFAULT_NOTE_RU);
        assertThat(testCourseIssue.getNoteKk()).isEqualTo(DEFAULT_NOTE_KK);
        assertThat(testCourseIssue.getNoteEn()).isEqualTo(DEFAULT_NOTE_EN);
        assertThat(testCourseIssue.getOrder()).isEqualTo(DEFAULT_ORDER);
        assertThat(testCourseIssue.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);

        // Validate the CourseIssue in Elasticsearch
        CourseIssue courseIssueEs = courseIssueSearchRepository.findOne(testCourseIssue.getId());
        assertThat(courseIssueEs).isEqualToIgnoringGivenFields(testCourseIssue);
    }

    @Test
    @Transactional
    public void createCourseIssueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = courseIssueRepository.findAll().size();

        // Create the CourseIssue with an existing ID
        courseIssue.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCourseIssueMockMvc.perform(post("/api/course-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseIssue)))
            .andExpect(status().isBadRequest());

        // Validate the CourseIssue in the database
        List<CourseIssue> courseIssueList = courseIssueRepository.findAll();
        assertThat(courseIssueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkIssueTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = courseIssueRepository.findAll().size();
        // set the field null
        courseIssue.setIssueType(null);

        // Create the CourseIssue, which fails.

        restCourseIssueMockMvc.perform(post("/api/course-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseIssue)))
            .andExpect(status().isBadRequest());

        List<CourseIssue> courseIssueList = courseIssueRepository.findAll();
        assertThat(courseIssueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameRuIsRequired() throws Exception {
        int databaseSizeBeforeTest = courseIssueRepository.findAll().size();
        // set the field null
        courseIssue.setNameRu(null);

        // Create the CourseIssue, which fails.

        restCourseIssueMockMvc.perform(post("/api/course-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseIssue)))
            .andExpect(status().isBadRequest());

        List<CourseIssue> courseIssueList = courseIssueRepository.findAll();
        assertThat(courseIssueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameKkIsRequired() throws Exception {
        int databaseSizeBeforeTest = courseIssueRepository.findAll().size();
        // set the field null
        courseIssue.setNameKk(null);

        // Create the CourseIssue, which fails.

        restCourseIssueMockMvc.perform(post("/api/course-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseIssue)))
            .andExpect(status().isBadRequest());

        List<CourseIssue> courseIssueList = courseIssueRepository.findAll();
        assertThat(courseIssueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCourseIssues() throws Exception {
        // Initialize the database
        courseIssueRepository.saveAndFlush(courseIssue);

        // Get all the courseIssueList
        restCourseIssueMockMvc.perform(get("/api/course-issues?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(courseIssue.getId().intValue())))
            .andExpect(jsonPath("$.[*].issueType").value(hasItem(DEFAULT_ISSUE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].nameRu").value(hasItem(DEFAULT_NAME_RU.toString())))
            .andExpect(jsonPath("$.[*].nameKk").value(hasItem(DEFAULT_NAME_KK.toString())))
            .andExpect(jsonPath("$.[*].nameEn").value(hasItem(DEFAULT_NAME_EN.toString())))
            .andExpect(jsonPath("$.[*].noteRu").value(hasItem(DEFAULT_NOTE_RU.toString())))
            .andExpect(jsonPath("$.[*].noteKk").value(hasItem(DEFAULT_NOTE_KK.toString())))
            .andExpect(jsonPath("$.[*].noteEn").value(hasItem(DEFAULT_NOTE_EN.toString())))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER.intValue())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getCourseIssue() throws Exception {
        // Initialize the database
        courseIssueRepository.saveAndFlush(courseIssue);

        // Get the courseIssue
        restCourseIssueMockMvc.perform(get("/api/course-issues/{id}", courseIssue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(courseIssue.getId().intValue()))
            .andExpect(jsonPath("$.issueType").value(DEFAULT_ISSUE_TYPE.toString()))
            .andExpect(jsonPath("$.nameRu").value(DEFAULT_NAME_RU.toString()))
            .andExpect(jsonPath("$.nameKk").value(DEFAULT_NAME_KK.toString()))
            .andExpect(jsonPath("$.nameEn").value(DEFAULT_NAME_EN.toString()))
            .andExpect(jsonPath("$.noteRu").value(DEFAULT_NOTE_RU.toString()))
            .andExpect(jsonPath("$.noteKk").value(DEFAULT_NOTE_KK.toString()))
            .andExpect(jsonPath("$.noteEn").value(DEFAULT_NOTE_EN.toString()))
            .andExpect(jsonPath("$.order").value(DEFAULT_ORDER.intValue()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCourseIssue() throws Exception {
        // Get the courseIssue
        restCourseIssueMockMvc.perform(get("/api/course-issues/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCourseIssue() throws Exception {
        // Initialize the database
        courseIssueService.save(courseIssue);

        int databaseSizeBeforeUpdate = courseIssueRepository.findAll().size();

        // Update the courseIssue
        CourseIssue updatedCourseIssue = courseIssueRepository.findOne(courseIssue.getId());
        // Disconnect from session so that the updates on updatedCourseIssue are not directly saved in db
        em.detach(updatedCourseIssue);
        updatedCourseIssue
            .issueType(UPDATED_ISSUE_TYPE)
            .nameRu(UPDATED_NAME_RU)
            .nameKk(UPDATED_NAME_KK)
            .nameEn(UPDATED_NAME_EN)
            .noteRu(UPDATED_NOTE_RU)
            .noteKk(UPDATED_NOTE_KK)
            .noteEn(UPDATED_NOTE_EN)
            .order(UPDATED_ORDER)
            .flagDeleted(UPDATED_FLAG_DELETED);

        restCourseIssueMockMvc.perform(put("/api/course-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCourseIssue)))
            .andExpect(status().isOk());

        // Validate the CourseIssue in the database
        List<CourseIssue> courseIssueList = courseIssueRepository.findAll();
        assertThat(courseIssueList).hasSize(databaseSizeBeforeUpdate);
        CourseIssue testCourseIssue = courseIssueList.get(courseIssueList.size() - 1);
        assertThat(testCourseIssue.getIssueType()).isEqualTo(UPDATED_ISSUE_TYPE);
        assertThat(testCourseIssue.getNameRu()).isEqualTo(UPDATED_NAME_RU);
        assertThat(testCourseIssue.getNameKk()).isEqualTo(UPDATED_NAME_KK);
        assertThat(testCourseIssue.getNameEn()).isEqualTo(UPDATED_NAME_EN);
        assertThat(testCourseIssue.getNoteRu()).isEqualTo(UPDATED_NOTE_RU);
        assertThat(testCourseIssue.getNoteKk()).isEqualTo(UPDATED_NOTE_KK);
        assertThat(testCourseIssue.getNoteEn()).isEqualTo(UPDATED_NOTE_EN);
        assertThat(testCourseIssue.getOrder()).isEqualTo(UPDATED_ORDER);
        assertThat(testCourseIssue.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);

        // Validate the CourseIssue in Elasticsearch
        CourseIssue courseIssueEs = courseIssueSearchRepository.findOne(testCourseIssue.getId());
        assertThat(courseIssueEs).isEqualToIgnoringGivenFields(testCourseIssue);
    }

    @Test
    @Transactional
    public void updateNonExistingCourseIssue() throws Exception {
        int databaseSizeBeforeUpdate = courseIssueRepository.findAll().size();

        // Create the CourseIssue

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCourseIssueMockMvc.perform(put("/api/course-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseIssue)))
            .andExpect(status().isCreated());

        // Validate the CourseIssue in the database
        List<CourseIssue> courseIssueList = courseIssueRepository.findAll();
        assertThat(courseIssueList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCourseIssue() throws Exception {
        // Initialize the database
        courseIssueService.save(courseIssue);

        int databaseSizeBeforeDelete = courseIssueRepository.findAll().size();

        // Get the courseIssue
        restCourseIssueMockMvc.perform(delete("/api/course-issues/{id}", courseIssue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean courseIssueExistsInEs = courseIssueSearchRepository.exists(courseIssue.getId());
        assertThat(courseIssueExistsInEs).isFalse();

        // Validate the database is empty
        List<CourseIssue> courseIssueList = courseIssueRepository.findAll();
        assertThat(courseIssueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCourseIssue() throws Exception {
        // Initialize the database
        courseIssueService.save(courseIssue);

        // Search the courseIssue
        restCourseIssueMockMvc.perform(get("/api/_search/course-issues?query=id:" + courseIssue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(courseIssue.getId().intValue())))
            .andExpect(jsonPath("$.[*].issueType").value(hasItem(DEFAULT_ISSUE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].nameRu").value(hasItem(DEFAULT_NAME_RU.toString())))
            .andExpect(jsonPath("$.[*].nameKk").value(hasItem(DEFAULT_NAME_KK.toString())))
            .andExpect(jsonPath("$.[*].nameEn").value(hasItem(DEFAULT_NAME_EN.toString())))
            .andExpect(jsonPath("$.[*].noteRu").value(hasItem(DEFAULT_NOTE_RU.toString())))
            .andExpect(jsonPath("$.[*].noteKk").value(hasItem(DEFAULT_NOTE_KK.toString())))
            .andExpect(jsonPath("$.[*].noteEn").value(hasItem(DEFAULT_NOTE_EN.toString())))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER.intValue())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CourseIssue.class);
        CourseIssue courseIssue1 = new CourseIssue();
        courseIssue1.setId(1L);
        CourseIssue courseIssue2 = new CourseIssue();
        courseIssue2.setId(courseIssue1.getId());
        assertThat(courseIssue1).isEqualTo(courseIssue2);
        courseIssue2.setId(2L);
        assertThat(courseIssue1).isNotEqualTo(courseIssue2);
        courseIssue1.setId(null);
        assertThat(courseIssue1).isNotEqualTo(courseIssue2);
    }
}
