package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.TestQuestion;
import kz.ast.rs.hcode.repository.TestQuestionRepository;
import kz.ast.rs.hcode.service.TestQuestionService;
import kz.ast.rs.hcode.repository.search.TestQuestionSearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TestQuestionResource REST controller.
 *
 * @see TestQuestionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class TestQuestionResourceIntTest {

    private static final String DEFAULT_BODY_RU = "AAAAAAAAAA";
    private static final String UPDATED_BODY_RU = "BBBBBBBBBB";

    private static final String DEFAULT_BODY_KK = "AAAAAAAAAA";
    private static final String UPDATED_BODY_KK = "BBBBBBBBBB";

    private static final String DEFAULT_BODY_EN = "AAAAAAAAAA";
    private static final String UPDATED_BODY_EN = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    @Autowired
    private TestQuestionRepository testQuestionRepository;

    @Autowired
    private TestQuestionService testQuestionService;

    @Autowired
    private TestQuestionSearchRepository testQuestionSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTestQuestionMockMvc;

    private TestQuestion testQuestion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TestQuestionResource testQuestionResource = new TestQuestionResource(testQuestionService);
        this.restTestQuestionMockMvc = MockMvcBuilders.standaloneSetup(testQuestionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TestQuestion createEntity(EntityManager em) {
        TestQuestion testQuestion = new TestQuestion()
            .bodyRu(DEFAULT_BODY_RU)
            .bodyKk(DEFAULT_BODY_KK)
            .bodyEn(DEFAULT_BODY_EN)
            .flagDeleted(DEFAULT_FLAG_DELETED);
        return testQuestion;
    }

    @Before
    public void initTest() {
        testQuestionSearchRepository.deleteAll();
        testQuestion = createEntity(em);
    }

    @Test
    @Transactional
    public void createTestQuestion() throws Exception {
        int databaseSizeBeforeCreate = testQuestionRepository.findAll().size();

        // Create the TestQuestion
        restTestQuestionMockMvc.perform(post("/api/test-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testQuestion)))
            .andExpect(status().isCreated());

        // Validate the TestQuestion in the database
        List<TestQuestion> testQuestionList = testQuestionRepository.findAll();
        assertThat(testQuestionList).hasSize(databaseSizeBeforeCreate + 1);
        TestQuestion testTestQuestion = testQuestionList.get(testQuestionList.size() - 1);
        assertThat(testTestQuestion.getBodyRu()).isEqualTo(DEFAULT_BODY_RU);
        assertThat(testTestQuestion.getBodyKk()).isEqualTo(DEFAULT_BODY_KK);
        assertThat(testTestQuestion.getBodyEn()).isEqualTo(DEFAULT_BODY_EN);
        assertThat(testTestQuestion.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);

        // Validate the TestQuestion in Elasticsearch
        TestQuestion testQuestionEs = testQuestionSearchRepository.findOne(testTestQuestion.getId());
        assertThat(testQuestionEs).isEqualToIgnoringGivenFields(testTestQuestion);
    }

    @Test
    @Transactional
    public void createTestQuestionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = testQuestionRepository.findAll().size();

        // Create the TestQuestion with an existing ID
        testQuestion.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTestQuestionMockMvc.perform(post("/api/test-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testQuestion)))
            .andExpect(status().isBadRequest());

        // Validate the TestQuestion in the database
        List<TestQuestion> testQuestionList = testQuestionRepository.findAll();
        assertThat(testQuestionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTestQuestions() throws Exception {
        // Initialize the database
        testQuestionRepository.saveAndFlush(testQuestion);

        // Get all the testQuestionList
        restTestQuestionMockMvc.perform(get("/api/test-questions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(testQuestion.getId().intValue())))
            .andExpect(jsonPath("$.[*].bodyRu").value(hasItem(DEFAULT_BODY_RU.toString())))
            .andExpect(jsonPath("$.[*].bodyKk").value(hasItem(DEFAULT_BODY_KK.toString())))
            .andExpect(jsonPath("$.[*].bodyEn").value(hasItem(DEFAULT_BODY_EN.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getTestQuestion() throws Exception {
        // Initialize the database
        testQuestionRepository.saveAndFlush(testQuestion);

        // Get the testQuestion
        restTestQuestionMockMvc.perform(get("/api/test-questions/{id}", testQuestion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(testQuestion.getId().intValue()))
            .andExpect(jsonPath("$.bodyRu").value(DEFAULT_BODY_RU.toString()))
            .andExpect(jsonPath("$.bodyKk").value(DEFAULT_BODY_KK.toString()))
            .andExpect(jsonPath("$.bodyEn").value(DEFAULT_BODY_EN.toString()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTestQuestion() throws Exception {
        // Get the testQuestion
        restTestQuestionMockMvc.perform(get("/api/test-questions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTestQuestion() throws Exception {
        // Initialize the database
        testQuestionService.save(testQuestion);

        int databaseSizeBeforeUpdate = testQuestionRepository.findAll().size();

        // Update the testQuestion
        TestQuestion updatedTestQuestion = testQuestionRepository.findOne(testQuestion.getId());
        // Disconnect from session so that the updates on updatedTestQuestion are not directly saved in db
        em.detach(updatedTestQuestion);
        updatedTestQuestion
            .bodyRu(UPDATED_BODY_RU)
            .bodyKk(UPDATED_BODY_KK)
            .bodyEn(UPDATED_BODY_EN)
            .flagDeleted(UPDATED_FLAG_DELETED);

        restTestQuestionMockMvc.perform(put("/api/test-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTestQuestion)))
            .andExpect(status().isOk());

        // Validate the TestQuestion in the database
        List<TestQuestion> testQuestionList = testQuestionRepository.findAll();
        assertThat(testQuestionList).hasSize(databaseSizeBeforeUpdate);
        TestQuestion testTestQuestion = testQuestionList.get(testQuestionList.size() - 1);
        assertThat(testTestQuestion.getBodyRu()).isEqualTo(UPDATED_BODY_RU);
        assertThat(testTestQuestion.getBodyKk()).isEqualTo(UPDATED_BODY_KK);
        assertThat(testTestQuestion.getBodyEn()).isEqualTo(UPDATED_BODY_EN);
        assertThat(testTestQuestion.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);

        // Validate the TestQuestion in Elasticsearch
        TestQuestion testQuestionEs = testQuestionSearchRepository.findOne(testTestQuestion.getId());
        assertThat(testQuestionEs).isEqualToIgnoringGivenFields(testTestQuestion);
    }

    @Test
    @Transactional
    public void updateNonExistingTestQuestion() throws Exception {
        int databaseSizeBeforeUpdate = testQuestionRepository.findAll().size();

        // Create the TestQuestion

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTestQuestionMockMvc.perform(put("/api/test-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(testQuestion)))
            .andExpect(status().isCreated());

        // Validate the TestQuestion in the database
        List<TestQuestion> testQuestionList = testQuestionRepository.findAll();
        assertThat(testQuestionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTestQuestion() throws Exception {
        // Initialize the database
        testQuestionService.save(testQuestion);

        int databaseSizeBeforeDelete = testQuestionRepository.findAll().size();

        // Get the testQuestion
        restTestQuestionMockMvc.perform(delete("/api/test-questions/{id}", testQuestion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean testQuestionExistsInEs = testQuestionSearchRepository.exists(testQuestion.getId());
        assertThat(testQuestionExistsInEs).isFalse();

        // Validate the database is empty
        List<TestQuestion> testQuestionList = testQuestionRepository.findAll();
        assertThat(testQuestionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTestQuestion() throws Exception {
        // Initialize the database
        testQuestionService.save(testQuestion);

        // Search the testQuestion
        restTestQuestionMockMvc.perform(get("/api/_search/test-questions?query=id:" + testQuestion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(testQuestion.getId().intValue())))
            .andExpect(jsonPath("$.[*].bodyRu").value(hasItem(DEFAULT_BODY_RU.toString())))
            .andExpect(jsonPath("$.[*].bodyKk").value(hasItem(DEFAULT_BODY_KK.toString())))
            .andExpect(jsonPath("$.[*].bodyEn").value(hasItem(DEFAULT_BODY_EN.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TestQuestion.class);
        TestQuestion testQuestion1 = new TestQuestion();
        testQuestion1.setId(1L);
        TestQuestion testQuestion2 = new TestQuestion();
        testQuestion2.setId(testQuestion1.getId());
        assertThat(testQuestion1).isEqualTo(testQuestion2);
        testQuestion2.setId(2L);
        assertThat(testQuestion1).isNotEqualTo(testQuestion2);
        testQuestion1.setId(null);
        assertThat(testQuestion1).isNotEqualTo(testQuestion2);
    }
}
