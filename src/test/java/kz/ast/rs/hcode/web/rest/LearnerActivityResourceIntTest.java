package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.LearnerActivity;
import kz.ast.rs.hcode.repository.LearnerActivityRepository;
import kz.ast.rs.hcode.service.LearnerActivityService;
import kz.ast.rs.hcode.repository.search.LearnerActivitySearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LearnerActivityResource REST controller.
 *
 * @see LearnerActivityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class LearnerActivityResourceIntTest {

    private static final Boolean DEFAULT_FLAG_PASSED = false;
    private static final Boolean UPDATED_FLAG_PASSED = true;

    @Autowired
    private LearnerActivityRepository learnerActivityRepository;

    @Autowired
    private LearnerActivityService learnerActivityService;

    @Autowired
    private LearnerActivitySearchRepository learnerActivitySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLearnerActivityMockMvc;

    private LearnerActivity learnerActivity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LearnerActivityResource learnerActivityResource = new LearnerActivityResource(learnerActivityService);
        this.restLearnerActivityMockMvc = MockMvcBuilders.standaloneSetup(learnerActivityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LearnerActivity createEntity(EntityManager em) {
        LearnerActivity learnerActivity = new LearnerActivity()
            .flagPassed(DEFAULT_FLAG_PASSED);
        return learnerActivity;
    }

    @Before
    public void initTest() {
        learnerActivitySearchRepository.deleteAll();
        learnerActivity = createEntity(em);
    }

    @Test
    @Transactional
    public void createLearnerActivity() throws Exception {
        int databaseSizeBeforeCreate = learnerActivityRepository.findAll().size();

        // Create the LearnerActivity
        restLearnerActivityMockMvc.perform(post("/api/learner-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(learnerActivity)))
            .andExpect(status().isCreated());

        // Validate the LearnerActivity in the database
        List<LearnerActivity> learnerActivityList = learnerActivityRepository.findAll();
        assertThat(learnerActivityList).hasSize(databaseSizeBeforeCreate + 1);
        LearnerActivity testLearnerActivity = learnerActivityList.get(learnerActivityList.size() - 1);
        assertThat(testLearnerActivity.isFlagPassed()).isEqualTo(DEFAULT_FLAG_PASSED);

        // Validate the LearnerActivity in Elasticsearch
        LearnerActivity learnerActivityEs = learnerActivitySearchRepository.findOne(testLearnerActivity.getId());
        assertThat(learnerActivityEs).isEqualToIgnoringGivenFields(testLearnerActivity);
    }

    @Test
    @Transactional
    public void createLearnerActivityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = learnerActivityRepository.findAll().size();

        // Create the LearnerActivity with an existing ID
        learnerActivity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLearnerActivityMockMvc.perform(post("/api/learner-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(learnerActivity)))
            .andExpect(status().isBadRequest());

        // Validate the LearnerActivity in the database
        List<LearnerActivity> learnerActivityList = learnerActivityRepository.findAll();
        assertThat(learnerActivityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllLearnerActivities() throws Exception {
        // Initialize the database
        learnerActivityRepository.saveAndFlush(learnerActivity);

        // Get all the learnerActivityList
        restLearnerActivityMockMvc.perform(get("/api/learner-activities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(learnerActivity.getId().intValue())))
            .andExpect(jsonPath("$.[*].flagPassed").value(hasItem(DEFAULT_FLAG_PASSED.booleanValue())));
    }

    @Test
    @Transactional
    public void getLearnerActivity() throws Exception {
        // Initialize the database
        learnerActivityRepository.saveAndFlush(learnerActivity);

        // Get the learnerActivity
        restLearnerActivityMockMvc.perform(get("/api/learner-activities/{id}", learnerActivity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(learnerActivity.getId().intValue()))
            .andExpect(jsonPath("$.flagPassed").value(DEFAULT_FLAG_PASSED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingLearnerActivity() throws Exception {
        // Get the learnerActivity
        restLearnerActivityMockMvc.perform(get("/api/learner-activities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLearnerActivity() throws Exception {
        // Initialize the database
        learnerActivityService.save(learnerActivity);

        int databaseSizeBeforeUpdate = learnerActivityRepository.findAll().size();

        // Update the learnerActivity
        LearnerActivity updatedLearnerActivity = learnerActivityRepository.findOne(learnerActivity.getId());
        // Disconnect from session so that the updates on updatedLearnerActivity are not directly saved in db
        em.detach(updatedLearnerActivity);
        updatedLearnerActivity
            .flagPassed(UPDATED_FLAG_PASSED);

        restLearnerActivityMockMvc.perform(put("/api/learner-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLearnerActivity)))
            .andExpect(status().isOk());

        // Validate the LearnerActivity in the database
        List<LearnerActivity> learnerActivityList = learnerActivityRepository.findAll();
        assertThat(learnerActivityList).hasSize(databaseSizeBeforeUpdate);
        LearnerActivity testLearnerActivity = learnerActivityList.get(learnerActivityList.size() - 1);
        assertThat(testLearnerActivity.isFlagPassed()).isEqualTo(UPDATED_FLAG_PASSED);

        // Validate the LearnerActivity in Elasticsearch
        LearnerActivity learnerActivityEs = learnerActivitySearchRepository.findOne(testLearnerActivity.getId());
        assertThat(learnerActivityEs).isEqualToIgnoringGivenFields(testLearnerActivity);
    }

    @Test
    @Transactional
    public void updateNonExistingLearnerActivity() throws Exception {
        int databaseSizeBeforeUpdate = learnerActivityRepository.findAll().size();

        // Create the LearnerActivity

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLearnerActivityMockMvc.perform(put("/api/learner-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(learnerActivity)))
            .andExpect(status().isCreated());

        // Validate the LearnerActivity in the database
        List<LearnerActivity> learnerActivityList = learnerActivityRepository.findAll();
        assertThat(learnerActivityList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLearnerActivity() throws Exception {
        // Initialize the database
        learnerActivityService.save(learnerActivity);

        int databaseSizeBeforeDelete = learnerActivityRepository.findAll().size();

        // Get the learnerActivity
        restLearnerActivityMockMvc.perform(delete("/api/learner-activities/{id}", learnerActivity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean learnerActivityExistsInEs = learnerActivitySearchRepository.exists(learnerActivity.getId());
        assertThat(learnerActivityExistsInEs).isFalse();

        // Validate the database is empty
        List<LearnerActivity> learnerActivityList = learnerActivityRepository.findAll();
        assertThat(learnerActivityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchLearnerActivity() throws Exception {
        // Initialize the database
        learnerActivityService.save(learnerActivity);

        // Search the learnerActivity
        restLearnerActivityMockMvc.perform(get("/api/_search/learner-activities?query=id:" + learnerActivity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(learnerActivity.getId().intValue())))
            .andExpect(jsonPath("$.[*].flagPassed").value(hasItem(DEFAULT_FLAG_PASSED.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LearnerActivity.class);
        LearnerActivity learnerActivity1 = new LearnerActivity();
        learnerActivity1.setId(1L);
        LearnerActivity learnerActivity2 = new LearnerActivity();
        learnerActivity2.setId(learnerActivity1.getId());
        assertThat(learnerActivity1).isEqualTo(learnerActivity2);
        learnerActivity2.setId(2L);
        assertThat(learnerActivity1).isNotEqualTo(learnerActivity2);
        learnerActivity1.setId(null);
        assertThat(learnerActivity1).isNotEqualTo(learnerActivity2);
    }
}
