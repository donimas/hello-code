package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.Course;
import kz.ast.rs.hcode.repository.CourseRepository;
import kz.ast.rs.hcode.service.CourseService;
import kz.ast.rs.hcode.repository.search.CourseSearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CourseResource REST controller.
 *
 * @see CourseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class CourseResourceIntTest {

    private static final BigDecimal DEFAULT_TOTAL_ISSUES = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_ISSUES = new BigDecimal(2);

    private static final BigDecimal DEFAULT_LECTURES_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_LECTURES_AMOUNT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_VIDEO_DURATION = new BigDecimal(1);
    private static final BigDecimal UPDATED_VIDEO_DURATION = new BigDecimal(2);

    private static final String DEFAULT_NAME_RU = "AAAAAAAAAA";
    private static final String UPDATED_NAME_RU = "BBBBBBBBBB";

    private static final String DEFAULT_NAME_KK = "AAAAAAAAAA";
    private static final String UPDATED_NAME_KK = "BBBBBBBBBB";

    private static final String DEFAULT_NAME_EN = "AAAAAAAAAA";
    private static final String UPDATED_NAME_EN = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_RU = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_RU = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_KK = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_KK = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_EN = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_EN = "BBBBBBBBBB";

    private static final String DEFAULT_BRIEF_RU = "AAAAAAAAAA";
    private static final String UPDATED_BRIEF_RU = "BBBBBBBBBB";

    private static final String DEFAULT_BRIEF_KK = "AAAAAAAAAA";
    private static final String UPDATED_BRIEF_KK = "BBBBBBBBBB";

    private static final String DEFAULT_BRIEF_EN = "AAAAAAAAAA";
    private static final String UPDATED_BRIEF_EN = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseService courseService;

    @Autowired
    private CourseSearchRepository courseSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCourseMockMvc;

    private Course course;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CourseResource courseResource = new CourseResource(courseService);
        this.restCourseMockMvc = MockMvcBuilders.standaloneSetup(courseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Course createEntity(EntityManager em) {
        Course course = new Course()
            .totalIssues(DEFAULT_TOTAL_ISSUES)
            .lecturesAmount(DEFAULT_LECTURES_AMOUNT)
            .videoDuration(DEFAULT_VIDEO_DURATION)
            .nameRu(DEFAULT_NAME_RU)
            .nameKk(DEFAULT_NAME_KK)
            .nameEn(DEFAULT_NAME_EN)
            .descriptionRu(DEFAULT_DESCRIPTION_RU)
            .descriptionKk(DEFAULT_DESCRIPTION_KK)
            .descriptionEn(DEFAULT_DESCRIPTION_EN)
            .briefRu(DEFAULT_BRIEF_RU)
            .briefKk(DEFAULT_BRIEF_KK)
            .briefEn(DEFAULT_BRIEF_EN)
            .flagDeleted(DEFAULT_FLAG_DELETED)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE);
        return course;
    }

    @Before
    public void initTest() {
        courseSearchRepository.deleteAll();
        course = createEntity(em);
    }

    @Test
    @Transactional
    public void createCourse() throws Exception {
        int databaseSizeBeforeCreate = courseRepository.findAll().size();

        // Create the Course
        restCourseMockMvc.perform(post("/api/courses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(course)))
            .andExpect(status().isCreated());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeCreate + 1);
        Course testCourse = courseList.get(courseList.size() - 1);
        assertThat(testCourse.getTotalIssues()).isEqualTo(DEFAULT_TOTAL_ISSUES);
        assertThat(testCourse.getLecturesAmount()).isEqualTo(DEFAULT_LECTURES_AMOUNT);
        assertThat(testCourse.getVideoDuration()).isEqualTo(DEFAULT_VIDEO_DURATION);
        assertThat(testCourse.getNameRu()).isEqualTo(DEFAULT_NAME_RU);
        assertThat(testCourse.getNameKk()).isEqualTo(DEFAULT_NAME_KK);
        assertThat(testCourse.getNameEn()).isEqualTo(DEFAULT_NAME_EN);
        assertThat(testCourse.getDescriptionRu()).isEqualTo(DEFAULT_DESCRIPTION_RU);
        assertThat(testCourse.getDescriptionKk()).isEqualTo(DEFAULT_DESCRIPTION_KK);
        assertThat(testCourse.getDescriptionEn()).isEqualTo(DEFAULT_DESCRIPTION_EN);
        assertThat(testCourse.getBriefRu()).isEqualTo(DEFAULT_BRIEF_RU);
        assertThat(testCourse.getBriefKk()).isEqualTo(DEFAULT_BRIEF_KK);
        assertThat(testCourse.getBriefEn()).isEqualTo(DEFAULT_BRIEF_EN);
        assertThat(testCourse.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);
        assertThat(testCourse.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testCourse.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);

        // Validate the Course in Elasticsearch
        Course courseEs = courseSearchRepository.findOne(testCourse.getId());
        assertThat(courseEs).isEqualToIgnoringGivenFields(testCourse);
    }

    @Test
    @Transactional
    public void createCourseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = courseRepository.findAll().size();

        // Create the Course with an existing ID
        course.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCourseMockMvc.perform(post("/api/courses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(course)))
            .andExpect(status().isBadRequest());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameRuIsRequired() throws Exception {
        int databaseSizeBeforeTest = courseRepository.findAll().size();
        // set the field null
        course.setNameRu(null);

        // Create the Course, which fails.

        restCourseMockMvc.perform(post("/api/courses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(course)))
            .andExpect(status().isBadRequest());

        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameKkIsRequired() throws Exception {
        int databaseSizeBeforeTest = courseRepository.findAll().size();
        // set the field null
        course.setNameKk(null);

        // Create the Course, which fails.

        restCourseMockMvc.perform(post("/api/courses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(course)))
            .andExpect(status().isBadRequest());

        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCourses() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get all the courseList
        restCourseMockMvc.perform(get("/api/courses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(course.getId().intValue())))
            .andExpect(jsonPath("$.[*].totalIssues").value(hasItem(DEFAULT_TOTAL_ISSUES.intValue())))
            .andExpect(jsonPath("$.[*].lecturesAmount").value(hasItem(DEFAULT_LECTURES_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].videoDuration").value(hasItem(DEFAULT_VIDEO_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].nameRu").value(hasItem(DEFAULT_NAME_RU.toString())))
            .andExpect(jsonPath("$.[*].nameKk").value(hasItem(DEFAULT_NAME_KK.toString())))
            .andExpect(jsonPath("$.[*].nameEn").value(hasItem(DEFAULT_NAME_EN.toString())))
            .andExpect(jsonPath("$.[*].descriptionRu").value(hasItem(DEFAULT_DESCRIPTION_RU.toString())))
            .andExpect(jsonPath("$.[*].descriptionKk").value(hasItem(DEFAULT_DESCRIPTION_KK.toString())))
            .andExpect(jsonPath("$.[*].descriptionEn").value(hasItem(DEFAULT_DESCRIPTION_EN.toString())))
            .andExpect(jsonPath("$.[*].briefRu").value(hasItem(DEFAULT_BRIEF_RU.toString())))
            .andExpect(jsonPath("$.[*].briefKk").value(hasItem(DEFAULT_BRIEF_KK.toString())))
            .andExpect(jsonPath("$.[*].briefEn").value(hasItem(DEFAULT_BRIEF_EN.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))));
    }

    @Test
    @Transactional
    public void getCourse() throws Exception {
        // Initialize the database
        courseRepository.saveAndFlush(course);

        // Get the course
        restCourseMockMvc.perform(get("/api/courses/{id}", course.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(course.getId().intValue()))
            .andExpect(jsonPath("$.totalIssues").value(DEFAULT_TOTAL_ISSUES.intValue()))
            .andExpect(jsonPath("$.lecturesAmount").value(DEFAULT_LECTURES_AMOUNT.intValue()))
            .andExpect(jsonPath("$.videoDuration").value(DEFAULT_VIDEO_DURATION.intValue()))
            .andExpect(jsonPath("$.nameRu").value(DEFAULT_NAME_RU.toString()))
            .andExpect(jsonPath("$.nameKk").value(DEFAULT_NAME_KK.toString()))
            .andExpect(jsonPath("$.nameEn").value(DEFAULT_NAME_EN.toString()))
            .andExpect(jsonPath("$.descriptionRu").value(DEFAULT_DESCRIPTION_RU.toString()))
            .andExpect(jsonPath("$.descriptionKk").value(DEFAULT_DESCRIPTION_KK.toString()))
            .andExpect(jsonPath("$.descriptionEn").value(DEFAULT_DESCRIPTION_EN.toString()))
            .andExpect(jsonPath("$.briefRu").value(DEFAULT_BRIEF_RU.toString()))
            .andExpect(jsonPath("$.briefKk").value(DEFAULT_BRIEF_KK.toString()))
            .andExpect(jsonPath("$.briefEn").value(DEFAULT_BRIEF_EN.toString()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)));
    }

    @Test
    @Transactional
    public void getNonExistingCourse() throws Exception {
        // Get the course
        restCourseMockMvc.perform(get("/api/courses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCourse() throws Exception {
        // Initialize the database
        courseService.save(course);

        int databaseSizeBeforeUpdate = courseRepository.findAll().size();

        // Update the course
        Course updatedCourse = courseRepository.findOne(course.getId());
        // Disconnect from session so that the updates on updatedCourse are not directly saved in db
        em.detach(updatedCourse);
        updatedCourse
            .totalIssues(UPDATED_TOTAL_ISSUES)
            .lecturesAmount(UPDATED_LECTURES_AMOUNT)
            .videoDuration(UPDATED_VIDEO_DURATION)
            .nameRu(UPDATED_NAME_RU)
            .nameKk(UPDATED_NAME_KK)
            .nameEn(UPDATED_NAME_EN)
            .descriptionRu(UPDATED_DESCRIPTION_RU)
            .descriptionKk(UPDATED_DESCRIPTION_KK)
            .descriptionEn(UPDATED_DESCRIPTION_EN)
            .briefRu(UPDATED_BRIEF_RU)
            .briefKk(UPDATED_BRIEF_KK)
            .briefEn(UPDATED_BRIEF_EN)
            .flagDeleted(UPDATED_FLAG_DELETED)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE);

        restCourseMockMvc.perform(put("/api/courses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCourse)))
            .andExpect(status().isOk());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeUpdate);
        Course testCourse = courseList.get(courseList.size() - 1);
        assertThat(testCourse.getTotalIssues()).isEqualTo(UPDATED_TOTAL_ISSUES);
        assertThat(testCourse.getLecturesAmount()).isEqualTo(UPDATED_LECTURES_AMOUNT);
        assertThat(testCourse.getVideoDuration()).isEqualTo(UPDATED_VIDEO_DURATION);
        assertThat(testCourse.getNameRu()).isEqualTo(UPDATED_NAME_RU);
        assertThat(testCourse.getNameKk()).isEqualTo(UPDATED_NAME_KK);
        assertThat(testCourse.getNameEn()).isEqualTo(UPDATED_NAME_EN);
        assertThat(testCourse.getDescriptionRu()).isEqualTo(UPDATED_DESCRIPTION_RU);
        assertThat(testCourse.getDescriptionKk()).isEqualTo(UPDATED_DESCRIPTION_KK);
        assertThat(testCourse.getDescriptionEn()).isEqualTo(UPDATED_DESCRIPTION_EN);
        assertThat(testCourse.getBriefRu()).isEqualTo(UPDATED_BRIEF_RU);
        assertThat(testCourse.getBriefKk()).isEqualTo(UPDATED_BRIEF_KK);
        assertThat(testCourse.getBriefEn()).isEqualTo(UPDATED_BRIEF_EN);
        assertThat(testCourse.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);
        assertThat(testCourse.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testCourse.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);

        // Validate the Course in Elasticsearch
        Course courseEs = courseSearchRepository.findOne(testCourse.getId());
        assertThat(courseEs).isEqualToIgnoringGivenFields(testCourse);
    }

    @Test
    @Transactional
    public void updateNonExistingCourse() throws Exception {
        int databaseSizeBeforeUpdate = courseRepository.findAll().size();

        // Create the Course

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCourseMockMvc.perform(put("/api/courses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(course)))
            .andExpect(status().isCreated());

        // Validate the Course in the database
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCourse() throws Exception {
        // Initialize the database
        courseService.save(course);

        int databaseSizeBeforeDelete = courseRepository.findAll().size();

        // Get the course
        restCourseMockMvc.perform(delete("/api/courses/{id}", course.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean courseExistsInEs = courseSearchRepository.exists(course.getId());
        assertThat(courseExistsInEs).isFalse();

        // Validate the database is empty
        List<Course> courseList = courseRepository.findAll();
        assertThat(courseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCourse() throws Exception {
        // Initialize the database
        courseService.save(course);

        // Search the course
        restCourseMockMvc.perform(get("/api/_search/courses?query=id:" + course.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(course.getId().intValue())))
            .andExpect(jsonPath("$.[*].totalIssues").value(hasItem(DEFAULT_TOTAL_ISSUES.intValue())))
            .andExpect(jsonPath("$.[*].lecturesAmount").value(hasItem(DEFAULT_LECTURES_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].videoDuration").value(hasItem(DEFAULT_VIDEO_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].nameRu").value(hasItem(DEFAULT_NAME_RU.toString())))
            .andExpect(jsonPath("$.[*].nameKk").value(hasItem(DEFAULT_NAME_KK.toString())))
            .andExpect(jsonPath("$.[*].nameEn").value(hasItem(DEFAULT_NAME_EN.toString())))
            .andExpect(jsonPath("$.[*].descriptionRu").value(hasItem(DEFAULT_DESCRIPTION_RU.toString())))
            .andExpect(jsonPath("$.[*].descriptionKk").value(hasItem(DEFAULT_DESCRIPTION_KK.toString())))
            .andExpect(jsonPath("$.[*].descriptionEn").value(hasItem(DEFAULT_DESCRIPTION_EN.toString())))
            .andExpect(jsonPath("$.[*].briefRu").value(hasItem(DEFAULT_BRIEF_RU.toString())))
            .andExpect(jsonPath("$.[*].briefKk").value(hasItem(DEFAULT_BRIEF_KK.toString())))
            .andExpect(jsonPath("$.[*].briefEn").value(hasItem(DEFAULT_BRIEF_EN.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Course.class);
        Course course1 = new Course();
        course1.setId(1L);
        Course course2 = new Course();
        course2.setId(course1.getId());
        assertThat(course1).isEqualTo(course2);
        course2.setId(2L);
        assertThat(course1).isNotEqualTo(course2);
        course1.setId(null);
        assertThat(course1).isNotEqualTo(course2);
    }
}
