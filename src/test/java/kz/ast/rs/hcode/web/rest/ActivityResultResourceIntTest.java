package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.ActivityResult;
import kz.ast.rs.hcode.repository.ActivityResultRepository;
import kz.ast.rs.hcode.service.ActivityResultService;
import kz.ast.rs.hcode.repository.search.ActivityResultSearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ActivityResultResource REST controller.
 *
 * @see ActivityResultResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class ActivityResultResourceIntTest {

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_RESULT = new BigDecimal(1);
    private static final BigDecimal UPDATED_RESULT = new BigDecimal(2);

    @Autowired
    private ActivityResultRepository activityResultRepository;

    @Autowired
    private ActivityResultService activityResultService;

    @Autowired
    private ActivityResultSearchRepository activityResultSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restActivityResultMockMvc;

    private ActivityResult activityResult;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ActivityResultResource activityResultResource = new ActivityResultResource(activityResultService);
        this.restActivityResultMockMvc = MockMvcBuilders.standaloneSetup(activityResultResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ActivityResult createEntity(EntityManager em) {
        ActivityResult activityResult = new ActivityResult()
            .note(DEFAULT_NOTE)
            .result(DEFAULT_RESULT);
        return activityResult;
    }

    @Before
    public void initTest() {
        activityResultSearchRepository.deleteAll();
        activityResult = createEntity(em);
    }

    @Test
    @Transactional
    public void createActivityResult() throws Exception {
        int databaseSizeBeforeCreate = activityResultRepository.findAll().size();

        // Create the ActivityResult
        restActivityResultMockMvc.perform(post("/api/activity-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityResult)))
            .andExpect(status().isCreated());

        // Validate the ActivityResult in the database
        List<ActivityResult> activityResultList = activityResultRepository.findAll();
        assertThat(activityResultList).hasSize(databaseSizeBeforeCreate + 1);
        ActivityResult testActivityResult = activityResultList.get(activityResultList.size() - 1);
        assertThat(testActivityResult.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testActivityResult.getResult()).isEqualTo(DEFAULT_RESULT);

        // Validate the ActivityResult in Elasticsearch
        ActivityResult activityResultEs = activityResultSearchRepository.findOne(testActivityResult.getId());
        assertThat(activityResultEs).isEqualToIgnoringGivenFields(testActivityResult);
    }

    @Test
    @Transactional
    public void createActivityResultWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = activityResultRepository.findAll().size();

        // Create the ActivityResult with an existing ID
        activityResult.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restActivityResultMockMvc.perform(post("/api/activity-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityResult)))
            .andExpect(status().isBadRequest());

        // Validate the ActivityResult in the database
        List<ActivityResult> activityResultList = activityResultRepository.findAll();
        assertThat(activityResultList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllActivityResults() throws Exception {
        // Initialize the database
        activityResultRepository.saveAndFlush(activityResult);

        // Get all the activityResultList
        restActivityResultMockMvc.perform(get("/api/activity-results?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(activityResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT.intValue())));
    }

    @Test
    @Transactional
    public void getActivityResult() throws Exception {
        // Initialize the database
        activityResultRepository.saveAndFlush(activityResult);

        // Get the activityResult
        restActivityResultMockMvc.perform(get("/api/activity-results/{id}", activityResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(activityResult.getId().intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.result").value(DEFAULT_RESULT.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingActivityResult() throws Exception {
        // Get the activityResult
        restActivityResultMockMvc.perform(get("/api/activity-results/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateActivityResult() throws Exception {
        // Initialize the database
        activityResultService.save(activityResult);

        int databaseSizeBeforeUpdate = activityResultRepository.findAll().size();

        // Update the activityResult
        ActivityResult updatedActivityResult = activityResultRepository.findOne(activityResult.getId());
        // Disconnect from session so that the updates on updatedActivityResult are not directly saved in db
        em.detach(updatedActivityResult);
        updatedActivityResult
            .note(UPDATED_NOTE)
            .result(UPDATED_RESULT);

        restActivityResultMockMvc.perform(put("/api/activity-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedActivityResult)))
            .andExpect(status().isOk());

        // Validate the ActivityResult in the database
        List<ActivityResult> activityResultList = activityResultRepository.findAll();
        assertThat(activityResultList).hasSize(databaseSizeBeforeUpdate);
        ActivityResult testActivityResult = activityResultList.get(activityResultList.size() - 1);
        assertThat(testActivityResult.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testActivityResult.getResult()).isEqualTo(UPDATED_RESULT);

        // Validate the ActivityResult in Elasticsearch
        ActivityResult activityResultEs = activityResultSearchRepository.findOne(testActivityResult.getId());
        assertThat(activityResultEs).isEqualToIgnoringGivenFields(testActivityResult);
    }

    @Test
    @Transactional
    public void updateNonExistingActivityResult() throws Exception {
        int databaseSizeBeforeUpdate = activityResultRepository.findAll().size();

        // Create the ActivityResult

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restActivityResultMockMvc.perform(put("/api/activity-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityResult)))
            .andExpect(status().isCreated());

        // Validate the ActivityResult in the database
        List<ActivityResult> activityResultList = activityResultRepository.findAll();
        assertThat(activityResultList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteActivityResult() throws Exception {
        // Initialize the database
        activityResultService.save(activityResult);

        int databaseSizeBeforeDelete = activityResultRepository.findAll().size();

        // Get the activityResult
        restActivityResultMockMvc.perform(delete("/api/activity-results/{id}", activityResult.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean activityResultExistsInEs = activityResultSearchRepository.exists(activityResult.getId());
        assertThat(activityResultExistsInEs).isFalse();

        // Validate the database is empty
        List<ActivityResult> activityResultList = activityResultRepository.findAll();
        assertThat(activityResultList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchActivityResult() throws Exception {
        // Initialize the database
        activityResultService.save(activityResult);

        // Search the activityResult
        restActivityResultMockMvc.perform(get("/api/_search/activity-results?query=id:" + activityResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(activityResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT.intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ActivityResult.class);
        ActivityResult activityResult1 = new ActivityResult();
        activityResult1.setId(1L);
        ActivityResult activityResult2 = new ActivityResult();
        activityResult2.setId(activityResult1.getId());
        assertThat(activityResult1).isEqualTo(activityResult2);
        activityResult2.setId(2L);
        assertThat(activityResult1).isNotEqualTo(activityResult2);
        activityResult1.setId(null);
        assertThat(activityResult1).isNotEqualTo(activityResult2);
    }
}
