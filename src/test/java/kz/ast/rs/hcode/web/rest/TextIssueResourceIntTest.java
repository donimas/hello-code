package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.TextIssue;
import kz.ast.rs.hcode.repository.TextIssueRepository;
import kz.ast.rs.hcode.service.TextIssueService;
import kz.ast.rs.hcode.repository.search.TextIssueSearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TextIssueResource REST controller.
 *
 * @see TextIssueResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class TextIssueResourceIntTest {

    private static final String DEFAULT_BODY_RU = "AAAAAAAAAA";
    private static final String UPDATED_BODY_RU = "BBBBBBBBBB";

    private static final String DEFAULT_BODY_KK = "AAAAAAAAAA";
    private static final String UPDATED_BODY_KK = "BBBBBBBBBB";

    private static final String DEFAULT_BODY_EN = "AAAAAAAAAA";
    private static final String UPDATED_BODY_EN = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE_RU = "AAAAAAAAAA";
    private static final String UPDATED_NOTE_RU = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE_KK = "AAAAAAAAAA";
    private static final String UPDATED_NOTE_KK = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE_EN = "AAAAAAAAAA";
    private static final String UPDATED_NOTE_EN = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    @Autowired
    private TextIssueRepository textIssueRepository;

    @Autowired
    private TextIssueService textIssueService;

    @Autowired
    private TextIssueSearchRepository textIssueSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTextIssueMockMvc;

    private TextIssue textIssue;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TextIssueResource textIssueResource = new TextIssueResource(textIssueService);
        this.restTextIssueMockMvc = MockMvcBuilders.standaloneSetup(textIssueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TextIssue createEntity(EntityManager em) {
        TextIssue textIssue = new TextIssue()
            .bodyRu(DEFAULT_BODY_RU)
            .bodyKk(DEFAULT_BODY_KK)
            .bodyEn(DEFAULT_BODY_EN)
            .noteRu(DEFAULT_NOTE_RU)
            .noteKk(DEFAULT_NOTE_KK)
            .noteEn(DEFAULT_NOTE_EN)
            .flagDeleted(DEFAULT_FLAG_DELETED);
        return textIssue;
    }

    @Before
    public void initTest() {
        textIssueSearchRepository.deleteAll();
        textIssue = createEntity(em);
    }

    @Test
    @Transactional
    public void createTextIssue() throws Exception {
        int databaseSizeBeforeCreate = textIssueRepository.findAll().size();

        // Create the TextIssue
        restTextIssueMockMvc.perform(post("/api/text-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(textIssue)))
            .andExpect(status().isCreated());

        // Validate the TextIssue in the database
        List<TextIssue> textIssueList = textIssueRepository.findAll();
        assertThat(textIssueList).hasSize(databaseSizeBeforeCreate + 1);
        TextIssue testTextIssue = textIssueList.get(textIssueList.size() - 1);
        assertThat(testTextIssue.getBodyRu()).isEqualTo(DEFAULT_BODY_RU);
        assertThat(testTextIssue.getBodyKk()).isEqualTo(DEFAULT_BODY_KK);
        assertThat(testTextIssue.getBodyEn()).isEqualTo(DEFAULT_BODY_EN);
        assertThat(testTextIssue.getNoteRu()).isEqualTo(DEFAULT_NOTE_RU);
        assertThat(testTextIssue.getNoteKk()).isEqualTo(DEFAULT_NOTE_KK);
        assertThat(testTextIssue.getNoteEn()).isEqualTo(DEFAULT_NOTE_EN);
        assertThat(testTextIssue.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);

        // Validate the TextIssue in Elasticsearch
        TextIssue textIssueEs = textIssueSearchRepository.findOne(testTextIssue.getId());
        assertThat(textIssueEs).isEqualToIgnoringGivenFields(testTextIssue);
    }

    @Test
    @Transactional
    public void createTextIssueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = textIssueRepository.findAll().size();

        // Create the TextIssue with an existing ID
        textIssue.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTextIssueMockMvc.perform(post("/api/text-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(textIssue)))
            .andExpect(status().isBadRequest());

        // Validate the TextIssue in the database
        List<TextIssue> textIssueList = textIssueRepository.findAll();
        assertThat(textIssueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTextIssues() throws Exception {
        // Initialize the database
        textIssueRepository.saveAndFlush(textIssue);

        // Get all the textIssueList
        restTextIssueMockMvc.perform(get("/api/text-issues?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(textIssue.getId().intValue())))
            .andExpect(jsonPath("$.[*].bodyRu").value(hasItem(DEFAULT_BODY_RU.toString())))
            .andExpect(jsonPath("$.[*].bodyKk").value(hasItem(DEFAULT_BODY_KK.toString())))
            .andExpect(jsonPath("$.[*].bodyEn").value(hasItem(DEFAULT_BODY_EN.toString())))
            .andExpect(jsonPath("$.[*].noteRu").value(hasItem(DEFAULT_NOTE_RU.toString())))
            .andExpect(jsonPath("$.[*].noteKk").value(hasItem(DEFAULT_NOTE_KK.toString())))
            .andExpect(jsonPath("$.[*].noteEn").value(hasItem(DEFAULT_NOTE_EN.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getTextIssue() throws Exception {
        // Initialize the database
        textIssueRepository.saveAndFlush(textIssue);

        // Get the textIssue
        restTextIssueMockMvc.perform(get("/api/text-issues/{id}", textIssue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(textIssue.getId().intValue()))
            .andExpect(jsonPath("$.bodyRu").value(DEFAULT_BODY_RU.toString()))
            .andExpect(jsonPath("$.bodyKk").value(DEFAULT_BODY_KK.toString()))
            .andExpect(jsonPath("$.bodyEn").value(DEFAULT_BODY_EN.toString()))
            .andExpect(jsonPath("$.noteRu").value(DEFAULT_NOTE_RU.toString()))
            .andExpect(jsonPath("$.noteKk").value(DEFAULT_NOTE_KK.toString()))
            .andExpect(jsonPath("$.noteEn").value(DEFAULT_NOTE_EN.toString()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTextIssue() throws Exception {
        // Get the textIssue
        restTextIssueMockMvc.perform(get("/api/text-issues/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTextIssue() throws Exception {
        // Initialize the database
        textIssueService.save(textIssue);

        int databaseSizeBeforeUpdate = textIssueRepository.findAll().size();

        // Update the textIssue
        TextIssue updatedTextIssue = textIssueRepository.findOne(textIssue.getId());
        // Disconnect from session so that the updates on updatedTextIssue are not directly saved in db
        em.detach(updatedTextIssue);
        updatedTextIssue
            .bodyRu(UPDATED_BODY_RU)
            .bodyKk(UPDATED_BODY_KK)
            .bodyEn(UPDATED_BODY_EN)
            .noteRu(UPDATED_NOTE_RU)
            .noteKk(UPDATED_NOTE_KK)
            .noteEn(UPDATED_NOTE_EN)
            .flagDeleted(UPDATED_FLAG_DELETED);

        restTextIssueMockMvc.perform(put("/api/text-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTextIssue)))
            .andExpect(status().isOk());

        // Validate the TextIssue in the database
        List<TextIssue> textIssueList = textIssueRepository.findAll();
        assertThat(textIssueList).hasSize(databaseSizeBeforeUpdate);
        TextIssue testTextIssue = textIssueList.get(textIssueList.size() - 1);
        assertThat(testTextIssue.getBodyRu()).isEqualTo(UPDATED_BODY_RU);
        assertThat(testTextIssue.getBodyKk()).isEqualTo(UPDATED_BODY_KK);
        assertThat(testTextIssue.getBodyEn()).isEqualTo(UPDATED_BODY_EN);
        assertThat(testTextIssue.getNoteRu()).isEqualTo(UPDATED_NOTE_RU);
        assertThat(testTextIssue.getNoteKk()).isEqualTo(UPDATED_NOTE_KK);
        assertThat(testTextIssue.getNoteEn()).isEqualTo(UPDATED_NOTE_EN);
        assertThat(testTextIssue.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);

        // Validate the TextIssue in Elasticsearch
        TextIssue textIssueEs = textIssueSearchRepository.findOne(testTextIssue.getId());
        assertThat(textIssueEs).isEqualToIgnoringGivenFields(testTextIssue);
    }

    @Test
    @Transactional
    public void updateNonExistingTextIssue() throws Exception {
        int databaseSizeBeforeUpdate = textIssueRepository.findAll().size();

        // Create the TextIssue

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTextIssueMockMvc.perform(put("/api/text-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(textIssue)))
            .andExpect(status().isCreated());

        // Validate the TextIssue in the database
        List<TextIssue> textIssueList = textIssueRepository.findAll();
        assertThat(textIssueList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTextIssue() throws Exception {
        // Initialize the database
        textIssueService.save(textIssue);

        int databaseSizeBeforeDelete = textIssueRepository.findAll().size();

        // Get the textIssue
        restTextIssueMockMvc.perform(delete("/api/text-issues/{id}", textIssue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean textIssueExistsInEs = textIssueSearchRepository.exists(textIssue.getId());
        assertThat(textIssueExistsInEs).isFalse();

        // Validate the database is empty
        List<TextIssue> textIssueList = textIssueRepository.findAll();
        assertThat(textIssueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTextIssue() throws Exception {
        // Initialize the database
        textIssueService.save(textIssue);

        // Search the textIssue
        restTextIssueMockMvc.perform(get("/api/_search/text-issues?query=id:" + textIssue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(textIssue.getId().intValue())))
            .andExpect(jsonPath("$.[*].bodyRu").value(hasItem(DEFAULT_BODY_RU.toString())))
            .andExpect(jsonPath("$.[*].bodyKk").value(hasItem(DEFAULT_BODY_KK.toString())))
            .andExpect(jsonPath("$.[*].bodyEn").value(hasItem(DEFAULT_BODY_EN.toString())))
            .andExpect(jsonPath("$.[*].noteRu").value(hasItem(DEFAULT_NOTE_RU.toString())))
            .andExpect(jsonPath("$.[*].noteKk").value(hasItem(DEFAULT_NOTE_KK.toString())))
            .andExpect(jsonPath("$.[*].noteEn").value(hasItem(DEFAULT_NOTE_EN.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TextIssue.class);
        TextIssue textIssue1 = new TextIssue();
        textIssue1.setId(1L);
        TextIssue textIssue2 = new TextIssue();
        textIssue2.setId(textIssue1.getId());
        assertThat(textIssue1).isEqualTo(textIssue2);
        textIssue2.setId(2L);
        assertThat(textIssue1).isNotEqualTo(textIssue2);
        textIssue1.setId(null);
        assertThat(textIssue1).isNotEqualTo(textIssue2);
    }
}
