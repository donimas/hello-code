package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.CourseChapter;
import kz.ast.rs.hcode.repository.CourseChapterRepository;
import kz.ast.rs.hcode.service.CourseChapterService;
import kz.ast.rs.hcode.repository.search.CourseChapterSearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CourseChapterResource REST controller.
 *
 * @see CourseChapterResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class CourseChapterResourceIntTest {

    private static final String DEFAULT_NOTE_RU = "AAAAAAAAAA";
    private static final String UPDATED_NOTE_RU = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE_KK = "AAAAAAAAAA";
    private static final String UPDATED_NOTE_KK = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE_EN = "AAAAAAAAAA";
    private static final String UPDATED_NOTE_EN = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_ORDER = new BigDecimal(1);
    private static final BigDecimal UPDATED_ORDER = new BigDecimal(2);

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    @Autowired
    private CourseChapterRepository courseChapterRepository;

    @Autowired
    private CourseChapterService courseChapterService;

    @Autowired
    private CourseChapterSearchRepository courseChapterSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCourseChapterMockMvc;

    private CourseChapter courseChapter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CourseChapterResource courseChapterResource = new CourseChapterResource(courseChapterService);
        this.restCourseChapterMockMvc = MockMvcBuilders.standaloneSetup(courseChapterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CourseChapter createEntity(EntityManager em) {
        CourseChapter courseChapter = new CourseChapter()
            .noteRu(DEFAULT_NOTE_RU)
            .noteKk(DEFAULT_NOTE_KK)
            .noteEn(DEFAULT_NOTE_EN)
            .order(DEFAULT_ORDER)
            .flagDeleted(DEFAULT_FLAG_DELETED);
        return courseChapter;
    }

    @Before
    public void initTest() {
        courseChapterSearchRepository.deleteAll();
        courseChapter = createEntity(em);
    }

    @Test
    @Transactional
    public void createCourseChapter() throws Exception {
        int databaseSizeBeforeCreate = courseChapterRepository.findAll().size();

        // Create the CourseChapter
        restCourseChapterMockMvc.perform(post("/api/course-chapters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseChapter)))
            .andExpect(status().isCreated());

        // Validate the CourseChapter in the database
        List<CourseChapter> courseChapterList = courseChapterRepository.findAll();
        assertThat(courseChapterList).hasSize(databaseSizeBeforeCreate + 1);
        CourseChapter testCourseChapter = courseChapterList.get(courseChapterList.size() - 1);
        assertThat(testCourseChapter.getNoteRu()).isEqualTo(DEFAULT_NOTE_RU);
        assertThat(testCourseChapter.getNoteKk()).isEqualTo(DEFAULT_NOTE_KK);
        assertThat(testCourseChapter.getNoteEn()).isEqualTo(DEFAULT_NOTE_EN);
        assertThat(testCourseChapter.getOrder()).isEqualTo(DEFAULT_ORDER);
        assertThat(testCourseChapter.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);

        // Validate the CourseChapter in Elasticsearch
        CourseChapter courseChapterEs = courseChapterSearchRepository.findOne(testCourseChapter.getId());
        assertThat(courseChapterEs).isEqualToIgnoringGivenFields(testCourseChapter);
    }

    @Test
    @Transactional
    public void createCourseChapterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = courseChapterRepository.findAll().size();

        // Create the CourseChapter with an existing ID
        courseChapter.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCourseChapterMockMvc.perform(post("/api/course-chapters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseChapter)))
            .andExpect(status().isBadRequest());

        // Validate the CourseChapter in the database
        List<CourseChapter> courseChapterList = courseChapterRepository.findAll();
        assertThat(courseChapterList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNoteRuIsRequired() throws Exception {
        int databaseSizeBeforeTest = courseChapterRepository.findAll().size();
        // set the field null
        courseChapter.setNoteRu(null);

        // Create the CourseChapter, which fails.

        restCourseChapterMockMvc.perform(post("/api/course-chapters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseChapter)))
            .andExpect(status().isBadRequest());

        List<CourseChapter> courseChapterList = courseChapterRepository.findAll();
        assertThat(courseChapterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNoteKkIsRequired() throws Exception {
        int databaseSizeBeforeTest = courseChapterRepository.findAll().size();
        // set the field null
        courseChapter.setNoteKk(null);

        // Create the CourseChapter, which fails.

        restCourseChapterMockMvc.perform(post("/api/course-chapters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseChapter)))
            .andExpect(status().isBadRequest());

        List<CourseChapter> courseChapterList = courseChapterRepository.findAll();
        assertThat(courseChapterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCourseChapters() throws Exception {
        // Initialize the database
        courseChapterRepository.saveAndFlush(courseChapter);

        // Get all the courseChapterList
        restCourseChapterMockMvc.perform(get("/api/course-chapters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(courseChapter.getId().intValue())))
            .andExpect(jsonPath("$.[*].noteRu").value(hasItem(DEFAULT_NOTE_RU.toString())))
            .andExpect(jsonPath("$.[*].noteKk").value(hasItem(DEFAULT_NOTE_KK.toString())))
            .andExpect(jsonPath("$.[*].noteEn").value(hasItem(DEFAULT_NOTE_EN.toString())))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER.intValue())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getCourseChapter() throws Exception {
        // Initialize the database
        courseChapterRepository.saveAndFlush(courseChapter);

        // Get the courseChapter
        restCourseChapterMockMvc.perform(get("/api/course-chapters/{id}", courseChapter.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(courseChapter.getId().intValue()))
            .andExpect(jsonPath("$.noteRu").value(DEFAULT_NOTE_RU.toString()))
            .andExpect(jsonPath("$.noteKk").value(DEFAULT_NOTE_KK.toString()))
            .andExpect(jsonPath("$.noteEn").value(DEFAULT_NOTE_EN.toString()))
            .andExpect(jsonPath("$.order").value(DEFAULT_ORDER.intValue()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCourseChapter() throws Exception {
        // Get the courseChapter
        restCourseChapterMockMvc.perform(get("/api/course-chapters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCourseChapter() throws Exception {
        // Initialize the database
        courseChapterService.save(courseChapter);

        int databaseSizeBeforeUpdate = courseChapterRepository.findAll().size();

        // Update the courseChapter
        CourseChapter updatedCourseChapter = courseChapterRepository.findOne(courseChapter.getId());
        // Disconnect from session so that the updates on updatedCourseChapter are not directly saved in db
        em.detach(updatedCourseChapter);
        updatedCourseChapter
            .noteRu(UPDATED_NOTE_RU)
            .noteKk(UPDATED_NOTE_KK)
            .noteEn(UPDATED_NOTE_EN)
            .order(UPDATED_ORDER)
            .flagDeleted(UPDATED_FLAG_DELETED);

        restCourseChapterMockMvc.perform(put("/api/course-chapters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCourseChapter)))
            .andExpect(status().isOk());

        // Validate the CourseChapter in the database
        List<CourseChapter> courseChapterList = courseChapterRepository.findAll();
        assertThat(courseChapterList).hasSize(databaseSizeBeforeUpdate);
        CourseChapter testCourseChapter = courseChapterList.get(courseChapterList.size() - 1);
        assertThat(testCourseChapter.getNoteRu()).isEqualTo(UPDATED_NOTE_RU);
        assertThat(testCourseChapter.getNoteKk()).isEqualTo(UPDATED_NOTE_KK);
        assertThat(testCourseChapter.getNoteEn()).isEqualTo(UPDATED_NOTE_EN);
        assertThat(testCourseChapter.getOrder()).isEqualTo(UPDATED_ORDER);
        assertThat(testCourseChapter.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);

        // Validate the CourseChapter in Elasticsearch
        CourseChapter courseChapterEs = courseChapterSearchRepository.findOne(testCourseChapter.getId());
        assertThat(courseChapterEs).isEqualToIgnoringGivenFields(testCourseChapter);
    }

    @Test
    @Transactional
    public void updateNonExistingCourseChapter() throws Exception {
        int databaseSizeBeforeUpdate = courseChapterRepository.findAll().size();

        // Create the CourseChapter

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCourseChapterMockMvc.perform(put("/api/course-chapters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(courseChapter)))
            .andExpect(status().isCreated());

        // Validate the CourseChapter in the database
        List<CourseChapter> courseChapterList = courseChapterRepository.findAll();
        assertThat(courseChapterList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCourseChapter() throws Exception {
        // Initialize the database
        courseChapterService.save(courseChapter);

        int databaseSizeBeforeDelete = courseChapterRepository.findAll().size();

        // Get the courseChapter
        restCourseChapterMockMvc.perform(delete("/api/course-chapters/{id}", courseChapter.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean courseChapterExistsInEs = courseChapterSearchRepository.exists(courseChapter.getId());
        assertThat(courseChapterExistsInEs).isFalse();

        // Validate the database is empty
        List<CourseChapter> courseChapterList = courseChapterRepository.findAll();
        assertThat(courseChapterList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCourseChapter() throws Exception {
        // Initialize the database
        courseChapterService.save(courseChapter);

        // Search the courseChapter
        restCourseChapterMockMvc.perform(get("/api/_search/course-chapters?query=id:" + courseChapter.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(courseChapter.getId().intValue())))
            .andExpect(jsonPath("$.[*].noteRu").value(hasItem(DEFAULT_NOTE_RU.toString())))
            .andExpect(jsonPath("$.[*].noteKk").value(hasItem(DEFAULT_NOTE_KK.toString())))
            .andExpect(jsonPath("$.[*].noteEn").value(hasItem(DEFAULT_NOTE_EN.toString())))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER.intValue())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CourseChapter.class);
        CourseChapter courseChapter1 = new CourseChapter();
        courseChapter1.setId(1L);
        CourseChapter courseChapter2 = new CourseChapter();
        courseChapter2.setId(courseChapter1.getId());
        assertThat(courseChapter1).isEqualTo(courseChapter2);
        courseChapter2.setId(2L);
        assertThat(courseChapter1).isNotEqualTo(courseChapter2);
        courseChapter1.setId(null);
        assertThat(courseChapter1).isNotEqualTo(courseChapter2);
    }
}
