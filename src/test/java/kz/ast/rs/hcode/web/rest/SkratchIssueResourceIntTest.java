package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.SkratchIssue;
import kz.ast.rs.hcode.repository.SkratchIssueRepository;
import kz.ast.rs.hcode.service.SkratchIssueService;
import kz.ast.rs.hcode.repository.search.SkratchIssueSearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SkratchIssueResource REST controller.
 *
 * @see SkratchIssueResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class SkratchIssueResourceIntTest {

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    @Autowired
    private SkratchIssueRepository skratchIssueRepository;

    @Autowired
    private SkratchIssueService skratchIssueService;

    @Autowired
    private SkratchIssueSearchRepository skratchIssueSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSkratchIssueMockMvc;

    private SkratchIssue skratchIssue;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SkratchIssueResource skratchIssueResource = new SkratchIssueResource(skratchIssueService);
        this.restSkratchIssueMockMvc = MockMvcBuilders.standaloneSetup(skratchIssueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SkratchIssue createEntity(EntityManager em) {
        SkratchIssue skratchIssue = new SkratchIssue()
            .url(DEFAULT_URL)
            .note(DEFAULT_NOTE)
            .flagDeleted(DEFAULT_FLAG_DELETED);
        return skratchIssue;
    }

    @Before
    public void initTest() {
        skratchIssueSearchRepository.deleteAll();
        skratchIssue = createEntity(em);
    }

    @Test
    @Transactional
    public void createSkratchIssue() throws Exception {
        int databaseSizeBeforeCreate = skratchIssueRepository.findAll().size();

        // Create the SkratchIssue
        restSkratchIssueMockMvc.perform(post("/api/skratch-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(skratchIssue)))
            .andExpect(status().isCreated());

        // Validate the SkratchIssue in the database
        List<SkratchIssue> skratchIssueList = skratchIssueRepository.findAll();
        assertThat(skratchIssueList).hasSize(databaseSizeBeforeCreate + 1);
        SkratchIssue testSkratchIssue = skratchIssueList.get(skratchIssueList.size() - 1);
        assertThat(testSkratchIssue.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testSkratchIssue.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testSkratchIssue.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);

        // Validate the SkratchIssue in Elasticsearch
        SkratchIssue skratchIssueEs = skratchIssueSearchRepository.findOne(testSkratchIssue.getId());
        assertThat(skratchIssueEs).isEqualToIgnoringGivenFields(testSkratchIssue);
    }

    @Test
    @Transactional
    public void createSkratchIssueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = skratchIssueRepository.findAll().size();

        // Create the SkratchIssue with an existing ID
        skratchIssue.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSkratchIssueMockMvc.perform(post("/api/skratch-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(skratchIssue)))
            .andExpect(status().isBadRequest());

        // Validate the SkratchIssue in the database
        List<SkratchIssue> skratchIssueList = skratchIssueRepository.findAll();
        assertThat(skratchIssueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSkratchIssues() throws Exception {
        // Initialize the database
        skratchIssueRepository.saveAndFlush(skratchIssue);

        // Get all the skratchIssueList
        restSkratchIssueMockMvc.perform(get("/api/skratch-issues?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(skratchIssue.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getSkratchIssue() throws Exception {
        // Initialize the database
        skratchIssueRepository.saveAndFlush(skratchIssue);

        // Get the skratchIssue
        restSkratchIssueMockMvc.perform(get("/api/skratch-issues/{id}", skratchIssue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(skratchIssue.getId().intValue()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSkratchIssue() throws Exception {
        // Get the skratchIssue
        restSkratchIssueMockMvc.perform(get("/api/skratch-issues/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSkratchIssue() throws Exception {
        // Initialize the database
        skratchIssueService.save(skratchIssue);

        int databaseSizeBeforeUpdate = skratchIssueRepository.findAll().size();

        // Update the skratchIssue
        SkratchIssue updatedSkratchIssue = skratchIssueRepository.findOne(skratchIssue.getId());
        // Disconnect from session so that the updates on updatedSkratchIssue are not directly saved in db
        em.detach(updatedSkratchIssue);
        updatedSkratchIssue
            .url(UPDATED_URL)
            .note(UPDATED_NOTE)
            .flagDeleted(UPDATED_FLAG_DELETED);

        restSkratchIssueMockMvc.perform(put("/api/skratch-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSkratchIssue)))
            .andExpect(status().isOk());

        // Validate the SkratchIssue in the database
        List<SkratchIssue> skratchIssueList = skratchIssueRepository.findAll();
        assertThat(skratchIssueList).hasSize(databaseSizeBeforeUpdate);
        SkratchIssue testSkratchIssue = skratchIssueList.get(skratchIssueList.size() - 1);
        assertThat(testSkratchIssue.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testSkratchIssue.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testSkratchIssue.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);

        // Validate the SkratchIssue in Elasticsearch
        SkratchIssue skratchIssueEs = skratchIssueSearchRepository.findOne(testSkratchIssue.getId());
        assertThat(skratchIssueEs).isEqualToIgnoringGivenFields(testSkratchIssue);
    }

    @Test
    @Transactional
    public void updateNonExistingSkratchIssue() throws Exception {
        int databaseSizeBeforeUpdate = skratchIssueRepository.findAll().size();

        // Create the SkratchIssue

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSkratchIssueMockMvc.perform(put("/api/skratch-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(skratchIssue)))
            .andExpect(status().isCreated());

        // Validate the SkratchIssue in the database
        List<SkratchIssue> skratchIssueList = skratchIssueRepository.findAll();
        assertThat(skratchIssueList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSkratchIssue() throws Exception {
        // Initialize the database
        skratchIssueService.save(skratchIssue);

        int databaseSizeBeforeDelete = skratchIssueRepository.findAll().size();

        // Get the skratchIssue
        restSkratchIssueMockMvc.perform(delete("/api/skratch-issues/{id}", skratchIssue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean skratchIssueExistsInEs = skratchIssueSearchRepository.exists(skratchIssue.getId());
        assertThat(skratchIssueExistsInEs).isFalse();

        // Validate the database is empty
        List<SkratchIssue> skratchIssueList = skratchIssueRepository.findAll();
        assertThat(skratchIssueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSkratchIssue() throws Exception {
        // Initialize the database
        skratchIssueService.save(skratchIssue);

        // Search the skratchIssue
        restSkratchIssueMockMvc.perform(get("/api/_search/skratch-issues?query=id:" + skratchIssue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(skratchIssue.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SkratchIssue.class);
        SkratchIssue skratchIssue1 = new SkratchIssue();
        skratchIssue1.setId(1L);
        SkratchIssue skratchIssue2 = new SkratchIssue();
        skratchIssue2.setId(skratchIssue1.getId());
        assertThat(skratchIssue1).isEqualTo(skratchIssue2);
        skratchIssue2.setId(2L);
        assertThat(skratchIssue1).isNotEqualTo(skratchIssue2);
        skratchIssue1.setId(null);
        assertThat(skratchIssue1).isNotEqualTo(skratchIssue2);
    }
}
