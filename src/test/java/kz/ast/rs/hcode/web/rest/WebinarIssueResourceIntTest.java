package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.WebinarIssue;
import kz.ast.rs.hcode.repository.WebinarIssueRepository;
import kz.ast.rs.hcode.service.WebinarIssueService;
import kz.ast.rs.hcode.repository.search.WebinarIssueSearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the WebinarIssueResource REST controller.
 *
 * @see WebinarIssueResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class WebinarIssueResourceIntTest {

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_KEY = "AAAAAAAAAA";
    private static final String UPDATED_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_CABINTE = "AAAAAAAAAA";
    private static final String UPDATED_CABINTE = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    @Autowired
    private WebinarIssueRepository webinarIssueRepository;

    @Autowired
    private WebinarIssueService webinarIssueService;

    @Autowired
    private WebinarIssueSearchRepository webinarIssueSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restWebinarIssueMockMvc;

    private WebinarIssue webinarIssue;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final WebinarIssueResource webinarIssueResource = new WebinarIssueResource(webinarIssueService);
        this.restWebinarIssueMockMvc = MockMvcBuilders.standaloneSetup(webinarIssueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WebinarIssue createEntity(EntityManager em) {
        WebinarIssue webinarIssue = new WebinarIssue()
            .url(DEFAULT_URL)
            .key(DEFAULT_KEY)
            .cabinte(DEFAULT_CABINTE)
            .note(DEFAULT_NOTE)
            .flagDeleted(DEFAULT_FLAG_DELETED);
        return webinarIssue;
    }

    @Before
    public void initTest() {
        webinarIssueSearchRepository.deleteAll();
        webinarIssue = createEntity(em);
    }

    @Test
    @Transactional
    public void createWebinarIssue() throws Exception {
        int databaseSizeBeforeCreate = webinarIssueRepository.findAll().size();

        // Create the WebinarIssue
        restWebinarIssueMockMvc.perform(post("/api/webinar-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(webinarIssue)))
            .andExpect(status().isCreated());

        // Validate the WebinarIssue in the database
        List<WebinarIssue> webinarIssueList = webinarIssueRepository.findAll();
        assertThat(webinarIssueList).hasSize(databaseSizeBeforeCreate + 1);
        WebinarIssue testWebinarIssue = webinarIssueList.get(webinarIssueList.size() - 1);
        assertThat(testWebinarIssue.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testWebinarIssue.getKey()).isEqualTo(DEFAULT_KEY);
        assertThat(testWebinarIssue.getCabinte()).isEqualTo(DEFAULT_CABINTE);
        assertThat(testWebinarIssue.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testWebinarIssue.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);

        // Validate the WebinarIssue in Elasticsearch
        WebinarIssue webinarIssueEs = webinarIssueSearchRepository.findOne(testWebinarIssue.getId());
        assertThat(webinarIssueEs).isEqualToIgnoringGivenFields(testWebinarIssue);
    }

    @Test
    @Transactional
    public void createWebinarIssueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = webinarIssueRepository.findAll().size();

        // Create the WebinarIssue with an existing ID
        webinarIssue.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWebinarIssueMockMvc.perform(post("/api/webinar-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(webinarIssue)))
            .andExpect(status().isBadRequest());

        // Validate the WebinarIssue in the database
        List<WebinarIssue> webinarIssueList = webinarIssueRepository.findAll();
        assertThat(webinarIssueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllWebinarIssues() throws Exception {
        // Initialize the database
        webinarIssueRepository.saveAndFlush(webinarIssue);

        // Get all the webinarIssueList
        restWebinarIssueMockMvc.perform(get("/api/webinar-issues?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(webinarIssue.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].key").value(hasItem(DEFAULT_KEY.toString())))
            .andExpect(jsonPath("$.[*].cabinte").value(hasItem(DEFAULT_CABINTE.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getWebinarIssue() throws Exception {
        // Initialize the database
        webinarIssueRepository.saveAndFlush(webinarIssue);

        // Get the webinarIssue
        restWebinarIssueMockMvc.perform(get("/api/webinar-issues/{id}", webinarIssue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(webinarIssue.getId().intValue()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.key").value(DEFAULT_KEY.toString()))
            .andExpect(jsonPath("$.cabinte").value(DEFAULT_CABINTE.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingWebinarIssue() throws Exception {
        // Get the webinarIssue
        restWebinarIssueMockMvc.perform(get("/api/webinar-issues/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWebinarIssue() throws Exception {
        // Initialize the database
        webinarIssueService.save(webinarIssue);

        int databaseSizeBeforeUpdate = webinarIssueRepository.findAll().size();

        // Update the webinarIssue
        WebinarIssue updatedWebinarIssue = webinarIssueRepository.findOne(webinarIssue.getId());
        // Disconnect from session so that the updates on updatedWebinarIssue are not directly saved in db
        em.detach(updatedWebinarIssue);
        updatedWebinarIssue
            .url(UPDATED_URL)
            .key(UPDATED_KEY)
            .cabinte(UPDATED_CABINTE)
            .note(UPDATED_NOTE)
            .flagDeleted(UPDATED_FLAG_DELETED);

        restWebinarIssueMockMvc.perform(put("/api/webinar-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedWebinarIssue)))
            .andExpect(status().isOk());

        // Validate the WebinarIssue in the database
        List<WebinarIssue> webinarIssueList = webinarIssueRepository.findAll();
        assertThat(webinarIssueList).hasSize(databaseSizeBeforeUpdate);
        WebinarIssue testWebinarIssue = webinarIssueList.get(webinarIssueList.size() - 1);
        assertThat(testWebinarIssue.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testWebinarIssue.getKey()).isEqualTo(UPDATED_KEY);
        assertThat(testWebinarIssue.getCabinte()).isEqualTo(UPDATED_CABINTE);
        assertThat(testWebinarIssue.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testWebinarIssue.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);

        // Validate the WebinarIssue in Elasticsearch
        WebinarIssue webinarIssueEs = webinarIssueSearchRepository.findOne(testWebinarIssue.getId());
        assertThat(webinarIssueEs).isEqualToIgnoringGivenFields(testWebinarIssue);
    }

    @Test
    @Transactional
    public void updateNonExistingWebinarIssue() throws Exception {
        int databaseSizeBeforeUpdate = webinarIssueRepository.findAll().size();

        // Create the WebinarIssue

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restWebinarIssueMockMvc.perform(put("/api/webinar-issues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(webinarIssue)))
            .andExpect(status().isCreated());

        // Validate the WebinarIssue in the database
        List<WebinarIssue> webinarIssueList = webinarIssueRepository.findAll();
        assertThat(webinarIssueList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteWebinarIssue() throws Exception {
        // Initialize the database
        webinarIssueService.save(webinarIssue);

        int databaseSizeBeforeDelete = webinarIssueRepository.findAll().size();

        // Get the webinarIssue
        restWebinarIssueMockMvc.perform(delete("/api/webinar-issues/{id}", webinarIssue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean webinarIssueExistsInEs = webinarIssueSearchRepository.exists(webinarIssue.getId());
        assertThat(webinarIssueExistsInEs).isFalse();

        // Validate the database is empty
        List<WebinarIssue> webinarIssueList = webinarIssueRepository.findAll();
        assertThat(webinarIssueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchWebinarIssue() throws Exception {
        // Initialize the database
        webinarIssueService.save(webinarIssue);

        // Search the webinarIssue
        restWebinarIssueMockMvc.perform(get("/api/_search/webinar-issues?query=id:" + webinarIssue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(webinarIssue.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].key").value(hasItem(DEFAULT_KEY.toString())))
            .andExpect(jsonPath("$.[*].cabinte").value(hasItem(DEFAULT_CABINTE.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WebinarIssue.class);
        WebinarIssue webinarIssue1 = new WebinarIssue();
        webinarIssue1.setId(1L);
        WebinarIssue webinarIssue2 = new WebinarIssue();
        webinarIssue2.setId(webinarIssue1.getId());
        assertThat(webinarIssue1).isEqualTo(webinarIssue2);
        webinarIssue2.setId(2L);
        assertThat(webinarIssue1).isNotEqualTo(webinarIssue2);
        webinarIssue1.setId(null);
        assertThat(webinarIssue1).isNotEqualTo(webinarIssue2);
    }
}
