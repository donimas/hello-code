package kz.ast.rs.hcode.web.rest;

import kz.ast.rs.hcode.HelloCodeApp;

import kz.ast.rs.hcode.domain.ActivityStatusHistory;
import kz.ast.rs.hcode.repository.ActivityStatusHistoryRepository;
import kz.ast.rs.hcode.service.ActivityStatusHistoryService;
import kz.ast.rs.hcode.repository.search.ActivityStatusHistorySearchRepository;
import kz.ast.rs.hcode.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.ast.rs.hcode.web.rest.TestUtil.sameInstant;
import static kz.ast.rs.hcode.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.ast.rs.hcode.domain.enumeration.ActivityStatus;
/**
 * Test class for the ActivityStatusHistoryResource REST controller.
 *
 * @see ActivityStatusHistoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloCodeApp.class)
public class ActivityStatusHistoryResourceIntTest {

    private static final ActivityStatus DEFAULT_STATUS = ActivityStatus.IN_PROGRESS;
    private static final ActivityStatus UPDATED_STATUS = ActivityStatus.SKIPPED;

    private static final ZonedDateTime DEFAULT_STARTED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_STARTED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_FINISHED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FINISHED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private ActivityStatusHistoryRepository activityStatusHistoryRepository;

    @Autowired
    private ActivityStatusHistoryService activityStatusHistoryService;

    @Autowired
    private ActivityStatusHistorySearchRepository activityStatusHistorySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restActivityStatusHistoryMockMvc;

    private ActivityStatusHistory activityStatusHistory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ActivityStatusHistoryResource activityStatusHistoryResource = new ActivityStatusHistoryResource(activityStatusHistoryService);
        this.restActivityStatusHistoryMockMvc = MockMvcBuilders.standaloneSetup(activityStatusHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ActivityStatusHistory createEntity(EntityManager em) {
        ActivityStatusHistory activityStatusHistory = new ActivityStatusHistory()
            .status(DEFAULT_STATUS)
            .startedDate(DEFAULT_STARTED_DATE)
            .finishedDate(DEFAULT_FINISHED_DATE);
        return activityStatusHistory;
    }

    @Before
    public void initTest() {
        activityStatusHistorySearchRepository.deleteAll();
        activityStatusHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createActivityStatusHistory() throws Exception {
        int databaseSizeBeforeCreate = activityStatusHistoryRepository.findAll().size();

        // Create the ActivityStatusHistory
        restActivityStatusHistoryMockMvc.perform(post("/api/activity-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityStatusHistory)))
            .andExpect(status().isCreated());

        // Validate the ActivityStatusHistory in the database
        List<ActivityStatusHistory> activityStatusHistoryList = activityStatusHistoryRepository.findAll();
        assertThat(activityStatusHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        ActivityStatusHistory testActivityStatusHistory = activityStatusHistoryList.get(activityStatusHistoryList.size() - 1);
        assertThat(testActivityStatusHistory.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testActivityStatusHistory.getStartedDate()).isEqualTo(DEFAULT_STARTED_DATE);
        assertThat(testActivityStatusHistory.getFinishedDate()).isEqualTo(DEFAULT_FINISHED_DATE);

        // Validate the ActivityStatusHistory in Elasticsearch
        ActivityStatusHistory activityStatusHistoryEs = activityStatusHistorySearchRepository.findOne(testActivityStatusHistory.getId());
        assertThat(testActivityStatusHistory.getStartedDate()).isEqualTo(testActivityStatusHistory.getStartedDate());
        assertThat(testActivityStatusHistory.getFinishedDate()).isEqualTo(testActivityStatusHistory.getFinishedDate());
        assertThat(activityStatusHistoryEs).isEqualToIgnoringGivenFields(testActivityStatusHistory, "startedDate", "finishedDate");
    }

    @Test
    @Transactional
    public void createActivityStatusHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = activityStatusHistoryRepository.findAll().size();

        // Create the ActivityStatusHistory with an existing ID
        activityStatusHistory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restActivityStatusHistoryMockMvc.perform(post("/api/activity-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityStatusHistory)))
            .andExpect(status().isBadRequest());

        // Validate the ActivityStatusHistory in the database
        List<ActivityStatusHistory> activityStatusHistoryList = activityStatusHistoryRepository.findAll();
        assertThat(activityStatusHistoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkStartedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = activityStatusHistoryRepository.findAll().size();
        // set the field null
        activityStatusHistory.setStartedDate(null);

        // Create the ActivityStatusHistory, which fails.

        restActivityStatusHistoryMockMvc.perform(post("/api/activity-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityStatusHistory)))
            .andExpect(status().isBadRequest());

        List<ActivityStatusHistory> activityStatusHistoryList = activityStatusHistoryRepository.findAll();
        assertThat(activityStatusHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllActivityStatusHistories() throws Exception {
        // Initialize the database
        activityStatusHistoryRepository.saveAndFlush(activityStatusHistory);

        // Get all the activityStatusHistoryList
        restActivityStatusHistoryMockMvc.perform(get("/api/activity-status-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(activityStatusHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].startedDate").value(hasItem(sameInstant(DEFAULT_STARTED_DATE))))
            .andExpect(jsonPath("$.[*].finishedDate").value(hasItem(sameInstant(DEFAULT_FINISHED_DATE))));
    }

    @Test
    @Transactional
    public void getActivityStatusHistory() throws Exception {
        // Initialize the database
        activityStatusHistoryRepository.saveAndFlush(activityStatusHistory);

        // Get the activityStatusHistory
        restActivityStatusHistoryMockMvc.perform(get("/api/activity-status-histories/{id}", activityStatusHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(activityStatusHistory.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.startedDate").value(sameInstant(DEFAULT_STARTED_DATE)))
            .andExpect(jsonPath("$.finishedDate").value(sameInstant(DEFAULT_FINISHED_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingActivityStatusHistory() throws Exception {
        // Get the activityStatusHistory
        restActivityStatusHistoryMockMvc.perform(get("/api/activity-status-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateActivityStatusHistory() throws Exception {
        // Initialize the database
        activityStatusHistoryService.save(activityStatusHistory);

        int databaseSizeBeforeUpdate = activityStatusHistoryRepository.findAll().size();

        // Update the activityStatusHistory
        ActivityStatusHistory updatedActivityStatusHistory = activityStatusHistoryRepository.findOne(activityStatusHistory.getId());
        // Disconnect from session so that the updates on updatedActivityStatusHistory are not directly saved in db
        em.detach(updatedActivityStatusHistory);
        updatedActivityStatusHistory
            .status(UPDATED_STATUS)
            .startedDate(UPDATED_STARTED_DATE)
            .finishedDate(UPDATED_FINISHED_DATE);

        restActivityStatusHistoryMockMvc.perform(put("/api/activity-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedActivityStatusHistory)))
            .andExpect(status().isOk());

        // Validate the ActivityStatusHistory in the database
        List<ActivityStatusHistory> activityStatusHistoryList = activityStatusHistoryRepository.findAll();
        assertThat(activityStatusHistoryList).hasSize(databaseSizeBeforeUpdate);
        ActivityStatusHistory testActivityStatusHistory = activityStatusHistoryList.get(activityStatusHistoryList.size() - 1);
        assertThat(testActivityStatusHistory.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testActivityStatusHistory.getStartedDate()).isEqualTo(UPDATED_STARTED_DATE);
        assertThat(testActivityStatusHistory.getFinishedDate()).isEqualTo(UPDATED_FINISHED_DATE);

        // Validate the ActivityStatusHistory in Elasticsearch
        ActivityStatusHistory activityStatusHistoryEs = activityStatusHistorySearchRepository.findOne(testActivityStatusHistory.getId());
        assertThat(testActivityStatusHistory.getStartedDate()).isEqualTo(testActivityStatusHistory.getStartedDate());
        assertThat(testActivityStatusHistory.getFinishedDate()).isEqualTo(testActivityStatusHistory.getFinishedDate());
        assertThat(activityStatusHistoryEs).isEqualToIgnoringGivenFields(testActivityStatusHistory, "startedDate", "finishedDate");
    }

    @Test
    @Transactional
    public void updateNonExistingActivityStatusHistory() throws Exception {
        int databaseSizeBeforeUpdate = activityStatusHistoryRepository.findAll().size();

        // Create the ActivityStatusHistory

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restActivityStatusHistoryMockMvc.perform(put("/api/activity-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityStatusHistory)))
            .andExpect(status().isCreated());

        // Validate the ActivityStatusHistory in the database
        List<ActivityStatusHistory> activityStatusHistoryList = activityStatusHistoryRepository.findAll();
        assertThat(activityStatusHistoryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteActivityStatusHistory() throws Exception {
        // Initialize the database
        activityStatusHistoryService.save(activityStatusHistory);

        int databaseSizeBeforeDelete = activityStatusHistoryRepository.findAll().size();

        // Get the activityStatusHistory
        restActivityStatusHistoryMockMvc.perform(delete("/api/activity-status-histories/{id}", activityStatusHistory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean activityStatusHistoryExistsInEs = activityStatusHistorySearchRepository.exists(activityStatusHistory.getId());
        assertThat(activityStatusHistoryExistsInEs).isFalse();

        // Validate the database is empty
        List<ActivityStatusHistory> activityStatusHistoryList = activityStatusHistoryRepository.findAll();
        assertThat(activityStatusHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchActivityStatusHistory() throws Exception {
        // Initialize the database
        activityStatusHistoryService.save(activityStatusHistory);

        // Search the activityStatusHistory
        restActivityStatusHistoryMockMvc.perform(get("/api/_search/activity-status-histories?query=id:" + activityStatusHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(activityStatusHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].startedDate").value(hasItem(sameInstant(DEFAULT_STARTED_DATE))))
            .andExpect(jsonPath("$.[*].finishedDate").value(hasItem(sameInstant(DEFAULT_FINISHED_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ActivityStatusHistory.class);
        ActivityStatusHistory activityStatusHistory1 = new ActivityStatusHistory();
        activityStatusHistory1.setId(1L);
        ActivityStatusHistory activityStatusHistory2 = new ActivityStatusHistory();
        activityStatusHistory2.setId(activityStatusHistory1.getId());
        assertThat(activityStatusHistory1).isEqualTo(activityStatusHistory2);
        activityStatusHistory2.setId(2L);
        assertThat(activityStatusHistory1).isNotEqualTo(activityStatusHistory2);
        activityStatusHistory1.setId(null);
        assertThat(activityStatusHistory1).isNotEqualTo(activityStatusHistory2);
    }
}
