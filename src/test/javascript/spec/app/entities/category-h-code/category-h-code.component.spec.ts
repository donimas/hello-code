/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { CategoryHCodeComponent } from '../../../../../../main/webapp/app/entities/category-h-code/category-h-code.component';
import { CategoryHCodeService } from '../../../../../../main/webapp/app/entities/category-h-code/category-h-code.service';
import { CategoryHCode } from '../../../../../../main/webapp/app/entities/category-h-code/category-h-code.model';

describe('Component Tests', () => {

    describe('CategoryHCode Management Component', () => {
        let comp: CategoryHCodeComponent;
        let fixture: ComponentFixture<CategoryHCodeComponent>;
        let service: CategoryHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CategoryHCodeComponent],
                providers: [
                    CategoryHCodeService
                ]
            })
            .overrideTemplate(CategoryHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CategoryHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CategoryHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new CategoryHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.categories[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
