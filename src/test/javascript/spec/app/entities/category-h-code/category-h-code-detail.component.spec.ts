/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { CategoryHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/category-h-code/category-h-code-detail.component';
import { CategoryHCodeService } from '../../../../../../main/webapp/app/entities/category-h-code/category-h-code.service';
import { CategoryHCode } from '../../../../../../main/webapp/app/entities/category-h-code/category-h-code.model';

describe('Component Tests', () => {

    describe('CategoryHCode Management Detail Component', () => {
        let comp: CategoryHCodeDetailComponent;
        let fixture: ComponentFixture<CategoryHCodeDetailComponent>;
        let service: CategoryHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CategoryHCodeDetailComponent],
                providers: [
                    CategoryHCodeService
                ]
            })
            .overrideTemplate(CategoryHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CategoryHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CategoryHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new CategoryHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.category).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
