/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { LearnerActivityHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/learner-activity-h-code/learner-activity-h-code-dialog.component';
import { LearnerActivityHCodeService } from '../../../../../../main/webapp/app/entities/learner-activity-h-code/learner-activity-h-code.service';
import { LearnerActivityHCode } from '../../../../../../main/webapp/app/entities/learner-activity-h-code/learner-activity-h-code.model';
import { CourseHCodeService } from '../../../../../../main/webapp/app/entities/course-h-code';
import { CourseIssueHCodeService } from '../../../../../../main/webapp/app/entities/course-issue-h-code';
import { UserService } from '../../../../../../main/webapp/app/shared';
import { ActivityStatusHistoryHCodeService } from '../../../../../../main/webapp/app/entities/activity-status-history-h-code';

describe('Component Tests', () => {

    describe('LearnerActivityHCode Management Dialog Component', () => {
        let comp: LearnerActivityHCodeDialogComponent;
        let fixture: ComponentFixture<LearnerActivityHCodeDialogComponent>;
        let service: LearnerActivityHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [LearnerActivityHCodeDialogComponent],
                providers: [
                    CourseHCodeService,
                    CourseIssueHCodeService,
                    UserService,
                    ActivityStatusHistoryHCodeService,
                    LearnerActivityHCodeService
                ]
            })
            .overrideTemplate(LearnerActivityHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LearnerActivityHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LearnerActivityHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new LearnerActivityHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.learnerActivity = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'learnerActivityListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new LearnerActivityHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.learnerActivity = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'learnerActivityListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
