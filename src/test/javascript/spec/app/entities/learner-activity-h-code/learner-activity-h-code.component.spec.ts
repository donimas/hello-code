/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { LearnerActivityHCodeComponent } from '../../../../../../main/webapp/app/entities/learner-activity-h-code/learner-activity-h-code.component';
import { LearnerActivityHCodeService } from '../../../../../../main/webapp/app/entities/learner-activity-h-code/learner-activity-h-code.service';
import { LearnerActivityHCode } from '../../../../../../main/webapp/app/entities/learner-activity-h-code/learner-activity-h-code.model';

describe('Component Tests', () => {

    describe('LearnerActivityHCode Management Component', () => {
        let comp: LearnerActivityHCodeComponent;
        let fixture: ComponentFixture<LearnerActivityHCodeComponent>;
        let service: LearnerActivityHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [LearnerActivityHCodeComponent],
                providers: [
                    LearnerActivityHCodeService
                ]
            })
            .overrideTemplate(LearnerActivityHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LearnerActivityHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LearnerActivityHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new LearnerActivityHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.learnerActivities[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
