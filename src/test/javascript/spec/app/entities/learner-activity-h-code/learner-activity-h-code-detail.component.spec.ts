/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { LearnerActivityHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/learner-activity-h-code/learner-activity-h-code-detail.component';
import { LearnerActivityHCodeService } from '../../../../../../main/webapp/app/entities/learner-activity-h-code/learner-activity-h-code.service';
import { LearnerActivityHCode } from '../../../../../../main/webapp/app/entities/learner-activity-h-code/learner-activity-h-code.model';

describe('Component Tests', () => {

    describe('LearnerActivityHCode Management Detail Component', () => {
        let comp: LearnerActivityHCodeDetailComponent;
        let fixture: ComponentFixture<LearnerActivityHCodeDetailComponent>;
        let service: LearnerActivityHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [LearnerActivityHCodeDetailComponent],
                providers: [
                    LearnerActivityHCodeService
                ]
            })
            .overrideTemplate(LearnerActivityHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LearnerActivityHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LearnerActivityHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new LearnerActivityHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.learnerActivity).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
