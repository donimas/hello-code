/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { LearnerGroupHCodeComponent } from '../../../../../../main/webapp/app/entities/learner-group-h-code/learner-group-h-code.component';
import { LearnerGroupHCodeService } from '../../../../../../main/webapp/app/entities/learner-group-h-code/learner-group-h-code.service';
import { LearnerGroupHCode } from '../../../../../../main/webapp/app/entities/learner-group-h-code/learner-group-h-code.model';

describe('Component Tests', () => {

    describe('LearnerGroupHCode Management Component', () => {
        let comp: LearnerGroupHCodeComponent;
        let fixture: ComponentFixture<LearnerGroupHCodeComponent>;
        let service: LearnerGroupHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [LearnerGroupHCodeComponent],
                providers: [
                    LearnerGroupHCodeService
                ]
            })
            .overrideTemplate(LearnerGroupHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LearnerGroupHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LearnerGroupHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new LearnerGroupHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.learnerGroups[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
