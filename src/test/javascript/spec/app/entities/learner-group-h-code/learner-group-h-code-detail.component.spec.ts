/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { LearnerGroupHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/learner-group-h-code/learner-group-h-code-detail.component';
import { LearnerGroupHCodeService } from '../../../../../../main/webapp/app/entities/learner-group-h-code/learner-group-h-code.service';
import { LearnerGroupHCode } from '../../../../../../main/webapp/app/entities/learner-group-h-code/learner-group-h-code.model';

describe('Component Tests', () => {

    describe('LearnerGroupHCode Management Detail Component', () => {
        let comp: LearnerGroupHCodeDetailComponent;
        let fixture: ComponentFixture<LearnerGroupHCodeDetailComponent>;
        let service: LearnerGroupHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [LearnerGroupHCodeDetailComponent],
                providers: [
                    LearnerGroupHCodeService
                ]
            })
            .overrideTemplate(LearnerGroupHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LearnerGroupHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LearnerGroupHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new LearnerGroupHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.learnerGroup).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
