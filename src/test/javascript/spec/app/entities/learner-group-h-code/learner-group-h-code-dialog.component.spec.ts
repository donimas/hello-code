/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { LearnerGroupHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/learner-group-h-code/learner-group-h-code-dialog.component';
import { LearnerGroupHCodeService } from '../../../../../../main/webapp/app/entities/learner-group-h-code/learner-group-h-code.service';
import { LearnerGroupHCode } from '../../../../../../main/webapp/app/entities/learner-group-h-code/learner-group-h-code.model';
import { UserService } from '../../../../../../main/webapp/app/shared';
import { SchoolHCodeService } from '../../../../../../main/webapp/app/entities/school-h-code';
import { GradeHCodeService } from '../../../../../../main/webapp/app/entities/grade-h-code';

describe('Component Tests', () => {

    describe('LearnerGroupHCode Management Dialog Component', () => {
        let comp: LearnerGroupHCodeDialogComponent;
        let fixture: ComponentFixture<LearnerGroupHCodeDialogComponent>;
        let service: LearnerGroupHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [LearnerGroupHCodeDialogComponent],
                providers: [
                    UserService,
                    SchoolHCodeService,
                    GradeHCodeService,
                    LearnerGroupHCodeService
                ]
            })
            .overrideTemplate(LearnerGroupHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LearnerGroupHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LearnerGroupHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new LearnerGroupHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.learnerGroup = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'learnerGroupListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new LearnerGroupHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.learnerGroup = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'learnerGroupListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
