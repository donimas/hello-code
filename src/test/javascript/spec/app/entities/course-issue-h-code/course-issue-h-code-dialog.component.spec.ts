/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseIssueHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/course-issue-h-code/course-issue-h-code-dialog.component';
import { CourseIssueHCodeService } from '../../../../../../main/webapp/app/entities/course-issue-h-code/course-issue-h-code.service';
import { CourseIssueHCode } from '../../../../../../main/webapp/app/entities/course-issue-h-code/course-issue-h-code.model';
import { CourseChapterHCodeService } from '../../../../../../main/webapp/app/entities/course-chapter-h-code';
import { CourseHCodeService } from '../../../../../../main/webapp/app/entities/course-h-code';

describe('Component Tests', () => {

    describe('CourseIssueHCode Management Dialog Component', () => {
        let comp: CourseIssueHCodeDialogComponent;
        let fixture: ComponentFixture<CourseIssueHCodeDialogComponent>;
        let service: CourseIssueHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseIssueHCodeDialogComponent],
                providers: [
                    CourseChapterHCodeService,
                    CourseHCodeService,
                    CourseIssueHCodeService
                ]
            })
            .overrideTemplate(CourseIssueHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseIssueHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseIssueHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CourseIssueHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.courseIssue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'courseIssueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CourseIssueHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.courseIssue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'courseIssueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
