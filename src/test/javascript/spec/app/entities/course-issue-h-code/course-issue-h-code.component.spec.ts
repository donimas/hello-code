/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseIssueHCodeComponent } from '../../../../../../main/webapp/app/entities/course-issue-h-code/course-issue-h-code.component';
import { CourseIssueHCodeService } from '../../../../../../main/webapp/app/entities/course-issue-h-code/course-issue-h-code.service';
import { CourseIssueHCode } from '../../../../../../main/webapp/app/entities/course-issue-h-code/course-issue-h-code.model';

describe('Component Tests', () => {

    describe('CourseIssueHCode Management Component', () => {
        let comp: CourseIssueHCodeComponent;
        let fixture: ComponentFixture<CourseIssueHCodeComponent>;
        let service: CourseIssueHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseIssueHCodeComponent],
                providers: [
                    CourseIssueHCodeService
                ]
            })
            .overrideTemplate(CourseIssueHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseIssueHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseIssueHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new CourseIssueHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.courseIssues[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
