/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseIssueHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/course-issue-h-code/course-issue-h-code-detail.component';
import { CourseIssueHCodeService } from '../../../../../../main/webapp/app/entities/course-issue-h-code/course-issue-h-code.service';
import { CourseIssueHCode } from '../../../../../../main/webapp/app/entities/course-issue-h-code/course-issue-h-code.model';

describe('Component Tests', () => {

    describe('CourseIssueHCode Management Detail Component', () => {
        let comp: CourseIssueHCodeDetailComponent;
        let fixture: ComponentFixture<CourseIssueHCodeDetailComponent>;
        let service: CourseIssueHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseIssueHCodeDetailComponent],
                providers: [
                    CourseIssueHCodeService
                ]
            })
            .overrideTemplate(CourseIssueHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseIssueHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseIssueHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new CourseIssueHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.courseIssue).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
