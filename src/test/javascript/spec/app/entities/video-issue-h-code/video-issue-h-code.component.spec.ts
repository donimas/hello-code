/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { VideoIssueHCodeComponent } from '../../../../../../main/webapp/app/entities/video-issue-h-code/video-issue-h-code.component';
import { VideoIssueHCodeService } from '../../../../../../main/webapp/app/entities/video-issue-h-code/video-issue-h-code.service';
import { VideoIssueHCode } from '../../../../../../main/webapp/app/entities/video-issue-h-code/video-issue-h-code.model';

describe('Component Tests', () => {

    describe('VideoIssueHCode Management Component', () => {
        let comp: VideoIssueHCodeComponent;
        let fixture: ComponentFixture<VideoIssueHCodeComponent>;
        let service: VideoIssueHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [VideoIssueHCodeComponent],
                providers: [
                    VideoIssueHCodeService
                ]
            })
            .overrideTemplate(VideoIssueHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VideoIssueHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VideoIssueHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new VideoIssueHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.videoIssues[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
