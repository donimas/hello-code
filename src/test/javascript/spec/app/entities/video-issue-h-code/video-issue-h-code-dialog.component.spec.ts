/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { VideoIssueHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/video-issue-h-code/video-issue-h-code-dialog.component';
import { VideoIssueHCodeService } from '../../../../../../main/webapp/app/entities/video-issue-h-code/video-issue-h-code.service';
import { VideoIssueHCode } from '../../../../../../main/webapp/app/entities/video-issue-h-code/video-issue-h-code.model';
import { CourseIssueHCodeService } from '../../../../../../main/webapp/app/entities/course-issue-h-code';

describe('Component Tests', () => {

    describe('VideoIssueHCode Management Dialog Component', () => {
        let comp: VideoIssueHCodeDialogComponent;
        let fixture: ComponentFixture<VideoIssueHCodeDialogComponent>;
        let service: VideoIssueHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [VideoIssueHCodeDialogComponent],
                providers: [
                    CourseIssueHCodeService,
                    VideoIssueHCodeService
                ]
            })
            .overrideTemplate(VideoIssueHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VideoIssueHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VideoIssueHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new VideoIssueHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.videoIssue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'videoIssueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new VideoIssueHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.videoIssue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'videoIssueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
