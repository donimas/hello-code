/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { VideoIssueHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/video-issue-h-code/video-issue-h-code-detail.component';
import { VideoIssueHCodeService } from '../../../../../../main/webapp/app/entities/video-issue-h-code/video-issue-h-code.service';
import { VideoIssueHCode } from '../../../../../../main/webapp/app/entities/video-issue-h-code/video-issue-h-code.model';

describe('Component Tests', () => {

    describe('VideoIssueHCode Management Detail Component', () => {
        let comp: VideoIssueHCodeDetailComponent;
        let fixture: ComponentFixture<VideoIssueHCodeDetailComponent>;
        let service: VideoIssueHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [VideoIssueHCodeDetailComponent],
                providers: [
                    VideoIssueHCodeService
                ]
            })
            .overrideTemplate(VideoIssueHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VideoIssueHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VideoIssueHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new VideoIssueHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.videoIssue).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
