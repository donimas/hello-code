/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { VideoIssueHCodeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/video-issue-h-code/video-issue-h-code-delete-dialog.component';
import { VideoIssueHCodeService } from '../../../../../../main/webapp/app/entities/video-issue-h-code/video-issue-h-code.service';

describe('Component Tests', () => {

    describe('VideoIssueHCode Management Delete Component', () => {
        let comp: VideoIssueHCodeDeleteDialogComponent;
        let fixture: ComponentFixture<VideoIssueHCodeDeleteDialogComponent>;
        let service: VideoIssueHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [VideoIssueHCodeDeleteDialogComponent],
                providers: [
                    VideoIssueHCodeService
                ]
            })
            .overrideTemplate(VideoIssueHCodeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VideoIssueHCodeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VideoIssueHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
