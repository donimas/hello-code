/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { TestVariantHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/test-variant-h-code/test-variant-h-code-dialog.component';
import { TestVariantHCodeService } from '../../../../../../main/webapp/app/entities/test-variant-h-code/test-variant-h-code.service';
import { TestVariantHCode } from '../../../../../../main/webapp/app/entities/test-variant-h-code/test-variant-h-code.model';
import { TestQuestionHCodeService } from '../../../../../../main/webapp/app/entities/test-question-h-code';

describe('Component Tests', () => {

    describe('TestVariantHCode Management Dialog Component', () => {
        let comp: TestVariantHCodeDialogComponent;
        let fixture: ComponentFixture<TestVariantHCodeDialogComponent>;
        let service: TestVariantHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TestVariantHCodeDialogComponent],
                providers: [
                    TestQuestionHCodeService,
                    TestVariantHCodeService
                ]
            })
            .overrideTemplate(TestVariantHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TestVariantHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TestVariantHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TestVariantHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.testVariant = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'testVariantListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TestVariantHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.testVariant = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'testVariantListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
