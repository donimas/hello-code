/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { TestVariantHCodeComponent } from '../../../../../../main/webapp/app/entities/test-variant-h-code/test-variant-h-code.component';
import { TestVariantHCodeService } from '../../../../../../main/webapp/app/entities/test-variant-h-code/test-variant-h-code.service';
import { TestVariantHCode } from '../../../../../../main/webapp/app/entities/test-variant-h-code/test-variant-h-code.model';

describe('Component Tests', () => {

    describe('TestVariantHCode Management Component', () => {
        let comp: TestVariantHCodeComponent;
        let fixture: ComponentFixture<TestVariantHCodeComponent>;
        let service: TestVariantHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TestVariantHCodeComponent],
                providers: [
                    TestVariantHCodeService
                ]
            })
            .overrideTemplate(TestVariantHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TestVariantHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TestVariantHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TestVariantHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.testVariants[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
