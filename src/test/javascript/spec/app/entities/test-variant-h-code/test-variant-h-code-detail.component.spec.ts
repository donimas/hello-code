/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { TestVariantHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/test-variant-h-code/test-variant-h-code-detail.component';
import { TestVariantHCodeService } from '../../../../../../main/webapp/app/entities/test-variant-h-code/test-variant-h-code.service';
import { TestVariantHCode } from '../../../../../../main/webapp/app/entities/test-variant-h-code/test-variant-h-code.model';

describe('Component Tests', () => {

    describe('TestVariantHCode Management Detail Component', () => {
        let comp: TestVariantHCodeDetailComponent;
        let fixture: ComponentFixture<TestVariantHCodeDetailComponent>;
        let service: TestVariantHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TestVariantHCodeDetailComponent],
                providers: [
                    TestVariantHCodeService
                ]
            })
            .overrideTemplate(TestVariantHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TestVariantHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TestVariantHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TestVariantHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.testVariant).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
