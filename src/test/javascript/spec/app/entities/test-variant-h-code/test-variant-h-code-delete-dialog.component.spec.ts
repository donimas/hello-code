/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { TestVariantHCodeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/test-variant-h-code/test-variant-h-code-delete-dialog.component';
import { TestVariantHCodeService } from '../../../../../../main/webapp/app/entities/test-variant-h-code/test-variant-h-code.service';

describe('Component Tests', () => {

    describe('TestVariantHCode Management Delete Component', () => {
        let comp: TestVariantHCodeDeleteDialogComponent;
        let fixture: ComponentFixture<TestVariantHCodeDeleteDialogComponent>;
        let service: TestVariantHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TestVariantHCodeDeleteDialogComponent],
                providers: [
                    TestVariantHCodeService
                ]
            })
            .overrideTemplate(TestVariantHCodeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TestVariantHCodeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TestVariantHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
