/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { SchoolHCodeComponent } from '../../../../../../main/webapp/app/entities/school-h-code/school-h-code.component';
import { SchoolHCodeService } from '../../../../../../main/webapp/app/entities/school-h-code/school-h-code.service';
import { SchoolHCode } from '../../../../../../main/webapp/app/entities/school-h-code/school-h-code.model';

describe('Component Tests', () => {

    describe('SchoolHCode Management Component', () => {
        let comp: SchoolHCodeComponent;
        let fixture: ComponentFixture<SchoolHCodeComponent>;
        let service: SchoolHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [SchoolHCodeComponent],
                providers: [
                    SchoolHCodeService
                ]
            })
            .overrideTemplate(SchoolHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SchoolHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SchoolHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new SchoolHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.schools[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
