/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { SchoolHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/school-h-code/school-h-code-detail.component';
import { SchoolHCodeService } from '../../../../../../main/webapp/app/entities/school-h-code/school-h-code.service';
import { SchoolHCode } from '../../../../../../main/webapp/app/entities/school-h-code/school-h-code.model';

describe('Component Tests', () => {

    describe('SchoolHCode Management Detail Component', () => {
        let comp: SchoolHCodeDetailComponent;
        let fixture: ComponentFixture<SchoolHCodeDetailComponent>;
        let service: SchoolHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [SchoolHCodeDetailComponent],
                providers: [
                    SchoolHCodeService
                ]
            })
            .overrideTemplate(SchoolHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SchoolHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SchoolHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new SchoolHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.school).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
