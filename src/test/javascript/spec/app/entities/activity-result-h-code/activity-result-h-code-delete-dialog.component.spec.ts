/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { ActivityResultHCodeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/activity-result-h-code/activity-result-h-code-delete-dialog.component';
import { ActivityResultHCodeService } from '../../../../../../main/webapp/app/entities/activity-result-h-code/activity-result-h-code.service';

describe('Component Tests', () => {

    describe('ActivityResultHCode Management Delete Component', () => {
        let comp: ActivityResultHCodeDeleteDialogComponent;
        let fixture: ComponentFixture<ActivityResultHCodeDeleteDialogComponent>;
        let service: ActivityResultHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [ActivityResultHCodeDeleteDialogComponent],
                providers: [
                    ActivityResultHCodeService
                ]
            })
            .overrideTemplate(ActivityResultHCodeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ActivityResultHCodeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ActivityResultHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
