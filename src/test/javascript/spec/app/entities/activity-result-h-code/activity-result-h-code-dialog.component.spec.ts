/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { ActivityResultHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/activity-result-h-code/activity-result-h-code-dialog.component';
import { ActivityResultHCodeService } from '../../../../../../main/webapp/app/entities/activity-result-h-code/activity-result-h-code.service';
import { ActivityResultHCode } from '../../../../../../main/webapp/app/entities/activity-result-h-code/activity-result-h-code.model';

describe('Component Tests', () => {

    describe('ActivityResultHCode Management Dialog Component', () => {
        let comp: ActivityResultHCodeDialogComponent;
        let fixture: ComponentFixture<ActivityResultHCodeDialogComponent>;
        let service: ActivityResultHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [ActivityResultHCodeDialogComponent],
                providers: [
                    ActivityResultHCodeService
                ]
            })
            .overrideTemplate(ActivityResultHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ActivityResultHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ActivityResultHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ActivityResultHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.activityResult = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'activityResultListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ActivityResultHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.activityResult = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'activityResultListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
