/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { ActivityResultHCodeComponent } from '../../../../../../main/webapp/app/entities/activity-result-h-code/activity-result-h-code.component';
import { ActivityResultHCodeService } from '../../../../../../main/webapp/app/entities/activity-result-h-code/activity-result-h-code.service';
import { ActivityResultHCode } from '../../../../../../main/webapp/app/entities/activity-result-h-code/activity-result-h-code.model';

describe('Component Tests', () => {

    describe('ActivityResultHCode Management Component', () => {
        let comp: ActivityResultHCodeComponent;
        let fixture: ComponentFixture<ActivityResultHCodeComponent>;
        let service: ActivityResultHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [ActivityResultHCodeComponent],
                providers: [
                    ActivityResultHCodeService
                ]
            })
            .overrideTemplate(ActivityResultHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ActivityResultHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ActivityResultHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ActivityResultHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.activityResults[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
