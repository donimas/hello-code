/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { ActivityResultHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/activity-result-h-code/activity-result-h-code-detail.component';
import { ActivityResultHCodeService } from '../../../../../../main/webapp/app/entities/activity-result-h-code/activity-result-h-code.service';
import { ActivityResultHCode } from '../../../../../../main/webapp/app/entities/activity-result-h-code/activity-result-h-code.model';

describe('Component Tests', () => {

    describe('ActivityResultHCode Management Detail Component', () => {
        let comp: ActivityResultHCodeDetailComponent;
        let fixture: ComponentFixture<ActivityResultHCodeDetailComponent>;
        let service: ActivityResultHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [ActivityResultHCodeDetailComponent],
                providers: [
                    ActivityResultHCodeService
                ]
            })
            .overrideTemplate(ActivityResultHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ActivityResultHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ActivityResultHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ActivityResultHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.activityResult).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
