/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { UserInfoHCodeComponent } from '../../../../../../main/webapp/app/entities/user-info-h-code/user-info-h-code.component';
import { UserInfoHCodeService } from '../../../../../../main/webapp/app/entities/user-info-h-code/user-info-h-code.service';
import { UserInfoHCode } from '../../../../../../main/webapp/app/entities/user-info-h-code/user-info-h-code.model';

describe('Component Tests', () => {

    describe('UserInfoHCode Management Component', () => {
        let comp: UserInfoHCodeComponent;
        let fixture: ComponentFixture<UserInfoHCodeComponent>;
        let service: UserInfoHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [UserInfoHCodeComponent],
                providers: [
                    UserInfoHCodeService
                ]
            })
            .overrideTemplate(UserInfoHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(UserInfoHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UserInfoHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new UserInfoHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.userInfos[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
