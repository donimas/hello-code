/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { UserInfoHCodeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/user-info-h-code/user-info-h-code-delete-dialog.component';
import { UserInfoHCodeService } from '../../../../../../main/webapp/app/entities/user-info-h-code/user-info-h-code.service';

describe('Component Tests', () => {

    describe('UserInfoHCode Management Delete Component', () => {
        let comp: UserInfoHCodeDeleteDialogComponent;
        let fixture: ComponentFixture<UserInfoHCodeDeleteDialogComponent>;
        let service: UserInfoHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [UserInfoHCodeDeleteDialogComponent],
                providers: [
                    UserInfoHCodeService
                ]
            })
            .overrideTemplate(UserInfoHCodeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(UserInfoHCodeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UserInfoHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
