/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { ActivityStatusHistoryHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/activity-status-history-h-code/activity-status-history-h-code-detail.component';
import { ActivityStatusHistoryHCodeService } from '../../../../../../main/webapp/app/entities/activity-status-history-h-code/activity-status-history-h-code.service';
import { ActivityStatusHistoryHCode } from '../../../../../../main/webapp/app/entities/activity-status-history-h-code/activity-status-history-h-code.model';

describe('Component Tests', () => {

    describe('ActivityStatusHistoryHCode Management Detail Component', () => {
        let comp: ActivityStatusHistoryHCodeDetailComponent;
        let fixture: ComponentFixture<ActivityStatusHistoryHCodeDetailComponent>;
        let service: ActivityStatusHistoryHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [ActivityStatusHistoryHCodeDetailComponent],
                providers: [
                    ActivityStatusHistoryHCodeService
                ]
            })
            .overrideTemplate(ActivityStatusHistoryHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ActivityStatusHistoryHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ActivityStatusHistoryHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ActivityStatusHistoryHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.activityStatusHistory).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
