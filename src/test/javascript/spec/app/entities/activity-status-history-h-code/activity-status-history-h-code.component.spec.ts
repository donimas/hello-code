/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { ActivityStatusHistoryHCodeComponent } from '../../../../../../main/webapp/app/entities/activity-status-history-h-code/activity-status-history-h-code.component';
import { ActivityStatusHistoryHCodeService } from '../../../../../../main/webapp/app/entities/activity-status-history-h-code/activity-status-history-h-code.service';
import { ActivityStatusHistoryHCode } from '../../../../../../main/webapp/app/entities/activity-status-history-h-code/activity-status-history-h-code.model';

describe('Component Tests', () => {

    describe('ActivityStatusHistoryHCode Management Component', () => {
        let comp: ActivityStatusHistoryHCodeComponent;
        let fixture: ComponentFixture<ActivityStatusHistoryHCodeComponent>;
        let service: ActivityStatusHistoryHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [ActivityStatusHistoryHCodeComponent],
                providers: [
                    ActivityStatusHistoryHCodeService
                ]
            })
            .overrideTemplate(ActivityStatusHistoryHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ActivityStatusHistoryHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ActivityStatusHistoryHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ActivityStatusHistoryHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.activityStatusHistories[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
