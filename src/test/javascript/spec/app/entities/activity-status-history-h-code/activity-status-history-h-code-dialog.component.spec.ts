/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { ActivityStatusHistoryHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/activity-status-history-h-code/activity-status-history-h-code-dialog.component';
import { ActivityStatusHistoryHCodeService } from '../../../../../../main/webapp/app/entities/activity-status-history-h-code/activity-status-history-h-code.service';
import { ActivityStatusHistoryHCode } from '../../../../../../main/webapp/app/entities/activity-status-history-h-code/activity-status-history-h-code.model';
import { LearnerActivityHCodeService } from '../../../../../../main/webapp/app/entities/learner-activity-h-code';
import { ActivityResultHCodeService } from '../../../../../../main/webapp/app/entities/activity-result-h-code';

describe('Component Tests', () => {

    describe('ActivityStatusHistoryHCode Management Dialog Component', () => {
        let comp: ActivityStatusHistoryHCodeDialogComponent;
        let fixture: ComponentFixture<ActivityStatusHistoryHCodeDialogComponent>;
        let service: ActivityStatusHistoryHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [ActivityStatusHistoryHCodeDialogComponent],
                providers: [
                    LearnerActivityHCodeService,
                    ActivityResultHCodeService,
                    ActivityStatusHistoryHCodeService
                ]
            })
            .overrideTemplate(ActivityStatusHistoryHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ActivityStatusHistoryHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ActivityStatusHistoryHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ActivityStatusHistoryHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.activityStatusHistory = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'activityStatusHistoryListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ActivityStatusHistoryHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.activityStatusHistory = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'activityStatusHistoryListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
