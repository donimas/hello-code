/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseChapterHCodeComponent } from '../../../../../../main/webapp/app/entities/course-chapter-h-code/course-chapter-h-code.component';
import { CourseChapterHCodeService } from '../../../../../../main/webapp/app/entities/course-chapter-h-code/course-chapter-h-code.service';
import { CourseChapterHCode } from '../../../../../../main/webapp/app/entities/course-chapter-h-code/course-chapter-h-code.model';

describe('Component Tests', () => {

    describe('CourseChapterHCode Management Component', () => {
        let comp: CourseChapterHCodeComponent;
        let fixture: ComponentFixture<CourseChapterHCodeComponent>;
        let service: CourseChapterHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseChapterHCodeComponent],
                providers: [
                    CourseChapterHCodeService
                ]
            })
            .overrideTemplate(CourseChapterHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseChapterHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseChapterHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new CourseChapterHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.courseChapters[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
