/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseChapterHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/course-chapter-h-code/course-chapter-h-code-dialog.component';
import { CourseChapterHCodeService } from '../../../../../../main/webapp/app/entities/course-chapter-h-code/course-chapter-h-code.service';
import { CourseChapterHCode } from '../../../../../../main/webapp/app/entities/course-chapter-h-code/course-chapter-h-code.model';
import { CourseHCodeService } from '../../../../../../main/webapp/app/entities/course-h-code';

describe('Component Tests', () => {

    describe('CourseChapterHCode Management Dialog Component', () => {
        let comp: CourseChapterHCodeDialogComponent;
        let fixture: ComponentFixture<CourseChapterHCodeDialogComponent>;
        let service: CourseChapterHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseChapterHCodeDialogComponent],
                providers: [
                    CourseHCodeService,
                    CourseChapterHCodeService
                ]
            })
            .overrideTemplate(CourseChapterHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseChapterHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseChapterHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CourseChapterHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.courseChapter = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'courseChapterListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CourseChapterHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.courseChapter = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'courseChapterListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
