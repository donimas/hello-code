/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseChapterHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/course-chapter-h-code/course-chapter-h-code-detail.component';
import { CourseChapterHCodeService } from '../../../../../../main/webapp/app/entities/course-chapter-h-code/course-chapter-h-code.service';
import { CourseChapterHCode } from '../../../../../../main/webapp/app/entities/course-chapter-h-code/course-chapter-h-code.model';

describe('Component Tests', () => {

    describe('CourseChapterHCode Management Detail Component', () => {
        let comp: CourseChapterHCodeDetailComponent;
        let fixture: ComponentFixture<CourseChapterHCodeDetailComponent>;
        let service: CourseChapterHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseChapterHCodeDetailComponent],
                providers: [
                    CourseChapterHCodeService
                ]
            })
            .overrideTemplate(CourseChapterHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseChapterHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseChapterHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new CourseChapterHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.courseChapter).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
