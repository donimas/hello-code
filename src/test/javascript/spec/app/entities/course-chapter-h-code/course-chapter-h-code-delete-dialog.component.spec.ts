/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseChapterHCodeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/course-chapter-h-code/course-chapter-h-code-delete-dialog.component';
import { CourseChapterHCodeService } from '../../../../../../main/webapp/app/entities/course-chapter-h-code/course-chapter-h-code.service';

describe('Component Tests', () => {

    describe('CourseChapterHCode Management Delete Component', () => {
        let comp: CourseChapterHCodeDeleteDialogComponent;
        let fixture: ComponentFixture<CourseChapterHCodeDeleteDialogComponent>;
        let service: CourseChapterHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseChapterHCodeDeleteDialogComponent],
                providers: [
                    CourseChapterHCodeService
                ]
            })
            .overrideTemplate(CourseChapterHCodeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseChapterHCodeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseChapterHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
