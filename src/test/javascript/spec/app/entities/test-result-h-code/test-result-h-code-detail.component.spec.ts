/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { TestResultHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/test-result-h-code/test-result-h-code-detail.component';
import { TestResultHCodeService } from '../../../../../../main/webapp/app/entities/test-result-h-code/test-result-h-code.service';
import { TestResultHCode } from '../../../../../../main/webapp/app/entities/test-result-h-code/test-result-h-code.model';

describe('Component Tests', () => {

    describe('TestResultHCode Management Detail Component', () => {
        let comp: TestResultHCodeDetailComponent;
        let fixture: ComponentFixture<TestResultHCodeDetailComponent>;
        let service: TestResultHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TestResultHCodeDetailComponent],
                providers: [
                    TestResultHCodeService
                ]
            })
            .overrideTemplate(TestResultHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TestResultHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TestResultHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TestResultHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.testResult).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
