/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { TestResultHCodeComponent } from '../../../../../../main/webapp/app/entities/test-result-h-code/test-result-h-code.component';
import { TestResultHCodeService } from '../../../../../../main/webapp/app/entities/test-result-h-code/test-result-h-code.service';
import { TestResultHCode } from '../../../../../../main/webapp/app/entities/test-result-h-code/test-result-h-code.model';

describe('Component Tests', () => {

    describe('TestResultHCode Management Component', () => {
        let comp: TestResultHCodeComponent;
        let fixture: ComponentFixture<TestResultHCodeComponent>;
        let service: TestResultHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TestResultHCodeComponent],
                providers: [
                    TestResultHCodeService
                ]
            })
            .overrideTemplate(TestResultHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TestResultHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TestResultHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TestResultHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.testResults[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
