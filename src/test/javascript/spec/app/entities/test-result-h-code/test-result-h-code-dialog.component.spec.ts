/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { TestResultHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/test-result-h-code/test-result-h-code-dialog.component';
import { TestResultHCodeService } from '../../../../../../main/webapp/app/entities/test-result-h-code/test-result-h-code.service';
import { TestResultHCode } from '../../../../../../main/webapp/app/entities/test-result-h-code/test-result-h-code.model';
import { TestQuestionHCodeService } from '../../../../../../main/webapp/app/entities/test-question-h-code';
import { TestVariantHCodeService } from '../../../../../../main/webapp/app/entities/test-variant-h-code';
import { UserService } from '../../../../../../main/webapp/app/shared';

describe('Component Tests', () => {

    describe('TestResultHCode Management Dialog Component', () => {
        let comp: TestResultHCodeDialogComponent;
        let fixture: ComponentFixture<TestResultHCodeDialogComponent>;
        let service: TestResultHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TestResultHCodeDialogComponent],
                providers: [
                    TestQuestionHCodeService,
                    TestVariantHCodeService,
                    UserService,
                    TestResultHCodeService
                ]
            })
            .overrideTemplate(TestResultHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TestResultHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TestResultHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TestResultHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.testResult = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'testResultListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TestResultHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.testResult = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'testResultListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
