/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { TestQuestionHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/test-question-h-code/test-question-h-code-detail.component';
import { TestQuestionHCodeService } from '../../../../../../main/webapp/app/entities/test-question-h-code/test-question-h-code.service';
import { TestQuestionHCode } from '../../../../../../main/webapp/app/entities/test-question-h-code/test-question-h-code.model';

describe('Component Tests', () => {

    describe('TestQuestionHCode Management Detail Component', () => {
        let comp: TestQuestionHCodeDetailComponent;
        let fixture: ComponentFixture<TestQuestionHCodeDetailComponent>;
        let service: TestQuestionHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TestQuestionHCodeDetailComponent],
                providers: [
                    TestQuestionHCodeService
                ]
            })
            .overrideTemplate(TestQuestionHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TestQuestionHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TestQuestionHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TestQuestionHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.testQuestion).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
