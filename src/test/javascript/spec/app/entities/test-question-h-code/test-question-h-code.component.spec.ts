/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { TestQuestionHCodeComponent } from '../../../../../../main/webapp/app/entities/test-question-h-code/test-question-h-code.component';
import { TestQuestionHCodeService } from '../../../../../../main/webapp/app/entities/test-question-h-code/test-question-h-code.service';
import { TestQuestionHCode } from '../../../../../../main/webapp/app/entities/test-question-h-code/test-question-h-code.model';

describe('Component Tests', () => {

    describe('TestQuestionHCode Management Component', () => {
        let comp: TestQuestionHCodeComponent;
        let fixture: ComponentFixture<TestQuestionHCodeComponent>;
        let service: TestQuestionHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TestQuestionHCodeComponent],
                providers: [
                    TestQuestionHCodeService
                ]
            })
            .overrideTemplate(TestQuestionHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TestQuestionHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TestQuestionHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TestQuestionHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.testQuestions[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
