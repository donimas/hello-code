/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { TestQuestionHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/test-question-h-code/test-question-h-code-dialog.component';
import { TestQuestionHCodeService } from '../../../../../../main/webapp/app/entities/test-question-h-code/test-question-h-code.service';
import { TestQuestionHCode } from '../../../../../../main/webapp/app/entities/test-question-h-code/test-question-h-code.model';
import { CourseIssueHCodeService } from '../../../../../../main/webapp/app/entities/course-issue-h-code';

describe('Component Tests', () => {

    describe('TestQuestionHCode Management Dialog Component', () => {
        let comp: TestQuestionHCodeDialogComponent;
        let fixture: ComponentFixture<TestQuestionHCodeDialogComponent>;
        let service: TestQuestionHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TestQuestionHCodeDialogComponent],
                providers: [
                    CourseIssueHCodeService,
                    TestQuestionHCodeService
                ]
            })
            .overrideTemplate(TestQuestionHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TestQuestionHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TestQuestionHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TestQuestionHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.testQuestion = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'testQuestionListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TestQuestionHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.testQuestion = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'testQuestionListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
