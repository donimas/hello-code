/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseProgressHCodeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/course-progress-h-code/course-progress-h-code-delete-dialog.component';
import { CourseProgressHCodeService } from '../../../../../../main/webapp/app/entities/course-progress-h-code/course-progress-h-code.service';

describe('Component Tests', () => {

    describe('CourseProgressHCode Management Delete Component', () => {
        let comp: CourseProgressHCodeDeleteDialogComponent;
        let fixture: ComponentFixture<CourseProgressHCodeDeleteDialogComponent>;
        let service: CourseProgressHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseProgressHCodeDeleteDialogComponent],
                providers: [
                    CourseProgressHCodeService
                ]
            })
            .overrideTemplate(CourseProgressHCodeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseProgressHCodeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseProgressHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
