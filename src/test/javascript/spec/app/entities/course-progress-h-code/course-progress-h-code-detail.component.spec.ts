/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseProgressHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/course-progress-h-code/course-progress-h-code-detail.component';
import { CourseProgressHCodeService } from '../../../../../../main/webapp/app/entities/course-progress-h-code/course-progress-h-code.service';
import { CourseProgressHCode } from '../../../../../../main/webapp/app/entities/course-progress-h-code/course-progress-h-code.model';

describe('Component Tests', () => {

    describe('CourseProgressHCode Management Detail Component', () => {
        let comp: CourseProgressHCodeDetailComponent;
        let fixture: ComponentFixture<CourseProgressHCodeDetailComponent>;
        let service: CourseProgressHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseProgressHCodeDetailComponent],
                providers: [
                    CourseProgressHCodeService
                ]
            })
            .overrideTemplate(CourseProgressHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseProgressHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseProgressHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new CourseProgressHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.courseProgress).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
