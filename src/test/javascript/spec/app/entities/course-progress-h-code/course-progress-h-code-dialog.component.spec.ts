/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseProgressHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/course-progress-h-code/course-progress-h-code-dialog.component';
import { CourseProgressHCodeService } from '../../../../../../main/webapp/app/entities/course-progress-h-code/course-progress-h-code.service';
import { CourseProgressHCode } from '../../../../../../main/webapp/app/entities/course-progress-h-code/course-progress-h-code.model';
import { CourseHCodeService } from '../../../../../../main/webapp/app/entities/course-h-code';
import { UserService } from '../../../../../../main/webapp/app/shared';
import { CourseIssueHCodeService } from '../../../../../../main/webapp/app/entities/course-issue-h-code';

describe('Component Tests', () => {

    describe('CourseProgressHCode Management Dialog Component', () => {
        let comp: CourseProgressHCodeDialogComponent;
        let fixture: ComponentFixture<CourseProgressHCodeDialogComponent>;
        let service: CourseProgressHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseProgressHCodeDialogComponent],
                providers: [
                    CourseHCodeService,
                    UserService,
                    CourseIssueHCodeService,
                    CourseProgressHCodeService
                ]
            })
            .overrideTemplate(CourseProgressHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseProgressHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseProgressHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CourseProgressHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.courseProgress = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'courseProgressListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CourseProgressHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.courseProgress = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'courseProgressListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
