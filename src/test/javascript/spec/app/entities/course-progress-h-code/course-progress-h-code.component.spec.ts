/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseProgressHCodeComponent } from '../../../../../../main/webapp/app/entities/course-progress-h-code/course-progress-h-code.component';
import { CourseProgressHCodeService } from '../../../../../../main/webapp/app/entities/course-progress-h-code/course-progress-h-code.service';
import { CourseProgressHCode } from '../../../../../../main/webapp/app/entities/course-progress-h-code/course-progress-h-code.model';

describe('Component Tests', () => {

    describe('CourseProgressHCode Management Component', () => {
        let comp: CourseProgressHCodeComponent;
        let fixture: ComponentFixture<CourseProgressHCodeComponent>;
        let service: CourseProgressHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseProgressHCodeComponent],
                providers: [
                    CourseProgressHCodeService
                ]
            })
            .overrideTemplate(CourseProgressHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseProgressHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseProgressHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new CourseProgressHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.courseProgresses[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
