/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { WebinarIssueHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/webinar-issue-h-code/webinar-issue-h-code-dialog.component';
import { WebinarIssueHCodeService } from '../../../../../../main/webapp/app/entities/webinar-issue-h-code/webinar-issue-h-code.service';
import { WebinarIssueHCode } from '../../../../../../main/webapp/app/entities/webinar-issue-h-code/webinar-issue-h-code.model';
import { CourseIssueHCodeService } from '../../../../../../main/webapp/app/entities/course-issue-h-code';

describe('Component Tests', () => {

    describe('WebinarIssueHCode Management Dialog Component', () => {
        let comp: WebinarIssueHCodeDialogComponent;
        let fixture: ComponentFixture<WebinarIssueHCodeDialogComponent>;
        let service: WebinarIssueHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [WebinarIssueHCodeDialogComponent],
                providers: [
                    CourseIssueHCodeService,
                    WebinarIssueHCodeService
                ]
            })
            .overrideTemplate(WebinarIssueHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(WebinarIssueHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(WebinarIssueHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new WebinarIssueHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.webinarIssue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'webinarIssueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new WebinarIssueHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.webinarIssue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'webinarIssueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
