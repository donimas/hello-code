/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { WebinarIssueHCodeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/webinar-issue-h-code/webinar-issue-h-code-delete-dialog.component';
import { WebinarIssueHCodeService } from '../../../../../../main/webapp/app/entities/webinar-issue-h-code/webinar-issue-h-code.service';

describe('Component Tests', () => {

    describe('WebinarIssueHCode Management Delete Component', () => {
        let comp: WebinarIssueHCodeDeleteDialogComponent;
        let fixture: ComponentFixture<WebinarIssueHCodeDeleteDialogComponent>;
        let service: WebinarIssueHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [WebinarIssueHCodeDeleteDialogComponent],
                providers: [
                    WebinarIssueHCodeService
                ]
            })
            .overrideTemplate(WebinarIssueHCodeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(WebinarIssueHCodeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(WebinarIssueHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
