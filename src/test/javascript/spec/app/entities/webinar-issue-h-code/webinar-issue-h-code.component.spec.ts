/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { WebinarIssueHCodeComponent } from '../../../../../../main/webapp/app/entities/webinar-issue-h-code/webinar-issue-h-code.component';
import { WebinarIssueHCodeService } from '../../../../../../main/webapp/app/entities/webinar-issue-h-code/webinar-issue-h-code.service';
import { WebinarIssueHCode } from '../../../../../../main/webapp/app/entities/webinar-issue-h-code/webinar-issue-h-code.model';

describe('Component Tests', () => {

    describe('WebinarIssueHCode Management Component', () => {
        let comp: WebinarIssueHCodeComponent;
        let fixture: ComponentFixture<WebinarIssueHCodeComponent>;
        let service: WebinarIssueHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [WebinarIssueHCodeComponent],
                providers: [
                    WebinarIssueHCodeService
                ]
            })
            .overrideTemplate(WebinarIssueHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(WebinarIssueHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(WebinarIssueHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new WebinarIssueHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.webinarIssues[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
