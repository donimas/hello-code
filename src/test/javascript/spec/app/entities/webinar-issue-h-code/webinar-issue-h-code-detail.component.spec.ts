/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { WebinarIssueHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/webinar-issue-h-code/webinar-issue-h-code-detail.component';
import { WebinarIssueHCodeService } from '../../../../../../main/webapp/app/entities/webinar-issue-h-code/webinar-issue-h-code.service';
import { WebinarIssueHCode } from '../../../../../../main/webapp/app/entities/webinar-issue-h-code/webinar-issue-h-code.model';

describe('Component Tests', () => {

    describe('WebinarIssueHCode Management Detail Component', () => {
        let comp: WebinarIssueHCodeDetailComponent;
        let fixture: ComponentFixture<WebinarIssueHCodeDetailComponent>;
        let service: WebinarIssueHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [WebinarIssueHCodeDetailComponent],
                providers: [
                    WebinarIssueHCodeService
                ]
            })
            .overrideTemplate(WebinarIssueHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(WebinarIssueHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(WebinarIssueHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new WebinarIssueHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.webinarIssue).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
