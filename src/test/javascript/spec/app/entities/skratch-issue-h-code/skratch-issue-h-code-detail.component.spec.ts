/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { SkratchIssueHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/skratch-issue-h-code/skratch-issue-h-code-detail.component';
import { SkratchIssueHCodeService } from '../../../../../../main/webapp/app/entities/skratch-issue-h-code/skratch-issue-h-code.service';
import { SkratchIssueHCode } from '../../../../../../main/webapp/app/entities/skratch-issue-h-code/skratch-issue-h-code.model';

describe('Component Tests', () => {

    describe('SkratchIssueHCode Management Detail Component', () => {
        let comp: SkratchIssueHCodeDetailComponent;
        let fixture: ComponentFixture<SkratchIssueHCodeDetailComponent>;
        let service: SkratchIssueHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [SkratchIssueHCodeDetailComponent],
                providers: [
                    SkratchIssueHCodeService
                ]
            })
            .overrideTemplate(SkratchIssueHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SkratchIssueHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SkratchIssueHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new SkratchIssueHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.skratchIssue).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
