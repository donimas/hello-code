/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { SkratchIssueHCodeComponent } from '../../../../../../main/webapp/app/entities/skratch-issue-h-code/skratch-issue-h-code.component';
import { SkratchIssueHCodeService } from '../../../../../../main/webapp/app/entities/skratch-issue-h-code/skratch-issue-h-code.service';
import { SkratchIssueHCode } from '../../../../../../main/webapp/app/entities/skratch-issue-h-code/skratch-issue-h-code.model';

describe('Component Tests', () => {

    describe('SkratchIssueHCode Management Component', () => {
        let comp: SkratchIssueHCodeComponent;
        let fixture: ComponentFixture<SkratchIssueHCodeComponent>;
        let service: SkratchIssueHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [SkratchIssueHCodeComponent],
                providers: [
                    SkratchIssueHCodeService
                ]
            })
            .overrideTemplate(SkratchIssueHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SkratchIssueHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SkratchIssueHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new SkratchIssueHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.skratchIssues[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
