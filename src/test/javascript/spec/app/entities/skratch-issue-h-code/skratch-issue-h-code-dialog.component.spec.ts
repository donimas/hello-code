/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { SkratchIssueHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/skratch-issue-h-code/skratch-issue-h-code-dialog.component';
import { SkratchIssueHCodeService } from '../../../../../../main/webapp/app/entities/skratch-issue-h-code/skratch-issue-h-code.service';
import { SkratchIssueHCode } from '../../../../../../main/webapp/app/entities/skratch-issue-h-code/skratch-issue-h-code.model';
import { CourseIssueHCodeService } from '../../../../../../main/webapp/app/entities/course-issue-h-code';

describe('Component Tests', () => {

    describe('SkratchIssueHCode Management Dialog Component', () => {
        let comp: SkratchIssueHCodeDialogComponent;
        let fixture: ComponentFixture<SkratchIssueHCodeDialogComponent>;
        let service: SkratchIssueHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [SkratchIssueHCodeDialogComponent],
                providers: [
                    CourseIssueHCodeService,
                    SkratchIssueHCodeService
                ]
            })
            .overrideTemplate(SkratchIssueHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SkratchIssueHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SkratchIssueHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SkratchIssueHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.skratchIssue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'skratchIssueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SkratchIssueHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.skratchIssue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'skratchIssueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
