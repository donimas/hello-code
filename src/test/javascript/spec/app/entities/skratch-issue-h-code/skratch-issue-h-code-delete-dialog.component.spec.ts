/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { SkratchIssueHCodeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/skratch-issue-h-code/skratch-issue-h-code-delete-dialog.component';
import { SkratchIssueHCodeService } from '../../../../../../main/webapp/app/entities/skratch-issue-h-code/skratch-issue-h-code.service';

describe('Component Tests', () => {

    describe('SkratchIssueHCode Management Delete Component', () => {
        let comp: SkratchIssueHCodeDeleteDialogComponent;
        let fixture: ComponentFixture<SkratchIssueHCodeDeleteDialogComponent>;
        let service: SkratchIssueHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [SkratchIssueHCodeDeleteDialogComponent],
                providers: [
                    SkratchIssueHCodeService
                ]
            })
            .overrideTemplate(SkratchIssueHCodeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SkratchIssueHCodeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SkratchIssueHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
