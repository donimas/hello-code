/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseHCodeComponent } from '../../../../../../main/webapp/app/entities/course-h-code/course-h-code.component';
import { CourseHCodeService } from '../../../../../../main/webapp/app/entities/course-h-code/course-h-code.service';
import { CourseHCode } from '../../../../../../main/webapp/app/entities/course-h-code/course-h-code.model';

describe('Component Tests', () => {

    describe('CourseHCode Management Component', () => {
        let comp: CourseHCodeComponent;
        let fixture: ComponentFixture<CourseHCodeComponent>;
        let service: CourseHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseHCodeComponent],
                providers: [
                    CourseHCodeService
                ]
            })
            .overrideTemplate(CourseHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new CourseHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.courses[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
