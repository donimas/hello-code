/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/course-h-code/course-h-code-detail.component';
import { CourseHCodeService } from '../../../../../../main/webapp/app/entities/course-h-code/course-h-code.service';
import { CourseHCode } from '../../../../../../main/webapp/app/entities/course-h-code/course-h-code.model';

describe('Component Tests', () => {

    describe('CourseHCode Management Detail Component', () => {
        let comp: CourseHCodeDetailComponent;
        let fixture: ComponentFixture<CourseHCodeDetailComponent>;
        let service: CourseHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseHCodeDetailComponent],
                providers: [
                    CourseHCodeService
                ]
            })
            .overrideTemplate(CourseHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new CourseHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.course).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
