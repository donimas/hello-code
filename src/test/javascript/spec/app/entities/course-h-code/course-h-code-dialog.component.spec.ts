/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { CourseHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/course-h-code/course-h-code-dialog.component';
import { CourseHCodeService } from '../../../../../../main/webapp/app/entities/course-h-code/course-h-code.service';
import { CourseHCode } from '../../../../../../main/webapp/app/entities/course-h-code/course-h-code.model';
import { UserService } from '../../../../../../main/webapp/app/shared';
import { GradeHCodeService } from '../../../../../../main/webapp/app/entities/grade-h-code';
import { CategoryHCodeService } from '../../../../../../main/webapp/app/entities/category-h-code';

describe('Component Tests', () => {

    describe('CourseHCode Management Dialog Component', () => {
        let comp: CourseHCodeDialogComponent;
        let fixture: ComponentFixture<CourseHCodeDialogComponent>;
        let service: CourseHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [CourseHCodeDialogComponent],
                providers: [
                    UserService,
                    GradeHCodeService,
                    CategoryHCodeService,
                    CourseHCodeService
                ]
            })
            .overrideTemplate(CourseHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CourseHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CourseHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CourseHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.course = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'courseListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CourseHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.course = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'courseListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
