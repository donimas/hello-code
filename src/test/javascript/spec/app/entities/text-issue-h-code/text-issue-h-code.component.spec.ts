/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { TextIssueHCodeComponent } from '../../../../../../main/webapp/app/entities/text-issue-h-code/text-issue-h-code.component';
import { TextIssueHCodeService } from '../../../../../../main/webapp/app/entities/text-issue-h-code/text-issue-h-code.service';
import { TextIssueHCode } from '../../../../../../main/webapp/app/entities/text-issue-h-code/text-issue-h-code.model';

describe('Component Tests', () => {

    describe('TextIssueHCode Management Component', () => {
        let comp: TextIssueHCodeComponent;
        let fixture: ComponentFixture<TextIssueHCodeComponent>;
        let service: TextIssueHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TextIssueHCodeComponent],
                providers: [
                    TextIssueHCodeService
                ]
            })
            .overrideTemplate(TextIssueHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TextIssueHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TextIssueHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TextIssueHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.textIssues[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
