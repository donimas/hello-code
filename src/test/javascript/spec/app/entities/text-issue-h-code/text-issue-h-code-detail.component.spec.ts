/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { TextIssueHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/text-issue-h-code/text-issue-h-code-detail.component';
import { TextIssueHCodeService } from '../../../../../../main/webapp/app/entities/text-issue-h-code/text-issue-h-code.service';
import { TextIssueHCode } from '../../../../../../main/webapp/app/entities/text-issue-h-code/text-issue-h-code.model';

describe('Component Tests', () => {

    describe('TextIssueHCode Management Detail Component', () => {
        let comp: TextIssueHCodeDetailComponent;
        let fixture: ComponentFixture<TextIssueHCodeDetailComponent>;
        let service: TextIssueHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TextIssueHCodeDetailComponent],
                providers: [
                    TextIssueHCodeService
                ]
            })
            .overrideTemplate(TextIssueHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TextIssueHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TextIssueHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TextIssueHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.textIssue).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
