/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { TextIssueHCodeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/text-issue-h-code/text-issue-h-code-delete-dialog.component';
import { TextIssueHCodeService } from '../../../../../../main/webapp/app/entities/text-issue-h-code/text-issue-h-code.service';

describe('Component Tests', () => {

    describe('TextIssueHCode Management Delete Component', () => {
        let comp: TextIssueHCodeDeleteDialogComponent;
        let fixture: ComponentFixture<TextIssueHCodeDeleteDialogComponent>;
        let service: TextIssueHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TextIssueHCodeDeleteDialogComponent],
                providers: [
                    TextIssueHCodeService
                ]
            })
            .overrideTemplate(TextIssueHCodeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TextIssueHCodeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TextIssueHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
