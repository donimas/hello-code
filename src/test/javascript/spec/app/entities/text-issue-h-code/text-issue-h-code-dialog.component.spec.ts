/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { TextIssueHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/text-issue-h-code/text-issue-h-code-dialog.component';
import { TextIssueHCodeService } from '../../../../../../main/webapp/app/entities/text-issue-h-code/text-issue-h-code.service';
import { TextIssueHCode } from '../../../../../../main/webapp/app/entities/text-issue-h-code/text-issue-h-code.model';
import { CourseIssueHCodeService } from '../../../../../../main/webapp/app/entities/course-issue-h-code';

describe('Component Tests', () => {

    describe('TextIssueHCode Management Dialog Component', () => {
        let comp: TextIssueHCodeDialogComponent;
        let fixture: ComponentFixture<TextIssueHCodeDialogComponent>;
        let service: TextIssueHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [TextIssueHCodeDialogComponent],
                providers: [
                    CourseIssueHCodeService,
                    TextIssueHCodeService
                ]
            })
            .overrideTemplate(TextIssueHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TextIssueHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TextIssueHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TextIssueHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.textIssue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'textIssueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TextIssueHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.textIssue = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'textIssueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
