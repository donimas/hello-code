/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { GradeHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/grade-h-code/grade-h-code-dialog.component';
import { GradeHCodeService } from '../../../../../../main/webapp/app/entities/grade-h-code/grade-h-code.service';
import { GradeHCode } from '../../../../../../main/webapp/app/entities/grade-h-code/grade-h-code.model';
import { ProgramHCodeService } from '../../../../../../main/webapp/app/entities/program-h-code';
import { CourseHCodeService } from '../../../../../../main/webapp/app/entities/course-h-code';

describe('Component Tests', () => {

    describe('GradeHCode Management Dialog Component', () => {
        let comp: GradeHCodeDialogComponent;
        let fixture: ComponentFixture<GradeHCodeDialogComponent>;
        let service: GradeHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [GradeHCodeDialogComponent],
                providers: [
                    ProgramHCodeService,
                    CourseHCodeService,
                    GradeHCodeService
                ]
            })
            .overrideTemplate(GradeHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GradeHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GradeHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new GradeHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.grade = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'gradeListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new GradeHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.grade = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'gradeListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
