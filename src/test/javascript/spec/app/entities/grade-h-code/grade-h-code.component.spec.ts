/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { GradeHCodeComponent } from '../../../../../../main/webapp/app/entities/grade-h-code/grade-h-code.component';
import { GradeHCodeService } from '../../../../../../main/webapp/app/entities/grade-h-code/grade-h-code.service';
import { GradeHCode } from '../../../../../../main/webapp/app/entities/grade-h-code/grade-h-code.model';

describe('Component Tests', () => {

    describe('GradeHCode Management Component', () => {
        let comp: GradeHCodeComponent;
        let fixture: ComponentFixture<GradeHCodeComponent>;
        let service: GradeHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [GradeHCodeComponent],
                providers: [
                    GradeHCodeService
                ]
            })
            .overrideTemplate(GradeHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GradeHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GradeHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new GradeHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.grades[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
