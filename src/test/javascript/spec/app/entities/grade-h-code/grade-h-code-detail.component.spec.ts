/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { GradeHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/grade-h-code/grade-h-code-detail.component';
import { GradeHCodeService } from '../../../../../../main/webapp/app/entities/grade-h-code/grade-h-code.service';
import { GradeHCode } from '../../../../../../main/webapp/app/entities/grade-h-code/grade-h-code.model';

describe('Component Tests', () => {

    describe('GradeHCode Management Detail Component', () => {
        let comp: GradeHCodeDetailComponent;
        let fixture: ComponentFixture<GradeHCodeDetailComponent>;
        let service: GradeHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [GradeHCodeDetailComponent],
                providers: [
                    GradeHCodeService
                ]
            })
            .overrideTemplate(GradeHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GradeHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GradeHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new GradeHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.grade).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
