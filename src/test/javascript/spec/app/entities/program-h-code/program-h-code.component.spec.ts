/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HelloCodeTestModule } from '../../../test.module';
import { ProgramHCodeComponent } from '../../../../../../main/webapp/app/entities/program-h-code/program-h-code.component';
import { ProgramHCodeService } from '../../../../../../main/webapp/app/entities/program-h-code/program-h-code.service';
import { ProgramHCode } from '../../../../../../main/webapp/app/entities/program-h-code/program-h-code.model';

describe('Component Tests', () => {

    describe('ProgramHCode Management Component', () => {
        let comp: ProgramHCodeComponent;
        let fixture: ComponentFixture<ProgramHCodeComponent>;
        let service: ProgramHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [ProgramHCodeComponent],
                providers: [
                    ProgramHCodeService
                ]
            })
            .overrideTemplate(ProgramHCodeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProgramHCodeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProgramHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ProgramHCode(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.programs[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
