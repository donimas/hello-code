/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HelloCodeTestModule } from '../../../test.module';
import { ProgramHCodeDetailComponent } from '../../../../../../main/webapp/app/entities/program-h-code/program-h-code-detail.component';
import { ProgramHCodeService } from '../../../../../../main/webapp/app/entities/program-h-code/program-h-code.service';
import { ProgramHCode } from '../../../../../../main/webapp/app/entities/program-h-code/program-h-code.model';

describe('Component Tests', () => {

    describe('ProgramHCode Management Detail Component', () => {
        let comp: ProgramHCodeDetailComponent;
        let fixture: ComponentFixture<ProgramHCodeDetailComponent>;
        let service: ProgramHCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [ProgramHCodeDetailComponent],
                providers: [
                    ProgramHCodeService
                ]
            })
            .overrideTemplate(ProgramHCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProgramHCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProgramHCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ProgramHCode(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.program).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
