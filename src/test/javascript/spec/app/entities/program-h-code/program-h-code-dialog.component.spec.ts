/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HelloCodeTestModule } from '../../../test.module';
import { ProgramHCodeDialogComponent } from '../../../../../../main/webapp/app/entities/program-h-code/program-h-code-dialog.component';
import { ProgramHCodeService } from '../../../../../../main/webapp/app/entities/program-h-code/program-h-code.service';
import { ProgramHCode } from '../../../../../../main/webapp/app/entities/program-h-code/program-h-code.model';
import { SchoolHCodeService } from '../../../../../../main/webapp/app/entities/school-h-code';
import { CourseHCodeService } from '../../../../../../main/webapp/app/entities/course-h-code';
import { GradeHCodeService } from '../../../../../../main/webapp/app/entities/grade-h-code';

describe('Component Tests', () => {

    describe('ProgramHCode Management Dialog Component', () => {
        let comp: ProgramHCodeDialogComponent;
        let fixture: ComponentFixture<ProgramHCodeDialogComponent>;
        let service: ProgramHCodeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HelloCodeTestModule],
                declarations: [ProgramHCodeDialogComponent],
                providers: [
                    SchoolHCodeService,
                    CourseHCodeService,
                    GradeHCodeService,
                    ProgramHCodeService
                ]
            })
            .overrideTemplate(ProgramHCodeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProgramHCodeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProgramHCodeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ProgramHCode(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.program = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'programListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ProgramHCode();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.program = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'programListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
